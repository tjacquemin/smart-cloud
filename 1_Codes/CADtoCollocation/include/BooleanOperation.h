#ifndef BOOLEANOPERATION_H
#define BOOLEANOPERATION_H
#include <cstdlib>
#include "Main.h"
#include "Node.h"
#include "Element.h"
#include "SurfaceCurve.h"
#include "SurfaceFace.h"
#include "KDTree.h"

class BooleanOperation {
public:
    BooleanOperation();
    BooleanOperation(const BooleanOperation& orig);
    virtual ~BooleanOperation();
    
    //General parameters
    int Dimension=2;
    KDTree* KDtree=NULL;
    KDTree* KDtreeBound=NULL;
    int HasSpikes=0;
    int BCTolerance=0;
    
    //Parameters for 2D problems
    TopoDS_Edge* EdgeLoc=NULL;
    int EdgesNumber=0;
    vector<vector<long>> IndexNode2D;
    long TotalBoundaryNodesNumber=0;
    
    //Functions for 2D problems
    void GetInnerNodesFromCAD_2D(Node*,long,TopoDS_Face);
    void GetInnerNodesFromCAD_3D(Node*,long,double);
    bool NodeInCAD_2D(Node*,long,TopoDS_Face,int);
    void GetInnerNodesFromBC_2D(SurfaceCurve*,int,TopoDS_Face,double,Node*,long);
    void LoadKDTreeBound2D(SurfaceCurve*,int);
    double isLeft(double,double,double,double,double,double);
    
    //Functions and variables for 3D problems
    void LoadSurfaceElements(Element*,long);
    void GetInnerNodesFromBC_3D(SurfaceFace*,int,double,Node*,long);
    bool NodeInCAD_3D(Node*,long,double);
    long NumberSurfElements=0;
    Element* SurfaceElements=NULL;
    long Count=0;
    BRepClass3d_SolidClassifier* ShapeCAD;
    TopoDS_Shape Shape3D;
    
    //Other functions
    void LoadKDTree(Node*,long);
    void ApplyLocToNeighboors(bool);
    void PrintLoading(long,long,string);
    
private:

};

#endif /* BOOLEANOPERATION_H */

