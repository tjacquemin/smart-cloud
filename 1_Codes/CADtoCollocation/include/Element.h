#ifndef ELEMENT_H
#define ELEMENT_H
#include <cstdlib>
#include "Node.h"
using namespace std;

class Element {
public:
    Element();
    Element(const Element& orig);
    virtual ~Element();
    void Initialize(int);
    void GetNormalFromNodes();
    void ReElementOrderNodes();
    Node** ElNode=NULL;
    double* Normal=NULL;
    long GlobalIndex=0;
    int ElDim=0;
    int RefSurf;
private:

};

#endif /* ELEMENT_H */
