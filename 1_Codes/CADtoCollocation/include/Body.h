#ifndef BODY_H
#define BODY_H

#include "Main.h"
#include "KDTree.h"
#include "SurfaceCurve.h"
#include "SurfaceFace.h"
#include "BooleanOperation.h"
using namespace std;

class Body {
public:
    Body();
    Body(const Body& orig);
    virtual ~Body();
    
    //Body definition
    int Dimension=2;
    TopoDS_Face Body2D;
    TopoDS_Shape Body3D;
    
    //Discretization properties
    double h, ThresholdRatio;
    string BooleanOperationType="CAD";
    string InnerDiscretizationType="Regular";
    int HasSpikes=0;
    int BCTolerance=0;
    int Relaxation=0;
    
    //2D boundary definition
    int SurfaceCvNumber=0;
    SurfaceCurve* SurfaceCv=NULL;
    int ClosedCvNum=0;
    Node* CornerNodes=NULL;
    
    //3D boundary definition
    int SurfaceFcNumber=0;
    SurfaceFace* SurfaceFc=NULL;
    
    //Box min/max dimensions
    double MinX, MaxX, MinY, MaxY, MinZ, MaxZ;
    
    //Box discretization
    long BoxNodeNumber=0;
    Node* BoxNode=NULL;
    
    //Body functions
    void Set2DBody(TopoDS_Face);
    void Set3DBody(TopoDS_Shape);
    double GetVolume();
    void LoadBoundaries();
    void DiscretizeBoundaries(double);
    void GetSurfaceDirections();
    void GetCornerNodes();
    void DiscretizeBody(string);
    void GetModelBox();
    
    //Body discretization
    void BoxDiscretization();
    void GetElementsAttachedToNodes();
    void GetElementsFromAdjacentRegions();
    void GetNodesInDomain();
    void RemoveNodesCloseToBoundaries(int);
    
    //Body functions for 2D curves
    void GetCurveStartEndConnections();
    void GetSurfaceDirection();
    
private:

};

#endif /* BODY_H */

