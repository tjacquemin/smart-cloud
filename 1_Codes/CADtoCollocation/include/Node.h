#ifndef NODE_H
#define NODE_H
#include <cstdlib>
#include <vector>
#include <Eigen/Dense>
#include <Eigen/Core>
using namespace Eigen;
using namespace std;

class Node {
public:
    Node();
    Node(const Node& orig);
    virtual ~Node();
    
    //Functions
    void Initialize(int);
    void InitializeNormal(Vector3d);
    void InitializeNormal3D(Vector3d,Vector3d);
    
    //General model parameters
    int Dimension=2;
    double* X=NULL;
    double* Normal=NULL;
    bool InDomain=false;
    bool InclusionIdentified=false;
    bool RelaxedNode=false;
    bool SurfNodeInDomain=true;
    int EdgeIndex=-1;
    long* GlobalIndex=NULL;
    bool NodeCloseToBound=false;
    vector<vector<long>> AttachedElements;
    vector<int> AttachedSet;
    vector<int> StartEnd;
    bool BCPrinted=false;
    
    vector<vector<double>> AttachedNormals;
    vector<int> AttachedSet3D;
    
    //Reference faces and nodes for 3D refinement
    vector<int> RefSurf;
    vector<int> RefEdge;
private:

};

#endif /* NODE_H */

