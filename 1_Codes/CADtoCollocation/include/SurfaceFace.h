#ifndef SURFACEFACE_H
#define SURFACEFACE_H
#include <cstdlib>
#include "Main.h"
#include "KDTree.h"
#include "Node.h"
#include "Element.h"
#include <gmsh.h>
using namespace std;

class SurfaceFace {
public:
    SurfaceFace();
    SurfaceFace(const SurfaceFace& orig);
    virtual ~SurfaceFace();
    //Geometry Data
    TopoDS_Face FaceGeom;
    typedef Handle(Geom_Surface) GeomSurface;
    GeomSurface SurfaceGeom;
    Vector3d AvgNormal;
    
    //Face Triangulation
    void TriangulateGmsh(double);
    double h=1;
    int SurfIndex=0;
    
    //Nodes
    Node* FaceNodes=NULL;
    long FaceNodesNumber=0;
    
    //Elements
    Element* FaceInnerElements=NULL;
    long InnerElementsNumber=0;
    
    //Functions
    void GetSurfaceAverageNormal();
    
    //Boundary properties
    void LoadBoundaryProperties(vector<string>);
    string SurfType="";
    int Dimension=3;
    bool InnerFace=false;
    bool BCArraysCreated=false;
    bool* LocalBoundaryCondition = NULL;
    bool* BC_D_Bool = NULL;
    bool* BC_N_Bool = NULL;
    float* BC_D = NULL;
    float* BC_N = NULL;
    int* BC_DOF = NULL;
private:

};

#endif /* SURFACEFACE_H */

