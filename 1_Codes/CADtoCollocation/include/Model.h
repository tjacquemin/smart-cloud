#ifndef MODEL_H
#define MODEL_H
#include <cstdlib>
#include "KDTree.h"
#include "Body.h"
#include "Main.h"
#include "Node.h"
#include "Element.h"
#include "SurfaceCurve.h"
#include "SurfaceFace.h"
#include "BooleanOperation.h"

using namespace std;

class Model {
public:
    Model();
    Model(const Model& orig);
    virtual ~Model();
    
    //Code running Option
    int PrintGeomFile=0;
    string GeoInpFile="";
    bool GeomFileAvailable=false;
    int PrintVTK=0;
    
    //Type definition
//    typedef bgi::rtree<value,bgi::quadratic<16>> rtreetype;
    
    //Model parameters
    int Dimension=2;
    double h=1;
    double h_reduc=0;
    long nx=1;
    long ny=1;
    long nz=1;
    long TargetNumNodes=0;
    double AreaVol=0;
    Standard_CString Path="";
    string InnerDiscretizationType="Regular";
    int Relaxation=0;
    string BooleanOperationType="CAD";
    double ThresholdRatio=1;
    double RunTime=0;
    int HasSpikes=0;
    int BCTolerance=0;
    
    //Geometry variables
    TopoDS_Shape TopoShape;
    TopoDS_Shape* Shapes=NULL;
    TopoDS_Face* Faces=NULL;
    int ShapesCount=0;
    
    //Bodies
    int BodiesNumber=0;
    Body* Bodies=NULL;
    
    //2D geometry
    int TotalSurfaceCvNumber=0;
    SurfaceCurve* SurfaceCv=NULL;
    int TotalClosedCvNum=0;
    
    //3D geometry
    SurfaceFace* SurfaceFc=NULL;
    bool ModelHasSolid=false;
    int TotalFaceNumber=0;
    long TotalBoundaryNodes=0;
    Node* BoundaryNode3D=NULL;
    
    //Functions
    void LoadStepFile();
    void GetModelBox();
    void LoadBodies();
    void LoadBodiesParameters();
    void GetModelVolume();
    void LoadBoundaries();
    void DiscretizeBoundaries();
    void LoadGlobalGeomArray();
    void LoadGeometryInstructions();
    void CalculateAreaVolume();
    bool HasSolid();
    void DiscretizeDomain();
    void LoadSurfaceBoundaries();
    void BodyRelaxation();
    
    //Read functions
    string trim(string);
    string split(string,char,int);
    
    //Delete nodes close to the edge
    void RemoveDuplicatedNodes();
    void RemoveDuplicatedNodesInEdges(vector<vector<long>>,double);
    void RemoveDuplicatedNodesInFaces(vector<vector<long>>,double);
//    rtreetype rtree;
    KDTree* tree;
    
    //Print functions
    std::ofstream *outfile;
    
    //Print geometry functions
    void PrintGeometryFile();
    void PrintGeometryInpFile();
    void PrintVTKFile();
    
    void PrintNodes2D(long*,string);
    void PrintInnerNodes(long*,string);
    void PrintNodes3D(long*,string);
    void IdentifyDuplicatedFaceNodes3D();
    
    void Fill3DBoundaryNodes();
    long Set3DNodesNumber();
    
    
    void PrintElements2D(string);
    void PrintElements3D(string);
    void PrintRefSurfEdge(int,int);
    
    void PrintBoundaries2D();
    void PrintBCRow2D(long,int,int,long);
    void PrintBoundaries3D();
    void PrintDiscretization();
    void PrintCollocationInputFile();
    int* SurfPrintOrder=NULL;
    int* SurfPrintOrderReverse=NULL;
    bool HasSurfPrintOrder=false;
    
    //General functions
    void PrintLoading(long,long,string);
    string ToString(double,int,int,bool);
    string ToString2(double,int);
    
    //Box min/max dimensions
    double MinX, MaxX, MinY, MaxY, MinZ, MaxZ;
    long EdgeNodeNumber=0;
    Node* EdgeNode=NULL;
    long BoxNodeNumber=0;
    Node* BoxNode=NULL;
    
    //Chain construction functions
    void GetCurveStartEndConnections();
    void ReorderCurves();
    void LoadClosedCurves();
    void SetInnerOuterCurves();
    double CumulDir;
    
private:
    
    void PrintNSet(int,int,string);

};

#endif /* MODEL_H */

