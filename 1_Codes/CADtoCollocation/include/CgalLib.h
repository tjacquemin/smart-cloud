#ifndef CGALLIB_H
#define CGALLIB_H

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/point_generators_3.h>
#include <CGAL/Side_of_triangle_mesh.h>
#include <CGAL/AABB_tree.h>
#include <CGAL/AABB_traits.h>
#include <vector>
#include <fstream>
#include <limits>

#include <list>
#include <CGAL/Simple_cartesian.h>

#endif /* CGALLIB_H */