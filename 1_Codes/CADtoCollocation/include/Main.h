#ifndef MAIN_H
#define MAIN_H

//General includes
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#include <omp.h>
#include <time.h>
#include <ctime>
#include <cstdlib>
#include <iterator>

//Open Cascade includes
#include <BRep_Tool.hxx>
#include <BRepTools.hxx> 
#include <BRepBuilderAPI_MakeVertex.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx> 
#include <BRepBuilderAPI_MakeSolid.hxx>
#include <BRepGProp.hxx>
#include <BRepBndLib.hxx>
#include <BRepMesh_IncrementalMesh.hxx>
#include <BRepMesh_DelaunayBaseMeshAlgo.hxx>
#include <BRepMesh_DiscretRoot.hxx>
#include <BRepMesh_FastDiscret.hxx>
#include <BRepClass3d_SolidClassifier.hxx>
#include <TopTools_MapOfShape.hxx>
#include <TopTools_DataMapOfShapeReal.hxx>
#include <GProp_GProps.hxx>
#include <GCPnts_UniformAbscissa.hxx>
#include <GCPnts_AbscissaPoint.hxx>
#include <ShapeAnalysis_Surface.hxx>

//Reading STEP file
#include <STEPControl_Reader.hxx> 
#include <TColgp_HArray1OfPnt.hxx>

//Topology includes
#include <TopoDS.hxx>
#include <TopoDS_Shape.hxx> 
#include <TopoDS_Vertex.hxx>
#include <TopoDS_Edge.hxx> 
#include <TopoDS_Face.hxx>
#include <TopoDS_Iterator.hxx>
#include <TopExp.hxx>
#include <TopTools_IndexedMapOfShape.hxx>
#include <TColgp_HArray1OfPnt.hxx>

//Geometry includes
#include <gp_Pnt.hxx> 
#include <Geom_Line.hxx>
#include <Geom_Ellipse.hxx>
#include <Geom2dAPI_ProjectPointOnCurve.hxx>
#include <GeomAPI_ProjectPointOnCurve.hxx>
#include <ShapeAnalysis_Edge.hxx>

//Intersection
#include <BOPAlgo_Builder.hxx>
#include <BRepExtrema_DistShapeShape.hxx>

//Eigen
//#include <Eigen/Dense>
//#include <Eigen/Core>
//using namespace Eigen;

#endif /* MAIN_H */

