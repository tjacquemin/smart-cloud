#ifndef SURFACECURVE_H
#define SURFACECURVE_H
#include <cstdlib>
#include "Main.h"
#include "Node.h"
#include "Element.h"
using namespace std;

class SurfaceCurve {
public:
    SurfaceCurve();
    SurfaceCurve(const SurfaceCurve& orig);
    virtual ~SurfaceCurve();
    
    //Type definition
    typedef Handle(Geom_Curve) GeomCurve;
    GeomCurve SurfaceGeomCurve;
    Standard_Real CvFirst, CvLast;
    
    //Curve variables
    int Dimension=2;
    long NumberNodes=0;
    long NumberElements=0;
    Node* CurveNode=NULL;
    Element* CurveElement=NULL;
    
    //Functions
    void Discretize(double);
    void FlipCurve();
    double GetCurveCumulEdges();
    void SetNormalDirections(int, bool);
    
    //Variables for ordering
    long PrevCvIndex=-1;
    long NextCvIndex=-1;
    
    //Boundary properties
    void LoadBoundaryProperties(vector<string>);
    string CurveType="";
    bool InnerCurve=false;
    bool BCArraysCreated=false;
    bool* LocalBoundaryCondition = NULL;
    bool* BC_D_Bool = NULL;
    bool* BC_N_Bool = NULL;
    float* BC_D = NULL;
    float* BC_N = NULL;
    int* BC_DOF = NULL;
    
    int GlobalCvNum=0;
    
private:

};

#endif /* CHAIN_H */

