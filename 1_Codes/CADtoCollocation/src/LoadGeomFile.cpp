#include "Model.h"

void Model::LoadGeometryInstructions()
{
    char* Path=NULL;
    Path = new char[GeoInpFile.length()+1];
    strncpy(Path, GeoInpFile.c_str(),GeoInpFile.length()+1);
    ifstream File;
    File.open(Path, ios::in);
    string str=" ";
    int CountSurf=0;
    bool StartFound=false,EndFound=false;
    while (EndFound==false)
    {
        getline(File, str);
        //Identify the start of the *Node section
        if (trim(str) == "*MODEL_INFORMATION")
        {
            StartFound=true;
            getline(File, str);
            //Create the edge print order array
            if (Dimension==2)
            {
                SurfPrintOrder=new int [TotalSurfaceCvNumber];
                SurfPrintOrderReverse=new int [TotalSurfaceCvNumber];
            }
            else if (Dimension==3)
            {
                SurfPrintOrder=new int [TotalFaceNumber];
                SurfPrintOrderReverse=new int [TotalFaceNumber];
            }
            HasSurfPrintOrder=true;
        }
        //Identify the end of the *Node section
        if (trim(str).at(0) == '*' && StartFound==true)
        {
            EndFound=true;
        }
        if (StartFound==true && EndFound==false)
        {
            //Identify the curve reference
            vector<string> PropLine;
            PropLine.resize(3+3*Dimension);
            for (int i=0; i<3+3*Dimension; i++)
            {
                PropLine[i]=split(str,',',i);
            }
            int CvNo=stoi(PropLine[0]);
            SurfPrintOrder[CountSurf]=CvNo;
            if (Dimension==2)
            {
                SurfaceCv[CountSurf].LoadBoundaryProperties(PropLine);
            }
            else if (Dimension==3)
            {
                SurfaceFc[CountSurf].LoadBoundaryProperties(PropLine);
            }
            CountSurf++;
        }
    }
    File.close();
    
    //Load the reverse print order
    for (int i=0; i<CountSurf; i++)
    {
        SurfPrintOrderReverse[SurfPrintOrder[i]]=i;
    }
}

string Model::trim(string str)
{
    //Function Trim (remove spaces from name start and end)
    const char* t = " \t\n\r\f\v";
    str.erase(str.find_last_not_of(t) + 1);
    str.erase(0, str.find_first_not_of(t));
    return str;
}

string Model::split(string str, char delimiter, int Location) 
{
    //Split the path
    string internal;
    stringstream ss(str); // Turn the string into a stream.
    string tok;
    int i=0;
    while (getline(ss, tok, delimiter))
    {
	if (i==Location) 
        {
            internal=tok;
            break;
        }
        i++;
    }
    return trim(internal);
}