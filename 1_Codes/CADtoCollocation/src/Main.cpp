#include <cstdlib>
#include "Model.h"
using namespace std;

int main(int argc, char** argv)
{    
    cout << "" << endl;
    cout << " *******************************************\n";
    cout << " *                                         *\n";
    cout << " *               Legato Team               *\n";
    cout << " *          Luxembourg University          *\n";
    cout << " *                                         *\n";
    cout << " *       Model Creator - Non Conforming    *\n";
    cout << " *            Thibault JACQUEMIN           *\n";
    cout << " *                2016 - 2020              *\n";
    cout << " *                                         *\n";
    cout << " *******************************************\n";
    cout << "" << endl;
    
    //Create the timer
    double StartTime=omp_get_wtime();
    
    //Create the model
    Model M;
    
    char* PathChar;
    PathChar=argv[1];
    //Get the information from the run command
    if (PathChar != NULL)
    {
        M.Path=Standard_CString(PathChar);
        M.Dimension=stoi(argv[2]);
        M.TargetNumNodes=stod(argv[3]);
        M.ThresholdRatio=stod(argv[4]);
        M.HasSpikes=stoi(argv[5]);
        M.BCTolerance=stoi(argv[6]);
        M.PrintGeomFile=stoi(argv[7]);
        M.InnerDiscretizationType=argv[8];
        M.Relaxation=stoi(argv[9]);
        M.BooleanOperationType=argv[10];
        M.GeoInpFile=argv[11];
        M.PrintVTK=stoi(argv[12]);
    }
    else
    {
        M.Path="/home/tjacquemin/PhD_Linux/Calculations/3D/3D_Adaptivity/3_SphereInclusion/Triangle/5000_2/SphereInclusion.step";
        M.Dimension=3;
        M.TargetNumNodes=5000;
        M.ThresholdRatio=0.3;
        M.HasSpikes=1;                          //Has angle smaller than 90°
        M.BCTolerance=1;                        // "0" for coarse approximation of the boundaries "1" for exact approximation of the boundaries
        M.PrintGeomFile=0;
        M.InnerDiscretizationType="Triangles";    // "Regular", "PDS", "Triangles"
        M.Relaxation=0;                         // "0" if Relaxation==false, "1" if Relaxation==true
        M.BooleanOperationType="CAD";         // for 2D: "WnCAD", "CAD", "Bound". for 3D: "FacesCAD", "CAD", "Bound".
        M.GeoInpFile="";
        M.PrintVTK=1;
    }
    
    //Load the step file into the Open Cascade environment
    cout << "Loading:" << endl;
    cout << " --> Loading STEP file: " << M.Path << endl;
    M.LoadStepFile();
    cout << endl;
    
    //Get the dimensions of the bounding box of the model
    M.GetModelBox();
    cout << "Box min/max dimensions:" << endl;
    cout << " --> MinX =" << M.ToString2(M.MinX,2) << ", MaxX =" << M.ToString2(M.MaxX,2) << endl;
    cout << " --> MinY =" << M.ToString2(M.MinY,2) << ", MaxY =" << M.ToString2(M.MaxY,2) << endl;
    cout << " --> MinZ =" << M.ToString2(M.MinZ,2) << ", MaxZ =" << M.ToString2(M.MaxZ,2) << endl;
    cout << endl;
    
    //Load and discretize the boundaries of the model
    cout << "Load the domain bodies and boundaries:" << endl;
    M.LoadBodies();
    M.LoadBodiesParameters();
    M.GetModelVolume();
    cout << " --> Model volume: " << M.AreaVol << endl;
    M.LoadBoundaries();
    cout << " --> Boundaries loaded." << endl;
    M.DiscretizeBoundaries();
    cout << " --> Boundaries discretized." << endl;
    cout << endl;
    
    if (M.PrintGeomFile==1)
    {
        cout << "Print geometry files:" << endl;
        M.PrintGeometryFile();
        M.PrintGeometryInpFile();
        cout << " --> Geometry file printed" << endl;
    }
    else
    {
        cout << "Get inner nodes:" << endl;
        M.DiscretizeDomain(); 
        cout << " --> Inner domain discretized" << endl;
        cout << endl;
        
        //Load the curves and surfaces in the bodies in global arrays
        M.LoadGlobalGeomArray();
        //Assess if a geometry file has been specified
        if (M.GeoInpFile!="")
            M.GeomFileAvailable=true;
        else
            M.GeomFileAvailable=false;
        if (M.GeomFileAvailable==true)
        {
            cout << "Load model instructions:" << endl;
            cout << " --> Loading GEOM file" << endl;
            M.LoadGeometryInstructions();
            cout << endl;
        }
        
        //Get the timer
        M.RunTime=omp_get_wtime()-StartTime;
        
        cout << "Print output file:" << endl;
        
        if (M.Dimension==3)
        {
            M.IdentifyDuplicatedFaceNodes3D();
            M.Fill3DBoundaryNodes();
        }
        if (M.GeoInpFile!="")
        {
            M.PrintCollocationInputFile();
        }
        M.PrintDiscretization();
        if (M.PrintVTK==1)
        {
            M.PrintVTKFile();
            cout << " --> VTK file printed" << endl;
        }
        cout << " --> Output file printed" << endl;
    }
    
    cout << "" << endl;
    cout << "____________________________________________\n";
    cout << "" << endl;
    return 0;
}

