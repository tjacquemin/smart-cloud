#include "Model.h"

void Model::PrintDiscretization()
{
    string InpuFileNam=Path;
    std::stringstream NewFileName;
    string OutputFileName=InpuFileNam.replace(InpuFileNam.end()-5,InpuFileNam.end(),"");
    NewFileName << OutputFileName << "-" << TargetNumNodes << "-" << fixed << setprecision(2)  << ThresholdRatio << ".inp";
    OutputFileName=NewFileName.str();
    outfile= new std::ofstream(OutputFileName);
    
    //Print the header
    *outfile << "*HEADING=" << OutputFileName << endl;
    *outfile << "*RUNTIME=" << ToString2(RunTime,1) << "s" << endl;
    
    //Print the nodes
    *outfile << "*NODE" << endl;
    //Print the boundary nodes
    long Count=1;
    if (Dimension==2)
    {
        PrintNodes2D(&Count,"Discretization");
    }
    else if (Dimension==3)
    {
        PrintNodes3D(&Count,"Discretization");
    }

    //Print the inner nodes
    PrintInnerNodes(&Count,"Discretization");
    
    //Print the boundary elements
    if (Dimension==2)
    {
        PrintElements2D("Discretization");
    }
    else if (Dimension==3)
    {
        PrintElements3D("Discretization");
    }
    
    //Print the node sets
    if (GeoInpFile!="")
    {
        for (int i=0; i<BodiesNumber; i++)
        {
            for (int j=0; j<Bodies[i].SurfaceCvNumber; j++)
            {
                PrintNSet(i,j,"Discretization");
            }
        }
        //Print the stress free node set
        PrintNSet(0,-1,"Discretization");
    }
    
    *outfile << "*END" << endl;
    outfile->close();
}

void Model::PrintNSet(int BodyIndex, int SetIndex, string PrintType)
{
    //If SetIndex=-1, print the stress free node set
    vector<long> NSet;
    NSet.clear();
    
    //Add the corner nodes to NSet
    for (int i=0; i<BodiesNumber; i++)
    {
        for (int j=0; j<Dimension*Bodies[i].SurfaceCvNumber; j++)
        {
            if (Bodies[i].CornerNodes[j].InDomain==true)
            {
                bool NodePushed=false;
                for (int k=0; k<Bodies[i].CornerNodes[j].AttachedSet.size(); k++)
                {
                    int Set=Bodies[i].CornerNodes[j].AttachedSet[k];
                    long NodeIndex=0;
                    if (Bodies[i].CornerNodes[j].StartEnd[k]==1)
                    {
                        NodeIndex=Bodies[i].SurfaceCv[Set].NumberNodes-1;
                    }
                    bool StressFree=true;
                    for (int l=0; l<Dimension; l++)
                    {
                        if (SurfaceCv[Set].BC_N_Bool[l]==false || SurfaceCv[Set].BC_N[l]!=0)
                        {
                            StressFree=false;
                        }
                    }
                    if ((StressFree==true && SetIndex==-1 && NodePushed==false) || (StressFree==false && BodyIndex==i && SetIndex==Set && NodePushed==false))
                    {
                        long NodeCount=Bodies[i].CornerNodes[j].GlobalIndex[0];
                        NSet.push_back(NodeCount);
                        NodePushed=true;
                        if (PrintType!="Discretization" && Bodies[i].CornerNodes[j].BCPrinted==false)
                        {
                            PrintBCRow2D(NodeCount,i,Set,NodeIndex);
                            Bodies[i].CornerNodes[j].BCPrinted=true;
                        }
                    }
                }
            }
        }
    }
    
    //Add the edge nodes to NSet
    for (int i=0; i<BodiesNumber; i++)
    {
        for (int j=0; j<Bodies[i].SurfaceCvNumber; j++)
        {
            if (SetIndex==j || SetIndex==-1)
            {
                bool StressFree=true;
                for (int k=0; k<Dimension; k++)
                {
                    if (SurfaceCv[j].BC_N_Bool[k]==false || SurfaceCv[j].BC_N[k]!=0)
                    {
                        StressFree=false;
                    }
                }
                if ((StressFree==true && SetIndex==-1) || (StressFree==false && SetIndex>=0))
                {
                    for (long k=1; k<Bodies[i].SurfaceCv[j].NumberNodes-1; k++)
                    {
                        long NodeCount=Bodies[i].SurfaceCv[j].CurveNode[k].GlobalIndex[0];
                        NSet.push_back(NodeCount);
                        if (PrintType!="Discretization" && Bodies[i].SurfaceCv[j].CurveNode[k].BCPrinted==false)
                        {
                            PrintBCRow2D(NodeCount,i,j,k);
                            Bodies[i].SurfaceCv[j].CurveNode[k].BCPrinted=true;
                        }
                    }
                }
            }
        }
    }
    
    if (NSet.size()>0 && PrintType=="Discretization")
    {
        if (SetIndex==-1)
        {
            *outfile << "*NSET,NSET=StressFree" << endl;
        }
        else
        {
            *outfile << "*NSET,NSET=NSet" << SetIndex << endl;
        }
        int RowCount=0;
        for (long i=0; i<NSet.size(); i++)
        {
            *outfile << NSet[i] << ", ";
            RowCount++;
            if (RowCount==10)
            {
                *outfile << endl;
                RowCount=0;
            }
        }
        if (RowCount>0)
        {
            *outfile << endl;
        }
    }
}

void Model::PrintNodes2D(long* Count, string PrintType)
{
    //Print the corner nodes
    for (int i=0; i<BodiesNumber; i++)
    {
        for (int j=0; j<Dimension*Bodies[i].SurfaceCvNumber; j++)
        {
            if (Bodies[i].CornerNodes[j].InDomain==true)
            {
                if (PrintType=="Discretization")
                    *outfile << *Count << ", " ;
                else
                    *outfile << ToString(*Count,0,15,false);
                *outfile << ToString(Bodies[i].CornerNodes[j].X[0],8,17,false) << ToString(Bodies[i].CornerNodes[j].X[1],8,17,false) << ToString(Bodies[i].CornerNodes[j].X[2],8,17,false);
                *outfile << ToString(Bodies[i].CornerNodes[j].Normal[0],8,17,false) << ToString(Bodies[i].CornerNodes[j].Normal[1],8,17,false) << ToString(Bodies[i].CornerNodes[j].Normal[2],8,17,true);
                *outfile << endl;
                Bodies[i].CornerNodes[j].GlobalIndex[0]=(*Count);
                (*Count)++;
            }
        }
    }
    //Print the edge nodes
    for (int i=0; i<BodiesNumber; i++)
    {
        for (int j=0; j<Bodies[i].SurfaceCvNumber; j++)
        {
            for (long k=1; k<Bodies[i].SurfaceCv[j].NumberNodes-1; k++)
            {
                if (PrintType=="Discretization")
                    *outfile << *Count << ", " ;
                else
                    *outfile << ToString(*Count,0,15,false);
                *outfile << ToString(Bodies[i].SurfaceCv[j].CurveNode[k].X[0],8,17,false) << ToString(Bodies[i].SurfaceCv[j].CurveNode[k].X[1],8,17,false) << ToString(Bodies[i].SurfaceCv[j].CurveNode[k].X[2],8,17,false);
                *outfile << ToString(Bodies[i].SurfaceCv[j].CurveNode[k].Normal[0],8,17,false) << ToString(Bodies[i].SurfaceCv[j].CurveNode[k].Normal[1],8,17,false) << ToString(Bodies[i].SurfaceCv[j].CurveNode[k].Normal[2],8,17,true);
                Bodies[i].SurfaceCv[j].CurveNode[k].GlobalIndex[0]=(*Count);
                *outfile << endl;
                (*Count)++;
            }
        }
    }
//    for (long i=0; i<TotalClosedCvNum; i++)
//    {
//        for (long j=0; j<ClosedCv[i].SurfaceCvNumber; j++)
//        {
//            for (long k=0; k<ClosedCv[i].SurfaceCv[j].NumberNodes-1; k++)
//            {
//                ClosedCv[i].SurfaceCv[j].CurveNode[k].GlobalIndex[0]=*Count;
//                if (PrintType=="Discretization")
//                    *outfile << *Count << ", " ;
//                else
//                    *outfile << ToString(*Count,0,15,false);
//                *outfile << ToString(ClosedCv[i].SurfaceCv[j].CurveNode[k].X[0],8,17,false) << ToString(ClosedCv[i].SurfaceCv[j].CurveNode[k].X[1],8,17,false);
//                if (PrintType=="Discretization")
//                {
//                    *outfile << ToString(ClosedCv[i].SurfaceCv[j].CurveNode[k].X[2],8,17,false) << ToString(ClosedCv[i].SurfaceCv[j].CurveNode[k].Normal[0],8,17,false) << ToString(ClosedCv[i].SurfaceCv[j].CurveNode[k].Normal[1],8,17,false) << ToString(ClosedCv[i].SurfaceCv[j].CurveNode[k].Normal[2],8,17,true);
//                }
//                else
//                {
//                    *outfile << ToString(ClosedCv[i].SurfaceCv[j].CurveNode[k].X[2],8,17,true);
//                }
//                *outfile << endl;
//                (*Count)++;
//            }
//        }
//    }
//    
//    //Get the index of the second nodes of the last elements of each curve
//    for (long i=0; i<TotalSurfaceCvNumber; i++)
//    {
//        long j=SurfaceCv[i].NumberNodes-1;
//        //Set the index of the last node
//        long NextCvIndex=SurfaceCv[i].NextCvIndex;
//        SurfaceCv[i].CurveNode[j].GlobalIndex[0]=SurfaceCv[NextCvIndex].CurveNode[0].GlobalIndex[0];
//    }
}

void Model::PrintInnerNodes(long* Count, string PrintType)
{
    for (int i=0; i<BodiesNumber; i++)
    {
        for (long j=0; j<Bodies[i].BoxNodeNumber; j++)
        {
            if (Bodies[i].BoxNode[j].InDomain==true)
            {
                if (PrintType=="Discretization")
                    *outfile << *Count << ", " ;
                else
                    *outfile << ToString(*Count,0,15,false);
                *outfile << ToString(Bodies[i].BoxNode[j].X[0],8,17,false) << ToString(Bodies[i].BoxNode[j].X[1],8,17,false) << ToString(Bodies[i].BoxNode[j].X[2],8,17,true) << endl;
                (*Count)++;
            }
        }
    }
}

void Model::IdentifyDuplicatedFaceNodes3D()
{
    //Create a vector with the nodes in the domain
    pointVec points;
    vector<vector<long>> indices;
    //Loop through all the faces
    for (int i=0; i<TotalFaceNumber-1; i++)
    {
        //Count the number of surf nodes
        long CountSurfNodes=0;
        for (int j=0; j<i+1; j++)
        {
            int SurfIndex=j;
            if (HasSurfPrintOrder==true)
                SurfIndex=SurfPrintOrderReverse[j];
            for (int k=0; k<SurfaceFc[SurfIndex].FaceNodesNumber; k++)
            {
                if (SurfaceFc[SurfIndex].FaceNodes[k].SurfNodeInDomain==true)
                    CountSurfNodes++;
            }
        }
        //Load the point vector
        points.clear();
        points.resize(CountSurfNodes);
        indices.clear();
        indices.resize(CountSurfNodes);

        CountSurfNodes=0;
        for (int j=0; j<i+1; j++)
        {
            int SurfIndex=j;
            if (HasSurfPrintOrder==true)
                SurfIndex=SurfPrintOrderReverse[j];
            for (int k=0; k<SurfaceFc[SurfIndex].FaceNodesNumber; k++)
            {
                if (SurfaceFc[SurfIndex].FaceNodes[k].SurfNodeInDomain==true)
                {
                    points[CountSurfNodes]=point_t({SurfaceFc[SurfIndex].FaceNodes[k].X[0],SurfaceFc[SurfIndex].FaceNodes[k].X[1],SurfaceFc[SurfIndex].FaceNodes[k].X[2]});
                    indices[CountSurfNodes].resize(2);
                    indices[CountSurfNodes][0]=SurfIndex;
                    indices[CountSurfNodes][1]=k;
                    CountSurfNodes++;
                }
            }
        }
        //Load the points in a KD tree
        KDTree treeSurf=KDTree(points);
        //For each node of the surface i+1, identify if the duplicated nodes
        int NextSurfIndex=i+1;
        if (HasSurfPrintOrder==true)
            NextSurfIndex=SurfPrintOrderReverse[i+1];
        for (int k=0; k<SurfaceFc[NextSurfIndex].FaceNodesNumber; k++)
        {
            //Define the queried point
            point_t PtQuery;
            PtQuery = {SurfaceFc[NextSurfIndex].FaceNodes[k].X[0],SurfaceFc[NextSurfIndex].FaceNodes[k].X[1],SurfaceFc[NextSurfIndex].FaceNodes[k].X[2]};
            auto QueryResult = treeSurf.neighborhood_indices(PtQuery,h/1000);
            for (int j=0; j<QueryResult.size(); j++)
            {
                SurfaceFc[NextSurfIndex].FaceNodes[k].GlobalIndex=SurfaceFc[indices[QueryResult[j]][0]].FaceNodes[indices[QueryResult[j]][1]].GlobalIndex;
                
                SurfaceFc[NextSurfIndex].FaceNodes[k].RefSurf.push_back(SurfaceFc[indices[QueryResult[j]][0]].FaceNodes[indices[QueryResult[j]][1]].RefSurf[0]);
                SurfaceFc[NextSurfIndex].FaceNodes[k].RefEdge.push_back(SurfaceFc[indices[QueryResult[j]][0]].FaceNodes[indices[QueryResult[j]][1]].RefEdge[0]);
                
                SurfaceFc[indices[QueryResult[j]][0]].FaceNodes[indices[QueryResult[j]][1]].RefSurf.push_back(SurfaceFc[NextSurfIndex].FaceNodes[k].RefSurf[0]);
                SurfaceFc[indices[QueryResult[j]][0]].FaceNodes[indices[QueryResult[j]][1]].RefEdge.push_back(SurfaceFc[NextSurfIndex].FaceNodes[k].RefEdge[0]);
            }
            if (QueryResult.size()>0)
            {
                SurfaceFc[NextSurfIndex].FaceNodes[k].SurfNodeInDomain=false;
            }
        }
    }    
}

void Model::Fill3DBoundaryNodes()
{
    //Set the node numbers and get the total number of nodes    
    TotalBoundaryNodes=Set3DNodesNumber();
    
    //Create and fill the boundary nodes
    BoundaryNode3D = new Node[TotalBoundaryNodes];
    for (long i=0; i<TotalBoundaryNodes; i++)
    {
        BoundaryNode3D[i].Initialize(3);
    }
    
    //Loop through all the faces
    for (int i=0; i<TotalFaceNumber; i++)
    {
        int SurfIndex=i;
        if (HasSurfPrintOrder==true)
            SurfIndex=SurfPrintOrderReverse[i];
        for (long j=0; j<SurfaceFc[SurfIndex].FaceNodesNumber; j++)
        {
            vector<double> TempNormal(3);
            for (int k=0; k<3; k++)
            {
                TempNormal[k]=round(SurfaceFc[SurfIndex].FaceNodes[j].Normal[k]*1e5)/1e5;
                if (abs(TempNormal[k])<1e-9)
                {
                    TempNormal[k]=0;
                }
            }
            long RefNode=SurfaceFc[SurfIndex].FaceNodes[j].GlobalIndex[0]-1;
            BoundaryNode3D[RefNode].AttachedNormals.push_back(TempNormal);
            BoundaryNode3D[RefNode].AttachedSet3D.push_back(SurfIndex);
            for (int k=0; k<3; k++)
            {
                BoundaryNode3D[RefNode].X[k]=SurfaceFc[SurfIndex].FaceNodes[j].X[k];
            }
        }
    }
    
//    //Print all the normals associated to a node
//    for (long i=0; i<TotalBoundaryNodes; i++)
//    {
//        for (long j=0; j<BoundaryNode3D[i].AttachedSet3D.size(); j++)
//        {
//            cout << i+1 << ", " << BoundaryNode3D[i].X[0]*1000 << ", " << BoundaryNode3D[i].X[1]*1000 << ", " << BoundaryNode3D[i].X[2]*1000 << ", Set=" << BoundaryNode3D[i].AttachedSet3D[j] << ", " << BoundaryNode3D[i].AttachedNormals[j][0]*1000 << ", " << BoundaryNode3D[i].AttachedNormals[j][1]*1000 << ", " << BoundaryNode3D[i].AttachedNormals[j][2]*1000 << endl;
//        }
//    }
}

long Model::Set3DNodesNumber()
{
    long Count;
    Count=1;
    //Loop through all the faces
    for (int i=0; i<TotalFaceNumber; i++)
    {
        int SurfIndex=i;
        if (HasSurfPrintOrder==true)
            SurfIndex=SurfPrintOrderReverse[i];
        for (long j=0; j<SurfaceFc[SurfIndex].FaceNodesNumber; j++)
        {
            if (SurfaceFc[SurfIndex].FaceNodes[j].SurfNodeInDomain==true)
            {
                SurfaceFc[SurfIndex].FaceNodes[j].GlobalIndex[0]=Count;
                Count++;
            }
        }
    }
    return Count-1;
}

void Model::PrintNodes3D(long* Count, string PrintType)
{
    //Loop through all the faces
    for (int i=0; i<TotalFaceNumber; i++)
    {
        int SurfIndex=i;
        if (HasSurfPrintOrder==true)
            SurfIndex=SurfPrintOrderReverse[i];
        for (long j=0; j<SurfaceFc[SurfIndex].FaceNodesNumber; j++)
        {
            if (SurfaceFc[SurfIndex].FaceNodes[j].SurfNodeInDomain==true)
            {
                SurfaceFc[SurfIndex].FaceNodes[j].GlobalIndex[0]=*Count;
                if (PrintType=="Discretization")
                    *outfile << *Count << ", " ;
                else
                    *outfile << ToString(*Count,0,15,false);
                *outfile << ToString(SurfaceFc[SurfIndex].FaceNodes[j].X[0],8,17,false) << ToString(SurfaceFc[SurfIndex].FaceNodes[j].X[1],8,17,false);
                if (PrintType=="Discretization")
                {
                    *outfile << ToString(SurfaceFc[SurfIndex].FaceNodes[j].X[2],8,17,false);
                    *outfile << ToString(SurfaceFc[SurfIndex].FaceNodes[j].Normal[0],8,17,false) << ToString(SurfaceFc[SurfIndex].FaceNodes[j].Normal[1],8,17,false) << ToString(SurfaceFc[SurfIndex].FaceNodes[j].Normal[2],8,17,true);
//                    *outfile << ", " << SurfaceFc[SurfIndex].FaceNodes[j].RefSurf << ", " << SurfaceFc[SurfIndex].FaceNodes[j].RefEdge;
                }
                else
                {
                    *outfile << ToString(SurfaceFc[SurfIndex].FaceNodes[j].X[2],8,17,true);
                }
                *outfile << endl;
                (*Count)++;
            }
        }
    }    
}

void Model::PrintElements2D(string PrintType)
{
    int LastNomalInc=3;
    if (PrintType=="Discretization")
    {
        *outfile << "*ELEMENT, type=T3D2" << endl;
    }
    else
    {
        *outfile << "#SURFACE_NUMBER=1" << endl;
        *outfile << "#SURFACE_TYPE=1,OUTER" << endl;
        
        //Count the number of surface elements
        long ElCount=0;
        for (int i=0; i<BodiesNumber; i++)
        {
            for (int j=0; j<Bodies[i].SurfaceCvNumber; j++)
            {
                ElCount+=Bodies[i].SurfaceCv[j].NumberElements;
            }
        }
        *outfile << "#SURFACE_EL=1,"<< ElCount <<",2D_LINEAR_WITH_BC" << endl;
        LastNomalInc=2;
    }
    long Count=1;
//    for (long i=0; i<TotalSurfaceCvNumber; i++)
//    {
//        for (long j=0; j<SurfaceCv[i].NumberElements; j++)
//        {
//            SurfaceCv[i].CurveElement[j].GlobalIndex=Count;
//            *outfile << Count << ", " << SurfaceCv[i].CurveElement[j].ElNode[0]->GlobalIndex[0] << ", " << SurfaceCv[i].CurveElement[j].ElNode[1]->GlobalIndex[0];
//            vector<double> TempNormal;
//            double Norm=0;
//            for (int k=0; k<3; k++)
//            {
//                TempNormal[k]=SurfaceCv[i].CurveElement[j].ElNode[0]->Normal[k]+SurfaceCv[i].CurveElement[j].ElNode[1]->Normal[k];
//                Norm+=pow(TempNormal[k],2);
//            }
//            Norm=pow(Norm,0.5);
//            for (int k=0; k<3; k++)
//            {
//                *outfile << ", " << TempNormal[k]/Norm;
//            }
//            *outfile << endl;
//            Count++;
//        }
//    }
    
    for (int i=0; i<BodiesNumber; i++)
    {
        for (int j=0; j<Bodies[i].SurfaceCvNumber; j++)
        {
            for (long k=0; k<Bodies[i].SurfaceCv[j].NumberElements; k++)
            {
                Bodies[i].SurfaceCv[j].CurveElement[k].GlobalIndex=Count;
                if (PrintType=="Discretization")
                {
                    *outfile << Count;
                }
                else
                {
                    *outfile << " " << Count;
                }
                *outfile << ", " << Bodies[i].SurfaceCv[j].CurveElement[k].ElNode[0]->GlobalIndex[0] << ", " << Bodies[i].SurfaceCv[j].CurveElement[k].ElNode[1]->GlobalIndex[0] << ", ";
                vector<double> TempNormal;
                TempNormal.resize(3);
                double Norm=0;
                for (int l=0; l<3; l++)
                {
                    TempNormal[l]=Bodies[i].SurfaceCv[j].CurveElement[k].ElNode[0]->Normal[l]+Bodies[i].SurfaceCv[j].CurveElement[k].ElNode[1]->Normal[l];
                    Norm+=pow(TempNormal[l],2);
                }
                Norm=pow(Norm,0.5);
                for (int l=0; l<LastNomalInc-1; l++)
                {
                    *outfile << ToString(TempNormal[l]/Norm,8,17,false);
                }
                *outfile << ToString(TempNormal[LastNomalInc-1]/Norm,8,17,true);
                
                //Print the BC associate to the element
                int GlobalCvNum=Bodies[i].SurfaceCv[j].GlobalCvNum;
                for (int k=0; k<Dimension; k++)
                {
                    if (SurfaceCv[GlobalCvNum].BC_D_Bool[k]==true)
                        *outfile << ", D";
                    else if (SurfaceCv[GlobalCvNum].BC_N_Bool[k]==true)
                        *outfile << ", N";
                    if (SurfaceCv[GlobalCvNum].LocalBoundaryCondition[k]==true)
                        *outfile << ", L";
                    else if (SurfaceCv[GlobalCvNum].LocalBoundaryCondition[k]==false)
                        *outfile << ", G";
                    if (SurfaceCv[GlobalCvNum].BC_D_Bool[k]==true)
                        *outfile << ", " << SurfaceCv[GlobalCvNum].BC_D[k];
                    else if (SurfaceCv[GlobalCvNum].BC_N_Bool[k]==true)
                        *outfile << ", " << SurfaceCv[GlobalCvNum].BC_N[k];
                }
                
                *outfile << endl;
                Count++;
            }
        }
    }
}

void Model::PrintElements3D(string PrintType)
{
    if (PrintType=="Discretization")
    {
        *outfile << "*ELEMENT, type=CPS3" << endl;
    }
    else
    {
        *outfile << "#SURFACE_NUMBER=1" << endl;
        *outfile << "#SURFACE_TYPE=1,OUTER" << endl;
    }
    //Count the number of surface elements
    long ElCount=0;
    for (int i=0; i<TotalFaceNumber; i++)
    {
        ElCount+=SurfaceFc[i].InnerElementsNumber;
    }
    if (PrintType!="Discretization")
    {
        *outfile << "#SURFACE_EL=1,"<< ElCount <<",3D_LINEAR_WITH_BC_CAD" << endl;
    }
    
    long Count=1;
    for (int i=0; i<TotalFaceNumber; i++)
    {
        for (long j=0; j<SurfaceFc[i].InnerElementsNumber; j++)
        {
            *outfile << Count << ", " << SurfaceFc[i].FaceInnerElements[j].ElNode[0]->GlobalIndex[0] << ", " << 
                                         SurfaceFc[i].FaceInnerElements[j].ElNode[1]->GlobalIndex[0] << ", " <<
                                         SurfaceFc[i].FaceInnerElements[j].ElNode[2]->GlobalIndex[0];
            if (GeoInpFile!="")
            {
                *outfile  << ", ";
                //Print the normal
                for (int k=0; k<2; k++)
                {
                    *outfile << ToString(SurfaceFc[i].FaceInnerElements[j].Normal[k],8,17,false);
                }
                *outfile << ToString(SurfaceFc[i].FaceInnerElements[j].Normal[2],8,17,true);
                //Print the BC associate to the element
                for (int k=0; k<Dimension; k++)
                {
                    if (SurfaceFc[i].BC_D_Bool[k]==true)
                        *outfile << ", D";
                    else if (SurfaceFc[i].BC_N_Bool[k]==true)
                        *outfile << ", N";
                    if (SurfaceFc[i].LocalBoundaryCondition[k]==true)
                        *outfile << ", L";
                    else if (SurfaceFc[i].LocalBoundaryCondition[k]==false)
                        *outfile << ", G";
                    if (SurfaceFc[i].BC_D_Bool[k]==true)
                        *outfile << ", " << SurfaceFc[i].BC_D[k];
                    else if (SurfaceFc[i].BC_N_Bool[k]==true)
                        *outfile << ", " << SurfaceFc[i].BC_N[k];
                }
            }
            
            //Print the reference surface and edge associated to the element if any
            PrintRefSurfEdge(i,j);
            
            *outfile << endl;
            Count++;
        }
    }    
//    *outfile << "*NSET,NSET=ConcaveSurface" << endl;
//    int RowCount=0;
//    //Loop through all the faces
//    for (int i=0; i<TotalFaceNumber; i++)
//    {
//        int SurfIndex=i;
//        if (HasSurfPrintOrder==true)
//            SurfIndex=SurfPrintOrderReverse[i];
//        for (long j=0; j<SurfaceFc[SurfIndex].FaceNodesNumber; j++)
//        {
//            if (SurfaceFc[SurfIndex].FaceNodes[j].SurfNodeInDomain==true)
//            {
//                *outfile << SurfaceFc[SurfIndex].FaceNodes[j].GlobalIndex[0] << ",";
//                RowCount++;
//                if (RowCount==10)
//                {
//                    *outfile << endl;
//                    RowCount=0;
//                }
//            }
//        }
//    }
//    if (RowCount!=0)
//    {
//        *outfile << endl;
//    }
}

void Model::PrintRefSurfEdge(int i, int j)
{
    int ElRefSurf=-1;
    int ElRefEdge=-1;
    //Identify if two nodes are connected to the same surface and edge
    for (int k=0; k<SurfaceFc[i].FaceInnerElements[j].ElNode[0]->RefSurf.size(); k++)
    {
        int S1=SurfaceFc[i].FaceInnerElements[j].ElNode[0]->RefSurf[k];
        int E1=SurfaceFc[i].FaceInnerElements[j].ElNode[0]->RefEdge[k];
        for (int l=0; l<SurfaceFc[i].FaceInnerElements[j].ElNode[1]->RefSurf.size(); l++)
        {
            int S2=SurfaceFc[i].FaceInnerElements[j].ElNode[1]->RefSurf[l];
            int E2=SurfaceFc[i].FaceInnerElements[j].ElNode[1]->RefEdge[l];
            if (S1==S2 && E1==E2)
            {
                ElRefSurf=S1;
                ElRefEdge=E1;
                break;
            }
        }
        for (int l=0; l<SurfaceFc[i].FaceInnerElements[j].ElNode[2]->RefSurf.size(); l++)
        {
            int S3=SurfaceFc[i].FaceInnerElements[j].ElNode[2]->RefSurf[l];
            int E3=SurfaceFc[i].FaceInnerElements[j].ElNode[2]->RefEdge[l];
            if (S1==S3 && E1==E3)
            {
                ElRefSurf=S1;
                ElRefEdge=E1;
                break;
            }
        }
    }
    for (int k=0; k<SurfaceFc[i].FaceInnerElements[j].ElNode[1]->RefSurf.size(); k++)
    {
        int S2=SurfaceFc[i].FaceInnerElements[j].ElNode[1]->RefSurf[k];
        int E2=SurfaceFc[i].FaceInnerElements[j].ElNode[1]->RefEdge[k];
        for (int l=0; l<SurfaceFc[i].FaceInnerElements[j].ElNode[2]->RefSurf.size(); l++)
        {
            int S3=SurfaceFc[i].FaceInnerElements[j].ElNode[2]->RefSurf[l];
            int E3=SurfaceFc[i].FaceInnerElements[j].ElNode[2]->RefEdge[l];
            if (S2==S3 && E2==E3)
            {
                ElRefSurf=S2;
                ElRefEdge=E2;
                break;
            }
        }
    }
    if (ElRefSurf>-1 && ElRefEdge>-1)
    {
        *outfile << ", " << ElRefSurf << ", " << ElRefEdge;
    }
    else
    {
        *outfile << ", -1, -1";
    }
    *outfile << ", " << SurfaceFc[i].FaceInnerElements[j].RefSurf;
}

void Model::PrintCollocationInputFile()
{
    string InpuFileNam=Path;
    std::stringstream NewFileName;
    string OutputFileName=InpuFileNam.replace(InpuFileNam.end()-5,InpuFileNam.end(),"");
    NewFileName << OutputFileName << "-" << TargetNumNodes << "-" << fixed << setprecision(2)  << ThresholdRatio << "-c.inp";
    OutputFileName=NewFileName.str();
    outfile= new std::ofstream(OutputFileName);
    
     //Print the header
    string str;
    string InpuFileName2=Path;
    std::stringstream HeaderFileNameStream;
    string HeaderFileName=InpuFileName2.replace(InpuFileName2.end()-5,InpuFileName2.end(),"");
    HeaderFileNameStream << HeaderFileName << ".hin";
    HeaderFileName=HeaderFileNameStream.str();
    char Path2[HeaderFileName.length()+1];
    strncpy(Path2, HeaderFileName.c_str(),HeaderFileName.length()+1);
    ifstream HeaderFile;
    //Open the selected file
    HeaderFile.open (Path2, ios::in);
    //Add all the lines of the file in a vector
    while (getline(HeaderFile, str))
    {
        if (str=="#NODE_DATA")
        {
            break;
        }
        else
        {
            *outfile << str << endl;
        }
    }
    HeaderFile.close();
    
    //Print the header
//    *outfile << "#ANALYSIS_TYPE=2D_PLANE_STRESS" << endl;
//    *outfile << "#PARAMETERS" << endl;
//    *outfile << " E=210000" << endl;
//    *outfile << " nu=0.3" << endl;
//    *outfile << "#METHOD=GFD" << endl;
//    *outfile << "#WINDOW_TYPE=SPLINE_4,0.75" << endl;
//    *outfile << "#SUP_SIZE=15,22" << endl;
//    *outfile << "#THREAD=2" << endl;
//    *outfile << "#VISIBILITY_TYPE=DIRECT_SURF" << endl;
//    *outfile << "#VISIBILITY_THRESHOLD=5" << endl;
//    *outfile << "#SOLVER=DIRECT_MUMPS" << endl;
//    *outfile << "#PRINT_OPTIONS" << endl;
//    *outfile << " CONDITION_NUM=TRUE" << endl;
//    *outfile << " TIME_SPLIT=TRUE" << endl;
    
    *outfile << "#NODE_DATA" << endl;
    //Print the boundary nodes
    long Count=1;
    if (Dimension==2)
    {
        PrintNodes2D(&Count,"Collocation");
    }
    else if (Dimension==3)
    {
        IdentifyDuplicatedFaceNodes3D();
        PrintNodes3D(&Count,"Collocation");
    }
    
    //Print the inner nodes
    PrintInnerNodes(&Count,"Collocation");
    
    //Print the boundary conditions
    *outfile << "#BOUNDARY" << endl;
    if (Dimension==2)
    {
//        PrintBoundaries2D();
        for (int i=0; i<BodiesNumber; i++)
        {
            for (int j=0; j<Bodies[i].SurfaceCvNumber; j++)
            {
                PrintNSet(i,j,"Collocation");
            }
        }
        //Print the stress free node set
        PrintNSet(0,-1,"Collocation");
    }
    else if (Dimension==3)
    {
        PrintBoundaries3D();
    }
    //Print the boundary elements
    if (Dimension==2)
    {
        PrintElements2D("Collocation");
    }
    else if (Dimension==3)
    {
        PrintElements3D("Collocation");
    }
    
    *outfile << "#END" << endl;
    outfile->close();
}

void Model::PrintBCRow2D(long NodeCount, int BodyIndex, int CurveIndex, long NodeIndex)
{
    double Nx=Bodies[BodyIndex].SurfaceCv[CurveIndex].CurveNode[NodeIndex].Normal[0];
    double Ny=Bodies[BodyIndex].SurfaceCv[CurveIndex].CurveNode[NodeIndex].Normal[1];
    
    int GlobalCvNum=Bodies[BodyIndex].SurfaceCv[CurveIndex].GlobalCvNum;
    if (SurfaceCv[GlobalCvNum].BC_D_Bool[0]==true && SurfaceCv[GlobalCvNum].BC_D_Bool[1]==false && SurfaceCv[GlobalCvNum].LocalBoundaryCondition[0]==true && abs(Ny)>0.8)
    {
        *outfile << " " << NodeCount << ", 2, " << ToString(Nx,8,17,false) << ToString(Ny,8,17,false) << "N, L, " << ToString(SurfaceCv[GlobalCvNum].BC_N[1],8,17,true) << endl;
        *outfile << " " << NodeCount << ", 1, " << ToString(Nx,8,17,false) << ToString(Ny,8,17,false) << "D, L, " << ToString(SurfaceCv[GlobalCvNum].BC_D[0],8,17,true) << endl;
    }
    else
    {
        *outfile << " " << NodeCount << ", *, " << ToString(Nx,8,17,false) << ToString(Ny,8,17,false);
        for (int k=0; k<Dimension; k++)
        {
            if (SurfaceCv[GlobalCvNum].BC_D_Bool[k]==true)
                *outfile << "D, ";
            else if (SurfaceCv[GlobalCvNum].BC_N_Bool[k]==true)
                *outfile << "N, ";
            if (SurfaceCv[GlobalCvNum].LocalBoundaryCondition[k]==true)
                *outfile << "L, ";
            else if (SurfaceCv[GlobalCvNum].LocalBoundaryCondition[k]==false)
                *outfile << "G, ";
            bool Last=false;
            if (k==Dimension-1)
                Last=true;
            if (SurfaceCv[GlobalCvNum].BC_D_Bool[k]==true)
                *outfile << ToString(SurfaceCv[GlobalCvNum].BC_D[k],8,17,Last);
            else if (SurfaceCv[GlobalCvNum].BC_N_Bool[k]==true)
                *outfile << ToString(SurfaceCv[GlobalCvNum].BC_N[k],8,17,Last);
        }
        *outfile << endl;
    }
}

void Model::PrintBoundaries2D()
{
    long Count=1;
    //Print he NSET stress free conditions
    for (long i=0; i<TotalSurfaceCvNumber; i++)
    {
        int ii=SurfPrintOrder[i];
        for (long j=0; j<SurfaceCv[ii].NumberNodes-1; j++)
        {
            *outfile << " " << Count << ", *, " << ToString(SurfaceCv[ii].CurveNode[j].Normal[0],8,17,false) << ToString(SurfaceCv[ii].CurveNode[j].Normal[1],8,17,false);
            //Set the boundary condition
            for (int k=0; k<Dimension; k++)
            {
                if (SurfaceCv[ii].BC_D_Bool[k]==true)
                    *outfile << "D, ";
                else if (SurfaceCv[ii].BC_N_Bool[k]==true)
                    *outfile << "N, ";
                if (SurfaceCv[ii].LocalBoundaryCondition[k]==true)
                    *outfile << "L, ";
                else if (SurfaceCv[ii].LocalBoundaryCondition[k]==false)
                    *outfile << "G, ";
                bool Last=false;
                if (k==Dimension-1)
                    Last=true;
                if (SurfaceCv[ii].BC_D_Bool[k]==true)
                    *outfile << ToString(SurfaceCv[ii].BC_D[k],8,17,Last);
                else if (SurfaceCv[ii].BC_N_Bool[k]==true)
                    *outfile << ToString(SurfaceCv[ii].BC_N[k],8,17,Last);
            }
            *outfile << endl;
            Count++;
        }
    }
    
//    for (long i=0; i<TotalSurfaceCvNumber; i++)
//    {
//        int ii=SurfPrintOrder[i];
//        for (long j=0; j<SurfaceCv[ii].NumberNodes-1; j++)
//        {
//            *outfile << " " << Count << ", *, " << ToString(SurfaceCv[ii].CurveNode[j].Normal[0],8,17,false) << ToString(SurfaceCv[ii].CurveNode[j].Normal[1],8,17,false);
//            //Set the boundary condition
//            for (int k=0; k<Dimension; k++)
//            {
//                if (SurfaceCv[ii].BC_D_Bool[k]==true)
//                    *outfile << "D, ";
//                else if (SurfaceCv[ii].BC_N_Bool[k]==true)
//                    *outfile << "N, ";
//                if (SurfaceCv[ii].LocalBoundaryCondition[k]==true)
//                    *outfile << "L, ";
//                else if (SurfaceCv[ii].LocalBoundaryCondition[k]==false)
//                    *outfile << "G, ";
//                bool Last=false;
//                if (k==Dimension-1)
//                    Last=true;
//                if (SurfaceCv[ii].BC_D_Bool[k]==true)
//                    *outfile << ToString(SurfaceCv[ii].BC_D[k],8,17,Last);
//                else if (SurfaceCv[ii].BC_N_Bool[k]==true)
//                    *outfile << ToString(SurfaceCv[ii].BC_N[k],8,17,Last);
//            }
//            *outfile << endl;
//            Count++;
//        }
//    }
}

void Model::PrintBoundaries3D()
{
    //Loop through all the boundary nodes
    for (long i=0; i<TotalBoundaryNodes; i++)
    {
        if (BoundaryNode3D[i].AttachedSet3D.size()==1 && SurfaceFc[BoundaryNode3D[i].AttachedSet3D[0]].LocalBoundaryCondition[0]==false)
        {
            int SurfIndex=BoundaryNode3D[i].AttachedSet3D[0];
            *outfile << " " << i+1 << ", *, " << ToString(BoundaryNode3D[i].AttachedNormals[0][0],8,17,false) << ToString(BoundaryNode3D[i].AttachedNormals[0][1],8,17,false) << ToString(BoundaryNode3D[i].AttachedNormals[0][2],8,17,false);
            //Set the boundary condition
            for (int k=0; k<Dimension; k++)
            {
                if (SurfaceFc[SurfIndex].BC_D_Bool[k]==true)
                    *outfile << "D, ";
                else if (SurfaceFc[SurfIndex].BC_N_Bool[k]==true)
                    *outfile << "N, ";
                if (SurfaceFc[SurfIndex].LocalBoundaryCondition[k]==true)
                    *outfile << "L, ";
                else if (SurfaceFc[SurfIndex].LocalBoundaryCondition[k]==false)
                    *outfile << "G, ";
                bool Last=false;
                if (k==Dimension-1)
                    Last=true;
                if (SurfaceFc[SurfIndex].BC_D_Bool[k]==true)
                    *outfile << ToString(SurfaceFc[SurfIndex].BC_D[k],8,17,Last);
                else if (SurfaceFc[SurfIndex].BC_N_Bool[k]==true)
                    *outfile << ToString(SurfaceFc[SurfIndex].BC_N[k],8,17,Last);
            }
            *outfile << endl;
        }
        else
        {
            vector<vector<double>> AttachedNormals;
            AttachedNormals.resize(3);
            vector<bool> BC_D_Bool(3);
            vector<bool> BC_N_Bool(3);
            vector<bool> LocalBoundaryCondition(3);
            vector<double> BC_D(3);
            vector<double> BC_N(3);
            vector<int> BC_DOF(3);
            //Initialize the vectors
            for (int j=0; j<3; j++)
            {
                AttachedNormals[j].resize(3);
                BC_D_Bool[j]=false;
                BC_N_Bool[j]=false;
                LocalBoundaryCondition[j]=false;
                BC_D[j]=0;
                BC_N[j]=0;
                BC_DOF[j]=j+1;
            }
            
            //Set the stress loading
            for (int j=0; j<BoundaryNode3D[i].AttachedSet3D.size(); j++)
            {
                int SurfIndex=BoundaryNode3D[i].AttachedSet3D[j];
                bool HasStressLoading=false;
                int LoadingDir=0;
                for (int k=0; k<3; k++)
                {
                    if (abs(SurfaceFc[SurfIndex].BC_N[k])>0)
                    {
                        HasStressLoading=true;
                        LoadingDir=k;
                    }
                }
                if (HasStressLoading==true)
                {
                    for (int k=0; k<3; k++)
                    {
                        //Set all the normals to the normals of the set
                        for (int kk=0; kk<3; kk++)
                        {
                            AttachedNormals[k][kk]=BoundaryNode3D[i].AttachedNormals[j][kk];
                        }
                        BC_N_Bool[k]=true;
                        //Convert local BC to global BC
                        LocalBoundaryCondition[k]=false;
                        if (SurfaceFc[SurfIndex].LocalBoundaryCondition[LoadingDir]==true)
                        {
                            BC_N[k]=BoundaryNode3D[i].AttachedNormals[j][k]*SurfaceFc[SurfIndex].BC_N[LoadingDir];
                        }
                        else
                        {
                            BC_N[k]=SurfaceFc[SurfIndex].BC_N[k];
                        }
                    }
                }
            }
            
            //Set the BCD conditions
            for (int j=0; j<BoundaryNode3D[i].AttachedSet3D.size(); j++)
            {
                int SurfIndex=BoundaryNode3D[i].AttachedSet3D[j];
                bool HasBCD=false;
                int BCDDir=0;
                for (int k=0; k<3; k++)
                {
                    if (SurfaceFc[SurfIndex].BC_D_Bool[k]==true)
                    {
                        HasBCD=true;
                        BCDDir=k;
                        if (SurfaceFc[SurfIndex].LocalBoundaryCondition[k]==true)
                        {
                            //Identify the direction of the normal
                            double MaxVal=0;
                            int MaxDir=0;
                            for (int kk=0; kk<3; kk++)
                            {
                                if (abs(BoundaryNode3D[i].AttachedNormals[j][kk])>MaxVal)
                                {
                                    MaxVal=abs(BoundaryNode3D[i].AttachedNormals[j][kk]);
                                    MaxDir=kk;
                                }
                            }
                            BC_D_Bool[MaxDir]=true;
                            LocalBoundaryCondition[MaxDir]=true;
                            BC_D[MaxDir]=SurfaceFc[SurfIndex].BC_D[k];
                            BC_DOF[MaxDir]=1;
                            
                            //Set the normal
                            Vector3d BCDNormal;
                            for (int kk=0; kk<3; kk++)
                            {
                                AttachedNormals[MaxDir][kk]=BoundaryNode3D[i].AttachedNormals[j][kk];
                                BCDNormal(kk)=BoundaryNode3D[i].AttachedNormals[j][kk];
                            }
                            
//                            //Get the ortogonal normal
//                            Vector3d VectTemp;
//                            Vector3d OrtNormal1;
//                            Vector3d OrtNormal2;
//                            for (int kk=0; kk<3; kk++)
//                            {
//                                VectTemp(kk)=0;
//                                OrtNormal1(kk)=0;
//                            }
//                            if (MaxDir==0)
//                                VectTemp(2)=1;
//                            else if (MaxDir==1)
//                                VectTemp(2)=1;
//                            else if (MaxDir==2)
//                                VectTemp(1)=1;
//                            OrtNormal1=BCDNormal.cross(VectTemp);
//                            OrtNormal2=BCDNormal.cross(OrtNormal1);
                            
                            //Set the normals and BCN in the other directions
                            int count=0;
                            for (int kk=0; kk<3; kk++)
                            {
                                if (BC_D_Bool[kk]==false && BC_N_Bool[kk]==false)
                                {
                                    //Set the normal
                                    for (int kkk=0; kkk<3; kkk++)
                                    {
//                                        if (count==0)
//                                        {
                                            //AttachedNormals[kk][kkk]=OrtNormal1(kkk);
                                            AttachedNormals[kk][kkk]=BoundaryNode3D[i].AttachedNormals[j][kkk];
//                                        }
//                                        else if (count>0)
//                                        {
//                                            AttachedNormals[kk][kkk]=OrtNormal2(kkk);
//                                        }
                                    }
                                    BC_N_Bool[kk]=true;
                                    BC_N[kk]=0;
//                                    BC_DOF[kk]=kk+1;
//                                    LocalBoundaryCondition[kk]=false;
                                    
                                    BC_DOF[kk]=count+2;
                                    LocalBoundaryCondition[kk]=true;
                                    count++;
                                }
                            }
                        }
                    }
                }
            }
            
            //Set the stress free boundary conditions
            for (int j=0; j<BoundaryNode3D[i].AttachedSet3D.size(); j++)
            {
                for (int k=0; k<3; k++)
                {
                    if (BC_D_Bool[k]==false && BC_N_Bool[k]==false)
                    {
                        for (int kk=0; kk<3; kk++)
                        {
                            AttachedNormals[k][kk]=BoundaryNode3D[i].AttachedNormals[j][kk];
                        }
                        BC_N_Bool[k]=true;
                        BC_N[k]=0;
                    }
                }
            }
            
            //Print the boundary conditions
            for (int j=0; j<Dimension; j++)
            {
                *outfile << " " << i+1;
                *outfile << ", " << BC_DOF[j] << ", ";
                *outfile << ToString(AttachedNormals[j][0],8,17,false) << ToString(AttachedNormals[j][1],8,17,false) << ToString(AttachedNormals[j][2],8,17,false);
                
                if (BC_D_Bool[j]==true)
                    *outfile << "D, ";
                else if (BC_N_Bool[j]==true)
                    *outfile << "N, ";
                
                if (LocalBoundaryCondition[j]==true)
                    *outfile << "L, ";
                else if (LocalBoundaryCondition[j]==false)
                    *outfile << "G, ";
                
                if (BC_D_Bool[j]==true)
                    *outfile << ToString(BC_D[j],8,17,true);
                else if (BC_N_Bool[j]==true)
                    *outfile << ToString(BC_N[j],8,17,true);
                *outfile << endl;
            }
        }
    }
}

void Model::PrintLoading(long i, long total, string StringVal)
{    
    int PrintInc=total/5000+1;
    if (i % PrintInc == 0 && i!= total)
    {
        double Percentage=(double)i/total*100;
        cout << StringVal << fixed << setprecision(1) << Percentage << "%      " << "\r";
    }
    else if (i==total)
    {
        cout << StringVal << "100%             " << endl;
    }
}

string Model::ToString(double Value, int Precision, int SpaceSize, bool Last)
{
    std::stringstream out;
    string TempStr;
    int Len;
    Value=round(Value*1e10)/1e10;
    if (Precision==0)
    {
        out << Value;
        Len=10;
    }
    else
    {
        out.precision(Precision);
        out << std::scientific;
        out << Value;
        Len=SpaceSize;
    }
    TempStr=out.str();
    if (Value >= 0)
    {
        TempStr=" " + TempStr;
    }
    if (Last==false)
    {
        TempStr=TempStr + ",";
    }
    while (TempStr.size()<Len && Last==false)
    {
        TempStr=TempStr + " ";
    }
    return TempStr;
}

string Model::ToString2(double Value, int Precision)
{
    std::stringstream out;
    out << fixed << setprecision(Precision) << Value;
    string TempStr=out.str();
//    if (Value >= 0)
//    {
//        TempStr=" " + TempStr;
//    }
    return TempStr;
}
