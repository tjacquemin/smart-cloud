#include "Model.h"

void Model::PrintGeometryFile()
{
    string InpuFileNam=Path;
    std::stringstream NewFileName;
    string OutputFileName=InpuFileNam.replace(InpuFileNam.end()-5,InpuFileNam.end(),"");
    NewFileName << OutputFileName << "-" << TargetNumNodes << "-" << fixed << setprecision(2)  << ThresholdRatio << "-g.out";
    OutputFileName=NewFileName.str();
    std::ofstream *outfile;
    outfile= new std::ofstream(OutputFileName);
    
    //Print the header
    *outfile << "*HEADING=" << OutputFileName << endl;
    *outfile << "*GEOMETRY_INFORMATION" << endl;
    //Print curve information
    if (Dimension==2)
    {
        *outfile << " GEOM_NUM  GEOM_TYPE   START_X        START_Y        START_Z        END_X          END_Y          END_Z" << endl;
        long CountSurf=0;
        for (int i=0; i<BodiesNumber; i++)
        {
            //Print the edge nodes
            for (int j=0; j<Bodies[i].SurfaceCvNumber; j++)
            {
                *outfile << ToString(CountSurf,0,11,false) << " EDGE,      " << ToString(Bodies[i].SurfaceCv[j].CurveNode[0].X[0],5,15,false) << ToString(Bodies[i].SurfaceCv[j].CurveNode[0].X[1],5,15,false) << ToString(Bodies[i].SurfaceCv[j].CurveNode[0].X[2],5,15,false);
                *outfile << ToString(Bodies[i].SurfaceCv[j].CurveNode[Bodies[i].SurfaceCv[j].NumberNodes-1].X[0],5,15,false) << ToString(Bodies[i].SurfaceCv[j].CurveNode[Bodies[i].SurfaceCv[j].NumberNodes-1].X[1],5,15,false) << ToString(Bodies[i].SurfaceCv[j].CurveNode[Bodies[i].SurfaceCv[j].NumberNodes-1].X[2],5,15,true);        
                *outfile << endl;
                CountSurf++;
            }
        }
        //Print the nodes of the CAD file
        long Count=1;
        *outfile << "*GEOMETRY_NODES" << endl;
        for (int i=0; i<BodiesNumber; i++)
        {
            //Print the corner nodes
            for (int j=0; j<Dimension*Bodies[i].SurfaceCvNumber; j++)
            {
                if (Bodies[i].CornerNodes[j].InDomain==true)
                {
                    *outfile << ToString(Count,0,11,false) << ToString(Bodies[i].CornerNodes[j].X[0],5,15,false) << ToString(Bodies[i].CornerNodes[j].X[1],5,15,false) << ToString(Bodies[i].CornerNodes[j].X[2],5,15,true) << endl;
                    Count++;
                }
            }
//            for (int j=0; j<Bodies[i].SurfaceCvNumber; j++)
//            {
//                *outfile << ToString(Count,0,11,false) << ToString(Bodies[i].SurfaceCv[j].CurveNode[0].X[0],5,15,false) << ToString(Bodies[i].SurfaceCv[j].CurveNode[0].X[1],5,15,false) << ToString(Bodies[i].SurfaceCv[j].CurveNode[0].X[2],5,15,true) << endl;
//                Count++;
//            }
        }
        //Print curve discretization
        Count=1;
        *outfile << "*EDGE_DISCRETIZATION" << endl;
        for (int i=0; i<BodiesNumber; i++)
        {
            //Print the corner nodes
            for (int j=0; j<Dimension*Bodies[i].SurfaceCvNumber; j++)
            {
                if (Bodies[i].CornerNodes[j].InDomain==true)
                {
                    *outfile << ToString(Count,0,11,false) << ToString(Bodies[i].CornerNodes[j].X[0],5,15,false) << ToString(Bodies[i].CornerNodes[j].X[1],5,15,false) << ToString(Bodies[i].CornerNodes[j].X[2],5,15,true) << endl;
                    Count++;
                }
            }
            //Print the edge nodes
            for (int j=0; j<Bodies[i].SurfaceCvNumber; j++)
            {
                for (long k=1; k<Bodies[i].SurfaceCv[j].NumberNodes-1; k++)
                {
                    *outfile << ToString(Count,0,11,false) << ToString(Bodies[i].SurfaceCv[j].CurveNode[k].X[0],5,15,false) << ToString(Bodies[i].SurfaceCv[j].CurveNode[k].X[1],5,15,false) << ToString(Bodies[i].SurfaceCv[j].CurveNode[k].X[2],5,15,true);
                    *outfile << endl;
                    Count++;
                }  
            }
        }
    }
    else if (Dimension==3)
    {
        *outfile << " GEOM_NUM  GEOM_TYPE   AVG_Nx         AVG_Ny         AVG_Nz" << endl;
        long CountSurf=0;
        for (int i=0; i<BodiesNumber; i++)
        {
            for (int j=0; j<Bodies[i].SurfaceFcNumber; j++)
            {
                *outfile << ToString(CountSurf,0,11,false) << " SURFACE,   " << ToString(Bodies[i].SurfaceFc[j].AvgNormal(0),5,15,false) << ToString(Bodies[i].SurfaceFc[j].AvgNormal(1),5,15,false) << ToString(Bodies[i].SurfaceFc[j].AvgNormal(2),5,15,true) << endl;
                CountSurf++;
            }
        }
        
        //Print the surface nodes
        long Count=1;
        CountSurf=0;
        *outfile << "*SURFACE_NODES" << endl;
        for (int i=0; i<BodiesNumber; i++)
        {
            for (int j=0; j<Bodies[i].SurfaceFcNumber; j++)
            {
                *outfile << " SURFACE " << CountSurf << endl;
                CountSurf++;
                for (long k=0; k<Bodies[i].SurfaceFc[j].FaceNodesNumber; k++)
                {
                    *outfile << ToString(Count,0,11,false) << ToString(Bodies[i].SurfaceFc[j].FaceNodes[k].X[0],5,15,false) << ToString(Bodies[i].SurfaceFc[j].FaceNodes[k].X[1],5,15,false) << ToString(Bodies[i].SurfaceFc[j].FaceNodes[k].X[2],5,15,true);
                    *outfile << endl;
                    Count++;
                }
            }
        }
    }
    *outfile << "*END" << endl;
    outfile->close();
}

void Model::PrintGeometryInpFile()
{
    string InpuFileNam=Path;
    std::stringstream NewFileName;
    string OutputFileName=InpuFileNam.replace(InpuFileNam.end()-5,InpuFileNam.end(),"");
    NewFileName << OutputFileName << "-" << TargetNumNodes << "-" << fixed << setprecision(2)  << ThresholdRatio << "-g.inp";
    OutputFileName=NewFileName.str();
    std::ofstream *outfile;
    outfile= new std::ofstream(OutputFileName);
    
    //Print the header
    *outfile << "*HEADING=" << OutputFileName << endl;
    *outfile << "*MODEL_INFORMATION" << endl;
    //Print curve information
    if (Dimension==2)
    {
        long CountSurf=0;
        for (int i=0; i<BodiesNumber; i++)
        {
            for (int j=0; j<Bodies[i].SurfaceCvNumber; j++)
            {
                *outfile << ToString(CountSurf,0,11,false) << " EDGE, O, N, L, 0.0, N, L, 0.0" << endl;
                CountSurf++;
            }
        }
    }
    else if (Dimension==3)
    {
        long CountSurf=0;
        for (int i=0; i<BodiesNumber; i++)
        {
            for (int j=0; j<Bodies[i].SurfaceFcNumber; j++)
            {
                *outfile << ToString(CountSurf,0,11,false) << " SURFACE, O, N, L, 0.0, N, L, 0.0, N, L, 0.0" << endl;
                CountSurf++;
            }
        }
    }
    
    *outfile << "*END" << endl;
    outfile->close();
}

void Model::PrintVTKFile()
{
    string InpuFileNam=Path;
    std::stringstream NewFileName;
    string OutputFileName=InpuFileNam.replace(InpuFileNam.end()-5,InpuFileNam.end(),"");
    NewFileName << OutputFileName << "-" << TargetNumNodes << "-" << fixed << setprecision(2)  << ThresholdRatio << ".vtk";
    OutputFileName=NewFileName.str();
    std::ofstream *outfile;
    outfile= new std::ofstream(OutputFileName);
    
    //Print the header
    
    *outfile << "# vtk DataFile Version 2.0" << endl;
    *outfile << OutputFileName << endl;
    *outfile << "ASCII" << endl;
    *outfile << endl;
    *outfile << "DATASET UNSTRUCTURED_GRID" << endl;
    
    //Print curve information
    if (Dimension==2)
    {
        //Count the number of points
        long Count=0;
        //Count the corner nodes
        for (int i=0; i<BodiesNumber; i++)
        {
            for (int j=0; j<Dimension*Bodies[i].SurfaceCvNumber; j++)
            {
                if (Bodies[i].CornerNodes[j].InDomain==true)
                {
                    Count++;
                }
            }
        }
        //Count the edge nodes
        for (int i=0; i<BodiesNumber; i++)
        {
            for (int j=0; j<Bodies[i].SurfaceCvNumber; j++)
            {
                Count+=Bodies[i].SurfaceCv[j].NumberNodes-2;
            }
        }
        //Count the box nodes
        for (int i=0; i<BodiesNumber; i++)
        {
            for (long j=0; j<Bodies[i].BoxNodeNumber; j++)
            {
                if (Bodies[i].BoxNode[j].InDomain==true)
                {
                    Count++;
                }
            }
        }
        *outfile << "POINTS " << ToString2(Count,0) << " float" << endl;
        //Print the corner nodes
        for (int i=0; i<BodiesNumber; i++)
        {
            for (int j=0; j<Dimension*Bodies[i].SurfaceCvNumber; j++)
            {
                if (Bodies[i].CornerNodes[j].InDomain==true)
                {
                    *outfile << "  " << ToString2(Bodies[i].CornerNodes[j].X[0],5) << " "  << ToString2(Bodies[i].CornerNodes[j].X[1],5) << " "  << ToString2(Bodies[i].CornerNodes[j].X[2],5) << endl;
                }
            }
        }
        //Print the edge nodes
        for (int i=0; i<BodiesNumber; i++)
        {
            for (int j=0; j<Bodies[i].SurfaceCvNumber; j++)
            {
                for (long k=1; k<Bodies[i].SurfaceCv[j].NumberNodes-1; k++)
                {
                    *outfile << "  " << ToString2(Bodies[i].SurfaceCv[j].CurveNode[k].X[0],5) << " "  << ToString2(Bodies[i].SurfaceCv[j].CurveNode[k].X[1],5) << " "  << ToString2(Bodies[i].SurfaceCv[j].CurveNode[k].X[2],5) << endl;
                }
            }
        }
        //Print the box nodes
        for (int i=0; i<BodiesNumber; i++)
        {
            for (long j=0; j<Bodies[i].BoxNodeNumber; j++)
            {
                if (Bodies[i].BoxNode[j].InDomain==true)
                {
                    *outfile << "  " << ToString2(Bodies[i].BoxNode[j].X[0],5) << " "  << ToString2(Bodies[i].BoxNode[j].X[1],5) << " "  << ToString2(Bodies[i].BoxNode[j].X[2],5) << endl;
                }
            }
        }
    }
    else if (Dimension==3)
    {
        //Count the number of points
        long Count=0;
        for (int i=0; i<TotalFaceNumber; i++)
        {
            int SurfIndex=i;
            if (HasSurfPrintOrder==true)
                SurfIndex=SurfPrintOrderReverse[i];
            for (long j=0; j<SurfaceFc[SurfIndex].FaceNodesNumber; j++)
            {
                if (SurfaceFc[SurfIndex].FaceNodes[j].SurfNodeInDomain==true)
                {
                    Count++;
                }
            }
        }
        for (int i=0; i<BodiesNumber; i++)
        {
            for (long j=0; j<Bodies[i].BoxNodeNumber; j++)
            {
                if (Bodies[i].BoxNode[j].InDomain==true)
                {
                    Count++;
                }
            }
        }
        *outfile << "POINTS " << ToString2(Count,0) << " float" << endl;
        
        //Print the face nodes
        for (int i=0; i<TotalFaceNumber; i++)
        {
            int SurfIndex=i;
            if (HasSurfPrintOrder==true)
                SurfIndex=SurfPrintOrderReverse[i];
            for (long j=0; j<SurfaceFc[SurfIndex].FaceNodesNumber; j++)
            {
                if (SurfaceFc[SurfIndex].FaceNodes[j].SurfNodeInDomain==true)
                {
                    *outfile << "  " << ToString2(SurfaceFc[SurfIndex].FaceNodes[j].X[0],5) << " "  << ToString2(SurfaceFc[SurfIndex].FaceNodes[j].X[1],5) << " "  << ToString2(SurfaceFc[SurfIndex].FaceNodes[j].X[2],5) << endl;
                }
            }
        }
        
        //Print the box nodes
        for (int i=0; i<BodiesNumber; i++)
        {
            for (long j=0; j<Bodies[i].BoxNodeNumber; j++)
            {
                if (Bodies[i].BoxNode[j].InDomain==true)
                {
                    *outfile << "  " << ToString2(Bodies[i].BoxNode[j].X[0],5) << " "  << ToString2(Bodies[i].BoxNode[j].X[1],5) << " "  << ToString2(Bodies[i].BoxNode[j].X[2],5) << endl;
                }
            }
        }
    }
    *outfile << endl;
    
    //Print the surface elements
    //Count face elements
    long CountEl=0;
    for (int i=0; i<TotalFaceNumber; i++)
    {
        CountEl+=SurfaceFc[i].InnerElementsNumber;
    }
    *outfile << "CELLS " << CountEl << " " << CountEl*4 << endl;

    for (int i=0; i<TotalFaceNumber; i++)
    {
        for (long j=0; j<SurfaceFc[i].InnerElementsNumber; j++)
        {
            *outfile << "3 " <<  SurfaceFc[i].FaceInnerElements[j].ElNode[0]->GlobalIndex[0]-1 << " " <<  SurfaceFc[i].FaceInnerElements[j].ElNode[1]->GlobalIndex[0]-1 << " ";
            *outfile <<  SurfaceFc[i].FaceInnerElements[j].ElNode[2]->GlobalIndex[0]-1 << " "<< endl;
        }
    }
    *outfile << "CELL_TYPES " << CountEl << endl;
    for (long j=0; j<CountEl; j++)
    {
        *outfile << "5" << endl;
    }
    outfile->close();
}