#include <vector>

#include "Node.h"

Node::Node() {
}

Node::Node(const Node& orig) {
}

Node::~Node() {
}

void Node::Initialize(int Dim)
{
    Dimension=Dim;
    //The node dimension is 3 for both 2D and 3D problems
    X = new double[3];
    AttachedElements.clear();
    GlobalIndex = new long[1];
    GlobalIndex[0]=-1;
    RefEdge.clear();
}

void Node::InitializeNormal(Vector3d CurveTangent)
{
    CurveTangent.normalize();
    Normal = new double[3];
    if (Dimension==2)
    {
        Normal[0]=CurveTangent(1);
        Normal[1]=-CurveTangent(0);
        Normal[2]=0;
    }
}

void Node::InitializeNormal3D(Vector3d TangentU, Vector3d TangentV)
{
    TangentU.normalize();
    TangentV.normalize();
    Vector3d TempNormal=TangentU.cross(TangentV);
    Normal = new double[3];
    Normal[0]=TempNormal(0);
    Normal[1]=TempNormal(1);
    Normal[2]=TempNormal(2);
    GlobalIndex = new long[1];
    GlobalIndex[0]=-1;
}