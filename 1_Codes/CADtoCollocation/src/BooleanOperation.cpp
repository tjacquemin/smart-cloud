#include "BooleanOperation.h"

BooleanOperation::BooleanOperation() {
}

BooleanOperation::BooleanOperation(const BooleanOperation& orig) {
}

BooleanOperation::~BooleanOperation() {
}

void BooleanOperation::GetInnerNodesFromCAD_2D(Node* BoxNode, long BoxNodeNumber, TopoDS_Face Face)
{
    //Load the KD tree
    LoadKDTree(BoxNode,BoxNodeNumber);
    
    //Load all the edges of the face
    TopTools_IndexedMapOfShape edgesAll;
    TopExp::MapShapes(Face,TopAbs_EDGE,edgesAll);
    EdgesNumber=edgesAll.Extent();
    EdgeLoc=new TopoDS_Edge[EdgesNumber];
    for(Standard_Integer i=0; i<EdgesNumber; i++)
    {
        //Get the geometry of the edge
        EdgeLoc[i] = TopoDS::Edge(edgesAll.FindKey(i+1));
    }
    
    //Test which nodes are in the domain
    //Loop through all the shapes
    for (long i=0; i<BoxNodeNumber; i++)
    {
        if (BoxNode[i].InclusionIdentified==false)
        {
            BoxNode[i].InDomain=NodeInCAD_2D(BoxNode,i,Face,1);
        }
        PrintLoading(i,BoxNodeNumber," --> Getting nodes locations   ");
    }
    cout << " --> Getting nodes locations   100%             " << endl;
}

void BooleanOperation::GetInnerNodesFromCAD_3D(Node* BoxNode, long BoxNodeNumber, double h)
{
    //Load the KD tree
    LoadKDTree(BoxNode,BoxNodeNumber);
    
    //Set the body
    ShapeCAD = new BRepClass3d_SolidClassifier(Shape3D);
    
    //Test which nodes are in the domain
    //Loop through all the shapes
    for (long i=0; i<BoxNodeNumber; i++)
    {
        if (BoxNode[i].InclusionIdentified==false)
        {
            BoxNode[i].InDomain=NodeInCAD_3D(BoxNode,i,h);
        }
        PrintLoading(i,BoxNodeNumber," --> Getting nodes locations   ");
    }
    cout << " --> Getting nodes locations   100%             " << endl;
}

void BooleanOperation::GetInnerNodesFromBC_2D(SurfaceCurve* SurfaceCv, int SurfaceCvNumber, TopoDS_Face Face, double h, Node* BoxNode, long BoxNodeNumber)
{
    //Load the KD tree of the internal nodes
    LoadKDTree(BoxNode,BoxNodeNumber);
    
    //Load the KD tree of the boundary nodes
    LoadKDTreeBound2D(SurfaceCv,SurfaceCvNumber);
    
    //Test which nodes are in the domain
    //Loop through all the shapes
    for (long i=0; i<BoxNodeNumber; i++)
    {
        if (BoxNode[i].InclusionIdentified==false)// || BoxNode[i].RelaxedNode==true)
        {
            //Get the closest boundary node to the considered box node
            point_t PtQuery = {BoxNode[i].X[0],BoxNode[i].X[1],BoxNode[i].X[2]};
            long NearestNodeIndex=KDtreeBound->nearest_index(PtQuery);
            long i1=IndexNode2D[NearestNodeIndex][0];
            long j1=IndexNode2D[NearestNodeIndex][1];
            Vector3d V1;
            V1(0)=SurfaceCv[i1].CurveNode[j1].X[0]-BoxNode[i].X[0];
            V1(1)=SurfaceCv[i1].CurveNode[j1].X[1]-BoxNode[i].X[1];
            V1(2)=SurfaceCv[i1].CurveNode[j1].X[2]-BoxNode[i].X[2];
            double Dist=pow(pow(V1(0),2)+pow(V1(1),2)+pow(V1(2),2),0.5);
            
            double FactorOnDist=0.9;
            bool NodeInDom=false;
            //Use the CAD file for the nodes close to the boundary
            if (Dist<1.2*h && BCTolerance==1)
            {
                NodeInDom=NodeInCAD_2D(BoxNode,i,Face,1);
                BoxNode[i].InDomain=NodeInDom;
                BoxNode[i].InclusionIdentified=true;
            }
            else
            {
                //Test the normal of each adjacent elements
                int NodeInDomInt=-1;
                for (int j=0; j<SurfaceCv[i1].CurveNode[j1].AttachedElements.size(); j++)
                {
                    Vector3d V2;
                    long CurveNo=SurfaceCv[i1].CurveNode[j1].AttachedElements[j][0];
                    long ElemNo=SurfaceCv[i1].CurveNode[j1].AttachedElements[j][1];
                    V2(0)=SurfaceCv[CurveNo].CurveElement[ElemNo].Normal[0];
                    V2(1)=SurfaceCv[CurveNo].CurveElement[ElemNo].Normal[1];
                    V2(2)=SurfaceCv[CurveNo].CurveElement[ElemNo].Normal[2];
                    V1.normalize();

                    //Get the cross product of V1 and V2
                    double VectProd=V1.dot(V2);
                    if (VectProd>0 && NodeInDomInt==-1)
                    {
                        NodeInDomInt=1;
                    }
                    else if (VectProd<=0 && NodeInDomInt==-1)
                    {
                        NodeInDomInt=0;
                    }
                    else if ((VectProd<=0 && NodeInDomInt==1) || (VectProd>0 && NodeInDomInt==0))
                    {
                        NodeInDomInt=-2;
                    }
                }
                if (NodeInDomInt==1)
                    NodeInDom=true;
                else if (NodeInDomInt==0)
                    NodeInDom=false;
                else if (NodeInDomInt==-2)
                {
                    NodeInDom=NodeInCAD_2D(BoxNode,i,Face,1);
                }
                BoxNode[i].InDomain=NodeInDom;
                BoxNode[i].InclusionIdentified=true;
                //Identify the box nodes close to the boundary
                auto QueryResult = KDtree->neighborhood_indices(PtQuery,Dist*FactorOnDist);
                //Identify the box nodes close to the boundary
                for (int j=0; j<QueryResult.size(); j++)
                {
                    BoxNode[QueryResult[j]].InDomain=NodeInDom;
                    BoxNode[QueryResult[j]].InclusionIdentified=true;
                }
            }
        }
        PrintLoading(i,BoxNodeNumber," --> Getting nodes locations   ");
    }
}

void BooleanOperation::LoadKDTree(Node* BoxNode, long BoxNodeNumber)
{
    //Create the vectors
    pointVec pointsAll(BoxNodeNumber);
    long* PtIndex=NULL;
    PtIndex=new long[BoxNodeNumber];
    
    //Fill the vectors with the BoxNodes in the domain
    for (long i=0; i<BoxNodeNumber; ++i)
    {
        pointsAll[i]=point_t({BoxNode[i].X[0],BoxNode[i].X[1],BoxNode[i].X[2]});
        PtIndex[i]=0;
    }
    //Load the KD tree
    KDtree = new KDTree(pointsAll);
}

void BooleanOperation::LoadKDTreeBound2D(SurfaceCurve* SurfaceCv, int SurfaceCvNumber)
{
    long Count=0;
    for (int i=0; i<SurfaceCvNumber; i++)
    {
        Count+=SurfaceCv[i].NumberNodes-1;
    }
    TotalBoundaryNodesNumber=Count;
    IndexNode2D.clear();
    IndexNode2D.resize(TotalBoundaryNodesNumber);
    
    //Create the vectors
    pointVec pointsAll(TotalBoundaryNodesNumber);
    
    //Fill the vectors with the boundary nodes
    Count=0;
    for (int i=0; i<SurfaceCvNumber; i++)
    {
        for (long j=0; j<SurfaceCv[i].NumberNodes-1; j++)
        {
            pointsAll[Count]=point_t({SurfaceCv[i].CurveNode[j].X[0],SurfaceCv[i].CurveNode[j].X[1],SurfaceCv[i].CurveNode[j].X[2]});
            IndexNode2D[Count].resize(2);
            IndexNode2D[Count][0]=i;
            IndexNode2D[Count][1]=j;
            Count++;
        }
    }
    
    //Load the KD tree
    KDtreeBound = new KDTree(pointsAll);
}

bool BooleanOperation::NodeInCAD_2D(Node* BoxNode, long BoxNodeIndex, TopoDS_Face Face, int Set)
{
    TopoDS_Vertex PntInnerVertex;
    bool NodeInDom=false;
    gp_Pnt PntInner(BoxNode[BoxNodeIndex].X[0],BoxNode[BoxNodeIndex].X[1],BoxNode[BoxNodeIndex].X[2]);
    PntInnerVertex=BRepBuilderAPI_MakeVertex(PntInner);
    BRepExtrema_DistShapeShape dst(PntInnerVertex, Face);
    dst.Perform();
    if (dst.IsDone())
    {
        if (dst.Value()==0)
            NodeInDom=true;
        else if (NodeInDom==false)
            NodeInDom=false;
    }
    
    if (Set==1)
    {
        //Get the closes distance to the boundary of the domain
        double Dist=0;
        for (int i=0; i<EdgesNumber; i++)
        {
            BRepExtrema_DistShapeShape dst(PntInnerVertex,EdgeLoc[i]);
            double TempDist=dst.Value();
            if (i==0)
            {
                Dist=TempDist;
            }
            else if (TempDist<Dist)
            {
                Dist=TempDist;
            }
        }

        //Identify all the points with the same position in the domain as the considered node
        point_t PtQuery;
        PtQuery = {BoxNode[BoxNodeIndex].X[0],BoxNode[BoxNodeIndex].X[1],BoxNode[BoxNodeIndex].X[2]};
        double FactorOnDist=0.9;
        auto QueryResult = KDtree->neighborhood_indices(PtQuery,Dist*FactorOnDist);
        //Identify the box nodes close to the boundary
        for (int i=0; i<QueryResult.size(); i++)
        {
            BoxNode[QueryResult[i]].InDomain=NodeInDom;
            BoxNode[QueryResult[i]].InclusionIdentified=true;
        }
    }
    return NodeInDom;
}

void BooleanOperation::LoadSurfaceElements(Element* SurfaceFcEl, long FaceElementNum)
{
    for (long i=0; i<FaceElementNum; i++)
    {
        SurfaceElements[Count]=SurfaceFcEl[i];
        Count++;
    }
    NumberSurfElements=Count;
}

bool BooleanOperation::NodeInCAD_3D(Node* BoxNode, long BoxNodeIndex, double h)
{
    TopoDS_Vertex PntInnerVertex;
    bool NodeInDom=false;
    gp_Pnt PntInner(BoxNode[BoxNodeIndex].X[0],BoxNode[BoxNodeIndex].X[1],BoxNode[BoxNodeIndex].X[2]);
    PntInnerVertex=BRepBuilderAPI_MakeVertex(PntInner);
    ShapeCAD->Perform(PntInner, h/1000);
    if (ShapeCAD->State() == TopAbs_IN)
    {
        NodeInDom=true;
    }
    else
    {
        NodeInDom=false;
    }
    
    BRepExtrema_DistShapeShape dst(PntInnerVertex, Shape3D);
    dst.Perform();
    double Dist=dst.Value();
    
    if (Dist<h/2)
    {
        NodeInDom=false;
    }
    
    //Identify all the points with the same position in the domain as the considered node
    point_t PtQuery;
    PtQuery = {BoxNode[BoxNodeIndex].X[0],BoxNode[BoxNodeIndex].X[1],BoxNode[BoxNodeIndex].X[2]};
    double FactorOnDist=0.9;
    auto QueryResult = KDtree->neighborhood_indices(PtQuery,Dist*FactorOnDist);
    //Identify the box nodes close to the boundary
    for (int i=0; i<QueryResult.size(); i++)
    {
        BoxNode[QueryResult[i]].InDomain=NodeInDom;
        BoxNode[QueryResult[i]].InclusionIdentified=true;
    }
    return NodeInDom;
}

double BooleanOperation::isLeft(double X0, double Y0, double X1, double Y1, double X2, double Y2)
{
    return ( (X1 - X0) * (Y2 - Y0)
           - (X2 - X0) * (Y1 - Y0) );
}

void BooleanOperation::PrintLoading(long i, long total, string StringVal)
{    
    int PrintInc=total/5000+1;
    if (i % PrintInc == 0 && i!= total)
    {
        double Percentage=(double)i/total*100;
        cout << StringVal << fixed << setprecision(1) << Percentage << "%      " << "\r";
    }
}
