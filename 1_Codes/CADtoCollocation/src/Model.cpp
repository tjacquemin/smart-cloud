#include "Model.h"

Model::Model() {
}

Model::Model(const Model& orig) {
}

Model::~Model(){
}

void Model::LoadStepFile()
{
    //Create a STEP file reader and loads the STEP file
    STEPControl_Reader reader;
    reader.ReadFile(Path);
    // Get the number of transferable roots
    Standard_Integer NbRoots = reader.NbRootsForTransfer();
    // Translates all transferable roots, and returns the number of successful translations
    Standard_Integer NbTrans = reader.TransferRoots();
    //Get the shape from the file
    TopoShape = reader.OneShape();
}

void Model::GetModelBox()
{
    //Compute the parameters of the bounding box
    Bnd_Box B;
    BRepBndLib::Add(TopoShape, B);
    B.Get(MinX, MinY, MinZ, MaxX, MaxY, MaxZ); 
    //Reduce the size of the box by h on each side
    h_reduc=h/2*pow(3,0.5);
    MinX=MinX+h_reduc;
    MinY=MinY+h_reduc;
    MinZ=MinZ+h_reduc;
    MaxX=MaxX-h_reduc;
    MaxY=MaxY-h_reduc;
    MaxZ=MaxZ-h_reduc;
}

void Model::LoadBodies()
{
    //Determine for 3D models if the CAD model inculdes a solit shape
    if (Dimension==3)
        ModelHasSolid=HasSolid();
    
    BodiesNumber=0;
    int i=0;
    //List the shapes of the CAD file and count the number of bodies
    TopoDS_Iterator itor1(TopoShape);
    cout << " --> The identified shapes are: " << endl;
    while (itor1.More())
    {
        //Get the shape from the iterator
        TopoDS_Shape shape=itor1.Value();
        TopAbs_ShapeEnum type = shape.ShapeType();
        
        //Print the shapes of the CAD file in the terminal
        cout << "     Shape " << i;
        if (type==TopAbs_COMPOUND)
           cout << " : Compound" << endl;
        else if (type==TopAbs_COMPSOLID)
           cout << " : CompSolid" << endl;
        else if (type==TopAbs_SOLID)
           cout << " : Solid" << endl;
        else if (type==TopAbs_SHELL)
           cout << " : Shell" << endl;
        else if (type==TopAbs_FACE)
           cout << " : Face" << endl;
        else if (type==TopAbs_WIRE)
           cout << " : Wire" << endl;
        else if (type==TopAbs_EDGE)
           cout << " : Edge" << endl;
        else if (type==TopAbs_VERTEX)
           cout << " : Vertex" << endl;
        else if (type==TopAbs_SHAPE)
           cout << " : Shape" << endl;
        
        //For the SHELL shape since the model is a 2D model
        if (((type==TopAbs_SHELL || type==TopAbs_FACE) && Dimension==2))
        {
            //Get all faces from shape
            TopTools_IndexedMapOfShape facesAll;
            TopExp::MapShapes(shape, TopAbs_FACE, facesAll);
            BodiesNumber+=facesAll.Extent();
        }
        else if (Dimension==3 && ((type==TopAbs_SHELL && ModelHasSolid==false) || (type==TopAbs_SOLID && ModelHasSolid==true)))
        {
            BodiesNumber++;
        }
        itor1.Next();
        i++;
    }
    
    //Create and load the bodies from the CAD file
    Bodies=new Body[BodiesNumber];
    int Count=0;
    TopoDS_Iterator itor2(TopoShape);
    while (itor2.More())
    {
        //Get the shape from the iterator
        TopoDS_Shape shape=itor2.Value();
        TopAbs_ShapeEnum type = shape.ShapeType();
        //For the SHELL shape since the model is a 2D model
        if (((type==TopAbs_SHELL || type==TopAbs_FACE) && Dimension==2))
        {
            //Get all faces from shape
            TopTools_IndexedMapOfShape facesAll;
            TopExp::MapShapes(shape, TopAbs_FACE, facesAll);
            for(int j=1; j<=facesAll.Extent(); j++)
            {
                Bodies[Count].Set2DBody(TopoDS::Face(facesAll.FindKey(j)));
                Count++;
            }
        }
        else if (Dimension==3 && ((type==TopAbs_SHELL && ModelHasSolid==false) || (type==TopAbs_SOLID && ModelHasSolid==true)))
        {
            Bodies[Count].Set3DBody(shape);
            Count++;
        }
        itor2.Next();
    }
}

void Model::LoadBodiesParameters()
{
    for (int i=0; i<BodiesNumber; i++)
    {
        Bodies[i].ThresholdRatio=ThresholdRatio;
        Bodies[i].HasSpikes=HasSpikes;
        Bodies[i].BCTolerance=BCTolerance;
        Bodies[i].Relaxation=Relaxation;
    }
}

bool Model::HasSolid()
{
    TopoDS_Iterator itor1(TopoShape);
    while (itor1.More())
    {
        TopoDS_Shape shape=itor1.Value();
        TopAbs_ShapeEnum type = shape.ShapeType();
        if (type==TopAbs_SOLID)
        {
            return true;
            break;
        }
        itor1.Next();
    }
    return false;
}

void Model::GetModelVolume()
{
    AreaVol=0;
    for (int i=0; i<BodiesNumber; i++)
    {
        AreaVol+=Bodies[i].GetVolume();
    }
    //Get an approximation of the value of h
    h=pow(AreaVol/pow(pow(TargetNumNodes,(float)1/(float)Dimension)-1,Dimension),(float)1/(float)Dimension);
}

void Model::LoadBoundaries()
{
    for (int i=0; i<BodiesNumber; i++)
    {
        Bodies[i].LoadBoundaries();
    }
}

void Model::DiscretizeBoundaries()
{
    for (int i=0; i<BodiesNumber; i++)
    {
        Bodies[i].DiscretizeBoundaries(h);
    }
}

void Model::DiscretizeDomain()
{
    for (int i=0; i<BodiesNumber; i++)
    {
        Bodies[i].InnerDiscretizationType=InnerDiscretizationType;
        Bodies[i].DiscretizeBody(BooleanOperationType);
    }
}

void Model::LoadGlobalGeomArray()
{
    TotalSurfaceCvNumber=0;
    TotalClosedCvNum=0;
    TotalFaceNumber=0;
    //Count the number of surface curves or faces
    for (int i=0; i<BodiesNumber; i++)
    {
        if (Dimension==2)
        {
            TotalSurfaceCvNumber+=Bodies[i].SurfaceCvNumber;
            TotalClosedCvNum+=Bodies[i].ClosedCvNum;
        }
        else if (Dimension==3)
            TotalFaceNumber+=Bodies[i].SurfaceFcNumber;
    }
    //Create the global geometry arrays
    if (Dimension==2)
    {
        SurfaceCv=new SurfaceCurve[TotalSurfaceCvNumber];
    }
    else if (Dimension==3)
        SurfaceFc=new SurfaceFace[TotalFaceNumber];
    //Load the geometry arrays
    int Count=0;
    int Count2=0;
    for (int i=0; i<BodiesNumber; i++)
    {
        if (Dimension==2)
        {
            for (int j=0; j<Bodies[i].SurfaceCvNumber; j++)
            {
                SurfaceCv[Count]=Bodies[i].SurfaceCv[j];
                Bodies[i].SurfaceCv[j].GlobalCvNum=Count;
                Count++;
            }
        }
        else if (Dimension==3)
        {
            for (int j=0; j<Bodies[i].SurfaceFcNumber; j++)
            {
                SurfaceFc[Count]=Bodies[i].SurfaceFc[j];
                Count++;
            }
        }
    }
}

void Model::RemoveDuplicatedNodes()
{
//    //Count the number of nodes in the domain
//    long Count=0;
//    for (int i=0; i<BodiesNumber; i++)
//    {
//        for (long j=0; j<Bodies[i].BoxNodeNumber; j++)
//        {
//            if (Bodies[i].BoxNode[j].InDomain==true)
//            {
//                Count++;
//            }
//        }
//    }
//    
//    //Create the vectors
//    long PointsSize=Count;
//    pointVec points(Count);
//    vector<vector<long>> PtIndex;
//    PtIndex.clear();
//    PtIndex.resize(Count);
//    Count=0;
//    
//    //Fill the rtree with the BoxNodes in the domain
//    for (int i=0; i<BodiesNumber; i++)
//    {
//        for (long j=0; j<Bodies[i].BoxNodeNumber; j++)
//        {
//            if (Bodies[i].BoxNode[j].InDomain==true)
//            {
//                points[Count]=point_t({Bodies[i].BoxNode[j].X[0],Bodies[i].BoxNode[j].X[1],Bodies[i].BoxNode[j].X[2]});
//                PtIndex[Count].resize(2);
//                PtIndex[Count][0]=i;
//                PtIndex[Count][1]=j;
//                Count++;
//                PrintLoading(Count-1,PointsSize," --> Loading the tree          ");
//            }
//        }
//    }
//    cout << " --> Loading the tree          100%             " << endl;
//    
//    //Load the KD tree
//    tree = new KDTree(points);
//    
//    //Set the threshold distance
//    double hmin=h*ThresholdRatio;
//    
//    if (Dimension==2)
//    {
//        //Remove the duplicates in the Vertex and Edge sets
//        RemoveDuplicatedNodesInEdges(PtIndex,hmin);
//    }
//    else if (Dimension==3)
//    {
//        //Remove the duplicates in the faces
//        RemoveDuplicatedNodesInFaces(PtIndex,hmin);
//    }
    
    cout << " --> Duplicated nodes removed  100%             " << endl;
}

void Model::RemoveDuplicatedNodesInFaces(vector<vector<long>> PtIndex, double hmin)
{
    //Define the queried point
    point_t PtQuery;
    //Loop through the bodies
    for (int i=0; i<BodiesNumber; i++)
    {
        //Loop through all the faces
        for (int j=0; j<Bodies[i].SurfaceFcNumber; j++)
        {
            for (long k=0; k<Bodies[i].SurfaceFc[j].FaceNodesNumber; k++)
            {
                //List the box nodes within a radius hmin of the boundary node
                PtQuery = {Bodies[i].SurfaceFc[j].FaceNodes[k].X[0],Bodies[i].SurfaceFc[j].FaceNodes[k].X[1],Bodies[i].SurfaceFc[j].FaceNodes[k].X[2]};
                auto QueryResult = tree->neighborhood_indices(PtQuery,hmin);
                for (int j=0; j<QueryResult.size(); j++)
                {
                    int BodyNum=PtIndex[QueryResult[j]][0];
                    long BoxNum=PtIndex[QueryResult[j]][1];
                    Bodies[BodyNum].BoxNode[BoxNum].InDomain=false;
                }
            }
        }
    }
}