#include "SurfaceCurve.h"

SurfaceCurve::SurfaceCurve() {
}

SurfaceCurve::SurfaceCurve(const SurfaceCurve& orig) {
}

SurfaceCurve::~SurfaceCurve()
{
    if (BCArraysCreated==true)
    {
        delete[] LocalBoundaryCondition;
        delete[] BC_D_Bool;
        delete[] BC_N_Bool;
        delete[] BC_D;
        delete[] BC_N;
        delete[]  BC_DOF;
    }
}

void SurfaceCurve::Discretize(double h)
{
    Standard_Real portionLength = GCPnts_AbscissaPoint::Length(GeomAdaptor_Curve(SurfaceGeomCurve), CvFirst, CvLast);
    NumberElements=portionLength/h;
    //Check that the calculated number of elements is larger than 1
    if (NumberElements<1){
        NumberElements=1;}
    
    NumberNodes=NumberElements+1;
    CurveElement=new Element[NumberElements];
    CurveNode=new Node[NumberNodes];
    
    //Load the curve nodes and elements
    for (Standard_Integer i=0; i<NumberNodes; i++)
    {
        gp_Pnt LocPoint;
        gp_Vec CurveTangent;
        //Get a uniform discretization of the curve
        GCPnts_AbscissaPoint NewAbscissaPoint(GeomAdaptor_Curve(SurfaceGeomCurve),(double)portionLength/(double)NumberElements*(double)i,CvFirst);
        //Get the point coordinates and the tangent vector
        SurfaceGeomCurve->D1(NewAbscissaPoint.Parameter(),LocPoint,CurveTangent);
        //Intiialize the node and the normal
        CurveNode[i].Initialize(Dimension);
        Vector3d CurveTangentTemp;
        for (int j=0; j<3; j++)
        {
            CurveTangentTemp(j)=CurveTangent.Coord(j+1);
        }
        CurveNode[i].InitializeNormal(CurveTangentTemp);
        CurveNode[i].X[0]=LocPoint.Coord().X();
        CurveNode[i].X[1]=LocPoint.Coord().Y();
        CurveNode[i].X[2]=LocPoint.Coord().Z();
        if (i>0)
        {
            CurveElement[i-1].Initialize(Dimension);
            CurveElement[i-1].ElNode[0]=&CurveNode[i-1];
            CurveElement[i-1].ElNode[1]=&CurveNode[i];
        }
    }
}

void SurfaceCurve::FlipCurve()
{
    //Flip the connections to the other curves
    long TempIndex=NextCvIndex;
    NextCvIndex=PrevCvIndex;
    PrevCvIndex=TempIndex;
    
    //Flip the nodes
    Node* TempNode;
    TempNode=new Node[NumberNodes];
    for (long i=0; i<NumberNodes/2; i++)
    {
        TempNode[i]=CurveNode[i];
        CurveNode[i]=CurveNode[NumberNodes-i-1];
        CurveNode[NumberNodes-i-1]=TempNode[i];
    }
    delete[] TempNode;
    
    //Flip the elements
    for (long i=0; i<NumberElements; i++)
    {
        CurveElement[i].ElNode[0]=&CurveNode[i];
        CurveElement[i].ElNode[1]=&CurveNode[i+1];
    }
}

double SurfaceCurve::GetCurveCumulEdges()
{
    //Define and initialize the cumulative cross product vector
    double Cumul=0;
    for (long i=0; i<NumberElements; i++)
    {
        //Set the cumul value. The elements need to be in the XY plane
        Cumul=(CurveElement[i].ElNode[1]->X[0]-CurveElement[i].ElNode[0]->X[0])*(CurveElement[i].ElNode[1]->X[1]+CurveElement[i].ElNode[0]->X[1]);
    }
    return Cumul;
}

void SurfaceCurve::SetNormalDirections(int Dir, bool OuterCv)
{
    Vector3d VElement, VNormal;
    VElement.resize(3,1);
    VNormal.resize(3,1);
    for (long i=0; i<NumberElements; i++)
    {
        //Set the element vector
        VElement(0)=CurveElement[i].ElNode[1]->X[0]-CurveElement[i].ElNode[0]->X[0];
        VElement(1)=CurveElement[i].ElNode[1]->X[1]-CurveElement[i].ElNode[0]->X[1];
        VElement(2)=CurveElement[i].ElNode[1]->X[2]-CurveElement[i].ElNode[0]->X[2];
        VElement.normalize();
        
        //For the start node of the element only except for the last element
        int jLast=1;
        if (i==NumberElements-1)
            jLast=2;
        for (int j=0; j<jLast; j++)
        {
            VNormal(0)=CurveElement[i].ElNode[j]->Normal[0];
            VNormal(1)=CurveElement[i].ElNode[j]->Normal[1];
            VNormal(2)=CurveElement[i].ElNode[j]->Normal[2];
            //Flip the normal if the dot product is negative
            bool FlipNormal=false;
            double CrossProd=VElement.cross(VNormal)(2);
            
            //If Dir==1
            if (CrossProd>0 && Dir==1 && OuterCv==true)
            {
                FlipNormal=false;
            }
            else if (CrossProd<0 && Dir==1 && OuterCv==true)
            {
                FlipNormal=true;
            }
            else if (CrossProd>0 && Dir==1 && OuterCv==false)
            {
                FlipNormal=true;
            }
            else if (CrossProd<0 && Dir==1 && OuterCv==false)
            {
                FlipNormal=false;
            }
            //If Dir==-1
            else if (CrossProd>0 && Dir==-1 && OuterCv==true)
            {
                FlipNormal=true;
            }
            else if (CrossProd<0 && Dir==-1 && OuterCv==true)
            {
                FlipNormal=false;
            }
            else if (CrossProd>0 && Dir==-1 && OuterCv==false)
            {
                FlipNormal=false;
            }
            else if (CrossProd<0 && Dir==-1 && OuterCv==false)
            {
                FlipNormal=true;
            }
            if (FlipNormal==true)
            {
                for (int k=0; k<3; k++)
                {
                    CurveElement[i].ElNode[j]->Normal[k]=-CurveElement[i].ElNode[j]->Normal[k];
                }
            }
        }
    }
}

void SurfaceCurve::LoadBoundaryProperties(vector<string> BoundProp)
{
    //Set the various values
    CurveType=BoundProp[1];
    if (CurveType=="EDGE")
    {
        Dimension=2;
    }
    
    //Create the various arrays of the surface
    BCArraysCreated = true;
    LocalBoundaryCondition = new bool[Dimension];
    BC_D_Bool = new bool[Dimension];
    BC_N_Bool = new bool[Dimension];
    BC_D = new float[Dimension];
    BC_N = new float[Dimension];
    BC_DOF = new int[Dimension];
    
    if (BoundProp[2]=="I")
    {
        InnerCurve=true;
    }
    else
    {
        InnerCurve=false;
    }
    for (int i=0; i<Dimension; i++)
    {
        if (BoundProp[3+3*i]=="D")
        {
            BC_D_Bool[i]=true;
            BC_N_Bool[i]=false;
        }
        else
        {
            BC_D_Bool[i]=false;
            BC_N_Bool[i]=true;
        }
        if (BoundProp[4+3*i]=="L")
        {
            LocalBoundaryCondition[i]=true;
        }
        else
        {
            LocalBoundaryCondition[i]=false;
        }
        if (BC_D_Bool[i]==true)
        {
            BC_D[i]=stof(BoundProp[5+3*i]);
            BC_N[i]=-999;
        }
        else if (BC_N_Bool[i]==true)
        {
            BC_D[i]=-999;
            BC_N[i]=stof(BoundProp[5+3*i]);
        }
        //cout << CurveType << ", " << InnerCurve << ", " << BC_D_Bool[i] << ", " << LocalBoundaryCondition[i] << ", " << BC_D[i] << ", " << BC_N_Bool[i] << ", " << LocalBoundaryCondition[i] << ", " << BC_N[i] << endl;
    }
}