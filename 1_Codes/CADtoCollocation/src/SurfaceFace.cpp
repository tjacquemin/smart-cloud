#include "SurfaceFace.h"

SurfaceFace::SurfaceFace() {
}

SurfaceFace::SurfaceFace(const SurfaceFace& orig) {
}

SurfaceFace::~SurfaceFace() {
}

void SurfaceFace::TriangulateGmsh(double h_Temp)
{
    //Set h
    h=h_Temp;
    
    //Initialize Gmsh
    char** TempChr=NULL;
    gmsh::initialize(0,TempChr);
    
    //Create three surfaces using the OpenCASCADE CAD kernel
    const void* ShapeOCCT=&FaceGeom;
    gmsh::vectorpair s1;
    gmsh::model::occ::importShapesNativePointer(ShapeOCCT,s1,false);
    SurfaceGeom=BRep_Tool::Surface(FaceGeom);
    
    //Synchronize the CAD model with the Gmsh model and set the mesh size at each point
    gmsh::model::occ::synchronize();
    std::vector<std::pair<int,int>> pnts;
    gmsh::model::getBoundary(s1, pnts, true, true, true);
    gmsh::model::mesh::setSize(pnts, h);
    
    //Generate 2D mesh
    gmsh::model::mesh::generate(2);
    
    //Get the inner nodes (nodes in the surface + boundary nodes)
    std::vector<std::size_t> NodeTags;
    std::vector<double> NodeCoord, NodeParamCoord;
    gmsh::model::mesh::getNodes(NodeTags,NodeCoord,NodeParamCoord,-1);
    FaceNodesNumber=NodeCoord.size()/3;
    FaceNodes=new Node[FaceNodesNumber];
    
    //Initialize the tree vectors
    pointVec points(FaceNodesNumber);
    vector<long> PtIndex;
    PtIndex.resize(FaceNodesNumber);
        
    for (long i=0; i<FaceNodesNumber; i++)
    {
        FaceNodes[i].Initialize(3);        
        //Set the node
        FaceNodes[i].X[0]=NodeCoord[3*i+0];
        FaceNodes[i].X[1]=NodeCoord[3*i+1];
        FaceNodes[i].X[2]=NodeCoord[3*i+2];
        
        //Get the normal
        gp_Pnt LocPnt;
        gp_Vec LocPntU, LocPntV;
        const gp_Pnt FacePnt(FaceNodes[i].X[0],FaceNodes[i].X[1],FaceNodes[i].X[2]);
        ShapeAnalysis_Surface sas(SurfaceGeom);
        // get UV of point on surface
        gp_Pnt2d FacePntLocCoord = sas.ValueOfUV(FacePnt, h/100);
        SurfaceGeom->D1(FacePntLocCoord.X(), FacePntLocCoord.Y(), LocPnt, LocPntU, LocPntV);
        Vector3d LocPntUVec,LocPntVVec;
        double Sign=1;
        if (FaceGeom.Orientation()==TopAbs_REVERSED)
        {
            Sign=-1;
        }
        for (int j=0; j<3; j++)
        {
            LocPntUVec(j)=Sign*LocPntU.Coord(j+1);
            LocPntVVec(j)=LocPntV.Coord(j+1);
        }
        FaceNodes[i].InitializeNormal3D(LocPntUVec,LocPntVVec);
        
        //Load the nodes into the tree vectors
        points[i]=point_t({FaceNodes[i].X[0],FaceNodes[i].X[1],FaceNodes[i].X[2]});
        PtIndex[i]=i;
    }
    
    //Load the nodes into a tree for identification of the edges
    KDTree* FaceNodesTree=NULL;
    FaceNodesTree = new KDTree(points);
    
    //Get the inner elements (surface triangulation)
    std::vector<int> eleTypes;
    gmsh::model::mesh::getElementTypes(eleTypes, 2);
    int eleType2D = eleTypes[0];
    
    std::vector<std::pair<int, int>> entities;
    gmsh::model::getEntities(entities, 2);
    //Get the number of elements
    for(std::size_t i = 0; i<entities.size(); i++)
    {
        int s = entities[i].second;
        std::vector<std::size_t> elementTags, nodeTags;
        gmsh::model::mesh::getElementsByType(eleType2D, elementTags, nodeTags, s);
        InnerElementsNumber+=elementTags.size();
    }
    FaceInnerElements=new Element[InnerElementsNumber];
    long count=0;
    for(std::size_t i=0; i<entities.size(); i++)
    {
        int s = entities[i].second;
        std::vector<std::size_t> elementTags, nodeTags;
        gmsh::model::mesh::getElementsByType(eleType2D,elementTags,nodeTags,s);
        for(long i=0; i<InnerElementsNumber; i++)
        {
            FaceInnerElements[count].Initialize(3);
            for (int j=0; j<3; j++)
            {
                FaceInnerElements[count].ElNode[j]=&FaceNodes[nodeTags[3*i+j]-1];
            }
            FaceInnerElements[count].GetNormalFromNodes();
            FaceInnerElements[count].ReElementOrderNodes();
            FaceInnerElements[count].RefSurf=SurfIndex;
            count++;
        }
    }
    GetSurfaceAverageNormal();
    gmsh::finalize();
    
    //Loop though all the edges of the face
    TopTools_IndexedMapOfShape edgesAll;
    TopExp::MapShapes(FaceGeom,TopAbs_EDGE,edgesAll);
    //Set the number of edges and create the SurfaceCv array
    int SurfaceCvNumber=edgesAll.Extent();
        
    for(Standard_Integer i=0; i<SurfaceCvNumber; i++)
    {
        //Get the geometry of the edge
        TopoDS_Edge EdgeLoc = TopoDS::Edge(edgesAll.FindKey(i+1));
        
        gmsh::initialize(0,TempChr);
        //Create three surfaces using the OpenCASCADE CAD kernel
        const void* ShapeOCCT2=&EdgeLoc;
        gmsh::vectorpair s2;
        gmsh::model::occ::importShapesNativePointer(ShapeOCCT2,s2,false);

        //Synchronize the CAD model with the Gmsh model and set the mesh size at each point
        gmsh::model::occ::synchronize();
        std::vector<std::pair<int,int>> pnts2;
        gmsh::model::getEntities(pnts2,0);
        gmsh::model::mesh::setSize(pnts2, h);

        //Generate 1D mesh
        gmsh::model::mesh::generate(1);
        
        //Get the inner nodes (nodes in the surface + boundary nodes)
        std::vector<std::size_t> NodeTags2;
        std::vector<double> NodeCoord2, NodeParamCoord2;
        gmsh::model::mesh::getNodes(NodeTags2,NodeCoord2,NodeParamCoord2,-1);
        int EdgeNodesNumber=NodeCoord2.size()/3;
        
        double hmin=h/(double)1000;
        if (EdgeNodesNumber>1)
        {
            for (int j=0; j<EdgeNodesNumber; j++)
            {
                //Find the discretization node associated to the edge node
                point_t pStart=point_t({NodeCoord2[3*j+0],NodeCoord2[3*j+1],NodeCoord2[3*j+2]});
                auto QueryResult1 = FaceNodesTree->neighborhood_indices(pStart,hmin);
                for (int k=0; k<QueryResult1.size(); k++)
                {
                    long NeighborIndex=PtIndex[QueryResult1[k]];
                    FaceNodes[NeighborIndex].RefSurf.push_back(SurfIndex);
                    FaceNodes[NeighborIndex].RefEdge.push_back(i);
                }
            }
        }
        
        gmsh::finalize();
    }
}

void SurfaceFace::GetSurfaceAverageNormal()
{
    AvgNormal(0)=0;
    AvgNormal(1)=0;
    AvgNormal(2)=0;
    for(long i=0; i<InnerElementsNumber; i++)
    {
        for (int j=0; j<3; j++)
        {
            AvgNormal(j)+=FaceInnerElements[i].Normal[j];
        }
    }
    AvgNormal.normalize();
}

void SurfaceFace::LoadBoundaryProperties(vector<string> BoundProp)
{
    //Set the various values
    SurfType=BoundProp[1];
    if (SurfType=="SURFACE")
    {
        Dimension=3;
    }
    
    //Create the various arrays of the surface
    BCArraysCreated = true;
    LocalBoundaryCondition = new bool[Dimension];
    BC_D_Bool = new bool[Dimension];
    BC_N_Bool = new bool[Dimension];
    BC_D = new float[Dimension];
    BC_N = new float[Dimension];
    BC_DOF = new int[Dimension];
    if (BoundProp[2]=="I")
    {
        InnerFace=true;
    }
    else
    {
        InnerFace=false;
    }
    for (int i=0; i<Dimension; i++)
    {
        if (BoundProp[3+3*i]=="D")
        {
            BC_D_Bool[i]=true;
            BC_N_Bool[i]=false;
        }
        else
        {
            BC_D_Bool[i]=false;
            BC_N_Bool[i]=true;
        }
        if (BoundProp[4+3*i]=="L")
        {
            LocalBoundaryCondition[i]=true;
        }
        else
        {
            LocalBoundaryCondition[i]=false;
        }
        if (BC_D_Bool[i]==true)
        {
            BC_D[i]=stof(BoundProp[5+3*i]);
            BC_N[i]=0;
        }
        else if (BC_N_Bool[i]==true)
        {
            BC_D[i]=0;
            BC_N[i]=stof(BoundProp[5+3*i]);
        }
    }
}