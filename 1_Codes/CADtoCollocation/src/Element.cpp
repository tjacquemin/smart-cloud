#include "Element.h"

Element::Element() {
}

Element::Element(const Element& orig) {
}

Element::~Element() {
}

void Element::Initialize(int ElDimTemp)
{
    ElDim=ElDimTemp;
    ElNode=new Node*[ElDim];
    Normal=new double[3];
}

void Element::GetNormalFromNodes()
{
    for (int i=0; i<3; i++)
    {
        Normal[i]=0;
    }
    //Sum the normals
    for (int i=0; i<3; i++)
    {
        for (int j=0; j<ElDim; j++)
        {
            Normal[i]+=ElNode[j]->Normal[i];
        }
    }
    //Get the average normal
    double Norm=0;
    for (int i=0; i<3; i++)
    {
        Norm+=pow(Normal[i],2);
    }
    Norm=pow(Norm,0.5);
    for (int i=0; i<3; i++)
    {
        Normal[i]=Normal[i]/Norm;
    }
}

void Element::ReElementOrderNodes()
{
    //Compute the scalar product of element normal based on N1N2 vect N1N3 with the element normal
    double* VectProd=NULL;
    double ScalarProd=0;
    VectProd=new double[3];
    
    VectProd[0]=(ElNode[1]->X[1]-ElNode[0]->X[1])*(ElNode[2]->X[2]-ElNode[0]->X[2])-(ElNode[1]->X[2]-ElNode[0]->X[2])*(ElNode[2]->X[1]-ElNode[0]->X[1]);
    VectProd[1]=(ElNode[1]->X[2]-ElNode[0]->X[2])*(ElNode[2]->X[0]-ElNode[0]->X[0])-(ElNode[1]->X[0]-ElNode[0]->X[0])*(ElNode[2]->X[2]-ElNode[0]->X[2]);
    VectProd[2]=(ElNode[1]->X[0]-ElNode[0]->X[0])*(ElNode[2]->X[1]-ElNode[0]->X[1])-(ElNode[1]->X[1]-ElNode[0]->X[1])*(ElNode[2]->X[0]-ElNode[0]->X[0]);
    
    ScalarProd=VectProd[0]*Normal[0]+VectProd[1]*Normal[1]+VectProd[2]*Normal[2];
    delete[] VectProd;
    if (ScalarProd<0)
    {
        Node* TempNodePtr=ElNode[1];
        ElNode[1]=ElNode[2];
        ElNode[2]=TempNodePtr;
    }
    
}