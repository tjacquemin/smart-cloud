#include "Body.h"

Body::Body() {
}

Body::Body(const Body& orig) {
}

Body::~Body() {
}

void Body::Set2DBody(TopoDS_Face TempBody)
{
    Dimension=2;
    Body2D=TempBody;
}

void Body::Set3DBody(TopoDS_Shape TempBody)
{
    Dimension=3;
    Body3D=TempBody;
}

double Body::GetVolume()
{
    GProp_GProps gprops;
    if (Dimension==2)
    {
        BRepGProp::SurfaceProperties(Body2D, gprops);
    }
    else if (Dimension==3)
    {
        BRepGProp::VolumeProperties(Body3D, gprops);
    }
    return gprops.Mass();
}

void Body::LoadBoundaries()
{
    if (Dimension==2)
    {
        //Get all the edges from the shape
        TopTools_IndexedMapOfShape edgesAll;
        TopExp::MapShapes(Body2D,TopAbs_EDGE,edgesAll);
        Standard_Real aFirst, aLast;
        //Set the number of edges and create the SurfaceCv array
        SurfaceCvNumber=edgesAll.Extent();
        cout << " --> Surface curves number: " << SurfaceCvNumber << endl;
        
        SurfaceCv=new SurfaceCurve[SurfaceCvNumber];
        for(Standard_Integer i=0; i<SurfaceCvNumber; i++)
        {
            //Get the geometry of the edge
            TopoDS_Edge EdgeLoc = TopoDS::Edge(edgesAll.FindKey(i+1));
            SurfaceCv[i].SurfaceGeomCurve = BRep_Tool::Curve(EdgeLoc, aFirst, aLast);
            SurfaceCv[i].CvFirst=aFirst;
            SurfaceCv[i].CvLast=aLast;
        }
    }
    else if (Dimension==3)
    {
        //Get all the faces from the shape
        TopTools_IndexedMapOfShape facesAll;
        TopExp::MapShapes(Body3D, TopAbs_FACE, facesAll);
        //Set the number of surfaces and create the surface array
        SurfaceFcNumber=facesAll.Extent();
        SurfaceFc=new SurfaceFace[SurfaceFcNumber];
        TopExp::MapShapes(Body3D, TopAbs_FACE, facesAll);
        for(Standard_Integer i=1; i<=SurfaceFcNumber; i++)
        {
            //Load the faces of the geometry
            SurfaceFc[i-1].FaceGeom = TopoDS::Face(facesAll.FindKey(i));
            SurfaceFc[i-1].SurfIndex=i-1;
        }
    }
}

void Body::DiscretizeBoundaries(double h_temp)
{
    h=h_temp;
    if (Dimension==2)
    {
        for (int i=0; i<SurfaceCvNumber; i++)
        {
            SurfaceCv[i].Discretize(h);
        }
        GetSurfaceDirections();
        GetCornerNodes();
    }
    else if (Dimension==3)
    {
        for (int i=0; i<SurfaceFcNumber; i++)
        {
            SurfaceFc[i].TriangulateGmsh(h);
        }
    }
}

void Body::GetSurfaceDirections()
{
    //Initialize a boolean operation class
    BooleanOperation NewBooleanOperation;
    NewBooleanOperation.Dimension=Dimension;
    NewBooleanOperation.HasSpikes=HasSpikes;
    NewBooleanOperation.BCTolerance=BCTolerance;
    Node* TempNode=NULL;
    TempNode = new Node[1];
    TempNode[0].Initialize(Dimension);
    
    if (Dimension==2)
    {
        for (int i=0; i<SurfaceCvNumber; i++)
        {           
            //Get the index of a middle node
            int MidNodeIndex=SurfaceCv[i].NumberNodes/2;
            //Get the coordinate of a node normal to the curve node in the direction of the normal
            for (int j=0; j<3; j++)
            {
                TempNode[0].X[j]=SurfaceCv[i].CurveNode[MidNodeIndex].X[j]+h/100*SurfaceCv[i].CurveNode[MidNodeIndex].Normal[j];
            }
            //Determine if the normal of an inner node of the curve shall be flipped
            if (NewBooleanOperation.NodeInCAD_2D(TempNode,0,Body2D,0)==true)
            {
                for (int j=0; j<SurfaceCv[i].NumberNodes; j++)
                {
                    for (int k=0; k<3; k++)
                    {
                        SurfaceCv[i].CurveNode[j].Normal[k]=-SurfaceCv[i].CurveNode[j].Normal[k];
                    }
                }
            }
        }
    }
}

void Body::GetCornerNodes()
{
    //Intitialize the corner nodes
    CornerNodes = new Node[Dimension*SurfaceCvNumber];
    for (int i=0; i<SurfaceCvNumber; i++)
    {
        //Fill the CornerNodes vector
        CornerNodes[Dimension*i+0].Initialize(Dimension);
        CornerNodes[Dimension*i+0].Normal = new double[3];
        CornerNodes[Dimension*i+1].Initialize(Dimension);
        CornerNodes[Dimension*i+1].Normal = new double[3];
        CornerNodes[Dimension*i+0].AttachedSet.clear();
        CornerNodes[Dimension*i+0].AttachedSet.push_back(i);
        CornerNodes[Dimension*i+0].StartEnd.clear();
        CornerNodes[Dimension*i+0].StartEnd.push_back(0);
        CornerNodes[Dimension*i+1].AttachedSet.clear();
        CornerNodes[Dimension*i+1].AttachedSet.push_back(i);
        CornerNodes[Dimension*i+1].StartEnd.clear();
        CornerNodes[Dimension*i+1].StartEnd.push_back(1);
        for (int j=0; j<3; j++)
        {
            CornerNodes[Dimension*i+0].X[j]=SurfaceCv[i].CurveNode[0].X[j];
            CornerNodes[Dimension*i+0].Normal[j]=SurfaceCv[i].CurveNode[0].Normal[j];
            CornerNodes[Dimension*i+1].X[j]=SurfaceCv[i].CurveNode[SurfaceCv[i].NumberNodes-1].X[j];
            CornerNodes[Dimension*i+1].Normal[j]=SurfaceCv[i].CurveNode[SurfaceCv[i].NumberNodes-1].Normal[j];
        }
    }
    
    //Load all the nodes in a tree
    //Create the vectors
    pointVec points(Dimension*SurfaceCvNumber);
    vector<long> PtIndex;
    PtIndex.resize(Dimension*SurfaceCvNumber);
    
    //Fill the points with the vector
    for (long i=0; i<Dimension*SurfaceCvNumber; i++)
    {
        points[i]=point_t({CornerNodes[i].X[0],CornerNodes[i].X[1],CornerNodes[i].X[2]});
        CornerNodes[i].InDomain=true;
        PtIndex[i]=i;
    }
    
    //Load the KD tree
    KDTree* BodyEdgeCornerNodeTree=NULL;
    BodyEdgeCornerNodeTree = new KDTree(points);
    
    double hmin=h/(double)1000;
    //Set the curve connections
    for (int i=0; i<Dimension*SurfaceCvNumber; i++)
    {
        if (CornerNodes[i].InDomain==true)
        {
            point_t pStart=point_t({CornerNodes[i].X[0],CornerNodes[i].X[1],CornerNodes[i].X[2]});
            auto QueryResult1 = BodyEdgeCornerNodeTree->neighborhood_indices(pStart,hmin);
            for (int j=0; j<QueryResult1.size(); j++)
            {
                long NeighborIndex=PtIndex[QueryResult1[j]];
                if (NeighborIndex != i)
                {
                    for (int k=0; k<3; k++)
                    {
                        CornerNodes[i].Normal[k]+=CornerNodes[NeighborIndex].Normal[k];
                    }
                }
                //Assign the pointer of the curve node to the corner index
                int CurveNum=0;
                int StartNode=false;
                if (NeighborIndex % 2 == 0)
                {
                    CurveNum=NeighborIndex/2;
                    StartNode=true;
                }
                else
                {
                    CurveNum=(NeighborIndex-1)/2;
                    StartNode=false;
                }
                if (StartNode==true)
                {
                    SurfaceCv[CurveNum].CurveNode[0].GlobalIndex=CornerNodes[i].GlobalIndex;
                }
                else
                {
                    SurfaceCv[CurveNum].CurveNode[SurfaceCv[CurveNum].NumberNodes-1].GlobalIndex=CornerNodes[i].GlobalIndex;
                }
                
                if (NeighborIndex > i)
                {
                    CornerNodes[NeighborIndex].InDomain=false;
                    CornerNodes[i].AttachedSet.push_back(CornerNodes[NeighborIndex].AttachedSet[0]);
                    CornerNodes[i].StartEnd.push_back(CornerNodes[NeighborIndex].StartEnd[0]);
                }
            }
            //Normalize the normal
            double Norm=0;
            for (int k=0; k<3; k++)
            {
                Norm+=pow(CornerNodes[i].Normal[k],2);
            }
            Norm=pow(Norm,0.5);
            for (int k=0; k<3; k++)
            {
                CornerNodes[i].Normal[k]=CornerNodes[i].Normal[k]/Norm;
            }
        }
    }
}

void Body::DiscretizeBody(string BooleanOperationTypeTemp)
{
    BooleanOperationType=BooleanOperationTypeTemp;
    GetModelBox();
    BoxDiscretization();
    GetNodesInDomain();
    RemoveNodesCloseToBoundaries(0);
}

void Body::GetModelBox()
{
    //Compute the parameters of the bounding box
    Bnd_Box B;
    if (Dimension==2)
    {
        BRepBndLib::Add(Body2D, B);
    }
    else if (Dimension==3)
    {
        BRepBndLib::Add(Body3D, B);
    }
    B.Get(MinX, MinY, MinZ, MaxX, MaxY, MaxZ); 
    //Reduce the size of the box by h on each side
    double h_reduc=h/2*pow(3,0.5);
    MinX=MinX+h_reduc;
    MinY=MinY+h_reduc;
    MinZ=MinZ+h_reduc;
    MaxX=MaxX-h_reduc;
    MaxY=MaxY-h_reduc;
    MaxZ=MaxZ-h_reduc;
}

void Body::BoxDiscretization()
{
    long nx=1;
    long ny=1;
    long nz=1;
    //Get the number of nodes nx, ny, nz in the directions x, y and z
    if (MaxX-MinX<=0)
        nx=1;
    else
        nx=int((MaxX-MinX)/h)+1;
    if (MaxY-MinY<=0)
        ny=1;
    else
        ny=int((MaxY-MinY)/h)+1;
    if (MaxZ-MinZ<=0)
        nz=1;
    else
        nz=int((MaxZ-MinZ)/h)+1;

    if (InnerDiscretizationType=="Regular")
    {
        BoxNodeNumber=nx*ny*nz;
        BoxNode=new Node[BoxNodeNumber];
        //Load the grid of nodes
        for (long i=0; i<nx; i++)
        {
            for (long j=0; j<ny; j++)
            {
                for (long k=0; k<nz; k++)
                {
                    BoxNode[i*ny*nz+j*nz+k].Initialize(Dimension);
                    if (nx>1)
                        BoxNode[i*ny*nz+j*nz+k].X[0]=MinX+i*(MaxX-MinX)/(nx-1);
                    else
                        BoxNode[i*ny*nz+j*nz+k].X[0]=(MaxX+MinX)/2;
                    if (ny>1)
                        BoxNode[i*ny*nz+j*nz+k].X[1]=MinY+j*(MaxY-MinY)/(ny-1);
                    else
                        BoxNode[i*ny*nz+j*nz+k].X[1]=(MaxY+MinY)/2;
                    if (nz>1)
                        BoxNode[i*ny*nz+j*nz+k].X[2]=MinZ+k*(MaxZ-MinZ)/(nz-1);
                    else
                        BoxNode[i*ny*nz+j*nz+k].X[2]=(MaxZ+MinZ)/2;
                }
            }
        }
    }
    else if (InnerDiscretizationType=="Triangles" && Dimension==2)
    {
        //Compute the number of box nodes
        if (MaxX-MinX<=0)
            nx=1;
        else
            nx=int((MaxX-MinX)/h)+1;
        if (MaxY-MinY<=0)
            ny=1;
        else
            ny=int((MaxY-MinY)/(h*pow(3,0.5)/2))+1;
        nz=1;
        if (ny%2==0)
        {
            BoxNodeNumber=(nx*ny+ny/2)*nz;
        }
        else
        {
            BoxNodeNumber=(nx*ny+(ny-1)/2)*nz;
        }
        BoxNode=new Node[BoxNodeNumber];
        long Count=0;
        //Load the grid of nodes
        for (long j=0; j<ny; j++)
        {
            long ColSize=nx;
            double StartXOffset=0;
            if (j%2!=0)
            {
                ColSize=nx+1;
                StartXOffset=(MaxX-MinX)/(ColSize-1)/2;
            }
            for (long i=0; i<ColSize; i++)
            {
                BoxNode[Count].Initialize(Dimension);
                BoxNode[Count].X[1]=MinY+j*(MaxY-MinY)/(ny-1);
                BoxNode[Count].X[0]=(MinX-StartXOffset)+i*(MaxX-MinX+2*StartXOffset)/(ColSize-1);
                BoxNode[Count].X[2]=(MaxZ+MinZ)/2;
                Count++;
            }
        }
    }
    else if (InnerDiscretizationType=="Triangles" && Dimension==3)
    {
        //Fill a surface, larger than the face XY of the box
        //Compute the number of box nodes
        Node* TempFaceNode=NULL;
        long TempFaceNodeNum=0;
        {
            if (MaxX-MinX<=0)
                nx=1;
            else
                nx=int((MaxX-MinX+2*h)/h)+1;
            if (MaxY-MinY<=0)
                ny=1;
            else
                ny=int((MaxY-MinY+2*h)/(h*pow(3,0.5)/2))+1;
            nz=1;
            if (ny%2==0)
            {
                TempFaceNodeNum=(nx*ny+ny/2)*nz;
            }
            else
            {
                TempFaceNodeNum=(nx*ny+(ny-1)/2)*nz;
            }
            TempFaceNode=new Node[TempFaceNodeNum];
            long Count=0;
            //Load the TempFaceNode of nodes
            for (long j=0; j<ny; j++)
            {
                long ColSize=nx;
                double StartXOffset=0;
                if (j%2!=0)
                {
                    ColSize=nx+1;
                    StartXOffset=(MaxX-MinX+2*h)/(ColSize-1)/2;
                }
                for (long i=0; i<ColSize; i++)
                {
                    TempFaceNode[Count].Initialize(Dimension);
                    TempFaceNode[Count].X[1]=MinY-h+j*(MaxY-MinY+2*h)/(ny-1);
                    TempFaceNode[Count].X[0]=(MinX-h-StartXOffset)+i*(MaxX-MinX+2*h+2*StartXOffset)/(ColSize-1);
                    TempFaceNode[Count].X[2]=(MaxZ+MinZ)/2;
                    Count++;
                }
            }
        }
        
        //Calculate the number of planes in the z direction
        nz=int((MaxZ-MinZ)/(h*pow(6,0.5)/3))+1;
        
        double OffX=h/2;
        double OffY=h*pow(3,0.5)/2/3;
        double OffZ=(MaxZ-MinZ)/(nz-1);//h*pow(6,0.5)/3;
        
        BoxNodeNumber=TempFaceNodeNum*nz;
        BoxNode=new Node[BoxNodeNumber];
        long Count=0;
        for (long i=0; i<nz; i++)
        {
            double CoordX=0;
            double CoordY=0;
            double CoordZ=MinZ+OffZ*i;
            double Mod=i%2;
            for (long j=0; j<TempFaceNodeNum; j++)
            {
                CoordX=TempFaceNode[j].X[0]+OffX*Mod;
                CoordY=TempFaceNode[j].X[1]+OffY*Mod;
                BoxNode[Count].Initialize(Dimension);
                BoxNode[Count].X[0]=CoordX;
                BoxNode[Count].X[1]=CoordY;
                BoxNode[Count].X[2]=CoordZ;
                if (BoxNode[Count].X[0]<MinX || BoxNode[Count].X[0]>MaxX || BoxNode[Count].X[1]<MinY || BoxNode[Count].X[1]>MaxY || BoxNode[Count].X[2]<MinZ || BoxNode[Count].X[2]>MaxZ)
                {
                    BoxNode[Count].InDomain=false;
                }
                Count++;
            }
        }
    }
}

void Body::GetCurveStartEndConnections()
{
    //Count the number of nodes in the body
    long Count=0;
    for (int i=0; i<SurfaceCvNumber; i++)
    {
        Count+=2;
    }
    
    //Create the vectors
    pointVec points(Count);
    vector<long> PtIndex;
    PtIndex.resize(Count);
    Count=0;
    
    //Fill the points with the  in the 
    for (long i=0; i<SurfaceCvNumber; i++)
    {
        points[Count]=point_t({SurfaceCv[i].CurveNode[0].X[0],SurfaceCv[i].CurveNode[0].X[1],SurfaceCv[i].CurveNode[0].X[2]});
        PtIndex[Count]=i;
        Count++;
        points[Count]=point_t({SurfaceCv[i].CurveNode[SurfaceCv[i].NumberNodes-1].X[0],SurfaceCv[i].CurveNode[SurfaceCv[i].NumberNodes-1].X[1],SurfaceCv[i].CurveNode[SurfaceCv[i].NumberNodes-1].X[2]});
        PtIndex[Count]=i;
        Count++;
    }
    
    //Load the KD tree
    KDTree* BodyEdgeCornerNodeTree=NULL;
    BodyEdgeCornerNodeTree = new KDTree(points);
        
    //Set the threshold distance
    double hmin=h/(double)1000;
    //Set the curve connections
    for (int i=0; i<SurfaceCvNumber; i++)
    {
        //Set the point queries
        //Define the queried point
        point_t pStart=point_t({SurfaceCv[i].CurveNode[0].X[0],SurfaceCv[i].CurveNode[0].X[1],SurfaceCv[i].CurveNode[0].X[2]});
        point_t pEnd=point_t({SurfaceCv[i].CurveNode[SurfaceCv[i].NumberNodes-1].X[0],SurfaceCv[i].CurveNode[SurfaceCv[i].NumberNodes-1].X[1],SurfaceCv[i].CurveNode[SurfaceCv[i].NumberNodes-1].X[2]});
        
        //Determine if the curve is closed
        double dist=pow(pow(pEnd[0]-pStart[0],2)+pow(pEnd[1]-pStart[1],2)+pow(pEnd[2]-pStart[2],2),0.5);
        if (dist<hmin)
        {
            SurfaceCv[i].PrevCvIndex=i;
            SurfaceCv[i].NextCvIndex=i;
        }
        else
        {
            //Search the curve node the closes to the start
            auto QueryResult1 = BodyEdgeCornerNodeTree->neighborhood_indices(pStart,hmin);
            for (int j=0; j<QueryResult1.size(); j++)
            {
                long CvNum=PtIndex[QueryResult1[j]];
                if (CvNum != i)
                {
                    SurfaceCv[i].PrevCvIndex=CvNum;
                }
            }
            //Search the curve node the closes to the end
            auto QueryResult2 = BodyEdgeCornerNodeTree->neighborhood_indices(pEnd,hmin);
            for (int j=0; j<QueryResult2.size(); j++)
            {
                long CvNum=PtIndex[QueryResult2[j]];
                if (CvNum != i)
                {
                    SurfaceCv[i].NextCvIndex=CvNum;
                }
            }
        }
    }
}

void Body::GetElementsAttachedToNodes()
{
    if (Dimension==2)
    {
        for (int i=0; i<SurfaceCvNumber; i++)
        {
            for (long j=0; j<SurfaceCv[i].NumberElements; j++)
            {
                for (int k=0; k<2; k++)
                {
                    SurfaceCv[i].CurveElement[j].ElNode[k]->AttachedElements.push_back({i,j});
                }
            }
        }
    }
    else if (Dimension==3)
    {
        for (long i=0; i<SurfaceFcNumber; i++)
        {
            for (long j=0; j<SurfaceFc[i].InnerElementsNumber; j++)
            {
                for (int k=0; k<3; k++)
                {
                    SurfaceFc[i].FaceInnerElements[j].ElNode[k]->AttachedElements.push_back({i,j});
                }
            }
        }
    }
}

void Body::GetElementsFromAdjacentRegions()
{
    //Count the number of nodes on the boundaries of the body
    long Count=0;
    if (Dimension==2)
    {
        for (int i=0; i<SurfaceCvNumber; i++)
        {
            Count+=SurfaceCv[i].NumberNodes;
        }
    }
    else if (Dimension==3)
    {
        for (int i=0; i<SurfaceFcNumber; i++)
        {
            Count+=SurfaceFc[i].FaceNodesNumber;
        }
    }
    
    //Create the vectors
    pointVec points(Count);
    vector<vector<long>> PtIndex;
    PtIndex.resize(Count);
    Count=0;
    
    //Fill the points with the nodes
    if (Dimension==2)
    {
        for (int i=0; i<SurfaceCvNumber; i++)
        {
            for (long j=0; j<SurfaceCv[i].NumberNodes; j++)
            {
                points[Count]=point_t({SurfaceCv[i].CurveNode[j].X[0],SurfaceCv[i].CurveNode[j].X[1],SurfaceCv[i].CurveNode[j].X[2]});
                PtIndex[Count]={i,j};
                Count++;
            }
        }
    }
    else if (Dimension==3)
    {
        for (int i=0; i<SurfaceFcNumber; i++)
        {
            for (long j=0; j<SurfaceFc[i].FaceNodesNumber; j++)
            {
                points[Count]=point_t({SurfaceFc[i].FaceNodes[j].X[0],SurfaceFc[i].FaceNodes[j].X[1],SurfaceFc[i].FaceNodes[j].X[2]});
                PtIndex[Count]={i,j};
                Count++;
            }
        }
    }
    
    //Load the KD tree
    KDTree* BodyBoundaryNodeTree=NULL;
    BodyBoundaryNodeTree = new KDTree(points);
    //Set the threshold distance
    double hmin=h/(double)1000;
    
    //Add the elements of adjacent regions to the nodes
    if (Dimension==2)
    {
        for (int i=0; i<SurfaceCvNumber; i++)
        {
            for (long j=0; j<SurfaceCv[i].NumberNodes; j++)
            {
                //Search the curve node the closes to the start
                if (SurfaceCv[i].CurveNode[j].AttachedElements.size()<2)
                {
                    point_t QueryPt=point_t({SurfaceCv[i].CurveNode[j].X[0],SurfaceCv[i].CurveNode[j].X[1],SurfaceCv[i].CurveNode[j].X[2]});
                    auto QueryResult = BodyBoundaryNodeTree->neighborhood_indices(QueryPt,hmin);
                    for (int k=0; k<QueryResult.size(); k++)
                    {
                        long CvNum=PtIndex[QueryResult[k]][0];
                        long CvNode=PtIndex[QueryResult[k]][1];
                        if (CvNum != i)
                        {
                            for (int l=0; l<SurfaceCv[CvNum].CurveNode[CvNode].AttachedElements.size(); l++)
                            {
                                long AssCv=SurfaceCv[CvNum].CurveNode[CvNode].AttachedElements[l][0];
                                long AssNode=SurfaceCv[CvNum].CurveNode[CvNode].AttachedElements[l][1];
                                SurfaceCv[i].CurveNode[j].AttachedElements.push_back({AssCv,AssNode});
                            }
                        }
                    }
                }
            }
        }
    }
    else if (Dimension==3)
    {
        for (int i=0; i<SurfaceFcNumber; i++)
        {
            for (long j=0; j<SurfaceFc[i].FaceNodesNumber; j++)
            {
                point_t QueryPt=point_t({SurfaceFc[i].FaceNodes[j].X[0],SurfaceFc[i].FaceNodes[j].X[1],SurfaceFc[i].FaceNodes[j].X[2]});
                auto QueryResult = BodyBoundaryNodeTree->neighborhood_indices(QueryPt,hmin);
                for (int k=0; k<QueryResult.size(); k++)
                {
                    long FcNum=PtIndex[QueryResult[k]][0];
                    long FcNode=PtIndex[QueryResult[k]][1];
                    if (FcNum != i)
                    {
                        for (int l=0; l<SurfaceFc[FcNum].FaceNodes[FcNode].AttachedElements.size(); l++)
                        {
                            long AssCv=SurfaceFc[FcNum].FaceNodes[FcNode].AttachedElements[l][0];
                            long AssNode=SurfaceFc[FcNum].FaceNodes[FcNode].AttachedElements[l][1];
                            SurfaceFc[i].FaceNodes[j].AttachedElements.push_back({AssCv,AssNode});
                        }
                    }
                }
            }
        }
    } 
}

void Body::GetNodesInDomain()
{   
    BooleanOperation NewBooleanOperation;
    NewBooleanOperation.Dimension=Dimension;
    NewBooleanOperation.HasSpikes=HasSpikes;
    NewBooleanOperation.BCTolerance=BCTolerance;
    if (BooleanOperationType=="CAD" && Dimension==2)
    {
        NewBooleanOperation.GetInnerNodesFromCAD_2D(BoxNode,BoxNodeNumber,Body2D);
    }
    else if (BooleanOperationType=="CAD" && Dimension==3)
    {
        //Set the 3D body
        NewBooleanOperation.Shape3D=Body3D;
        NewBooleanOperation.GetInnerNodesFromCAD_3D(BoxNode,BoxNodeNumber,h);
    }
    else if (BooleanOperationType=="Bound" && Dimension==2)
    {
        NewBooleanOperation.GetInnerNodesFromBC_2D(SurfaceCv,SurfaceCvNumber,Body2D,h,BoxNode,BoxNodeNumber);
    }
    else if (BooleanOperationType=="Bound" && Dimension==3)
    {
        
    }
    else if (BooleanOperationType!="CAD" && Dimension==3)
    {
        //Count the number of surface elements
        long countEl=0;
        for (int i=0; i<SurfaceFcNumber; i++)
        {
            countEl+=SurfaceFc[i].InnerElementsNumber;
        }
        NewBooleanOperation.NumberSurfElements=countEl;
        NewBooleanOperation.SurfaceElements=new Element[countEl];
        for (int i=0; i<SurfaceFcNumber; i++)
        {
            NewBooleanOperation.LoadSurfaceElements(SurfaceFc[i].FaceInnerElements,SurfaceFc[i].InnerElementsNumber);
        }
//        NewBooleanOperation.GetInnerNodesFromMesh(SurfaceFc,SurfaceFcNumber,h,BoxNode,BoxNodeNumber,Body3D,BooleanOperationType);
    }
}

void Body::RemoveNodesCloseToBoundaries(int ForRelax)
{
    //Count the number of nodes in the body
    long Count=0;
    for (long i=0; i<BoxNodeNumber; i++)
    {
        if (BoxNode[i].InDomain==true || ForRelax==1)
        {
            Count++;
        }
    }
    
    //Create the vectors
    pointVec points(Count);
    vector<long> PtIndex;
    PtIndex.resize(Count);
    Count=0;
    //Fill the rtree with the BoxNodes in the 
    for (long i=0; i<BoxNodeNumber; i++)
    {
        if (BoxNode[i].InDomain==true || ForRelax==1)
        {
            points[Count]=point_t({BoxNode[i].X[0],BoxNode[i].X[1],BoxNode[i].X[2]});
            PtIndex[Count]=i;
            Count++;
        }
    }
    
    //Load the KD tree
    KDTree* BodyInnerNodeTree=NULL;
    BodyInnerNodeTree = new KDTree(points);
    
    //Set the threshold distance
    double hmin=h*ThresholdRatio;
    
    //Define the queried point
    point_t PtQuery;
    if (Dimension==2)
    {
        //Remove the duplicates in the Vertex and Edge sets
        for (long i=0; i<SurfaceCvNumber; i++)
        {
            //For each boundary node, search for the box nodes too close to the boundary
            for (long j=0; j<SurfaceCv[i].NumberNodes; j++)
            {
                //List the box nodes within a radius hmin of the boundary node
                PtQuery = {SurfaceCv[i].CurveNode[j].X[0],SurfaceCv[i].CurveNode[j].X[1],SurfaceCv[i].CurveNode[j].X[2]};
                auto QueryResult = BodyInnerNodeTree->neighborhood_indices(PtQuery,hmin);
                for (int j=0; j<QueryResult.size(); j++)
                {
                    long BoxNum=PtIndex[QueryResult[j]];
                    if (ForRelax==1)
                        BoxNode[BoxNum].NodeCloseToBound=true;
                    else
                        BoxNode[BoxNum].InDomain=false;
                    
                }
            }
        }
    }
    else if (Dimension==3)
    {
        //Remove the duplicates in the faces
        for (int i=0; i<SurfaceFcNumber; i++)
        {
            for (long j=0; j<SurfaceFc[i].FaceNodesNumber; j++)
            {
                //List the box nodes within a radius hmin of the boundary node
                PtQuery = {SurfaceFc[i].FaceNodes[j].X[0],SurfaceFc[i].FaceNodes[j].X[1],SurfaceFc[i].FaceNodes[j].X[2]};
                auto QueryResult = BodyInnerNodeTree->neighborhood_indices(PtQuery,hmin);
                for (int j=0; j<QueryResult.size(); j++)
                {
                    long BoxNum=PtIndex[QueryResult[j]];
                    if (ForRelax==1)
                        BoxNode[BoxNum].NodeCloseToBound=true;
                    else
                        BoxNode[BoxNum].InDomain=false;
                }
            }
        }
    }
    
    //Clear the vectors and delete the tree
    points.clear();
    PtIndex.clear();
}