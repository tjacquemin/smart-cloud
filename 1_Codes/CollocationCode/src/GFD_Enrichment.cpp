#include "GFD_Problem.h"

double GFD_Problem::Angle(Model* M,long CollocNode, int Dim,long SingNodes)
{
    double Value=0;
    double ValX=0;
    double ValY=0;
    if (SingNodes==-1)
    {
        ValX=M->Nodes[CollocNode].Normal[Dim][0];
        ValY=M->Nodes[CollocNode].Normal[Dim][1];
    }
    else
    {
        ValX=M->Nodes[CollocNode].X[0]-M->Nodes[SingNodes].X[0];
        ValY=M->Nodes[CollocNode].X[1]-M->Nodes[SingNodes].X[1];
    }
    double Dist=pow(pow(ValX,2)+pow(ValY,2),0.5);
    if (ValY>=0)
        Value=acos(ValX/Dist);
    else
        Value=-acos(ValX/Dist);
    return Value;
}

void GFD_Problem::LoadDistToSingular(Model* M)
{
    //Compute the distance to the singularity for each node
    for (long i=1; i<=M->NodeNumber; i++)
    {
        double Val=0;
        //Get the distance to the singular node
        for (int j=0; j<M->Dimension;j++)
        {
            Val+=pow(M->Nodes[i].X[j]-M->Nodes[M->SingularNode].X[j],2);
        }
        M->Nodes[i].DistToSing=pow(Val,0.5);

        //Assess if the node is in the enrichment zone
        if (M->Nodes[i].DistToSing<M->DistThreshold)
            M->Nodes[i].EnrichedNode=true;
        else
            M->Nodes[i].EnrichedNode=false;
    }
}

void GFD_Problem::LoadU1Values(Model* M, int ThreadIndex, int TreadNum)
{
    //Get the start and end node of the thread
    long Start, End;
    M->GetStartEndNode(ThreadIndex,TreadNum,Start, End,M->NodeNumber);
    
    //Get the U1 values
    double* U1=NULL;
    U1=new double[M->Dimension];
    for (long i=Start; i<=End; i++)
    {
        if (M->SingularEnrichment=="LSHAPE" && M->Dimension==2)
        {
            double Alpha=0.544483737;
            double Q=0.543075579;
            double Epsilon=-3*M->pi/4;
            double Kapa=3-4*M->nu;

            double Rad=M->Nodes[i].DistToSing;
            if (M->FullEnrichment==false)
            {
                M->Nodes[i].U1 = new double[M->Dimension];
                M->Nodes[i].U1[0]=pow(Rad,Alpha);
                M->Nodes[i].U1[1]=M->Nodes[i].U1[0];
            }
            else
            {                
                double Theta2=3*M->pi/4+Angle(M,i,0,M->SingularNode);
                if (Theta2>M->pi)
                    Theta2-=2*M->pi;
                double a1_=(Kapa-Q*(Alpha+1))*cos(Alpha*Theta2)-Alpha*cos((Alpha-2)*Theta2);
                double a2_=(Kapa+Q*(Alpha+1))*sin(Alpha*Theta2)+Alpha*sin((Alpha-2)*Theta2);
                U1[0]=(a1_*cos(Epsilon)-a2_*sin(Epsilon))*pow(Rad,Alpha);
                U1[1]=(a1_*sin(Epsilon)+a2_*cos(Epsilon))*pow(Rad,Alpha);

            }
        }
        else if (M->SingularEnrichment=="CRACKED_PLATE" && M->Dimension==2)
        {
            double p=(1+M->nu)*pow(2*M->pi,-0.5)/M->E;
            double alpha=0.5;
            double rad=M->Nodes[i].DistToSing;
            
            if (M->FullEnrichment==false)
            {
                U1[0]=p*pow(rad,alpha);
                U1[1]=U1[0];
            }
            else
            {
                double beta=0.5;
                double q1=(3-M->nu)/(1+M->nu)-1; //For plane stress problems
                double q2=(3-M->nu)/(1+M->nu)+1; //For plane stress problems
                double theta=Angle(M,i,0,M->SingularNode);
                U1[0]=pow(rad,alpha)*p*cos(beta*theta)*(q1+2*pow(sin(beta*theta),2));
                U1[1]=pow(rad,alpha)*p*sin(beta*theta)*(q2-2*pow(cos(beta*theta),2));
            }
        }

        //Add the values of the enrichment function to the node properties
        M->Nodes[i].U1 = new double[M->Dimension];
        M->Nodes[i].U1[0]=U1[0];
        M->Nodes[i].U1[1]=U1[1];
    }
    
    //Destroy the pointer
    delete[] U1;
}

void GFD_Problem::RefineEnrichmentZone(Model* M)
{
    //Create the Min and Max vectors
    MinU1=new double[M->Dimension];
    MaxU1=new double[M->Dimension];
    //Initialize the Min and Max Vectors
    for (int j=0; j<M->Dimension; j++)
    {
        MinU1[j]=M->Nodes[1].U1[j];
        MaxU1[j]=M->Nodes[1].U1[j];
    }
    //Get the Min and Max values
    for (long i=1; i<=M->NodeNumber; i++)
    {
        for (int j=0; j<M->Dimension; j++)
        {
            if (M->Nodes[i].U1[j]<MinU1[j])
                MinU1[j]=M->Nodes[i].U1[j];
            if (M->Nodes[i].U1[j]>MaxU1[j])
                MaxU1[j]=M->Nodes[i].U1[j];
        }
    }

    //Exclude the enrichment nodes in the Zero enrichent zone
    for (long i=1; i<=M->NodeNumber; i++)
    {
        if (M->Nodes[i].EnrichedNode==true)
        {
            long LocNode=0;
            for (int j=0; j<M->Nodes[i].SupSize; j++)
            {
                LocNode=M->Nodes[i].SupNodes[j];
                if (abs(M->Nodes[LocNode].U1[0]) < abs(MaxU1[0]-MinU1[0])/M->ScaleFactor || abs(M->Nodes[LocNode].U1[1]) < abs(MaxU1[1]-MinU1[1])/M->ScaleFactor)
//                if (abs(M->Nodes[LocNode].U1[0])==0 || abs(M->Nodes[LocNode].U1[1])==0)
                {
                    M->Nodes[i].EnrichedNode=false;
                }
            }
            if (abs(M->Nodes[i].U1[0]) < abs(MaxU1[0]-MinU1[0])/M->ScaleFactor || abs(M->Nodes[i].U1[1]) < abs(MaxU1[1]-MinU1[1])/M->ScaleFactor)
            {
                M->Nodes[i].EnrichedNode=false;
            }
        }
    }
}

void GFD_Problem::GetModifiedMatricesC(Model* M, long i, MatrixXd& C1, MatrixXd& C2, MatrixXd& C3)
{
    MatrixXd U1_1(1,1);
    MatrixXd U1_2(1,1);
    MatrixXd U1_3(1,1);
    
    if (M->SingularEnrichment != "" && M->Nodes[i].EnrichedNode==true)
    {
        U1_1.resize(lTayl,lTayl);
        U1_2.resize(lTayl,lTayl);
        for (int j=0; j<lTayl;j++)
        {
            for (int k=0; k<lTayl;k++)
            {
                U1_1(j,k)=0;
                U1_2(j,k)=0;
            }
        }
        for (int j=0; j<lTayl;j++)
        {
            U1_1(j,j)=M->Nodes[i].U1[0];
            U1_2(j,j)=M->Nodes[i].U1[1];
        }
        
        if (M->SingularEnrichment=="LSHAPE")
        {
            GetU1_LShape(M,i,U1_1,U1_2,U1_3);
        }
        else if (M->SingularEnrichment=="CRACKED_PLATE")
        {
            GetU1_CrackedPlate(M,i,U1_1,U1_2,U1_3);
        }
        
        //Fill the rest of the matrix U1_1
        U1_1(3,1)=2*U1_1(1,0);
        U1_1(4,1)=U1_1(2,0);U1_1(4,2)=U1_1(1,0);
        U1_1(5,2)=2*U1_1(2,0);
        //Fill the rest of the matrix U1_2
        U1_2(3,1)=2*U1_2(1,0);
        U1_2(4,1)=U1_2(2,0);U1_2(4,2)=U1_2(1,0);
        U1_2(5,2)=2*U1_2(2,0);
        
        //Create a matrix G to transform the F into F1 at the boundary of the enrichment domain
        MatrixXd G(M->Nodes[i].SupSize+1,M->Dimension);
        for (int j=0; j<M->Nodes[i].SupSize;j++)
        {
            long LocNode=M->Nodes[i].SupNodes[j];
            if (M->Nodes[LocNode].EnrichedNode==false)
            {
                G(j+1,0)=1/M->Nodes[LocNode].U1[0];
                G(j+1,1)=1/M->Nodes[LocNode].U1[1];
            }
            else
            {
                G(j+1,0)=1;
                G(j+1,1)=1;
            }
        }
        G(0,0)=G(1,0);
        G(0,1)=G(1,1);
       
        //Create matrices D1, D2 and D3 with the 0 derivative and all derivatives
        MatrixXd D1(lTayl,M->Nodes[i].SupSize+1);
        MatrixXd D2(lTayl,M->Nodes[i].SupSize+1);
        for (int j=0; j<lTayl;j++)
        {
            for (int k=0;k<M->Nodes[i].SupSize+1;k++)
            {
                if (j==0)
                {
                    D1(j,k)=0;
                    D2(j,k)=0;
                }
                else
                {
                    D1(j,k)=C1(j-1,k)*G(k,0);
                    D2(j,k)=C1(j-1,k)*G(k,1);
                }
            }
        }
        D1(0,0)=1;
        D2(0,0)=1;
        
        //Update the marix C with the enrichment
        C1=U1_1*D1;
        C2=U1_2*D2;
    }
    else if (M->SingularEnrichment != "" && M->Nodes[i].EnrichedNode==false)
    {
        //Create a matrix G to transform the F into F1 at the boundary of the enrichment domain
        MatrixXd G(M->Nodes[i].SupSize+1,M->Dimension);
        for (int j=0; j<M->Nodes[i].SupSize;j++)
        {
            long LocNode=M->Nodes[i].SupNodes[j];
            if (M->Nodes[LocNode].EnrichedNode==true)
            {
                G(j+1,0)=1*M->Nodes[LocNode].U1[0];
                G(j+1,1)=1*M->Nodes[LocNode].U1[1];
            }
            else
            {
                G(j+1,0)=1;
                G(j+1,1)=1;
            }
        }
        G(0,0)=G(1,0);
        G(0,1)=G(1,1);
        
        //Create matrices D1 and D2 with the 0 derivative and all derivatives
        MatrixXd D1(lTayl,M->Nodes[i].SupSize+1);
        MatrixXd D2(lTayl,M->Nodes[i].SupSize+1);
        for (int j=0; j<lTayl;j++)
        {
            for (int k=0;k<M->Nodes[i].SupSize+1;k++)
            {
                if (j==0)
                {
                    D1(j,k)=0;
                    D2(j,k)=0;
                }
                else
                {
                    D1(j,k)=C1(j-1,k)*G(k,0);
                    D2(j,k)=C1(j-1,k)*G(k,1);
                }
            }
        }
        D1(0,0)=1;
        D2(0,0)=1;
        //Update the marix C with the enrichment
        C1=D1;
        C2=D2;
    }
}

void GFD_Problem::GetU1_LShape(Model* M, long i, MatrixXd& U1_1, MatrixXd& U1_2, MatrixXd& U1_3)
{
    double Alpha=0.544483737;
    double Q=0.543075579;
    double Epsilon=-3*M->pi/4;
    double Kapa=3-4*M->nu;
    
    //Get the matrices U1 and U2 to transform the U2 derivatives into U derivatives
    double Rad=M->Nodes[i].DistToSing;
    double Theta=Angle(M,i,0,M->SingularNode);
    double Theta2=3*M->pi/4+Angle(M,i,0,M->SingularNode);

    if (Theta>M->pi)
        Theta-=2*M->pi;
    if (Theta2>M->pi)
        Theta2-=2*M->pi;

    double a1_=(Kapa-Q*(Alpha+1))*cos(Alpha*Theta2)-Alpha*cos((Alpha-2)*Theta2);
    double a2_=(Kapa+Q*(Alpha+1))*sin(Alpha*Theta2)+Alpha*sin((Alpha-2)*Theta2);
    double a1_Theta=-Alpha*(Kapa-Q*(Alpha+1))*sin(Alpha*Theta2)+Alpha*(Alpha-2)*sin((Alpha-2)*Theta2);
    double a2_Theta= Alpha*(Kapa+Q*(Alpha+1))*cos(Alpha*Theta2)+Alpha*(Alpha-2)*cos((Alpha-2)*Theta2);
    double a1_ThetaTheta=-pow(Alpha,2)*(Kapa-Q*(Alpha+1))*cos(Alpha*Theta2)+Alpha*pow(Alpha-2,2)*cos((Alpha-2)*Theta2);
    double a2_ThetaTheta=-pow(Alpha,2)*(Kapa+Q*(Alpha+1))*sin(Alpha*Theta2)-Alpha*pow(Alpha-2,2)*sin((Alpha-2)*Theta2);

    double a3_=a1_*cos(Epsilon)-a2_*sin(Epsilon);
    double a3_Theta=a1_Theta*cos(Epsilon)-a2_Theta*sin(Epsilon);
    double a4_=a1_*sin(Epsilon)+a2_*cos(Epsilon);
    double a4_Theta=a1_Theta*sin(Epsilon)+a2_Theta*cos(Epsilon);

    //First column of the matrix
    if (M->FullEnrichment==false)
    {
        U1_1(1,0)=Alpha*cos(Theta)*pow(Rad,Alpha-1);
        U1_1(2,0)=Alpha*sin(Theta)*pow(Rad,Alpha-1);
        U1_1(3,0)=(Alpha*(Alpha-1)*pow(cos(Theta),2)+Alpha*pow(sin(Theta),2))*pow(Rad,Alpha-2);
        U1_1(4,0)=Alpha*(Alpha-2)*cos(Theta)*sin(Theta)*pow(Rad,Alpha-2);
        U1_1(5,0)=(Alpha*(Alpha-1)*pow(sin(Theta),2)+Alpha*pow(cos(Theta),2))*pow(Rad,Alpha-2);                
        U1_2(1,0)=U1_1(1,0);
        U1_2(2,0)=U1_1(2,0);
        U1_2(3,0)=U1_1(3,0);
        U1_2(4,0)=U1_1(4,0);
        U1_2(5,0)=U1_1(5,0);
    }
    else
    {
        double a5_,a6_,a7_,a8_;
        //Ux_x, Ux,y
        a5_=cos(Theta)*Alpha*a3_-sin(Theta)*a3_Theta;
        a6_=sin(Theta)*Alpha*a3_+cos(Theta)*a3_Theta;
        U1_1(1,0)=pow(Rad,Alpha-1)*a5_;
        U1_1(2,0)=pow(Rad,Alpha-1)*a6_;
        //Uy_x, Uy,y
        a7_=cos(Theta)*Alpha*a4_-sin(Theta)*a4_Theta;
        a8_=sin(Theta)*Alpha*a4_+cos(Theta)*a4_Theta;
        U1_2(1,0)=pow(Rad,Alpha-1)*a7_;
        U1_2(2,0)=pow(Rad,Alpha-1)*a8_;

        //Second derivatives of a3_and a4_
        double a3_ThetaTheta=a1_ThetaTheta*cos(Epsilon)-a2_ThetaTheta*sin(Epsilon);
        double a4_ThetaTheta=a1_ThetaTheta*sin(Epsilon)+a2_ThetaTheta*cos(Epsilon);

        //Derivatives of a5_and a6_
        double a5_Theta=-Alpha*a3_*sin(Theta)+cos(Theta)*(Alpha-1)*a3_Theta-sin(Theta)*a3_ThetaTheta;
        double a6_Theta= Alpha*a3_*cos(Theta)+sin(Theta)*(Alpha-1)*a3_Theta+cos(Theta)*a3_ThetaTheta;

        //Derivatives of a7_and a8_
        double a7_Theta=-Alpha*a4_*sin(Theta)+cos(Theta)*(Alpha-1)*a4_Theta-sin(Theta)*a4_ThetaTheta;
        double a8_Theta= Alpha*a4_*cos(Theta)+sin(Theta)*(Alpha-1)*a4_Theta+cos(Theta)*a4_ThetaTheta;

        //Ux_xx,Ux_xy_Ux_yy
        U1_1(3,0)=pow(Rad,Alpha-2)*(cos(Theta)*(Alpha-1)*a5_-sin(Theta)*a5_Theta);
        U1_1(4,0)=pow(Rad,Alpha-2)*(sin(Theta)*(Alpha-1)*a5_+cos(Theta)*a5_Theta);
        U1_1(5,0)=pow(Rad,Alpha-2)*(sin(Theta)*(Alpha-1)*a6_+cos(Theta)*a6_Theta);
        //Uy_xx,Uy_xy_Uy_yy
        U1_2(3,0)=pow(Rad,Alpha-2)*(cos(Theta)*(Alpha-1)*a7_-sin(Theta)*a7_Theta);
        U1_2(4,0)=pow(Rad,Alpha-2)*(sin(Theta)*(Alpha-1)*a7_+cos(Theta)*a7_Theta);
        U1_2(5,0)=pow(Rad,Alpha-2)*(sin(Theta)*(Alpha-1)*a8_+cos(Theta)*a8_Theta);
    }
}

void GFD_Problem::GetU1_CrackedPlate(Model* M, long i, MatrixXd& U1_1, MatrixXd& U1_2, MatrixXd& U1_3)
{
    double p=(1+M->nu)*pow(2*M->pi,-0.5)/M->E;
    double alpha=0.5;
    double beta=0.5;
    double rad=M->Nodes[i].DistToSing;
    double theta=Angle(M,i,0,M->SingularNode);
    if (theta>M->pi)
        theta-=2*M->pi;
    
    double a_=p*pow(rad,alpha);
    double a_r=p*alpha*pow(rad,alpha-1);
    double a_rr=p*alpha*(alpha-1)*pow(rad,alpha-2);
    
    if (M->FullEnrichment==false)
    {
        U1_1(1,0)=cos(theta)*a_r;
        U1_1(2,0)=sin(theta)*a_r;
        U1_1(3,0)=pow(cos(theta),2)*a_rr+pow(sin(theta),2)/rad*a_r;
        U1_1(4,0)=cos(theta)*sin(theta)*(a_rr-a_r/rad);
        U1_1(5,0)=pow(sin(theta),2)*a_rr+pow(cos(theta),2)/rad*a_r;            
        U1_2(1,0)=U1_1(1,0);
        U1_2(2,0)=U1_1(2,0);
        U1_2(3,0)=U1_1(3,0);
        U1_2(4,0)=U1_1(4,0);
        U1_2(5,0)=U1_1(5,0);
    }
    else
    {
        double q1=(3-M->nu)/(1+M->nu)-1; //For plane stress problems
        double q2=(3-M->nu)/(1+M->nu)+1; //For plane stress problems
        double b1_=cos(beta*theta)*(q1+2*pow(sin(beta*theta),2));
        double b2_=sin(beta*theta)*(q2-2*pow(cos(beta*theta),2));
        double b1_theta=-beta*sin(beta*theta)*(q1+2*pow(sin(beta*theta),2))+cos(beta*theta)*(4*beta*cos(beta*theta)*sin(beta*theta));
        double b2_theta=+beta*cos(beta*theta)*(q2-2*pow(cos(beta*theta),2))+sin(beta*theta)*(4*beta*cos(beta*theta)*sin(beta*theta));
        double b1_thetatheta=(4*beta*cos(beta*theta)*sin(beta*theta))*(-2*beta*sin(beta*theta))-pow(beta,2)*cos(beta*theta)*(q1+2*pow(sin(beta*theta),2))+cos(beta*theta)*pow(beta,2)*(-4*pow(sin(beta*theta),2)+4*pow(cos(beta*theta),2));
        double b2_thetatheta=(4*beta*cos(beta*theta)*sin(beta*theta))*(+2*beta*cos(beta*theta))-pow(beta,2)*sin(beta*theta)*(q2-2*pow(sin(beta*theta),2))+sin(beta*theta)*pow(beta,2)*(-4*pow(sin(beta*theta),2)+4*pow(cos(beta*theta),2));

        //Ux_x, Ux,y
        U1_1(1,0)=cos(theta)*a_r*b1_-sin(theta)/rad*a_*b1_theta;
        U1_1(2,0)=sin(theta)*a_r*b1_+cos(theta)/rad*a_*b1_theta;
        //Uy_x, Uy,y
        U1_2(1,0)=cos(theta)*a_r*b2_-sin(theta)/rad*a_*b2_theta;
        U1_2(2,0)=sin(theta)*a_r*b2_+cos(theta)/rad*a_*b2_theta;

        //Ux_xx,Ux_xy_Ux_yy
        double c1_1=cos(theta)*a_rr*b1_+sin(theta)/pow(rad,2)*a_*b1_theta-sin(theta)/rad*a_r*b1_theta;
        double c2_1=-sin(theta)*a_r*b1_+cos(theta)*a_r*b1_theta-cos(theta)/rad*a_*b1_theta-sin(theta)/rad*a_*b1_thetatheta;
        U1_1(3,0)=cos(theta)*c1_1-sin(theta)/rad*c2_1;
        U1_1(4,0)=sin(theta)*c1_1+cos(theta)/rad*c2_1;
        double c3_1=sin(theta)*a_rr*b1_-cos(theta)/pow(rad,2)*a_*b1_theta+cos(theta)/rad*a_r*b1_theta;
        double c4_1=cos(theta)*a_r*b1_+sin(theta)*a_r*b1_theta-sin(theta)/rad*a_*b1_theta+cos(theta)/rad*a_*b1_thetatheta;
        U1_1(5,0)=sin(theta)*c3_1+cos(theta)/rad*c4_1;
        
        //Uy_xx,Uy_xy_Uy_yy
        double c1_2=cos(theta)*a_rr*b2_+sin(theta)/pow(rad,2)*a_*b2_theta-sin(theta)/rad*a_r*b2_theta;
        double c2_2=-sin(theta)*a_r*b2_+cos(theta)*a_r*b2_theta-cos(theta)/rad*a_*b2_theta-sin(theta)/rad*a_*b2_thetatheta;
        U1_2(3,0)=cos(theta)*c1_2-sin(theta)/rad*c2_2;
        U1_2(4,0)=sin(theta)*c1_2+cos(theta)/rad*c2_2;
        double c3_2=sin(theta)*a_rr*b2_-cos(theta)/pow(rad,2)*a_*b2_theta+cos(theta)/rad*a_r*b2_theta;
        double c4_2=cos(theta)*a_r*b2_+sin(theta)*a_r*b2_theta-sin(theta)/rad*a_*b2_theta+cos(theta)/rad*a_*b2_thetatheta;
        U1_2(5,0)=sin(theta)*c3_2+cos(theta)/rad*c4_2;
    }
}