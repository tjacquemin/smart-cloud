#include "GFD_Intrinsic.h"

GFD_Intrinsic::GFD_Intrinsic() {
}

GFD_Intrinsic::GFD_Intrinsic(const GFD_Intrinsic& orig) {
}

GFD_Intrinsic::~GFD_Intrinsic() {
}

void GFD_Intrinsic::InitializeProblem(Model* M, long* TripStressTreadStartIncTemp)
{
    Dimension=M->Dimension;
    TripStressTreadStartInc=TripStressTreadStartIncTemp;
    
    //Get the terms of the Hooke’s Law
    if (M->ModelType=="2D_PLANE_STRAIN" || Dimension==3)
    {
        d1=M->E*(1-M->nu)/((1+M->nu)*(1-2*M->nu));
        d2=M->E*M->nu/((1+M->nu)*(1-2*M->nu));
        d3=M->E*(1-2*M->nu)/((1+M->nu)*(1-2*M->nu));
    }
    else if (M->ModelType=="2D_PLANE_STRESS")
    {
        d1=M->E/(1-pow(M->nu,2));
        d2=M->E*M->nu/(1-pow(M->nu,2));
        d3=M->E*(1-M->nu)/(1-pow(M->nu,2));
    }
    
    //Get the dimension of the matrix of the Taylor's expansion
    if (Dimension==2)
    {
        if (M->ApproxOrder==2)
        {
            lTayl=6;
        }
        else if (M->ApproxOrder==3)
        {
            lTayl=10;
        }
        else if (M->ApproxOrder==4)
        {
            lTayl=15;
        }
    }
    else if (Dimension==3)
    {
        lTayl=10;
    }
}

void GFD_Intrinsic::LoadDerivativeCoefficients(Model* M, Node* Nodes, long NodeNum, int ThreadIndex, int TreadNum)
{
    long Start, End;
    //Get the start and end node of the thread
    M->GetStartEndNode(ThreadIndex,TreadNum,Start, End,NodeNum);
    
    //Add the coefficients to the defined functions
    for (long i=Start; i<=End; i++)
    {
        if (Start==1)
        {
            M->PrintLoading(i-Start,End-Start,"Node Derivative Loading: ", false);
        }
        
        //Discard the enrichment for the singular node
        bool Enrich=true;
        double Dist=pow(Nodes[i].X[0],2)+pow(Nodes[i].X[1],2);
        if (M->FNum==2 || M->FNum==3)
        {
            Dist=pow(Nodes[i].X[0],2);
            if (pow(Nodes[i].X[1],2)<Dist)
                Dist=pow(Nodes[i].X[1],2);
        }
        if (Dist==0)
        {
            Enrich=false;
            cout << "Enrich False: " << i << ", M->NumberEnrichmentCoeff=" << M->NumberEnrichmentCoeff << endl;
        }
        
        //Get the initial matrix C1
        MatrixXd C1=C_Matrix(M,&Nodes[i],Enrich);
        
        //Fill the matrix K for each particle of the support
        int StartIndex=0;
        if (M->Inter==true)
        {
            StartIndex=1;
        }
        for (long j=0; j<Nodes[i].SupSize; j++)
        {
            if (M->Inter==true)
            {
                Nodes[i].Coeff[j][0]=0;
            }   
            for (int jj=StartIndex; jj<lTayl; jj++)
            {
                Nodes[i].Coeff[j][jj]=C1(jj-StartIndex,j);
            }
            for (int jj=lTayl; jj<lTayl+M->NumberEnrichmentCoeff; jj++)
            {
                if (Enrich==true)
                {
                    Nodes[i].CoeffEnrich[j][jj-lTayl]=C1(jj-StartIndex,j);
                }
                else
                {
                    Nodes[i].CoeffEnrich[j][jj-lTayl]=0;
                }
            }
        }
    }
}

MatrixXd GFD_Intrinsic::C_Matrix(Model* M, Node* CollocNode, bool Enrichment)
{
    MatrixXd A(1,1);
    MatrixXd A2(1,1);
    MatrixXd B(1,1);
    MatrixXd C;
    MatrixXd P(1,1);
    VectorXd PTemp(1);
    MatrixXd W(1,1);
    int SupSize=CollocNode->SupSize;
    int MatA_Size=lTayl;
    if (Enrichment==true)
    {
        MatA_Size=lTayl+M->NumberEnrichmentCoeff;
    }
    
    A.resize(MatA_Size,MatA_Size);
    
    int MatA_Size2=MatA_Size;
    if (M->Inter==true)
    {
        MatA_Size2=MatA_Size-1;
        A2.resize(MatA_Size2,MatA_Size2);
    }
    B.resize(MatA_Size2,SupSize);
    P.resize(MatA_Size,SupSize);
    PTemp.resize(MatA_Size);
    W.resize(SupSize,SupSize);
    //Initialize the matrices P, W and B3;
    for (int i=0; i<SupSize; i++)
    {
        for (int j=0; j<SupSize; j++)
        {
            W(i,j)=0;
        }
        for (int j=0; j<MatA_Size; j++)
        {
            P(j,i)=0;
        }
        for (int j=0; j<MatA_Size2; j++)
        {
            B(j,i)=0;
        }
    }
    
    //Fill the matrices P, W and B
    for (int i=0; i<SupSize; i++)
    {
        double z_i=0;
        long LocNodeNum=CollocNode->SupNodes[i];
        
        //Fill the matrix W
        {
            //Get the distance Z_i of the support node to the reference Node
            for (int m=0; m<Dimension; m++)
            {
                z_i+=pow(M->Nodes[LocNodeNum].X[m]-CollocNode->X[m],2);
            }
            z_i=pow(z_i,0.5);
            //Get Voronoi weight if the function is selected
            double SupNodeVol=1;
            if (M->Voronoi==true && CollocNode->BoundaryNode==false)
                SupNodeVol=CollocNode->SupNodeVol[i];

            //Calculate the weight associated to the considered support node
            double SupRad=CollocNode->SupRadius;
            if (CollocNode->QuadSelection==true)
                SupRad=CollocNode->SupRadA[i];

            double W_=pow(SupNodeVol*M->w(z_i/SupRad),2);
            if (CollocNode->QuadSelection==true)
            {
                W_=CollocNode->SupNodeW[i];
            }
            W(i,i)=W_;
        }
        
        //Fill the matrix P
        {
            for (int j=0; j<lTayl; j++)
            {
                double Value1=1;
                for (int m=0; m<Dimension; m++)
                {
                    Value1=Value1*pow(M->Nodes[LocNodeNum].X[m]-CollocNode->X[m],M->Exponents(j,m))/M->factorial(M->Exponents(j,m));
                }
                if (M->SingProd==true)
                {
                    PTemp(j)=Value1;
                }
                else
                {
                    P(j,i)=Value1;
                }
            }
            if (M->SingProd==true && M->SingProdSplit==false)
            {
                P(0,i)=PTemp(0)*M->GetEnrichmentFunctionVal(CollocNode->NodeNum,M->FNum,1,0)+PTemp(1)*M->GetEnrichmentFunctionVal(CollocNode->NodeNum,M->FNum,1,1)
                      +PTemp(2)*M->GetEnrichmentFunctionVal(CollocNode->NodeNum,M->FNum,1,2)+PTemp(3)*M->GetEnrichmentFunctionVal(CollocNode->NodeNum,M->FNum,1,3)
                      +PTemp(4)*M->GetEnrichmentFunctionVal(CollocNode->NodeNum,M->FNum,1,4)+PTemp(5)*M->GetEnrichmentFunctionVal(CollocNode->NodeNum,M->FNum,1,5);
                P(1,i)=PTemp(1)*M->GetEnrichmentFunctionVal(CollocNode->NodeNum,M->FNum,1,0)+2*PTemp(3)*M->GetEnrichmentFunctionVal(CollocNode->NodeNum,M->FNum,1,1)
                      +PTemp(4)*M->GetEnrichmentFunctionVal(CollocNode->NodeNum,M->FNum,1,2);
                P(2,i)=PTemp(2)*M->GetEnrichmentFunctionVal(CollocNode->NodeNum,M->FNum,1,0)+2*PTemp(5)*M->GetEnrichmentFunctionVal(CollocNode->NodeNum,M->FNum,1,2)
                      +PTemp(4)*M->GetEnrichmentFunctionVal(CollocNode->NodeNum,M->FNum,1,1);
                P(3,i)=PTemp(3)*M->GetEnrichmentFunctionVal(CollocNode->NodeNum,M->FNum,1,0);
                P(4,i)=PTemp(4)*M->GetEnrichmentFunctionVal(CollocNode->NodeNum,M->FNum,1,0);
                P(5,i)=PTemp(5)*M->GetEnrichmentFunctionVal(CollocNode->NodeNum,M->FNum,1,0);
            }
            else if (M->SingProd==true && M->SingProdSplit==true)
            {
                P(0,i)=PTemp(0);
                P(1,i)=PTemp(1);
                P(2,i)=PTemp(2);
                P(3,i)=PTemp(3);
                P(4,i)=PTemp(4);
                P(5,i)=PTemp(5);
            }
            double Factor=1;
            if (M->Inter==true)
            {
                Factor=0;
            }
            if (Enrichment==true && M->SingProd==false)
            {
                for (int j=lTayl; j<lTayl+M->NumberEnrichmentCoeff; j++)
                {
                    P(j,i)= P(0,i)*M->GetEnrichmentFunctionVal(CollocNode->NodeNum,M->FNum,j-lTayl+1,0)*Factor+P(1,i)*M->GetEnrichmentFunctionVal(CollocNode->NodeNum,M->FNum,j-lTayl+1,1)+
                            P(2,i)*M->GetEnrichmentFunctionVal(CollocNode->NodeNum,M->FNum,j-lTayl+1,2)+P(3,i)*M->GetEnrichmentFunctionVal(CollocNode->NodeNum,M->FNum,j-lTayl+1,3)+
                            P(4,i)*M->GetEnrichmentFunctionVal(CollocNode->NodeNum,M->FNum,j-lTayl+1,4)+P(5,i)*M->GetEnrichmentFunctionVal(CollocNode->NodeNum,M->FNum,j-lTayl+1,5);
                }
            }
        }
        
        //Fill the matrix B
        {
            int StartIndex=0;
            if (M->Inter==true)
            {
                StartIndex=1;
            }
            for (int j=0; j<MatA_Size2; j++)
            {
                B(j,i)= W(i,i)*P(j+StartIndex,i);
            }
        }
    }
    
    A=P*W*P.transpose();
    
    //Fill the matrix A2 and fill the first column of B
    if (M->Inter==true)
    {
        for (int i=0; i<MatA_Size2; i++)
        {
            for (int j=0; j<MatA_Size2; j++)
            {
                A2(i,j)=A(i+1,j+1);
            }
            B(i,0)=-A(0,i+1);
        }
    }
    else
    {
        A2=A;
    }
    //Get the inverse of the matrix A2
    MatrixXd A2Inv=A2.inverse();
    double cond=0;
    if(M->PrintConditionNumber==1)
    {
        //Get the matrix conditioning
        cond = A2Inv.norm()*A2.norm();
        if (MinCond==0 && MaxCond==0)
        {
            MinCond=cond;
            MaxCond=cond;
        }
        if (cond>MaxCond)
        {
            MaxCond=cond;
        }
        if (cond<MinCond)
        {
            MinCond=cond;
        }
    }
    
    C=A2Inv*B;
    return C;
}

void GFD_Intrinsic::LoadVectors(Model* M,double* F_Vec,vector<vector<Trow>>& StiffRows,T* TripStress,T* TripStrain, 
        int ThreadIndex, int TreadNum)
{
    long Start, End;
    
    //Get the start and end node of the thread
    M->GetStartEndNode(ThreadIndex,TreadNum,Start, End,M->NodeNumber);
    
    long StressMatInc=TripStressTreadStartInc[ThreadIndex];
    long StiffMatInc=TripStiffTreadStartInc[ThreadIndex];
    
    //Resize the StiffRows vector to the number of rows
    StiffRows[ThreadIndex].resize((End-Start+1)*Dimension);
    
    for (long i=Start; i<=End; i++)
    {
        int SupSize;
        long LocNodeNum;
        
        //Fill the matrix K for each particle of the suppot
        SupSize=M->Nodes[i].SupSize;
        int IndexOffset=1;
        if (M->Nodes[i].CollocationPointOffset==true)
        {
            IndexOffset=0;
        }
        
        //Resize the current row of the StiffRows vector to the number of triplets
        if (M->Nodes[i].BoundaryNode==false || M->BCTreatType=="LAGRANGE" || M->InterpBC==true)
        {
            for (int j=0; j<Dimension; j++)
            {
                StiffRows[ThreadIndex][Dimension*(i-Start)+j].Trip.resize(SupSize*Dimension);
            }
        }
        if (M->Nodes[i].BoundaryNode==true && M->InterpBC==false)
        {
            for (int j=0; j<Dimension; j++)
            {
                int TripSize=0;
                if (M->Nodes[i].BC_D_Bool[j]==true)
                {
                    TripSize=SupSize*Dimension;
                }
                else if (M->Nodes[i].BC_N_Bool[j]==true)
                {
                    TripSize=SupSize*Dimension;
                }
                StiffRows[ThreadIndex][Dimension*(i-Start)+j].Trip.resize(TripSize);
            }
        }
        
        for (long j=0; j<SupSize; j++)
        {
            VectorXd Coeff(1);
            Coeff.resize(lTayl+M->NumberEnrichmentCoeff);
            for (int jj=0; jj<lTayl; jj++)
            {
                Coeff(jj)=M->Nodes[i].Coeff[j][jj];
            }
            for (int jj=lTayl; jj<lTayl+M->NumberEnrichmentCoeff; jj++)
            {
                Coeff(jj)=M->Nodes[i].CoeffEnrich[j][jj-lTayl];
            }
            //Get the number of the support or reference node
            LocNodeNum=M->Nodes[i].SupNodes[j];
            if (Dimension==2)
            {
                LoadRows2D_Stress(M,Coeff,i,LocNodeNum,j,SupSize,TripStress,StressMatInc);
                if (M->PrintStrain==true || M->ErrEstimator==true)
                {
//                    LoadRows2D_Strain(Coeff,Coeff2,i,LocNodeNum,j,SupSize,TripStrain,StressMatInc);
                }
                LoadRows2D_Stiff(M,Coeff,i,LocNodeNum,j,SupSize,F_Vec,StiffRows,StiffMatInc,ThreadIndex,i-Start);
            }
            else if (Dimension==3)
            {
//                LoadRows3D_Stress(Coeff,i,LocNodeNum,j,SupSize,TripStress,StressMatInc);
//                LoadRows3D_Stiff(M,Coeff,i,LocNodeNum,j,SupSize,F_Vec,TripStiff,StiffMatInc);
            }
        }
        
        for (int k=0; k<Dimension; k++)
        {
            if (M->Nodes[i].BoundaryNode==false)
                StiffMatInc+=SupSize*Dimension;
            else if (M->Nodes[i].BC_N_Bool[k]==true || M->Nodes[i].BC_D_Bool[k]==true)
                StiffMatInc+=SupSize*Dimension;
        }
        StressMatInc+=SupSize*(6+9*(Dimension-2));
    }
}

void GFD_Intrinsic::LoadRows2D_Stress(Model* M,VectorXd Coeff,long i,long LocNodeNum,int j, int SupSize,T* TripStress, long& StressMatInc)
{
    //Load the triplet vectors - Method GFD
    double TempVal1,TempVal2,TempVal3,TempVal4,TempVal5,TempVal6;
    double a=0;
    double b=0;
    if (M->NumberEnrichmentCoeff>0)
    {
        a=Coeff(6);
    }
    if (M->FNum==2)
    {
        b=Coeff(7);
    }
    Coeff(0)=0;
    if (i==LocNodeNum)
        Coeff(0)=1;
    
    //Stress Matrix - Equation 1 - Sigma11
    if (M->SingProd==true)
    {
        TempVal1=d1*(Coeff(0)*M->GetEnrichmentFunctionVal(i,M->FNum,1,1)+Coeff(1)*M->GetEnrichmentFunctionVal(i,M->FNum,1,0));
        TempVal2=d2*(Coeff(0)*M->GetEnrichmentFunctionVal(i,M->FNum,1,2)+Coeff(2)*M->GetEnrichmentFunctionVal(i,M->FNum,1,0));
    }
    else
    {
        TempVal1=d1*(Coeff(1)+a*M->GetEnrichmentFunctionVal(i,M->FNum,1,1));
        TempVal2=d2*(Coeff(2)+a*M->GetEnrichmentFunctionVal(i,M->FNum,1,2));
    }
    
    if (M->FNum==2)
    {
        TempVal1+=b*M->GetEnrichmentFunctionVal(i,M->FNum,2,1);
        TempVal2+=b*M->GetEnrichmentFunctionVal(i,M->FNum,2,2);
    }
    TripStress[StressMatInc+SupSize*Dimension*0+j*Dimension+0]=T((i-1)*(Dimension+1)+0,(LocNodeNum-1)*Dimension+0,TempVal1);
    TripStress[StressMatInc+SupSize*Dimension*0+j*Dimension+1]=T((i-1)*(Dimension+1)+0,(LocNodeNum-1)*Dimension+1,TempVal2);
    //Stress Matrix - Equation 2 - Sigma12
    if (M->SingProd==true)
    {
        TempVal3=d3/2*(Coeff(2)*M->GetEnrichmentFunctionVal(i,M->FNum,1,0)+Coeff(0)*M->GetEnrichmentFunctionVal(i,M->FNum,1,2));
        TempVal4=d3/2*(Coeff(1)*M->GetEnrichmentFunctionVal(i,M->FNum,1,0)+Coeff(0)*M->GetEnrichmentFunctionVal(i,M->FNum,1,1));
    }
    else
    {
        TempVal3=d3/2*(Coeff(2)+a*M->GetEnrichmentFunctionVal(i,M->FNum,1,2));
        TempVal4=d3/2*(Coeff(1)+a*M->GetEnrichmentFunctionVal(i,M->FNum,1,1));
    }
    if (M->FNum==2)
    {
        TempVal3+=b*M->GetEnrichmentFunctionVal(i,M->FNum,2,2);
        TempVal4+=b*M->GetEnrichmentFunctionVal(i,M->FNum,2,1);
    }
    TripStress[StressMatInc+SupSize*Dimension*1+j*Dimension+0]=T((i-1)*(Dimension+1)+1,(LocNodeNum-1)*Dimension+0,TempVal3);
    TripStress[StressMatInc+SupSize*Dimension*1+j*Dimension+1]=T((i-1)*(Dimension+1)+1,(LocNodeNum-1)*Dimension+1,TempVal4);
    //Stress Matrix - Equation 3 - Sigma22
    if (M->SingProd==true)
    {
        TempVal5=d2*(Coeff(1)*M->GetEnrichmentFunctionVal(i,M->FNum,1,0)+Coeff(0)*M->GetEnrichmentFunctionVal(i,M->FNum,1,1));
        TempVal6=d1*(Coeff(2)*M->GetEnrichmentFunctionVal(i,M->FNum,1,0)+Coeff(0)*M->GetEnrichmentFunctionVal(i,M->FNum,1,2));
    }
    else
    {
        TempVal5=d2*(Coeff(1)+a*M->GetEnrichmentFunctionVal(i,M->FNum,1,1));
        TempVal6=d1*(Coeff(2)+a*M->GetEnrichmentFunctionVal(i,M->FNum,1,2));
    }
    if (M->FNum==2)
    {
        TempVal5+=b*M->GetEnrichmentFunctionVal(i,M->FNum,2,1);
        TempVal6+=b*M->GetEnrichmentFunctionVal(i,M->FNum,2,2);
    }
    TripStress[StressMatInc+SupSize*Dimension*2+j*Dimension+0]=T((i-1)*(Dimension+1)+2,(LocNodeNum-1)*Dimension+0,TempVal5);
    TripStress[StressMatInc+SupSize*Dimension*2+j*Dimension+1]=T((i-1)*(Dimension+1)+2,(LocNodeNum-1)*Dimension+1,TempVal6);
}

void GFD_Intrinsic::LoadRows2D_Stiff(Model* M,VectorXd Coeff,long i,long LocNodeNum,int j, int SupSize, double* F_Vec,vector<vector<Trow>>& StiffRows, long& StiffMatInc, int ThreadIndex, long ThrRowIndex)
{
    double TempValU1_1,TempValU1_2,TempValU2_1,TempValU2_2;
    double u_=Coeff(0);
    double a=0;
    double b=0;
    if (M->NumberEnrichmentCoeff>0)
    {
        a=Coeff(6);
    }
    if (M->FNum==2)
    {
        b=Coeff(7);
    }
    Coeff(0)=0;
    if (i==LocNodeNum)
        Coeff(0)=1;
    
    //Compute the equilibrium equation
    if (M->Nodes[i].BoundaryNode==false || M->Stabilization==true)
    {
        //Equilibrium - Equation 1
        double U_xx=Coeff(3)*M->GetEnrichmentFunctionVal(i,M->FNum,1,0)+2*Coeff(1)*M->GetEnrichmentFunctionVal(i,M->FNum,1,1)+Coeff(0)*M->GetEnrichmentFunctionVal(i,M->FNum,1,3);
        double U_xy=Coeff(4)*M->GetEnrichmentFunctionVal(i,M->FNum,1,0)+Coeff(1)*M->GetEnrichmentFunctionVal(i,M->FNum,1,2)+Coeff(2)*M->GetEnrichmentFunctionVal(i,M->FNum,1,1)+Coeff(0)*M->GetEnrichmentFunctionVal(i,M->FNum,1,4);
        double U_yy=Coeff(5)*M->GetEnrichmentFunctionVal(i,M->FNum,1,0)+2*Coeff(2)*M->GetEnrichmentFunctionVal(i,M->FNum,1,2)+Coeff(0)*M->GetEnrichmentFunctionVal(i,M->FNum,1,5);
        if (M->SingProd==true)
        {
            TempValU1_1=d1*U_xx+d3/2*U_yy;
            TempValU1_2=(d2+d3/2)*U_xy;
        }
        else
        {
            TempValU1_1=d1*(Coeff(3)+a*M->GetEnrichmentFunctionVal(i,M->FNum,1,3))+d3/2*(Coeff(5)+a*M->GetEnrichmentFunctionVal(i,M->FNum,1,5));
            TempValU1_2=(d2+d3/2)*(Coeff(4)+a*M->GetEnrichmentFunctionVal(i,M->FNum,1,4));
        }
        
        //Equilibrium - Equation 2
        if (M->SingProd==true)
        {
            TempValU2_1=(d2+d3/2)*U_xy;
            TempValU2_2=d1*U_yy+d3/2*U_xx;
        }
        else
        {
            TempValU2_1=(d2+d3/2)*(Coeff(4)+a*M->GetEnrichmentFunctionVal(i,M->FNum,1,4));
            TempValU2_2=d1*(Coeff(5)+a*M->GetEnrichmentFunctionVal(i,M->FNum,1,5))+d3/2*(Coeff(3)+a*M->GetEnrichmentFunctionVal(i,M->FNum,1,3));
        }
        if (M->FNum==2)
        {
            //Equilibrium - Equation 1
            TempValU1_1+=d1*(b*M->GetEnrichmentFunctionVal(i,M->FNum,2,3))+d3/2*(b*M->GetEnrichmentFunctionVal(i,M->FNum,2,5));
            TempValU1_2+=(d2+d3/2)*(b*M->GetEnrichmentFunctionVal(i,M->FNum,2,4));
            //Equilibrium - Equation 2
            TempValU2_1+=(d2+d3/2)*(b*M->GetEnrichmentFunctionVal(i,M->FNum,2,4));
            TempValU2_2+=d1*(b*M->GetEnrichmentFunctionVal(i,M->FNum,2,5))+d3/2*(b*M->GetEnrichmentFunctionVal(i,M->FNum,2,3));
        }
    }
    //If no boundary condition is applied to the node
    if (M->Nodes[i].BoundaryNode==false)
    {
        //Equilibrium - Equation 1
//        TripStiff[StiffMatInc+SupSize*Dimension*0+j*Dimension+0]=T((i-1)*Dimension+0,(LocNodeNum-1)*Dimension+0,TempValU1_1);
//        TripStiff[StiffMatInc+SupSize*Dimension*0+j*Dimension+1]=T((i-1)*Dimension+0,(LocNodeNum-1)*Dimension+1,TempValU1_2);
        StiffRows[ThreadIndex][ThrRowIndex*Dimension+0].Trip[j*Dimension+0]=T((i-1)*Dimension+0,(LocNodeNum-1)*Dimension+0,TempValU1_1);
        StiffRows[ThreadIndex][ThrRowIndex*Dimension+0].Trip[j*Dimension+1]=T((i-1)*Dimension+0,(LocNodeNum-1)*Dimension+1,TempValU1_2);
        //Equilibrium - Equation 2
//        TripStiff[StiffMatInc+SupSize*Dimension*1+j*Dimension+0]=T((i-1)*Dimension+1,(LocNodeNum-1)*Dimension+0,TempValU2_1);
//        TripStiff[StiffMatInc+SupSize*Dimension*1+j*Dimension+1]=T((i-1)*Dimension+1,(LocNodeNum-1)*Dimension+1,TempValU2_2);
        StiffRows[ThreadIndex][ThrRowIndex*Dimension+1].Trip[j*Dimension+0]=T((i-1)*Dimension+1,(LocNodeNum-1)*Dimension+0,TempValU2_1);
        StiffRows[ThreadIndex][ThrRowIndex*Dimension+1].Trip[j*Dimension+1]=T((i-1)*Dimension+1,(LocNodeNum-1)*Dimension+1,TempValU2_2);
        //Set the value of the force vector
        if (j==0)
        {
            F_Vec[(i-1)*Dimension+0]=0;
            F_Vec[(i-1)*Dimension+1]=0;
        }
    }
    else
    {
        //Load the triplet vectors - Method GFD
        double TempVal11_1,TempVal11_2,TempVal12_1,TempVal12_2,TempVal22_1,TempVal22_2;
        //Stress Matrix - Equation 1 - Sigma11
        if (M->SingProd==true)
        {
            TempVal11_1=d1*(Coeff(0)*M->GetEnrichmentFunctionVal(i,M->FNum,1,1)+Coeff(1)*M->GetEnrichmentFunctionVal(i,M->FNum,1,0));
            TempVal11_2=d2*(Coeff(0)*M->GetEnrichmentFunctionVal(i,M->FNum,1,2)+Coeff(2)*M->GetEnrichmentFunctionVal(i,M->FNum,1,0));
        }
        else
        {
            TempVal11_1=d1*(Coeff(1)+a*M->GetEnrichmentFunctionVal(i,M->FNum,1,1));
            TempVal11_2=d2*(Coeff(2)+a*M->GetEnrichmentFunctionVal(i,M->FNum,1,2));
        }
        
        //Stress Matrix - Equation 2 - Sigma12
        if (M->SingProd==true)
        {
            TempVal12_1=d3/2*(Coeff(2)*M->GetEnrichmentFunctionVal(i,M->FNum,1,0)+Coeff(0)*M->GetEnrichmentFunctionVal(i,M->FNum,1,2));
            TempVal12_2=d3/2*(Coeff(1)*M->GetEnrichmentFunctionVal(i,M->FNum,1,0)+Coeff(0)*M->GetEnrichmentFunctionVal(i,M->FNum,1,1));
        }
        else
        {
            TempVal12_1=d3/2*(Coeff(2)+a*M->GetEnrichmentFunctionVal(i,M->FNum,1,2));
            TempVal12_2=d3/2*(Coeff(1)+a*M->GetEnrichmentFunctionVal(i,M->FNum,1,1));
        }
        
        //Stress Matrix - Equation 3 - Sigma22
        if (M->SingProd==true)
        {
            TempVal22_1=d2*(Coeff(1)*M->GetEnrichmentFunctionVal(i,M->FNum,1,0)+Coeff(0)*M->GetEnrichmentFunctionVal(i,M->FNum,1,1));
            TempVal22_2=d1*(Coeff(2)*M->GetEnrichmentFunctionVal(i,M->FNum,1,0)+Coeff(0)*M->GetEnrichmentFunctionVal(i,M->FNum,1,2));
        }
        else
        {
            TempVal22_1=d2*(Coeff(1)+a*M->GetEnrichmentFunctionVal(i,M->FNum,1,1));
            TempVal22_2=d1*(Coeff(2)+a*M->GetEnrichmentFunctionVal(i,M->FNum,1,2));
        }
        if (M->FNum==2)
        {
            //Stress Matrix - Equation 1 - Sigma11
            TempVal11_1+=d1*(b*M->GetEnrichmentFunctionVal(i,M->FNum,2,1));
            TempVal11_2+=d2*(b*M->GetEnrichmentFunctionVal(i,M->FNum,2,2));
            //Stress Matrix - Equation 2 - Sigma12
            TempVal12_1+=d3/2*(b*M->GetEnrichmentFunctionVal(i,M->FNum,2,2));
            TempVal12_2+=d3/2*(b*M->GetEnrichmentFunctionVal(i,M->FNum,2,1));
            //Stress Matrix - Equation 3 - Sigma22
            TempVal22_1+=d2*(b*M->GetEnrichmentFunctionVal(i,M->FNum,2,1));
            TempVal22_2+=d1*(b*M->GetEnrichmentFunctionVal(i,M->FNum,2,2));
        }
        int IncBound=0;
        for (int k=0; k<Dimension; k++)
        {
            //Set the Dirichlet BC if any
            if (M->Nodes[i].BC_D_Bool[k]==true)
            {
                double U_Comb=0;
                double TempVal7=0;
                double TempVal8=0;
                if (M->Inter==true && j==0)
                {
                    U_Comb=1;
                }
                else if (M->Inter==true && j!=0)
                {
                    U_Comb=0;
                }
                else if (M->Inter==false)
                {
                    if (M->SingProd==true)
                    {
                        U_Comb=u_*M->GetEnrichmentFunctionVal(i,M->FNum,1,0);
                    }
                    else
                    {
                        U_Comb=u_+a*M->GetEnrichmentFunctionVal(i,M->FNum,1,0);
                    }
                    if (M->FNum==2)
                    {
                        U_Comb+=b*M->GetEnrichmentFunctionVal(i,M->FNum,2,0);
                    }
                }
                if (k==0)
                {
                    TempVal7=U_Comb;
                }
                else if (k==1)
                {
                    TempVal8=U_Comb;
                }
//                TripStiff[StiffMatInc+SupSize*Dimension*k+j*Dimension+0]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+0,TempVal7);
//                TripStiff[StiffMatInc+SupSize*Dimension*k+j*Dimension+1]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+1,TempVal8);
                StiffRows[ThreadIndex][ThrRowIndex*Dimension+k].Trip[j*Dimension+0]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+0,TempVal7);
                StiffRows[ThreadIndex][ThrRowIndex*Dimension+k].Trip[j*Dimension+1]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+1,TempVal8);
                if (j==0)
                {
                    if (M->SingProd==true && M->Inter==true)
                    {
                        F_Vec[(i-1)*Dimension+k]=M->Nodes[i].BC_D[k]/M->GetEnrichmentFunctionVal(i,M->FNum,1,0);
                    }
                    else
                    {
                        F_Vec[(i-1)*Dimension+k]=M->Nodes[i].BC_D[k];
                    }
                }
            }
            //Set the Neumann BC if no Dirichlet BC is present for the considered DOF
            else if (M->Nodes[i].BC_N_Bool[k]==true && M->Nodes[i].BC_D_Bool[k]==false)
            {
                double theta=0;
                if (M->Nodes[i].LocalBoundaryCondition[k]==true)
                {
                    theta=Angle(M,i,k,-1);
                }
                if (M->Nodes[i].BC_DOF[k]==0)
                {
                    double TempVal7,TempVal8;
                    if (M->Nodes[i].LocalBoundaryCondition[k]==true)
                    {
                        double TempVal11_1_loc=TempVal11_1*pow(cos(theta),2)+TempVal12_1*sin(2*theta)+TempVal22_1*pow(sin(theta),2);
                        double TempVal11_2_loc=TempVal11_2*pow(cos(theta),2)+TempVal12_2*sin(2*theta)+TempVal22_2*pow(sin(theta),2);
                        double TempVal12_1_loc=-0.5*TempVal11_1*sin(2*theta)+TempVal12_1*cos(2*theta)+0.5*TempVal22_1*sin(2*theta);
                        double TempVal12_2_loc=-0.5*TempVal11_2*sin(2*theta)+TempVal12_2*cos(2*theta)+0.5*TempVal22_2*sin(2*theta);
                        TempVal7=TempVal11_1_loc*1 + TempVal12_1_loc*0;
                        TempVal8=TempVal11_2_loc*1 + TempVal12_2_loc*0;
                    }
                    else
                    {
                        TempVal7=TempVal11_1*M->Nodes[i].Normal[k][0] + TempVal12_1*M->Nodes[i].Normal[k][1];
                        TempVal8=TempVal11_2*M->Nodes[i].Normal[k][0] + TempVal12_2*M->Nodes[i].Normal[k][1];
                    }
                    if ((M->PartialStab==false && M->Stabilization==true) || (M->PartialStab==true && M->Nodes[i].BC_N[0] + M->Nodes[i].BC_N[1] == 0))
                    {
                        TempVal7=TempVal7-0.5*M->Nodes[i].CharactLength*M->Nodes[i].ProdN*TempValU1_1;
                        TempVal8=TempVal8-0.5*M->Nodes[i].CharactLength*M->Nodes[i].ProdN*TempValU1_2;
                    }
//                    TripStiff[StiffMatInc+SupSize*Dimension*k+j*Dimension+0]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+0,TempVal7);
//                    TripStiff[StiffMatInc+SupSize*Dimension*k+j*Dimension+1]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+1,TempVal8);
                    StiffRows[ThreadIndex][ThrRowIndex*Dimension+k].Trip[j*Dimension+0]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+0,TempVal7);
                    StiffRows[ThreadIndex][ThrRowIndex*Dimension+k].Trip[j*Dimension+1]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+1,TempVal8);
                    IncBound+=SupSize*Dimension;
                }
                else if (M->Nodes[i].BC_DOF[k]==1)
                {
                    double TempVal7,TempVal8;
                    if (M->Nodes[i].LocalBoundaryCondition[k]==true)
                    {
                        double TempVal12_1_loc=-0.5*TempVal11_1*sin(2*theta)+TempVal12_1*cos(2*theta)+0.5*TempVal22_1*sin(2*theta);
                        double TempVal12_2_loc=-0.5*TempVal11_2*sin(2*theta)+TempVal12_2*cos(2*theta)+0.5*TempVal22_2*sin(2*theta);
                        double TempVal22_1_loc=TempVal11_1*pow(sin(theta),2)-TempVal12_1*sin(2*theta)+TempVal22_1*pow(cos(theta),2);
                        double TempVal22_2_loc=TempVal11_2*pow(sin(theta),2)-TempVal12_2*sin(2*theta)+TempVal22_2*pow(cos(theta),2);
                        TempVal7=TempVal12_1_loc*1 + TempVal22_1_loc*0;
                        TempVal8=TempVal12_2_loc*1 + TempVal22_2_loc*0;
                    }
                    else
                    {
                        TempVal7=TempVal12_1*M->Nodes[i].Normal[k][0] + TempVal22_1*M->Nodes[i].Normal[k][1];
                        TempVal8=TempVal12_2*M->Nodes[i].Normal[k][0] + TempVal22_2*M->Nodes[i].Normal[k][1];
                    }
                    if ((M->PartialStab==false && M->Stabilization==true) || (M->PartialStab==true && M->Nodes[i].BC_N[0] + M->Nodes[i].BC_N[1] == 0))
                    {
                        TempVal7=TempVal7-0.5*M->Nodes[i].CharactLength*M->Nodes[i].ProdN*TempValU2_1;
                        TempVal8=TempVal8-0.5*M->Nodes[i].CharactLength*M->Nodes[i].ProdN*TempValU2_2;
                    }
//                    TripStiff[StiffMatInc+SupSize*Dimension*k+j*Dimension+0]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+0,TempVal7);
//                    TripStiff[StiffMatInc+SupSize*Dimension*k+j*Dimension+1]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+1,TempVal8);
                    StiffRows[ThreadIndex][ThrRowIndex*Dimension+k].Trip[j*Dimension+0]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+0,TempVal7);
                    StiffRows[ThreadIndex][ThrRowIndex*Dimension+k].Trip[j*Dimension+1]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+1,TempVal8);
                }
                if (j==0)
                {
                    F_Vec[(i-1)*Dimension+k]=-M->Nodes[i].BC_N[k];
                }
            }
        }
    }
}

double GFD_Intrinsic::Angle(Model* M,long CollocNode, int Dim,long SingNodes)
{
    double Value=0;
    double ValX=0;
    double ValY=0;
    if (SingNodes==-1)
    {
        ValX=M->Nodes[CollocNode].Normal[Dim][0];
        ValY=M->Nodes[CollocNode].Normal[Dim][1];
    }
    else
    {
        ValX=M->Nodes[CollocNode].X[0]-M->Nodes[SingNodes].X[0];
        ValY=M->Nodes[CollocNode].X[1]-M->Nodes[SingNodes].X[1];
    }
    double Dist=pow(pow(ValX,2)+pow(ValY,2),0.5);
    if (ValY>=0)
        Value=acos(ValX/Dist);
    else
        Value=-acos(ValX/Dist);
    return Value;
}
