#include "ErrorEstimator.h"

ErrorEstimator::ErrorEstimator() {
}

ErrorEstimator::ErrorEstimator(const ErrorEstimator& orig) {
}

ErrorEstimator::~ErrorEstimator() {
}

void ErrorEstimator::ComputeErrorEstimator(Model* M,StencilSelect* S,LinearProblem* L)
{
    Dimension=M->Dimension;
    
    //Resize the estimator and the error estimation arrays
    if (M->EstimVar=="STRESS" || M->EstimVar=="STRAIN")
    {
        Estim.resize((M->NodeNumber+1)*3*(Dimension-1));
    }
    else if (M->EstimVar=="DISPLACEMENT")
    {
        Estim.resize((M->NodeNumber+1)*Dimension);
    }
    ErrorEstim.resize(M->NodeNumber);
    if (M->ZZ_DirectVMS==true)
        EstimVMS.resize(M->NodeNumber);
    
    if(M->EstimatorType=="GR")
    {
        #pragma omp parallel num_threads(M->ThreadNumber) 
        {
            ComputeErrorEstimator_GR(M,omp_get_thread_num(),M->ThreadNumber,L);
        }
    }
    else if (M->EstimatorType=="BEN")
    {
        #pragma omp parallel num_threads(M->ThreadNumber) 
        {
            ComputeErrorEstimator_BEN(M,omp_get_thread_num(),M->ThreadNumber,L);
        }
    }
    else if (M->EstimatorType=="BEN_RES")
    {
        //Fill the first and second derivatives vectors
        FillFirstSecondDerivatives(M,L);
        #pragma omp parallel num_threads(M->ThreadNumber) 
        {
            ComputeErrorEstimator_BEN_RES(M,omp_get_thread_num(),M->ThreadNumber,L);
        }
    }
    else if (M->EstimatorType=="TAYLOR")
    {
        ComputeErrorEstimator_TAY(M,L);
    }
    else if (M->EstimatorType=="RES")
    {
        ComputeErrorEstimator_RES(M,S,L);
    }
}

void ErrorEstimator::ComputeErrorEstimator_TAY(Model* M,LinearProblem* L)
{
    ErrorEstimU.resize(M->NodeNumber,Dimension);
    double *Deriv;
    int DeriveNum;
    long LocNodeNum;
    PetscScalar TempU1, TempU2, TempU3;
    int RowXU, RowYU;
    
    //Set the drivation vector
    DeriveNum=5;
    if (Dimension==3)
    {
        DeriveNum=9;
    }
    Deriv = new double [DeriveNum];
    
    for (int m=0; m<Dimension; m++)
    {
        for (long i=1; i<=M->NodeNumber; i++)
        {
            RowXU=Dimension*(i-1)+m;
            //Initialize the derivatives
            for (int k=0; k<DeriveNum; k++)
            {
                Deriv[k]=0;
            }
            //Calculate the drivatives at the Node i
            for (int j=0; j<M->Nodes[i].SupSize; j++)
            {
                LocNodeNum=M->Nodes[i].SupNodes[j];
                RowYU=Dimension*(LocNodeNum-1)+m;
//                VecGetValues(U_Petsc,1,&RowXU,&TempU1);
                TempU1=L->U(RowXU);
                for (int k=0; k<DeriveNum; k++)
                {
                    TempU2=L->U(RowYU);
//                    VecGetValues(U_Petsc,1,&RowYU,&TempU2);
                    Deriv[k]=Deriv[k]+M->Nodes[i].Coeff[j][k]*(TempU2-TempU1);
                }
            }
            //Once the derivatives calculated, calculate the error estimator
            ErrorEstimU(i-1,m)=0;
            for (int j=0; j<M->Nodes[i].SupSize; j++)
            {
                LocNodeNum=M->Nodes[i].SupNodes[j];
                RowYU=Dimension*(LocNodeNum-1)+m;
                double DistSq=0;
                for (int mm=0; mm< Dimension; mm++)
                {
                    DistSq=DistSq+pow(M->Nodes[LocNodeNum].X[mm]-M->Nodes[i].X[mm],2);
                }
                double ww=exp(-DistSq/(M->Nodes[i].SupRadiusSq*pow(M->SupRatio,2)));
                if (Dimension==2)
                {
                    ErrorEstimU(i-1,m)=ErrorEstimU(i-1,m)+ww*pow(TempU1-TempU2+
                            Deriv[3]*(M->Nodes[LocNodeNum].X[0]-M->Nodes[i].X[0])+
                            Deriv[4]*(M->Nodes[LocNodeNum].X[1]-M->Nodes[i].X[1])+
                            Deriv[0]/2*pow(M->Nodes[LocNodeNum].X[0]-M->Nodes[i].X[0],2)+
                            Deriv[1]*(M->Nodes[LocNodeNum].X[0]-M->Nodes[i].X[0])*(M->Nodes[LocNodeNum].X[1]-M->Nodes[i].X[1])+
                            Deriv[2]/2*pow(M->Nodes[LocNodeNum].X[1]-M->Nodes[i].X[1],2),2);
                }
            }
        }
    }
}

//void ErrorEstimator::ComputeErrorEstimator_RES(Model* M,LinearProblem* L)
//{
//    
//            
//    ErrorEstimU.resize(M->NodeNumber,Dimension);
//    double *Deriv;
//    int DeriveNum;
//    long LocNodeNum;
//    double TempS11_X, TempS12_X, TempS22_X;
//    double TempS11_Y, TempS12_Y, TempS22_Y;
//    long RowS11_X, RowS12_X, RowS22_X;
//    long RowS11_Y, RowS12_Y, RowS22_Y;
//    double DerivNorm; 
//    
//    //Set the drivation vector
//    DeriveNum=4;
//    Deriv = new double [DeriveNum];
//    
//    for (long i=1; i<=M->NodeNumber; i++)
//    {
//        //Get the stress vector rows
//        RowS11_X=3*(Dimension-1)*(i-1);
//        RowS12_X=3*(Dimension-1)*(i-1)+1;
//        RowS22_X=3*(Dimension-1)*(i-1)+2;
//        //Get the determined stress values
//        TempS11_X=L->S(RowS11_X);
//        TempS12_X=L->S(RowS12_X);
//        TempS22_X=L->S(RowS22_X);
//        
//        //Initialize the derivatives
//        for (int k=0; k<DeriveNum; k++)
//        {
//            Deriv[k]=0;
//        }
//        //Calculate the drivatives at the Node i
//        for (int j=0; j<M->Nodes[i].SupSize; j++)
//        {
//            //Get the node number of the support node
//            LocNodeNum=M->Nodes[i].SupNodes[j];
//            RowS11_Y=3*(Dimension-1)*(LocNodeNum-1);
//            RowS12_Y=3*(Dimension-1)*(LocNodeNum-1)+1;
//            RowS22_Y=3*(Dimension-1)*(LocNodeNum-1)+2;
//            //Getthe stress values associated to the support node
//            TempS11_Y=L->S(RowS11_Y);
//            TempS12_Y=L->S(RowS12_Y);
//            TempS22_Y=L->S(RowS22_Y);
//            //Calculate the norm of the derivative coefficients
//            DerivNorm=pow(pow(M->Nodes[i].Coeff[j][3],2)+pow(M->Nodes[i].Coeff[j][4],2),0.5);
//            //Calculate the derivative of the stress variables
//            Deriv[0]=Deriv[0]+M->Nodes[i].Coeff[j][3]*(RowS11_Y-RowS11_X)/DerivNorm;
//            Deriv[1]=Deriv[1]+M->Nodes[i].Coeff[j][4]*(RowS12_Y-RowS12_X)/DerivNorm;
//            Deriv[2]=Deriv[2]+M->Nodes[i].Coeff[j][3]*(RowS12_Y-RowS12_X)/DerivNorm;
//            Deriv[3]=Deriv[3]+M->Nodes[i].Coeff[j][4]*(RowS22_Y-RowS22_X)/DerivNorm;
//        }
//        //Once the derivatives calculated, calculate the error estimator
//        ErrorEstimU(i-1,0)=Deriv[0]+Deriv[1];
//        ErrorEstimU(i-1,1)=Deriv[2]+Deriv[3];
//    }    
//}

void ErrorEstimator::ComputeErrorEstimator_GR(Model* M,int ThreadIndex,int TreadNum,LinearProblem* L)
{
    int SupSize=-1;
    long LocNodeNum=-1;
    
    MatrixXd Exponents;
    MatrixXd B1;
    int l=-1;
    if (Dimension==2)
    {
        l=6;
    }
    else if (Dimension==3)
    {
        l=10;
    }
    B1.resize(l,1);
    Exponents.resize(l,Dimension);
    //Initialise the B matrix
    for (int i=0; i<l; i++)
    {
        B1(i,0)=0;
    }
    B1(0,0)=1;
    for (int i=0; i<l; i++)
    {
        for (int j=0; j<Dimension; j++)
        {
            Exponents(i,j)=0;
        }
    }
    
    //Initialise the Exponent matrix
    if (Dimension==2)
    {
        //x                 y
        Exponents(1,0)=1;   Exponents(2,1)=1;
        //x^2               xy
        Exponents(3,0)=2;   Exponents(4,0)=1;Exponents(4,1)=1;
        //y^2               xy
        Exponents(5,1)=2;
    }
    else if (Dimension==3)
    {
        //x                 y                   z
        Exponents(1,0)=1;   Exponents(2,1)=1;   Exponents(3,2)=1;
        //x^2               xy                                   xz
        Exponents(4,0)=2;   Exponents(5,0)=1;Exponents(5,1)=1;   Exponents(6,0)=1;Exponents(6,2)=1;
        //y^2               yz                                   z^2
        Exponents(7,1)=2;   Exponents(8,1)=1;Exponents(8,2)=1;   Exponents(9,2)=2;
    }
    
    long Start=-1, End=-1;
    //Get the start and end node of the thread
    M->GetStartEndNode(ThreadIndex,TreadNum,Start, End,M->NodeNumber);
    
    //Initialize the error estimators for the considered collocation nodes
    for (long i=Start; i<=End; i++)
    {        
        for (int j=0; j<3*(Dimension-1); j++)
        {
            Estim((i-1)*3*(Dimension-1)+j)=0;
        }
    }
    
    for (long i=Start; i<=End; i++)
    {
        if (Start==1)
        {
            M->PrintLoading(i-Start,End-Start,"Computing error estimator: ", false);
        }
        
        MatrixXd A_,A_inv;
        MatrixXd Mat_P, TempA, Id;
        int StiffCol;
        
        //Initialize the size of the vectors and matrices
        A_.resize(l,l);
        Id= MatrixXd::Identity(l, l);
        
        if (M->StencilScaling<1)
        {
            SupSize=M->Nodes[i].SupSize*M->StencilScaling;
            double Dist=0;
            for (int m=0; m<M->Dimension; m++)
            {
                Dist+=pow(M->Nodes[i].X[m]-M->Nodes[M->Nodes[i].SupNodes[SupSize-1]].X[m],2);
            }
            M->Nodes[i].SupRadiusSqEstim=Dist*pow(M->SupScaling,2);
        }
        else if (M->StencilScaling>1)
        {
            SupSize=M->Nodes[i].SupSizeEstim;
        }
        else
        {
            SupSize=M->Nodes[i].SupSize;
            M->Nodes[i].SupRadiusSqEstim=M->Nodes[i].SupRadiusSq;
        }
            
        Mat_P.resize(l,SupSize);
        //Fill the matrices A for each particle of the suppot
        for (long j=0; j<SupSize; j++)
        {
            MatrixXd P;
            double DistX1, DistX2, DistX3;
            DistX3=0;
            P.resize(l,1);
            if (M->StencilScaling>1)
            {
                LocNodeNum=M->Nodes[i].SupNodesEstim[j];
            }
            else
            {
                LocNodeNum=M->Nodes[i].SupNodes[j];
            }
            
            for (int k=0; k<l; k++)
            {
                Mat_P(k,j)=pow(M->Nodes[LocNodeNum].X[0]-M->Nodes[i].X[0],Exponents(k,0)) *
                           pow(M->Nodes[LocNodeNum].X[1]-M->Nodes[i].X[1],Exponents(k,1));
                if (Dimension==3)
                {
                    Mat_P(k,j)=Mat_P(k,j)*pow(M->Nodes[LocNodeNum].X[2]-M->Nodes[i].X[2],Exponents(k,2));
                }
                P(k,0)=Mat_P(k,j);
            }
            
            DistX1=M->Nodes[i].X[0]-M->Nodes[LocNodeNum].X[0];
            DistX2=M->Nodes[i].X[1]-M->Nodes[LocNodeNum].X[1];
            if (Dimension==3)
            {
                DistX3=M->Nodes[i].X[2]-M->Nodes[LocNodeNum].X[2];
            }
            TempA=P*P.transpose()*ws(M,DistX1,DistX2,DistX3,pow(M->Nodes[i].SupRadiusSqEstim,0.5),0,0);
            if (j==0)
            {
                A_=TempA;
            }
            else
            {
                A_=A_+TempA;
            }
        }
        MatrixXd C_;
        A_inv=A_.lu().solve(Id);
        
        C_=B1.transpose()*A_inv;
        StiffCol=0;
        for (long j=0; j<SupSize; j++)
        {
            double a=pow(M->Nodes[i].SupRadiusSqEstim,0.5);
            if (M->StencilScaling>1)
            {
                LocNodeNum=M->Nodes[i].SupNodesEstim[j];
            }
            else
            {
                LocNodeNum=M->Nodes[i].SupNodes[j];
            }
            
            double CC_;
            MatrixXd P;
            P.resize(l,1);
            //Set the values to zero
            for (int k=0; k<l; k++)
            {
                P(k,0)=Mat_P(k,j);
            }
            double ZVal=0;
            if (Dimension==3)
            {
                ZVal=M->Nodes[i].X[2]-M->Nodes[LocNodeNum].X[2];
            }
            CC_=(C_*P)(0,0)*ws(M,M->Nodes[i].X[0]-M->Nodes[LocNodeNum].X[0],M->Nodes[i].X[1]-M->Nodes[LocNodeNum].X[1],ZVal,a,0,0);
            for (int j=0; j<3*(Dimension-1); j++)
            {
                if (M->EstimVar=="STRAIN")
                    Estim((i-1)*3*(Dimension-1)+j)+=CC_*L->Epsilon((LocNodeNum-1)*3*(Dimension-1)+j);
                else if (M->EstimVar=="STRESS")
                    Estim((i-1)*3*(Dimension-1)+j)+=CC_*L->S((LocNodeNum-1)*3*(Dimension-1)+j);
            }
            //Set directly the estimator
            if (M->ZZ_DirectVMS==true)
            {
                //Initialize EstimVMS
                if (j==0)
                    EstimVMS(i-1)=0;
                if (Dimension==2)
                {
                    EstimVMS(i-1)+=CC_*pow(pow(L->S((LocNodeNum-1)*3*(Dimension-1)+0),2)- L->S((LocNodeNum-1)*3*(Dimension-1)+0)* L->S((LocNodeNum-1)*3*(Dimension-1)+2)+pow( L->S((LocNodeNum-1)*3*(Dimension-1)+2),2)+3*pow( L->S((LocNodeNum-1)*3*(Dimension-1)+1),2),0.5);
                }
                else if (Dimension==3)
                {
                    double Term1=pow(L->S((LocNodeNum-1)*3*(Dimension-1)+0)-L->S((LocNodeNum-1)*3*(Dimension-1)+3),2);
                    double Term2=pow(L->S((LocNodeNum-1)*3*(Dimension-1)+3)-L->S((LocNodeNum-1)*3*(Dimension-1)+5),2);
                    double Term3=pow(L->S((LocNodeNum-1)*3*(Dimension-1)+5)-L->S((LocNodeNum-1)*3*(Dimension-1)+0),2);
                    double Term4=6*(pow(L->S((LocNodeNum-1)*3*(Dimension-1)+1),2)+pow(L->S((LocNodeNum-1)*3*(Dimension-1)+2),2)+pow(L->S((LocNodeNum-1)*3*(Dimension-1)+4),2));
                    EstimVMS(i-1)+=CC_*pow(Term1+Term2+Term3+Term4,0.5)/2;
                }
            }
        }
        //Compute the error estimator
        if (M->EstimVar=="STRESS")
        {
            double VMS_Estim=0;
            double VMS_Approx=0;
            if (Dimension==2)
            {
                VMS_Estim=pow(pow(Estim((i-1)*3*(Dimension-1)+0),2)-Estim((i-1)*3*(Dimension-1)+0)*Estim((i-1)*3*(Dimension-1)+2)+pow(Estim((i-1)*3*(Dimension-1)+2),2)+3*pow(Estim((i-1)*3*(Dimension-1)+1),2),0.5);
                VMS_Approx=pow(pow(L->S((i-1)*3*(Dimension-1)+0),2)- L->S((i-1)*3*(Dimension-1)+0)* L->S((i-1)*3*(Dimension-1)+2)+pow( L->S((i-1)*3*(Dimension-1)+2),2)+3*pow( L->S((i-1)*3*(Dimension-1)+1),2),0.5);
            }
            else if (Dimension==3)
            {
                double Term1Estim=pow(Estim((i-1)*3*(Dimension-1)+0)-Estim((i-1)*3*(Dimension-1)+3),2);
                double Term2Estim=pow(Estim((i-1)*3*(Dimension-1)+3)-Estim((i-1)*3*(Dimension-1)+5),2);
                double Term3Estim=pow(Estim((i-1)*3*(Dimension-1)+5)-Estim((i-1)*3*(Dimension-1)+0),2);
                double Term4Estim=6*(pow(Estim((i-1)*3*(Dimension-1)+1),2)+pow(Estim((i-1)*3*(Dimension-1)+2),2)+pow(Estim((i-1)*3*(Dimension-1)+4),2));
                VMS_Estim=pow(Term1Estim+Term2Estim+Term3Estim+Term4Estim,0.5)/2;
                
                double Term1Approx=pow(L->S((i-1)*3*(Dimension-1)+0)-L->S((i-1)*3*(Dimension-1)+3),2);
                double Term2Approx=pow(L->S((i-1)*3*(Dimension-1)+3)-L->S((i-1)*3*(Dimension-1)+5),2);
                double Term3Approx=pow(L->S((i-1)*3*(Dimension-1)+5)-L->S((i-1)*3*(Dimension-1)+0),2);
                double Term4Approx=6*(pow(L->S((i-1)*3*(Dimension-1)+1),2)+pow(L->S((i-1)*3*(Dimension-1)+2),2)+pow(L->S((i-1)*3*(Dimension-1)+4),2));
                VMS_Approx=pow(Term1Approx+Term2Approx+Term3Approx+Term4Approx,0.5)/2;
            }
            if (M->ZZ_DirectVMS==true)
            {
                VMS_Estim=EstimVMS(i-1);
            }
            ErrorEstim(i-1)=abs(VMS_Estim-VMS_Approx);
        }
        else if (M->EstimVar=="DISPLACEMENT")
        {
            if (Dimension==2)
            {
            }
        }
    }
}

void ErrorEstimator::ComputeErrorEstimator_BEN(Model* M,int ThreadIndex,int TreadNum,LinearProblem* L)
{
    long Start=-1, End=-1;
    //Get the start and end node of the thread
    M->GetStartEndNode(ThreadIndex,TreadNum,Start, End,M->NodeNumber);
    
    //Initialize the error estimators for the considered collocation nodes
    for (long i=Start; i<=End; i++)
    {
        for (int j=0; j<Dimension; j++)
        {
            Estim((i-1)*Dimension+j)=0;
        }
    }
    
    //For each node considered
    for (long i=Start; i<=End; i++)
    {        
        //Get the third derivatives
        double U1_xxx=0;
        double U1_xxy=0;
        double U1_yyy=0;
        double U1_xyy=0;
        double U2_xxx=0;
        double U2_xxy=0;
        double U2_yyy=0;
        double U2_xyy=0;
        
        //Get the derivatives
        for (int j=0; j<M->Nodes[i].SupSize; j++)
        {            
            long LocNodeNum=M->Nodes[i].SupNodes[j];
            //Derivatives for U1
            U1_xxx+=L->U(Dimension*(LocNodeNum-1)+0)*M->Nodes[i].Coeff[j][5];
            U1_xxy+=L->U(Dimension*(LocNodeNum-1)+0)*M->Nodes[i].Coeff[j][6];
            U1_yyy+=L->U(Dimension*(LocNodeNum-1)+0)*M->Nodes[i].Coeff[j][7];
            U1_xyy+=L->U(Dimension*(LocNodeNum-1)+0)*M->Nodes[i].Coeff[j][8];
            //Derivatives for U2
            U2_xxx+=L->U(Dimension*(LocNodeNum-1)+1)*M->Nodes[i].Coeff[j][5];
            U2_xxy+=L->U(Dimension*(LocNodeNum-1)+1)*M->Nodes[i].Coeff[j][6];
            U2_yyy+=L->U(Dimension*(LocNodeNum-1)+1)*M->Nodes[i].Coeff[j][7];
            U2_xyy+=L->U(Dimension*(LocNodeNum-1)+1)*M->Nodes[i].Coeff[j][8];
        }
        
        for (int j=0; j<M->Nodes[i].SupSize; j++)
        {
            long LocNodeNum=M->Nodes[i].SupNodes[j];
            //Get the weight
            double DistX1=M->Nodes[i].X[0]-M->Nodes[LocNodeNum].X[0];
            double DistX2=M->Nodes[i].X[1]-M->Nodes[LocNodeNum].X[1];
            double s=pow(pow(DistX1,2)+pow(DistX2,2),0.5)/pow(M->Nodes[i].SupRadiusSq,0.5);
            double W=0;
            if (s<1)
            {
                W=pow(abs(1-6*pow(s,2)+8*pow(s,3)-3*pow(s,4)),M->SupExp);
            }
            Estim((i-1)*Dimension+0)+=W*1/6*(U1_xxx*pow(DistX1,3)+U1_yyy*pow(DistX2,3)+3*U1_xxy*pow(DistX1,2)*DistX2+3*U1_xyy*DistX1*pow(DistX2,2));
            Estim((i-1)*Dimension+1)+=W*1/6*(U2_xxx*pow(DistX1,3)+U2_yyy*pow(DistX2,3)+3*U2_xxy*pow(DistX1,2)*DistX2+3*U2_xyy*DistX1*pow(DistX2,2));
        }
        Estim((i-1)*Dimension+0)=Estim((i-1)*Dimension+0)/M->Nodes[i].SupSize;
        Estim((i-1)*Dimension+1)=Estim((i-1)*Dimension+1)/M->Nodes[i].SupSize;
    }
}

void ErrorEstimator::FillFirstSecondDerivatives(Model* M, LinearProblem* L)
{
    int NumbDeriv2=Dimension+3*(Dimension-1);
    U_.resize(Dimension,M->NodeNumber*NumbDeriv2);
    
    int NumbDeriv3=4;
    if (Dimension==2)
    {
        NumbDeriv3=4;
    }
    U_xxx.resize(Dimension,M->NodeNumber*NumbDeriv3);
    
    //Fill the fist and second derivatives
    for (long i=1; i<=M->NodeNumber; i++)
    {
        //k is the dimension of the displacement field
        for (int k=0; k<Dimension; k++)
        {
            //Fill the derivatives
            for (int m=0; m<NumbDeriv2; m++)
            {
                //Initialize the derivative value to zero
                U_(k,NumbDeriv2*(i-1)+m)=0;
                //Get the derivative value based on the field values at the support nodes
                for (int j=0; j<M->Nodes[i].SupSize; j++)
                {
                    long LocNodeNum=M->Nodes[i].SupNodes[j];
                    U_(k,NumbDeriv2*(i-1)+m)+=L->U(Dimension*(LocNodeNum-1)+k)*M->Nodes[i].Coeff[j][m];
                }
            }
        }
    }
    
    //Fill the third derivatives
    for (long i=1; i<=M->NodeNumber; i++)
    {
        for (int k=0; k<Dimension; k++)
        {
            for (int j=0; j<M->Nodes[i].SupSize; j++)
            {
                long LocNodeNum=M->Nodes[i].SupNodes[j];
                //U_xxx, U_xxy, U_xyy, U_yyy
                U_xxx(k,NumbDeriv3*(i-1)+0)+=U_(k,NumbDeriv2*(LocNodeNum-1)+2)*M->Nodes[i].Coeff[j][0];
                U_xxx(k,NumbDeriv3*(i-1)+1)+=(U_(k,NumbDeriv2*(LocNodeNum-1)+2)*M->Nodes[i].Coeff[j][1]+U_(k,NumbDeriv2*(LocNodeNum-1)+3)*M->Nodes[i].Coeff[j][0])/2;
                U_xxx(k,NumbDeriv3*(i-1)+2)+=(U_(k,NumbDeriv2*(LocNodeNum-1)+4)*M->Nodes[i].Coeff[j][0]+U_(k,NumbDeriv2*(LocNodeNum-1)+3)*M->Nodes[i].Coeff[j][1])/2;
                U_xxx(k,NumbDeriv3*(i-1)+3)+=U_(k,NumbDeriv2*(LocNodeNum-1)+4)*M->Nodes[i].Coeff[j][1];
            }
        }
    }
}

void ErrorEstimator::ComputeErrorEstimator_BEN_RES(Model* M,int ThreadIndex,int TreadNum,LinearProblem* L)
{
    long Start=-1, End=-1;
    int NumbDeriv3=4;
    if (Dimension==2)
    {
        NumbDeriv3=4;
    }
    
    //Get the start and end node of the thread
    M->GetStartEndNode(ThreadIndex,TreadNum,Start, End,M->NodeNumber);
    
    //For each node considered
    for (long i=Start; i<=End; i++)
    {
        for (int k=0; k<Dimension; k++)
        {
            //Initialize the error estimator
            Estim((i-1)*Dimension+k)=0;
            
            for (int j=0; j<M->Nodes[i].SupSize; j++)
            {
                long LocNodeNum=M->Nodes[i].SupNodes[j];
                //Get the weight
                double DistX1=M->Nodes[i].X[0]-M->Nodes[LocNodeNum].X[0];
                double DistX2=M->Nodes[i].X[1]-M->Nodes[LocNodeNum].X[1];
                double s=pow(pow(DistX1,2)+pow(DistX2,2),0.5)/pow(M->Nodes[i].SupRadiusSq,0.5);
                double W=0;
                if (s<1)
                {
                    W=pow(abs(1-6*pow(s,2)+8*pow(s,3)-3*pow(s,4)),M->SupExp);
                }
                Estim((i-1)*Dimension+k)+=W*1/6*(U_xxx(k,NumbDeriv3*(i-1)+0)*pow(DistX1,3)+
                                                 U_xxx(k,NumbDeriv3*(i-1)+3)*pow(DistX2,3)+
                                                 3*U_xxx(k,NumbDeriv3*(i-1)+1)*pow(DistX1,2)*DistX2+
                                                 3*U_xxx(k,NumbDeriv3*(i-1)+2)*DistX1*pow(DistX2,2));
            }
            Estim((i-1)*Dimension+k)=Estim((i-1)*Dimension+k)/M->Nodes[i].SupSize;
        }
    }
}

double ErrorEstimator::w(double s, int deriv)
{
    double ww;
    s=abs(s);
    if (deriv==0)
    {
        ww=1-6*pow(s,2)+8*pow(s,3)-3*pow(s,4);
    }
    else if (deriv==1)
    {
        ww=-12*s+24*pow(s,2)-12*pow(s,3);
    }
    else if (deriv==2)
    {
        ww=-12+48*s-36*pow(s,2);   
    }
    return ww;
}

double ErrorEstimator::ws(Model* M, double X, double Y, double Z, double rad, int derivX, int derivY)
{
    double ReturnVal=0;
    if (M->EstimWeight=="SPLINE_4")
        ReturnVal=w_MLS(X,Y,Z,rad,derivX,derivY);
    else if (M->EstimWeight=="NONE")
        ReturnVal=(double) 1.0;
    return ReturnVal;
}

double ErrorEstimator::w_MLS(double X, double Y, double Z, double rad, int derivX, int derivY)
{
    double value;
    double s=pow(pow(X,2)+pow(Y,2)+pow(Z,2),0.5)/rad;
    if (s==0)
    {
        if (derivX==0 && derivY==0)
        {
            value=w(s,0);
        }
        else if (derivX==1 || derivY==1)
        {
            value=0;
        }
        else if (derivX==2 || derivY==2)
        {
            value=-12/rad;
        }
    }
    else if (derivX==0 && derivY==0)
    {
        value=w(s,0);
    }
    else if (derivX==1 && derivY==0)
    {
        value=X/pow(rad,2)*w(s,1);
    }
    else if (derivX==0 && derivY==1)
    {
        value=Y/pow(rad,2)*w(s,1);
    }
    else if (derivX==1 && derivY==1)
    {
        value=(w(s,2)-w(s,1)/s)*(X*Y)/(pow(s,2)*pow(rad,4))+w(s,1)/pow(rad,2);
    }
    else if (derivX==2 && derivY==0)
    {
        value=(w(s,2)-w(s,1)/s)*(X*X)/(pow(s,2)*pow(rad,4));
    }
    else if (derivX==0 && derivY==2)
    {
        value=(w(s,2)-w(s,1)/s)*(Y*Y)/(pow(s,2)*pow(rad,4));
    }
    return value;
}
