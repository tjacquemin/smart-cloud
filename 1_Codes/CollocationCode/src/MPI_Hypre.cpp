#include "MPI_Solver.h"

void MPI_Solver::LoadHypreParameters(Model* M)
{
    //Send-Receive the paprameters for the Hypre PC and solver
    MPI_Status status;
    if (rank==0)
    {
        H_CoarsenType=M->H_CoarsenType;
        H_StrongThreshold=M->H_StrongThreshold;
        H_AggNumLevel=M->H_AggNumLevel;
        H_MaxCoarseSize=M->H_MaxCoarseSize;
        H_MaxLevels=M->H_MaxLevels;
        H_MaxAMGIter=M->H_MaxAMGIter;
        H_RelaxType=M->H_RelaxType;
        H_InterpType=M->H_InterpType;
        H_SmoothType=M->H_SmoothType;
        H_NumSweeps=M->H_NumSweeps;
        H_AMGTol=M->H_AMGTol;
        H_NonGalerkTol=M->H_NonGalerkTol;
        H_CycleType=M->H_CycleType;
        for (int DestRank=1;DestRank<size;DestRank++)
        {
            MPI_Send(&H_CoarsenType, 1, MPI_INT, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(&H_StrongThreshold, 1, MPI_DOUBLE, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(&H_AggNumLevel, 1, MPI_INT, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(&H_MaxCoarseSize, 1, MPI_INT, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(&H_MaxLevels, 1, MPI_INT, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(&H_MaxAMGIter, 1, MPI_INT, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(&H_RelaxType, 1, MPI_INT, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(&H_InterpType, 1, MPI_INT, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(&H_SmoothType, 1, MPI_INT, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(&H_NumSweeps, 1, MPI_INT, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(&H_AMGTol, 1, MPI_DOUBLE, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(&H_NonGalerkTol, 1, MPI_DOUBLE, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(&H_CycleType, 1, MPI_INT, DestRank, 123, MPI_COMM_WORLD);
        }
    }
    else
    {
        MPI_Recv(&H_CoarsenType, 1, MPI_INT, 0, 123, MPI_COMM_WORLD,&status);
        MPI_Recv(&H_StrongThreshold, 1, MPI_DOUBLE, 0, 123, MPI_COMM_WORLD,&status);
        MPI_Recv(&H_AggNumLevel, 1, MPI_INT, 0, 123, MPI_COMM_WORLD,&status);
        MPI_Recv(&H_MaxCoarseSize, 1, MPI_INT, 0, 123, MPI_COMM_WORLD,&status);
        MPI_Recv(&H_MaxLevels, 1, MPI_INT, 0, 123, MPI_COMM_WORLD,&status);
        MPI_Recv(&H_MaxAMGIter, 1, MPI_INT, 0, 123, MPI_COMM_WORLD,&status);
        MPI_Recv(&H_RelaxType, 1, MPI_INT, 0, 123, MPI_COMM_WORLD,&status);
        MPI_Recv(&H_InterpType, 1, MPI_INT, 0, 123, MPI_COMM_WORLD,&status);
        MPI_Recv(&H_SmoothType, 1, MPI_INT, 0, 123, MPI_COMM_WORLD,&status);
        MPI_Recv(&H_NumSweeps, 1, MPI_INT, 0, 123, MPI_COMM_WORLD,&status);
        MPI_Recv(&H_AMGTol, 1, MPI_DOUBLE, 0, 123, MPI_COMM_WORLD,&status);
        MPI_Recv(&H_NonGalerkTol, 1, MPI_DOUBLE, 0, 123, MPI_COMM_WORLD,&status);
        MPI_Recv(&H_CycleType, 1, MPI_INT, 0, 123, MPI_COMM_WORLD,&status);
    }
}

void MPI_Solver::CreateHypreMatrix()
{   
    nrows=TripStiff[LocTripletLength-1].row()-TripStiff[0].row()+1;
    ilower=TripStiff[0].row();
    iupper=TripStiff[LocTripletLength-1].row();
        
    //Create the matrix
    HYPRE_Init();
    HYPRE_IJMatrixCreate(comm, ilower, iupper, ilower, iupper, &ij_matrix);
    HYPRE_IJMatrixSetObjectType(ij_matrix, HYPRE_PARCSR);
    HYPRE_IJMatrixInitialize(ij_matrix);
}

void MPI_Solver::LoadHypreMatrix()
{
    //Create and fill the vectors to load the matrix
    int* ncols;
    int* rows;
    int* cols;
    double* values;

    ncols = new int[nrows];
    rows = new int[nrows];
    cols = new int[LocTripletLength];
    values = new double[LocTripletLength];
    
    int RowCount=0;
    int ColCount=1;
    int RowIndex=TripStiff[0].row();
    
    rows[0]=RowIndex;
    cols[0]=TripStiff[0].col();
    values[0]=TripStiff[0].value();
    
    for (PetscInt i=1; i<LocTripletLength; i++)
    {
        if (TripStiff[i].row()>RowIndex)
        {
            //Save the index of the row and set the index of the next row
            rows[RowCount]=RowIndex;
            RowIndex=TripStiff[i].row();
            //Save the number of columns and 
            ncols[RowCount]=ColCount;
            //Increment the row index and initialize the number of columns
            RowCount++;
            ColCount=1;
        }
        else
        {
            ColCount++;
        }
        cols[i]=TripStiff[i].col();
        values[i]=TripStiff[i].value();
    }
    //Set the value for the last row
    RowIndex=TripStiff[LocTripletLength-1].row();
    ncols[RowCount]=ColCount;
    rows[RowCount]=RowIndex;
        
    //Fill the matrix
    HYPRE_IJMatrixSetValues(ij_matrix, nrows, ncols, rows, cols, values);
    
    //Assemble the matrix and get the parcsr matrix object
    HYPRE_IJMatrixAssemble(ij_matrix);
    HYPRE_IJMatrixGetObject(ij_matrix, (void**) &parcsr_ij);
}

void MPI_Solver::CreateLoadHypreForce()
{
    /* Create the rhs and solution */
    HYPRE_IJVectorCreate(MPI_COMM_WORLD, ilower, iupper,&b_H);
    HYPRE_IJVectorSetObjectType(b_H, HYPRE_PARCSR);
    HYPRE_IJVectorInitialize(b_H);

    HYPRE_IJVectorCreate(MPI_COMM_WORLD, ilower, iupper,&x_H);
    HYPRE_IJVectorSetObjectType(x_H, HYPRE_PARCSR);
    HYPRE_IJVectorInitialize(x_H);

    /* Set the rhs values to h^2 and the solution to zero */
    {
        double *rhs_values, *x_values;
        int    *rows;

        rhs_values =  (double*) calloc(nrows, sizeof(double));
        x_values =  (double*) calloc(nrows, sizeof(double));
        rows = (int*) calloc(nrows, sizeof(int));

        for (long i=0; i<nrows; i++)
        {
            rhs_values[i] = F_VecAll[i+ilower];
            x_values[i] = 0.0;
            rows[i] = ilower + i;
        }

        HYPRE_IJVectorSetValues(b_H, nrows, rows, rhs_values);
        HYPRE_IJVectorSetValues(x_H, nrows, rows, x_values);

        free(x_values);
        free(rhs_values);
        free(rows);
    }


    /*  As with the matrix, for testing purposes, one may wish to read in a rhs:
       HYPRE_IJVectorRead( <filename>, MPI_COMM_WORLD,
                                 HYPRE_PARCSR, &b );
       as an alternative to the
       following sequence of HYPRE_IJVectors calls:
       Create, SetObjectType, Initialize, SetValues, and Assemble
    */
    HYPRE_IJVectorAssemble(b_H);
    HYPRE_IJVectorGetObject(b_H, (void **) &par_b);
    HYPRE_IJVectorAssemble(x_H);
    HYPRE_IJVectorGetObject(x_H, (void **) &par_x);
//    
//    HYPRE_IJMatrixPrint(ij_matrix, "IJ.out.A");
//    HYPRE_IJVectorPrint(b_H, "IJ.out.b");
}

void MPI_Solver::SolveHypre(Model* M)
{
    int    restart = 30;
    HYPRE_Solver solver, precond;
    
    //Create the solver
    HYPRE_ParCSRFlexGMRESCreate(MPI_COMM_WORLD, &solver); 
    
    /* Set some parameters (See Reference Manual for more parameters) */
    HYPRE_FlexGMRESSetKDim(solver, restart);
    HYPRE_FlexGMRESSetTol(solver,SolverTol); /* conv. tolerance */
    HYPRE_FlexGMRESSetPrintLevel(solver, 2); /* print solve info */
    HYPRE_FlexGMRESSetLogging(solver, 1); /* needed to get run info later */
    HYPRE_FlexGMRESSetMaxIter(solver,SolverMaxIt);
        
    if (SolverPC=="ILU")
    {
//        HYPRE_ParCSRPilutCreate(MPI_COMM_WORLD, &precond);
//        HYPRE_ParCSRPilutSetFactorRowSize(precond,40);
//        HYPRE_ParCSRPilutSolve(precond,parcsr_ij, par_b, par_x);

        HYPRE_EuclidCreate(MPI_COMM_WORLD, &precond);
        HYPRE_EuclidSetLevel(precond, 1);

    }
    else if (SolverPC=="BOOMERAMG")
    {
        //Now set up the AMG preconditioner and specify any parameters
        HYPRE_BoomerAMGCreate(&precond);
        //Print all the amg solution info (3)
        HYPRE_BoomerAMGSetPrintLevel(precond, 3); 
        HYPRE_BoomerAMGSetCoarsenType(precond, H_CoarsenType);
        HYPRE_BoomerAMGSetStrongThreshold(precond,H_StrongThreshold);
        HYPRE_BoomerAMGSetAggNumLevels(precond,H_AggNumLevel);
        HYPRE_BoomerAMGSetMaxCoarseSize(precond,H_MaxCoarseSize);
        HYPRE_BoomerAMGSetMaxLevels(precond,H_MaxLevels);
        HYPRE_BoomerAMGSetMaxIter(precond,H_MaxAMGIter);
        
        HYPRE_BoomerAMGSetDomainType(precond,1);
        HYPRE_BoomerAMGSetInterpType(precond,H_InterpType);
//      HYPRE_BoomerAMGSetRelaxType(precond, H_RelaxType);
        
        //Smoother options
        HYPRE_BoomerAMGSetSmoothType(precond, H_SmoothType);
//        HYPRE_BoomerAMGSetSmoothNumLevels(precond,3);
//        HYPRE_BoomerAMGSetOverlap(precond,2);
//        HYPRE_BoomerAMGSetVariant(precond,3); 

        
        HYPRE_BoomerAMGSetNumSweeps(precond, H_NumSweeps);
        HYPRE_BoomerAMGSetTol(precond, H_AMGTol);
        if (H_NonGalerkTol>=0)
        {
            HYPRE_BoomerAMGSetNonGalerkinTol(precond,H_NonGalerkTol);
        }
        HYPRE_BoomerAMGSetCycleType(precond,H_CycleType);
        
        //Set the number of PDEs
        HYPRE_BoomerAMGSetNumFunctions(precond,Dimension);
    }
     
//    HYPRE_BoomerAMGSetOldDefault(precond);
//    HYPRE_BoomerAMGSetup(precond, parcsr_ij,par_b,par_x);
//    HYPRE_BoomerAMGSolve(precond,parcsr_ij,par_b,par_x);
    
    if (rank==0)
    {
        M->Print("Setting the preconditioner ...",true);
    }
    
    
    //Set the FlexGMRES preconditioner
    if (SolverPC=="ILU")
    {
        HYPRE_FlexGMRESSetPrecond(solver, (HYPRE_PtrToSolverFcn) HYPRE_EuclidSolve,
                        (HYPRE_PtrToSolverFcn) HYPRE_EuclidSetup, precond);
//        HYPRE_FlexGMRESSetPrecond(solver, (HYPRE_PtrToSolverFcn) HYPRE_ParCSRPilutSolve,
//                        (HYPRE_PtrToSolverFcn) HYPRE_ParCSRPilutSetup, precond);
    }
    else if (SolverPC=="BOOMERAMG")
    {
        HYPRE_FlexGMRESSetPrecond(solver, (HYPRE_PtrToSolverFcn) HYPRE_BoomerAMGSolve,
                        (HYPRE_PtrToSolverFcn) HYPRE_BoomerAMGSetup, precond);
    }
    
    //Print the output to a file
    fpos_t pos;
    fgetpos(stdout, &pos);
    int fd = dup(fileno(stdout));
    if (rank == 0)
    {
        string Path=M->InputFilePath;
        Path.replace(Path.end()-3,Path.end(),"txt");
        char* PathChar=new char[Path.length()+1];
        strcpy(PathChar,Path.c_str());
        std::freopen(PathChar, "w", stdout);
    }
    //Setup the preconditioner and solve
    HYPRE_ParCSRFlexGMRESSetup(solver, parcsr_ij, par_b, par_x);
    
    if (rank==0)
    {
        M->Print("Solving the Linear system ...",true);
    }

    HYPRE_ParCSRFlexGMRESSolve(solver, parcsr_ij, par_b, par_x);
    //Return the number of iterations and the residual norm
    HYPRE_FlexGMRESGetNumIterations(solver, &SolveHypreNumIterations);
    HYPRE_FlexGMRESGetFinalRelativeResidualNorm(solver, &SolveHypreFinalResNorm);
//    HYPRE_BoomerAMGGetNumIterations(solver, &SolveHypreNumIterations);
//    HYPRE_BoomerAMGGetFinalRelativeResidualNorm(solver, &SolveHypreFinalResNorm);
    
    if (rank == 0)
    {
        fflush(stdout);
        //Close file and restore standard output to stdout - which should be the terminal
        dup2(fd, fileno(stdout));
        close(fd);
        clearerr(stdout);
        fsetpos(stdout, &pos);
        M->Print("Problem solved.",true);
        printf(" Iterations = %d\n", SolveHypreNumIterations);
        printf(" Final Relative Residual Norm = %e\n", SolveHypreFinalResNorm);
    }

    /* Destory solver and preconditioner */
    HYPRE_ParCSRFlexGMRESDestroy(solver);
    if (SolverPC=="ILU")
    {
        HYPRE_ParCSRPilutDestroy(precond);
    }
    else if (SolverPC=="BOOMERAMG")
    {
        HYPRE_BoomerAMGDestroy(precond);
    }
    
    //Get the results
    FillUVectLocHypre();
    
    //Clear the objects
    HYPRE_IJMatrixDestroy(ij_matrix);
    HYPRE_IJVectorDestroy(b_H);
    HYPRE_IJVectorDestroy(x_H);
    HYPRE_Finalize();
}

void MPI_Solver::FillUVectLocHypre()
{
    UVectLoc=new double[Iend-Istart];
    PetscScalar TempUVal;
    for (PetscInt i=Istart; i<Iend; i++)
    {
        HYPRE_IJVectorGetValues (x_H,1,&i,&TempUVal);
        UVectLoc[i-Istart]=TempUVal;
    }
}

void MPI_Solver::ReturnSolverInfoHypre(LinearProblem* L)
{
    if (rank == 0)
    {
        L->SolverIterationNum=SolveHypreNumIterations;
        char buffer [50];
        int n, a=5, b=3;
        n=sprintf (buffer,"Final Relative Residual Norm = %e\n", SolveHypreFinalResNorm);
        L->SolverConvergenceStatus=buffer;
    }    
}