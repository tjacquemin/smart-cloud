#include "StencilSelect.h"

StencilSelect::StencilSelect() {
}

StencilSelect::StencilSelect(const StencilSelect& orig) {
}

StencilSelect::~StencilSelect() {
}

void StencilSelect::LoadSupportCGAL(Model* M, Node* CollocNodes, long CollocNodesSize, int LoadStep)
{
    //Set setencil selection properties from the global model
    Dimension=M->Dimension;
    pi=M->pi;
    lTayl=M->lTayl;
    
    if ((M->VisibilityType=="DIRECT_SURF" || M->Adaptivity==true) && LoadStep==0)
    {
        //Load the elements connected to the nodes
        LoadConnectedElements(M);
    }
    
    #pragma omp parallel num_threads(M->ThreadNumber) 
    {
        long* IndexArrayLoc=NULL;
        double* DistArrayLoc=NULL;
    
        int ThreadNum=omp_get_thread_num();
        long Start, End;
        //Get the start and end node of the thread
        M->GetStartEndNode(ThreadNum,M->ThreadNumber,Start, End,CollocNodesSize);
        
        for (long i=Start; i<=End; i++)
        {
            if (Start==1)
            {
                M->PrintLoading(i-Start,End-Start,"Loading the stencil nodes: ", false);
            }
            
            double SupScalingNum=4.0;
            bool SupLoaded=false;
            while (SupLoaded==false)
            {
                int k=0, SupSize=0;
                k=SupSizeInt;
                if (SupSizeBound>k)
                {
                    k=SupSizeBound;
                }
                if (MaxVariableSupSize>k && StencilSelection=="COND")
                {
                    k=MaxVariableSupSize;
                }
                //Scale the number of selected nodes by a factor SupScalingNum in case of visibility criterion or many nodes at the same distance
                k=k*SupScalingNum;
                
                //Initalize and load the neighbour node arrays
                IndexArrayLoc=new long[k];
                DistArrayLoc=new double[k];
                GetNeighbourNodes(M,SupScalingNum,&CollocNodes[i],&k,&SupSize,IndexArrayLoc,DistArrayLoc);
                
                //Determine is a boundary node is included in the support
                bool HasBCNode=false;
                for(int j=0; j<k; j++)
                {
                    if (M->Nodes[IndexArrayLoc[j]].BoundaryNode==true)
                    {
                        HasBCNode=true;
                    }
                }
//                if (HasBCNode==true)
//                {
//                    SupSize=SupSizeBound;
//                }
                
                //Select and load the support nodes
                if (HasBCNode==true && M->VisibilityType=="DIRECT_SURF")
                {
                    LoadVisibilitySupportFromBE(M,&CollocNodes[i],IndexArrayLoc,DistArrayLoc,k, SupSize);
                }
                else if (StencilSelection=="COND")
                {
                    LoadSupportConditionNumber(M,&CollocNodes[i],IndexArrayLoc,DistArrayLoc,k,SupSize);
                }
                else if (StencilSelection=="AREA")
                {
                    LoadAreaSupport(M,&CollocNodes[i],IndexArrayLoc,DistArrayLoc,k,SupSize,SupScalingNum);
                }
                else if (HasBCNode==true && M->VisibilityType=="DIRECT")
                {
                    LoadVisibilitySupport(M,&CollocNodes[i],IndexArrayLoc,DistArrayLoc,k, SupSize);
                }
                else if (HasBCNode==false && StencilSelection=="QUADRAN")
                {
                    LoadSupportQuad(M,&CollocNodes[i],IndexArrayLoc,DistArrayLoc,k,SupSize);
                }
                else if (HasBCNode==false && StencilSelection=="ANISOTROPIC")
                {
                    LoadSupportAnisotropic(M,&CollocNodes[i],IndexArrayLoc,DistArrayLoc,k,SupSize);
                }
                else
                {
                    LoadNormalSupport(M,&CollocNodes[i],IndexArrayLoc,DistArrayLoc,k,SupSize);
                }
                if (M->ErrEstimator==true && M->StencilScaling>1)
                {
                    LoadNormalSupportEstim(M,&CollocNodes[i],IndexArrayLoc,DistArrayLoc,k,SupSize);
                }
                delete[] DistArrayLoc;
                delete[] IndexArrayLoc;
                SupLoaded=true;
                SupScalingNum++;
            }
        }
    }
    
    LoadCoefficients(M,CollocNodes,CollocNodesSize);
    //Compute the characteristic length if stabilization requested
    if (M->Stabilization==true)
    {
        M->ComputeCharactLength();
    }
}

void StencilSelect::GetNeighbourNodes(Model* M, double SupScaling, Node* CollocNode, int* k, int* SupSize, long* IndexArrayLoc, double* DistArrayLoc)
{
    if (CollocNode->BoundaryNode==true)
    {
        *k=SupSizeBound*SupScaling;
        *SupSize=SupSizeBound;
    }
    else
    {
        *k=SupSizeInt*SupScaling;
        *SupSize=SupSizeInt;
    }

    //Create the search query
    Point_3* query=NULL;
    if (M->Dimension==2)
    {
        if (CollocNode->CollocationPointOffset==false)
        {
            query= new Point_3(CollocNode->X[0],CollocNode->X[1], 0.0);
        }
        else
        {
            query= new Point_3(CollocNode->CollocX[0],CollocNode->CollocX[1],0.0);
        }
    }
    else
    {
        query= new Point_3(CollocNode->X[0],CollocNode->X[1],CollocNode->X[2]);
    }
        
    Distance tr_dist;
    // search K nearest neighbours
    K_neighbor_search search(M->tree, *query, *k, 0.0,true,tr_dist,true);

    int j=0;
    //Fill the index and distance array with the values using the iterator
    for(K_neighbor_search::iterator it = search.begin(); it != search.end(); it++)
    {
        IndexArrayLoc[j] = boost::get<1>(it->first);
        DistArrayLoc[j] = tr_dist.inverse_of_transformed_distance(it->second);
        j++;
    }
    delete query;
}

void StencilSelect::LoadSupportConditionNumber(Model* M,Node* CollocNode, long* IndexArray, double* DistArray, int k, int SupSize)
{
    bool CondOK=false;
    double SupRadZero;
    int CountSup=0;
    int j=0;
        
    while (CondOK==false && SupSize<MaxVariableSupSize)
    {
        SupRadZero=DistArray[SupSize-1];
        //Count the number of support nodes
        j=0;
        CountSup=0;
        while (DistArray[j]<=SupRadZero*1.001 && j<k)
        {
            CountSup++;
            j++;
        }
        
        //Get the matrix conditioning
        vector<long> EmptyVector;
        double cond=GetSupportConditioning(M,CollocNode,IndexArray,CountSup,EmptyVector,0,SupRadZero);
        
        //Increase the support size if the conditioning of the matrix is larger than the defined threshold
        if (cond<MaxSupportCond)
        {
            CondOK=true;
        }
        else
        {
            SupSize=SupSize*1.2;
        }
    }
    
    CollocNode->SupSize=CountSup;
    CollocNode->SupNodes = new long[CountSup];
    CollocNode->SupNodesX = new double*[CountSup];
    j=0;
    for (j=0;j<CountSup;j++)
    {
        CollocNode->SupNodes[j]=IndexArray[j];
        CollocNode->SupNodesX[j]=M->Nodes[IndexArray[j]].X;
    }
    CollocNode->SupRadiusSq=pow(SupRadZero*SupScaling,2);
    CollocNode->SupRadius=SupRadZero*SupScaling;
}

void StencilSelect::LoadNormalSupport(Model* M,Node* CollocNode,long* IndexArray,double* DistArray,int k, int SupSize)
{
    double SupRadZero = DistArray[SupSize-1];
    CollocNode->SupRadiusSq=pow(SupRadZero*SupScaling,2);
    CollocNode->SupRadius=SupRadZero*SupScaling;
    
    int CountSup=0;
    int j=0;
    while (DistArray[j]<=SupRadZero*1.001 && j<k)
    {
        CountSup++;
        j++;
    }
    CollocNode->SupSize=CountSup;
    CollocNode->SupNodes = new long[CountSup];
    CollocNode->SupNodesX = new double*[CountSup];
    j=0;
    for (j=0;j<CountSup;j++)
    {
        CollocNode->SupNodes[j]=IndexArray[j];
        CollocNode->SupNodesX[j]=M->Nodes[IndexArray[j]].X;
    }
}

void StencilSelect::LoadNormalSupportEstim(Model* M,Node* CollocNode,long* IndexArray,double* DistArray,int k, int SupSize)
{
    int TempSize=SupSize*M->StencilScaling;
    double SupRadZero = DistArray[TempSize-1];
    CollocNode->SupRadiusSqEstim=pow(SupRadZero*SupScaling,2);
    CollocNode->SupRadiusEstim=SupRadZero*SupScaling;
    
    int CountSup=0;
    int j=0;
    while (DistArray[j]<=SupRadZero*1.001 && j<k)
    {
        CountSup++;
        j++;
    }
    CollocNode->SupSizeEstim=CountSup;
    CollocNode->SupNodesEstim = new long[CountSup];
    j=0;
    for (j=0;j<CountSup;j++)
    {
        CollocNode->SupNodesEstim[j]=IndexArray[j];
    }
}

void StencilSelect::LoadCoefficients(Model* M, Node* CollocNodes, long CollocNodesSize)
{
    for (long i=1; i<=CollocNodesSize; i++)
    {
        int CountSup=CollocNodes[i].SupSize;
        //Initialize the Coeff array if an estimator is requested by the user
        int DeriveNum=0;
        if (M->ErrEstimator==true || M->WeakBC==true || M->Method == "GFD" || M->Method == "GFD_XI" || M->Method == "GFD_XE")
        {
            CollocNodes[i].Coeff = new double *[CountSup];
            if (M->SingularEnrichment != "")
            {
                CollocNodes[i].Coeff2 = new double *[CountSup];
            }
            if (M->Method == "GFD_XI")
            {
                CollocNodes[i].CoeffEnrich = new double *[CountSup];
            }
            
            if (Dimension==2)
            {
                if (M->ApproxOrder==2)
                {
                    DeriveNum=5;
                }
                else if (M->ApproxOrder==3)
                {
                    DeriveNum=9;
                }
                else if (M->ApproxOrder==4)
                {
                    DeriveNum=14;
                }
            }
            else if (Dimension==3)
            {
                DeriveNum=9;
            }
            
            if (CollocNodes[i].CollocationPointOffset==true || CollocNodes[i].CollocDOF==false || M->Method == "GFD_XI" || M->Method == "GFD_XE")
            {
                DeriveNum=DeriveNum+1;
            }
            
            for (int ii=0; ii<CountSup; ii++)
            {
                CollocNodes[i].Coeff[ii] = new double[DeriveNum];
                if (M->SingularEnrichment != "")
                {
                    CollocNodes[i].Coeff2[ii] = new double[DeriveNum];
                }
                if (M->Method == "GFD_XI")
                {
                    CollocNodes[i].CoeffEnrich[ii] = new double[M->NumberEnrichmentCoeff];
                }
            }
        }
    }
}

double StencilSelect::GetSupportConditioning(Model* M,Node* CollocNode, long* IndexArray,int SupSize, vector<long> IndexVector, int ArraySelector, double SupRadZero)
{
    MatrixXd A(1,1);
    int MatA_Size=M->lTayl-1;
    A.resize(MatA_Size,MatA_Size);
    //Initialize the matrix A
    for (int ii=0; ii<MatA_Size; ii++)
    {
        for (int jj=0; jj<MatA_Size; jj++)
        {
            A(ii,jj)=0;
        }
    }

    for (int j=0; j<SupSize; j++)
    {
        long SupIndex=0;
        //Get the support node index from the IndexArray or the IndexVector
        if (ArraySelector==0)
            SupIndex=IndexArray[j];
        else if (ArraySelector==1)
            SupIndex=IndexVector[j];
        
        //Get the distance z_i of the support node to the reference Node
        double z_i=M->DistanceNN(CollocNode->X,M->Nodes[SupIndex].X);
        //Calculate the weight associated to the considered support node
        double SupRad=SupRadZero*SupScaling;
        double SupNodeVol=1;
        double W_=pow(SupNodeVol*M->w(z_i/SupRad),2);

        //Calculte the moments
        int StartIndex=1; //value of 1 if the collocation point is at a node
        for (int jl=StartIndex; jl<lTayl; jl++)
        {
            double Value1=1;
            for (int m=0; m<Dimension; m++)
            {
                Value1=Value1*pow(M->Nodes[SupIndex].X[m]-CollocNode->X[m],M->Exponents(jl,m))/M->factorial(M->Exponents(jl,m));
            }

            for (int jc=jl; jc<lTayl; jc++)
            {
                double Value2=1;
                for (int m=0; m<Dimension; m++)
                {
                    Value2= Value2*pow(M->Nodes[SupIndex].X[m]-CollocNode->X[m],M->Exponents(jc,m))/M->factorial(M->Exponents(jc,m)); 
                }
                double Value=W_*Value1*Value2;
                A(jl-StartIndex,jc-StartIndex)+=Value;

                if (jl != jc)
                {
                    //Fill the symmetric part of the matrix A
                    A(jc-StartIndex,jl-StartIndex)+=Value;
                }
            }
        }
    }
    
    //Get the matrix conditioning
    JacobiSVD<MatrixXd> svd(A);
    double cond = svd.singularValues()(0)/svd.singularValues()(svd.singularValues().size()-1);
    return cond;
}
