#include "CAD_Model.h"

CAD_Model::CAD_Model() {
}

CAD_Model::CAD_Model(const CAD_Model& orig) {
}

CAD_Model::~CAD_Model() {
}

void CAD_Model::LoadCAD_Model(string PathToCAD, int Dim)
{
    //Create a STEP file reader and loads the STEP file
    int PathLen=PathToCAD.length(); 
    char char_array[PathLen+1];
    strcpy(char_array, PathToCAD.c_str()); 
    STEPControl_Reader reader;
    reader.ReadFile(Standard_CString(char_array));
    // Get the number of transferable roots
    Standard_Integer NbRoots = reader.NbRootsForTransfer();
    // translates all transferable roots, and returns the number of successful translations
    Standard_Integer NbTrans = reader.TransferRoots();
    //Get the shape from the file
    TopoShape = reader.OneShape();
    ShapeCAD = new BRepClass3d_SolidClassifier(TopoShape);
    //Load the domain dimension
    Dimension=Dim;
    
    //Load the shapes
    TopoDS_Iterator itor(TopoShape);
    while (itor.More())
    {
        const TopoDS_Shape& shape = itor.Value();
        TopAbs_ShapeEnum type = shape.ShapeType();
        //For the SHELL shape since the model is a 2D model
        if (type==TopAbs_SHELL)
        {
            //Get all faces from shape
            TopTools_IndexedMapOfShape facesAll;
            TopExp::MapShapes(shape,TopAbs_FACE,facesAll);
            NumFaces=facesAll.Extent();
            FaceLoc=new TopoDS_Face[NumFaces];
            for(Standard_Integer i=1; i<=facesAll.Extent(); i++)
            {
                FaceLoc[i-1] = TopoDS::Face(facesAll.FindKey(i));
            }
        }
        itor.Next();
    }
}

bool CAD_Model::NodeInCAD(double* NewX)
{
    //Test which nodes are in the domain
    bool NodeInDomain=true;
    if (Dimension==2)
    {
        //Loop through all the shapes
        for(int i=0; i<NumFaces; i++)
        {
            //Set the search point
            gp_Pnt PntInner(NewX[0],NewX[1],NewX[2]);
            TopoDS_Vertex PntInnerVertex=BRepBuilderAPI_MakeVertex(PntInner);
            //Determine the distance between the point and the face
            BRepExtrema_DistShapeShape dst(PntInnerVertex, FaceLoc[i]);
            dst.Perform();
            if (dst.IsDone())
            {
                if (dst.Value()!=0)
                {
                    NodeInDomain=false;
                }
            }
        }
    }
    else if (Dimension==3)
    {
        TopoDS_Vertex PntInnerVertex;
        gp_Pnt PntInner(NewX[0],NewX[1],NewX[2]);
        PntInnerVertex=BRepBuilderAPI_MakeVertex(PntInner);
        ShapeCAD->Perform(PntInner, 1e-10);
        if (ShapeCAD->State() == TopAbs_IN)
        {
            NodeInDomain=true;
        }
        else
        {
            NodeInDomain=false;
        }
        if (NewX[0]>2.94540710 && NewX[0]<2.94540711 && NewX[1]>-0.0313221974 && NewX[1]<-0.0313221973)
        {
//            if (NodeInDomain==true)
//                cout << "In Domain" << endl;
//            else
//                cout << "Out of the Domain" << endl;
        }
    }
    return NodeInDomain;
}

Vector3d CAD_Model::PointProjection(Vector3d InpP, Vector3d &Normal, Vector3d ElNormal)
{
    //Set the point to be projected onto the curve
    gp_Pnt paa(InpP(0),InpP(1),InpP(2));
    Vector3d ProjPTemp;
    double Dist=0;
    bool DistSet=false;
    int min_i=0;
    int min_ii=1;
    //Get edges from shape
    for(int i=0; i<NumFaces; i++)
    {
        TopTools_IndexedMapOfShape edgesAll;
        TopExp::MapShapes(FaceLoc[i],TopAbs_EDGE,edgesAll);
        Standard_Real aFirst, aLast;
        for(Standard_Integer ii=1; ii<=edgesAll.Extent(); ii++)
        {
            //Get the geometry of the edge
            TopoDS_Edge EdgeLoc = TopoDS::Edge(edgesAll.FindKey(ii));
            gp_Pln Pln;
            Handle(Geom2d_Curve) aCurve = GeomAPI::To2d(BRep_Tool::Curve(EdgeLoc, aFirst, aLast), Pln); 

            //Get the projected point on the curve
            gp_Pnt2d paa2(InpP(0),InpP(1));
            Geom2dAPI_ProjectPointOnCurve PntsProjected(paa2,aCurve,aFirst, aLast);
            if (PntsProjected.NbPoints()>0)
            {
                gp_Pnt2d ppp=PntsProjected.NearestPoint();
                double TempDist=pow(pow(ppp.Coord(1)-InpP(0),2)+pow(ppp.Coord(2)-InpP(1),2),0.5);;
                if (DistSet==false || TempDist<Dist)
                {
                    Dist=TempDist;
                    DistSet=true;
                    ProjPTemp(0)=ppp.Coord(1);
                    ProjPTemp(1)=ppp.Coord(2);
                    ProjPTemp(2)=0;
                    min_i=i;
                    min_ii=ii;
                }
            }
        }
    }
    
    //Get the normal to the curve
    TopTools_IndexedMapOfShape edgesAll;
    TopExp::MapShapes(FaceLoc[min_i],TopAbs_EDGE,edgesAll);
    TopoDS_Edge EdgeLoc = TopoDS::Edge(edgesAll.FindKey(min_ii));
    Standard_Real aFirst, aLast;
    gp_Pln Pln;
    Handle(Geom2d_Curve) aCurve = GeomAPI::To2d(BRep_Tool::Curve(EdgeLoc, aFirst, aLast), Pln); 
    //Get the projected point on the curve
    gp_Pnt2d paa2(InpP(0),InpP(1));
    Geom2dAPI_ProjectPointOnCurve PntsProjected(paa2,aCurve,aFirst, aLast);
    
    gp_Pnt2d LocPoint;
    gp_Vec2d CurveTangent;
    aCurve->D1(PntsProjected.LowerDistanceParameter(),LocPoint,CurveTangent);

    Normal(0)=CurveTangent.Coord(2);
    Normal(1)=-CurveTangent.Coord(1);
    Normal(2)=0;
    if (Normal.dot(ElNormal)<0)
    {
        Normal(0)=-Normal(0);
        Normal(1)=-Normal(1);
    }
    Normal.normalize();
    return ProjPTemp;
}

Vector3d CAD_Model::PointProjection3D(Vector3d InpP, Vector3d &Normal, Vector3d ElNormal)
{
    //Set the point to be projected onto the curve
    Vector3d ProjPTemp;
    double Dist=0;
    bool DistSet=false;
    int min_i=0;
    gp_Pnt ppp_min;
    
    //Get the faces from the shape
    for(int i=0; i<NumFaces; i++)
    {
        Handle_Geom_Surface S = BRep_Tool::Surface(FaceLoc[i]);
        //Get the projected point on the curve
        gp_Pnt paa(InpP(0),InpP(1),InpP(2));
        GeomAPI_ProjectPointOnSurf PntsProjected(paa,S);
        if (PntsProjected.NbPoints()>0)
        {
            gp_Pnt ppp=PntsProjected.NearestPoint();
            double TempDist=pow(pow(ppp.Coord(1)-InpP(0),2)+pow(ppp.Coord(2)-InpP(1),2)+pow(ppp.Coord(3)-InpP(2),2),0.5);
            if (DistSet==false || abs(TempDist)<Dist)
            {
                Dist=abs(TempDist);
                DistSet=true;
                ProjPTemp(0)=ppp.Coord().X();
                ProjPTemp(1)=ppp.Coord().Y();
                ProjPTemp(2)=ppp.Coord().Z();
                ppp_min=ppp;
                min_i=i;
            }
        }
    }
    
    //Get the normal to the curve
    Handle_Geom_Surface S = BRep_Tool::Surface(FaceLoc[min_i]);
    ShapeAnalysis_Surface sas(S);
    
    // get UV of point on surface
    gp_Pnt2d FacePntLocCoord = sas.ValueOfUV(ppp_min, 1e-10);
    gp_Pnt LocPnt;
    gp_Vec LocPntU, LocPntV;
    S->D1(FacePntLocCoord.X(), FacePntLocCoord.Y(), LocPnt, LocPntU, LocPntV);
    
    Vector3d LocPntUVec,LocPntVVec;
    double Sign=1;
    if (FaceLoc[min_i].Orientation()==TopAbs_REVERSED)
    {
        Sign=-1;
    }
    for (int j=0; j<3; j++)
    {
        LocPntUVec(j)=Sign*LocPntU.Coord(j+1);
        LocPntVVec(j)=LocPntV.Coord(j+1);
    }
    
    LocPntUVec.normalize();
    LocPntVVec.normalize();
    Normal=LocPntUVec.cross(LocPntVVec);

    return ProjPTemp;
}

Vector3d CAD_Model::PointProjectionEdge(Vector3d InpP,int RefSurf,int RefEdge)
{
    TopTools_IndexedMapOfShape edgesAll;
    TopExp::MapShapes(FaceLoc[RefSurf],TopAbs_EDGE,edgesAll);
    Standard_Real aFirst, aLast;
       
    //Get the geometry of the edge
    TopoDS_Edge EdgeLoc = TopoDS::Edge(edgesAll.FindKey(RefEdge+1));
    Handle(Geom_Curve) aCurve = BRep_Tool::Curve(EdgeLoc, aFirst, aLast);

    //Get the projected point on the curve
    gp_Pnt paa(InpP(0),InpP(1),InpP(2));
    GeomAPI_ProjectPointOnCurve PntsProjected(paa,aCurve,aFirst, aLast);
    if (PntsProjected.NbPoints()>0)
    {
        gp_Pnt ppp=PntsProjected.NearestPoint();
        InpP(0)=ppp.Coord(1);
        InpP(1)=ppp.Coord(2);
        InpP(2)=ppp.Coord(3);
    }
    return InpP;
}

Vector3d CAD_Model::GetNormal(Vector3d InpP, int RefSurf)
{
    //Project the point on the surface
    gp_Pnt paa(InpP(0),InpP(1),InpP(2));
    Handle_Geom_Surface S = BRep_Tool::Surface(FaceLoc[RefSurf]);
    GeomAPI_ProjectPointOnSurf PntsProjected(paa,S);
    gp_Pnt ppp=PntsProjected.NearestPoint();
    //Get the parameters u,v of the project point
    ShapeAnalysis_Surface sas(S);
    gp_Pnt2d FacePntLocCoord = sas.ValueOfUV(ppp, 1e-10);
    gp_Pnt LocPnt;
    gp_Vec LocPntU, LocPntV;
    //Get the local vectors tangent to the surface at the projected point
    S->D1(FacePntLocCoord.X(), FacePntLocCoord.Y(), LocPnt, LocPntU, LocPntV);
    //Get the direction of the normal
    Vector3d LocPntUVec,LocPntVVec;
    double Sign=1;
    if (FaceLoc[RefSurf].Orientation()==TopAbs_REVERSED)
    {
        Sign=-1;
    }
    for (int j=0; j<3; j++)
    {
        LocPntUVec(j)=Sign*LocPntU.Coord(j+1);
        LocPntVVec(j)=LocPntV.Coord(j+1);
    }
    LocPntUVec.normalize();
    LocPntVVec.normalize();
    Vector3d PointNormal=LocPntUVec.cross(LocPntVVec);
    return PointNormal;
}