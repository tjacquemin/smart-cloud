#include "DoxygenDoc.h"
#include "Model.h"
#include "FileReader.h"
#include "StencilSelect.h"
#include "LinearProblem.h"
#include "OutputFile.h"
#include "VTKOutFile.h"
#include "MPI_Solver.h"
#include "Seq_Solver.h"
#include "ErrorEstimator.h"
#include "Refinement.h"

int main(int argc, char** argv)
{
    int rank, size;
    PetscInitialize(&argc,&argv,PETSC_NULL, PETSC_NULL);
    
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    //Create the model class
    Model* MainModel=NULL;
    
    //Create a file reader
    FileReader* InputFileReader=NULL;
    InputFileReader=new FileReader;
            
    //Create the stencil selection class
    StencilSelect* StencilSelection=NULL;
    StencilSelection = new StencilSelect;
    
    //Create the linear problem
    LinearProblem* Problem=NULL;
    
    //Variables on all processes
    string FilePath;
    char* PathChar;
    double StartTime, PrepEndTime, BuildEndTime, SolveEndTime, PostEndTime;
    //Run the preprocessing using multithreading on the master node
    if (rank==0)
    {
        //Create the model on the master node
        MainModel = new Model;
        MainModel->ProcessNumber=size;
        
        //Program header
        cout << " *******************************************\n";
        cout << " *                                         *\n";
        cout << " *               Legato Team               *\n";
        cout << " *          Luxembourg University          *\n";
        cout << " *                                         *\n";
        cout << " *        Meshless Collocation Code        *\n";
        cout << " *            Thibault JACQUEMIN           *\n";
        cout << " *                2016 - 2021              *\n";
        cout << " *                                         *\n";
        cout << " *******************************************\n";
        
        StartTime=omp_get_wtime();
        MainModel->RunTime = new double[7];
        
        PathChar=argv[1];
        if (PathChar == NULL)
        {
            FilePath="/home/tjacquemin/PhD_Linux/Calculations/2D/2D_Adaptivity/3_Adaptivity/3_GearKey/2_CADModel/Rev2/NoRelax/2DGear_Surface-40000-0.30-c-It1-c.inp"; 
        }
        else
        {
            FilePath=string(PathChar);
        }
        
        cout << "" << endl;
        MainModel->Print("Running: " + FilePath,false);
        time_t TimeAtStart = time(0);
        
        cout << " Start Time: " << ctime(&TimeAtStart) << endl;
        cout << "" << endl;
        cout << " *******************************************" << endl;
        cout << " *              Preprocessing              *" << endl;
        cout << " *******************************************" << endl;
        cout << "" << endl;
        
        //Load the analysis file and create the model
        InputFileReader->LoadInputFileArray(FilePath,MainModel);
        InputFileReader->LoadInputs(MainModel,StencilSelection);
        MainModel->ModelInitialization();
        MainModel->Print("Model loaded from the input file.",true);
        
        //Algorthim from CGAL library
        MainModel->Print("Loading the node supports.",true);
        StencilSelection->LoadSupportCGAL(MainModel,MainModel->Nodes,MainModel->NodeNumber,0);
        MainModel->Print("Supports successfully loaded.",true);
        
        if (MainModel->Voronoi==true || MainModel->Method=="GFD_XE")
        {
            StencilSelection->LoadVoronoi_AllNodes(MainModel);
        }
        
        //Get the support nodes of the voronoi corner nodes for the case GFD_XE
        if (MainModel->Method=="GFD_XE")
        {
            MainModel->Print("Loading the Vornoi corner nodes supports.",true);
            StencilSelection->LoadSupportCGAL(MainModel,MainModel->CNodes,MainModel->CNodesNum,1);
            MainModel->Print("Supports successfully loaded.",true);
        }
        //Create the linear problem on the master node
        Problem = new LinearProblem;

        //Getting the time spent for preliminary tasks
        PrepEndTime=omp_get_wtime();
        MainModel->RunTime[0] = PrepEndTime-StartTime;

        //Build the problem based on the defined model on one process using mutithread
        MainModel->Print("Building the Problem.",true);
        Problem->Build(MainModel);
        
        //Saving the time spent to build the system
        BuildEndTime=omp_get_wtime();
        
        MainModel->RunTime[1] = BuildEndTime-PrepEndTime;
        MainModel->Print("Model Successfully Built.",true);
        
        Problem->LoadMatrices(MainModel); 
    }
    if (rank==0)
    {
        cout << "" << endl;
        cout << " *******************************************" << endl;
        cout << " *               Calculation               *" << endl;
        cout << " *******************************************" << endl;
        cout << "" << endl;
    }
    
    if (size>1)
    {
        MPI_Solver MPI_Solve;
        MPI_Solve.Solve(MainModel,Problem,rank,size);
    }
    
    if (rank==0)
    {
        if (size==1)
        {
            //Solve the system using the Sequential solver
            Seq_Solver Seq_Solve;           
            Seq_Solve.Solve(MainModel,Problem);
        }
        MainModel->Print(Problem->IterationStopReason_Str,true);
        
        SolveEndTime=omp_get_wtime();
        MainModel->RunTime[2] = SolveEndTime-BuildEndTime;
        
        if (rank==0)
        {
            cout << "" << endl;
            cout << " *******************************************" << endl;
            cout << " *             Postprocessing              *" << endl;
            cout << " *******************************************" << endl;
            cout << "" << endl;
        }

        Problem->ComputeSolution(MainModel);
        
        ErrorEstimator* Estimator;
        //Compute the error estimator
        if (MainModel->ErrEstimator==true)
        {
            Estimator= new ErrorEstimator;
            Estimator->ComputeErrorEstimator(MainModel,StencilSelection,Problem);
        }
        
        //Postprocessing Time
        PostEndTime=omp_get_wtime();
        MainModel->RunTime[3]=PostEndTime-SolveEndTime;
        
        //Total Time
        MainModel->RunTime[4]=PostEndTime-StartTime;
        
        //Print the solution to an output file
        OutputFile* Output=NULL;
        Output=new OutputFile;
        Output->PrintOutputFile(MainModel,StencilSelection,Problem,Estimator,FilePath);
        MainModel->Print("Results saved to the .out file.",true);
        
        //Print VTK output file if requested
        if (MainModel->PrintVTK==true)
        {
            VTKOutFile* VTKFile=NULL;
            VTKFile= new VTKOutFile;
            VTKFile->PrintVTKOutputFile(MainModel,Problem,Estimator,FilePath);
        }
        
        //Create the improved model class 
        Refinement* InitRefinement;
        if (MainModel->Adaptivity==true)
        {
            InitRefinement= new Refinement;
            //Get the interation number
            InitRefinement->SetIterationNumber(MainModel,FilePath);
            //Identify which nodes have a high estimated error
            InitRefinement->NewRefinementNodes(MainModel,StencilSelection,Estimator);
            //Print the new input file
            InitRefinement->PrintRefinedModelInpFile(MainModel,FilePath);
            InitRefinement->PrintRefinedModelVtkFile(MainModel,FilePath);
            if (MainModel->DirectCollocRefinement==true)
            {
                InitRefinement->PrintRefinedCollocInpFile(MainModel,FilePath);
            }
        }
        
        double RefinementEndTime;
        RefinementEndTime=omp_get_wtime();
        MainModel->RunTime[5]=RefinementEndTime-PostEndTime;        
        
        if (MainModel->TimeSplit==true)
        {
            cout << " Prep. Runtime=" << MainModel->RunTime[0] << "s" << endl;
            cout << " Buil. Runtime=" << MainModel->RunTime[1] << "s" << endl;
            cout << " Solv. Runtime=" << MainModel->RunTime[2] << "s" << endl;
            cout << " PPro. Runtime=" << MainModel->RunTime[3] << "s" << endl;
            cout << " Refi. Runtime=" << MainModel->RunTime[5] << "s" << endl;
        }
        cout << " Total Runtime=" << MainModel->RunTime[4] << "s" << endl;
        cout << " ----------------------------------------------------------------------" << endl ;
        cout << " " << endl;
        
        //Delete the various vectors
        if (MainModel->PrintStrain==true || MainModel->ErrEstimator==true)
        {
            delete [] Problem->TripStrain;
        }
        delete MainModel;
        delete Problem;
    }
    
    PetscFinalize();
    return 0;
}
