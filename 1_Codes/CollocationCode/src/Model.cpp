#include <stdlib.h>
#include "Model.h"

Model::Model() {
}

Model::Model(const Model& orig) {
}

Model::~Model() {
    delete [] Nodes;
}

void Model::ModelInitialization()
{
    //Get the size of the support matrix
    int lTayl=6;
    if (Dimension==2)
    {
        if (ApproxOrder==2)
        {
            lTayl=6;
        }
        else if (ApproxOrder==3)
        {
            lTayl=10;
        }
        else if (ApproxOrder==4)
        {
            lTayl=15;
        }
    }
    else if (Dimension==3)
    {
        lTayl=10;
    }
    
    //Initialise the Exponent matrix
    Exponents.resize(lTayl,Dimension);
    for (int i=0; i<lTayl; i++)
    {
        for (int j=0; j<Dimension; j++)
        {
            Exponents(i,j)=0;
        }
    }
    
    //Fill the exponent matrix
    if (Dimension==2)
    {
        if (ApproxOrder>=2)
        {
            //d/dx, d/dy, d2/dx2, d2/dxdy, d2/dy2
            Exponents(1,0)=1;Exponents(2,1)=1;
            Exponents(3,0)=2;
            Exponents(4,0)=1;Exponents(4,1)=1;
            Exponents(5,1)=2;
        } 
        if (ApproxOrder>=3)
        {
            //d3/dx3, d3/dx2dy, d3/dy3, d3/dxdy3
            Exponents(6,0)=3;
            Exponents(7,0)=2;Exponents(7,1)=1;
            Exponents(8,1)=3;
            Exponents(9,0)=1;Exponents(9,1)=2;
        }
        if (ApproxOrder>=4)
        {
            //d4/dx4, d4/dx3dy, d4/dx2dy2, d4/dy4, d4/dxdy3
            Exponents(10,0)=4;
            Exponents(11,0)=3;Exponents(11,1)=1;
            Exponents(12,0)=2;Exponents(12,1)=2;
            Exponents(13,1)=4;
            Exponents(14,0)=1;Exponents(14,1)=3;
        }
    }
    else if (Dimension==3)
    {
        Exponents(1,0)=1;Exponents(2,1)=1;Exponents(3,2)=1;
        Exponents(4,0)=2;Exponents(7,1)=2;Exponents(9,2)=2;
        Exponents(5,0)=1;Exponents(5,1)=1;
        Exponents(6,0)=1;Exponents(6,2)=1;
        Exponents(8,1)=1;Exponents(8,2)=1;
    }
    
    //Load all the nodes of the domain into a KD tree
    LoadTree();
}

void Model::LoadTree()
{
    long LastNodeNum=0;
    if (IncludeSingularNodes==false && VisibilityType=="DIRECT")
    {
        LastNodeNum=NodeNumber+SingularNodeNumber;
    }
    else
    {
        LastNodeNum=NodeNumber;
    }
    
    std::vector<Point_3> points(LastNodeNum);
    std::vector<long> indices(LastNodeNum);
    
    for (long i=1; i<=LastNodeNum; i++)
    {
        if (Dimension==2)
        {
            points[i-1]=Point_3(Nodes[i].X[0],Nodes[i].X[1],0.0);
        }
        else if (Dimension==3)
        {
            points[i-1]=Point_3(Nodes[i].X[0],Nodes[i].X[1],Nodes[i].X[2]);
        }
        indices[i-1]=i;
    }
    
    // Insert number_of_data_points in the trees
    tree.insert(
    boost::make_zip_iterator(boost::make_tuple(points.begin(),indices.begin())),
    boost::make_zip_iterator(boost::make_tuple(points.end(),indices.end())));
}

//Split a string based on a delimiter
string Model::split(string str, char delimiter, int Location) 
{
    string internal="";
    stringstream ss(str); 
    string tok;
    int i=0;
    while (getline(ss, tok, delimiter))
    {
	if (i==Location) 
        {
            internal=tok;
            break;
        }
        i++;
    }
    return trim(internal);
}

//Function Trim (remove spaces from name start and end)
string Model::trim(string str)
{
    string::size_type pos = str.find_last_not_of('\r');
    if (pos != string::npos) {str.erase(pos + 1); }
    pos = str.find_last_not_of(' ');
    if (pos != string::npos) {str.erase(pos + 1); }
    pos = str.find_first_not_of(' ');
    if (pos != string::npos) {str.erase(0, pos); }
    return str;
}

void Model::ProcessMemUsage(double& vm_usage, double& resident_set)
{
   using std::ios_base;
   using std::ifstream;
   using std::string;

   vm_usage     = 0.0;
   resident_set = 0.0;

   // 'file' stat seems to give the most reliable results
   //
   ifstream stat_stream("/proc/self/stat",ios_base::in);

   // dummy vars for leading entries in stat that we don't care about
   //
   string pid, comm, state, ppid, pgrp, session, tty_nr;
   string tpgid, flags, minflt, cminflt, majflt, cmajflt;
   string utime, stime, cutime, cstime, priority, nice;
   string O, itrealvalue, starttime;

   // the two fields we want
   //
   unsigned long vsize;
   long rss;

   stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
               >> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
               >> utime >> stime >> cutime >> cstime >> priority >> nice
               >> O >> itrealvalue >> starttime >> vsize >> rss; // don't care about the rest

   stat_stream.close();

   long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
   vm_usage     = vsize / 1024.0/1000;
   resident_set = rss * page_size_kb/1000;
}

void Model::Print(string StringVal, bool MemoryDisplay)
{
    const char* CharVal;
    CharVal=StringVal.c_str();
    cout << " " ;
    if (MemoryDisplay==false)
    {
        int CharLength=StringVal.length();
        int j=0, RowCount=0;
        for(int i=0; i<CharLength; i++)
        {
            cout << CharVal[i];
            j++;
            if (j>=MaxRowWidth)
            {
                cout << "" <<endl;
                cout << " ";
                j=0;
                RowCount++;
            }
        }
        cout << "" <<endl;
        if (RowCount>0) cout << "" <<endl;
    }
    else
    {
        double vm, rss;
        ProcessMemUsage(vm, rss);
        cout << left << setw(MaxRowWidth);
        cout << StringVal;
        cout << setw(7);
        cout << right << setprecision(1) << fixed << vm << " MB" << endl;
    }
    
}

void Model::PrintLoading(long i, long total, string StringVal, bool MemoryDisplay)
{
    int PrintInc=total/500+1;
    if (i % PrintInc == 0 && i!= total)
    {
        double Percentage=(double)i/total*100;
        cout << " " << StringVal << setprecision(1) <<  Percentage << "%";
        cout << '\r';
    }
    else if (i== total)
    {
        Print(StringVal+"100%", true);
    }
}

void Model::GetStartEndNode(int ThreadIndex, int TreadNum, long& Start, long& End, long NumNodes)
{
    for (int i=0; i< TreadNum; i++)
    {
        if (ThreadIndex==i)
        {
            Start=(int)((NumNodes*ThreadIndex)/TreadNum)+1;
            End=(int)((NumNodes*(ThreadIndex+1))/TreadNum);
        }
        if (ThreadIndex==0){Start=1;}
        if (ThreadIndex==TreadNum-1){End=NumNodes;}
    } 
}

void Model::LoadSurface2D()
{
    for (int i=0; i<SurfaceNumber; i++)
    {
        Surfaces[i].points = new Point[Surfaces[i].ElementNumber+1];
        long LocNode=0;
        for (int j=0; j<Surfaces[i].ElementNumber; j++)
        {
            LocNode=Surfaces[i].Elements[j+1][0];
            Surfaces[i].points[j]=Point(Nodes[LocNode].X[0],Nodes[LocNode].X[1]);
        }
        LocNode=Surfaces[i].Elements[Surfaces[i].ElementNumber-1][2];
        Surfaces[i].points[Surfaces[i].ElementNumber]=Point(Nodes[LocNode].X[0],Nodes[LocNode].X[1]);
    }
}

string Model::ToString(double Value, int Precision, int SpaceSize, bool Last, int RoundVal)
{
    //Round the value to indicated power exponent
    if (RoundVal>0)
    {
        Value=round(Value*pow(10,RoundVal))/pow(10,RoundVal);
    }
    if (abs(Value)<1e-15)
    {
        Value=0;
    }
    
    std::stringstream out;
    string TempStr;
    int Len;
    if (Precision==0)
    {
        out << Value;
        Len=10;
    }
    else
    {
        out.precision(Precision);
        out << std::scientific;
        out << Value;
        Len=SpaceSize;
    }
    TempStr=out.str();
    if (Value >= 0)
    {
        TempStr=" " + TempStr;
    }
    if (Last==false)
    {
        TempStr=TempStr + ",";
    }
    while (TempStr.size()<Len && Last==false)
    {
        TempStr=TempStr + " ";
    }
    return TempStr;
}

void Model::ComputeCharactLength()
{
    if (Dimension==2)
    {
        for (long i=1; i<= NodeNumber; i++)
        {
            Nodes[i].CharactLength=pow(Nodes[i].SupRadiusSq*pi,0.5)*pow(Nodes[i].SupSize,-1/Dimension);
        }
    }
    else if (Dimension==3)
    {
        for (long i=1; i<= NodeNumber; i++)
        {
            Nodes[i].CharactLength=pow(Nodes[i].SupRadiusSq,0.5)*pow(4*pi/3,1/Dimension)*pow(Nodes[i].SupSize,-1/Dimension);
        }
    }
    for (long i=1; i<= NodeNumber; i++)
    {
        double TempVal=1;
        for (int j=0; j<Dimension; j++)
        {
            TempVal=TempVal+Nodes[i].X[j];
        }
        Nodes[i].ProdN=TempVal;
    }
}

double Model::ErrorNorm(string ErrorType, string ErrorNormalization, VectorXd RefereSolution, VectorXd ApproxSolution, int ValIndex, int Inc, int Estimator)
{
    double ErrorVal=0;
    double NormalizationVal=1;
    
    //Initialize the normalization
    if (ErrorNormalization=="WEIGHTED")
    {
        NormalizationVal=NodeNumber;
    }
    else if (ErrorNormalization=="RELATIVE")
    {
        NormalizationVal=0;
    }
    
    //Compute the error norm
    for (long i=0; i<NodeNumber; i++)
    {
        double Val=0;
        double Ref=0;
        //Get the difference for normal stress or displacement
        if ((ValIndex<3*(Dimension-1) && Inc>Dimension) || (Inc==Dimension && ValIndex<Dimension))
        {
            Val=ApproxSolution(i*Inc+ValIndex);
            Ref=RefereSolution(i*Inc+ValIndex);
        }
        else if (Inc==Dimension && ValIndex==Dimension)
        {
            for (int j=0; j<Dimension; j++)
            {
                Val+=pow(ApproxSolution(i*Inc+j),2);
                Ref+=pow(RefereSolution(i*Inc+j),2);
            }
            Val=pow(Val,0.5);
            Ref=pow(Ref,0.5);
        }
        //Get the difference for von Mises stress components
        else if (ValIndex==3*(Dimension-1))
        {
            if (Dimension==2 && ModelType=="2D_PLANE_STRESS")
            {
                Val=pow(pow(ApproxSolution(i*Inc+0),2)+pow(ApproxSolution(i*Inc+2),2)-ApproxSolution(i*Inc+0)*ApproxSolution(i*Inc+2)+3*pow(ApproxSolution(i*Inc+1),2),0.5);
                if (Estimator==2)
                    Ref=RefereSolution(i);
                else
                    Ref=pow(pow(RefereSolution(i*Inc+0),2)+pow(RefereSolution(i*Inc+2),2)-RefereSolution(i*Inc+0)*RefereSolution(i*Inc+2)+3*pow(RefereSolution(i*Inc+1),2),0.5);
            }
            else if (Dimension==2 && ModelType=="2D_PLANE_STRAIN")
            {
                double d1=E*(1-nu)/((1+nu)*(1-2*nu));
                double d2=E*nu/((1+nu)*(1-2*nu));
                double E22_Val=(ApproxSolution(i*Inc+2)-d2/d1*ApproxSolution(i*Inc+0))/(d1-pow(d2,2)/d1);
                double E11_Val=(ApproxSolution(i*Inc+0)-d2*E22_Val)/d1;
                double S33_Val=d2*(E11_Val+E22_Val);
                if (Estimator==2)
                {
                    Ref=RefereSolution(i);
                }
                else
                {
                    double E22_Ref=(RefereSolution(i*Inc+2)-d2/d1*RefereSolution(i*Inc+0))/(d1-pow(d2,2)/d1);
                    double E11_Ref=(RefereSolution(i*Inc+0)-d2*E22_Ref)/d1;
                    double S33_Ref=d2*(E11_Ref+E22_Ref);
                    Ref=pow(0.5*(pow(RefereSolution(i*Inc+0)-RefereSolution(i*Inc+2),2)+pow(RefereSolution(i*Inc+0)-S33_Ref,2)+pow(RefereSolution(i*Inc+2)-S33_Ref,2)
                            +6*pow(RefereSolution(i*Inc+1),2)),0.5);
                }
                Val=pow(0.5*(pow(ApproxSolution(i*Inc+0)-ApproxSolution(i*Inc+2),2)+pow(ApproxSolution(i*Inc+0)-S33_Val,2)+pow(ApproxSolution(i*Inc+2)-S33_Val,2)
                        +6*pow(ApproxSolution(i*Inc+1),2)),0.5);
              
            }
            else if (Dimension==3)
            {
                Val=pow(0.5*(pow(ApproxSolution(i*Inc+0)-ApproxSolution(i*Inc+3),2)+pow(ApproxSolution(i*Inc+0)-ApproxSolution(i*Inc+5),2)+pow(ApproxSolution(i*Inc+3)-ApproxSolution(i*Inc+5),2)
                        +6*(pow(ApproxSolution(i*Inc+1),2)+pow(ApproxSolution(i*Inc+2),2)+pow(ApproxSolution(i*Inc+4),2))),0.5);
                if (Estimator==2)
                {
                    Ref=RefereSolution(i);
                }
                else
                {
                    Ref=pow(0.5*(pow(RefereSolution(i*Inc+0)-RefereSolution(i*Inc+3),2)+pow(RefereSolution(i*Inc+0)-RefereSolution(i*Inc+5),2)+pow(RefereSolution(i*Inc+3)-RefereSolution(i*Inc+5),2)
                        +6*(pow(RefereSolution(i*Inc+1),2)+pow(RefereSolution(i*Inc+2),2)+pow(RefereSolution(i*Inc+4),2))),0.5);
                }
            }
        }
        
        double ValRef=Val;
        if (Estimator==1 && EstimatorType=="BEN")
        {
            Val=0;
        }
        
        if (Ref>=9.9999E30)
        {
            if (ErrorNormalization=="WEIGHTED")
            {
                NormalizationVal--;
            }
        }
        else
        {
            if (ErrorType=="L1_ERROR")
            {
                ErrorVal+=abs(Val-Ref);
            }
            else if (ErrorType=="L2_ERROR")
            {
                ErrorVal+=pow(Val-Ref,2);
            }
            else if (ErrorType=="LInf_ERROR")
            {
                if (abs(Val-Ref)>ErrorVal)
                    ErrorVal=abs(Val-Ref);
            }
            if (ErrorNormalization=="RELATIVE")
            {
                if (Estimator==1 && EstimatorType=="BEN")
                {
                    NormalizationVal+=pow(ValRef,2);
                }
                else
                {
                    NormalizationVal+=pow(Ref,2);
                }
            }
        }
    }
    
    //Take the square root for the L2 norms
    if (ErrorType=="L2_ERROR")
        ErrorVal=pow(ErrorVal,0.5);
    
    //Nomalize the error
    if (ErrorNormalization=="RELATIVE")
    {
        NormalizationVal=pow(NormalizationVal,0.5);
    }
    ErrorVal=ErrorVal/NormalizationVal;
    return ErrorVal;
}

//Compute the distance between two nodes
double Model::DistanceNN(double* XCollocN, double* XSupN)
{
    double Dist=0;
    for (int m=0; m<Dimension; m++)
    {
        Dist+=pow(XCollocN[m]-XSupN[m],2);
    }
    Dist=pow(Dist,0.5);
    return Dist;
}

//Get a radial basis weight
double Model::w(double s)
{
    double ww=0;
    double Expo=0;
    s=abs(s);
    
    double Limit=1;
    if(WindowFunctionType=="EXP1" || WindowFunctionType=="EXP2")
    {
        if(WindowFunctionType=="EXP1") 
            Expo=(double)1;
        else if(WindowFunctionType=="EXP2")
            Expo=(double)2.5;
        if (s<=(double)1)
        {
            ww=exp(-(pow(s,Expo)/pow(SupRatio,2)));
            ww=1-s;
        }
        else
        {
            ww=0;   
        }
    }
    else if(WindowFunctionType=="SPLINE_3")
    {
        if (s>=0 && s<0.5)
        {
            ww=(double)2/(double)3-4*pow(s,2)+4*pow(s,3);
        }
        else if (s>0.5 && s<1)
        {
            ww=(double)4/(double)3-4*s+4*pow(s,2)-(double)4/(double)3*pow(s,3);
        }
        else if (s>=1)
        {
            ww=0;
        }
        if (ww>0)
            ww=pow(ww,SupExp);
        else
            ww=0;
    }
    else if(WindowFunctionType=="SPLINE_4")
    {
        if (s<Limit)
        {
            ww=1-6*pow(s,2)+8*pow(s,3)-3*pow(s,4);
        }
        else if (s>=Limit)
        {
            ww=0;
        }
        if (ww>0)
            ww=pow(ww,SupExp);
        else
            ww=0;
    }
    else if(WindowFunctionType=="LINEAR")
    {
        if (s<1)
        {
            ww=1-s;
        }
        else if (s>=1)
        {
            ww=0;
        }
    }
    if (ww<MinWeight)
    {
        ww=MinWeight;
    }
    return ww;
}

//Get the factorial of a number a
double Model::factorial(int a)
{
    double TempVal=1;
    for (int i=1; i<=a; i++)
    {
        TempVal=TempVal*i;
    }
    return TempVal;
}

double Model::GetEnrichmentFunctionVal(long CollocNode, int FNum, int XFuncIndex, int DerivNum)
{
    double Val=0;
    double X=Nodes[CollocNode].X[0];
    double Y=Nodes[CollocNode].X[1];
    double Alpha=0.544483737;
    double S=pow(X,2)+pow(Y,2);
    
    if (S<1E-20 && DerivNum==0)
    {
        Val=1;
    }
    else if (S<1E-20 && DerivNum>0)
    {
        Val=0;
    }
    else if (FNum==1)
    {
        if (DerivNum==0)
        {
            Val=pow(S,Alpha/2);
        }
        else if (DerivNum==1)
        {
            Val=Alpha*X*pow(S,Alpha/2-1);
        }
        else if (DerivNum==2)
        {
            Val=Alpha*Y*pow(S,Alpha/2-1);
        }
        else if (DerivNum==3)
        {
            Val=Alpha*pow(S,Alpha/2-1)+Alpha*(Alpha/2-1)*2*pow(X,2)*pow(S,Alpha/2-2);
        }
        else if (DerivNum==4)
        {
            Val=Alpha*(Alpha/2-1)*2*X*Y*pow(S,Alpha/2-2);
        }
        else if (DerivNum==5)
        {
            Val=Alpha*pow(S,Alpha/2-1)+Alpha*(Alpha/2-1)*2*pow(Y,2)*pow(S,Alpha/2-2);
        }
    }
    else if (FNum==3)
    {
        if (DerivNum==0)
        {
            Val=X;
        }
        else if (DerivNum==1)
        {
            Val=1;
        }
        else
        {
            Val=0;
        }
    }
    else if (FNum==2 && XFuncIndex==1)
    {
        if (DerivNum==0)
        {
            Val=X*pow(S,(Alpha-1)/2);
        }
        else if (DerivNum==1)
        {
            Val=pow(S,(Alpha-1)/2)+pow(X,2)*(Alpha-1)*pow(S,(Alpha-3)/2);
        }
        else if (DerivNum==2)
        {
            Val=X*Y*(Alpha-1)*pow(S,(Alpha-3)/2);
        }
        else if (DerivNum==3)
        {
            Val=X*(Alpha-1)*pow(S,(Alpha-3)/2)+2*X*(Alpha-1)*pow(S,(Alpha-3)/2)+pow(X,3)*(Alpha-1)*(Alpha-3)*pow(S,(Alpha-5)/2);
        }
        else if (DerivNum==4)
        {
            Val=Y*(Alpha-1)*pow(S,(Alpha-3)/2)+pow(X,2)*Y*(Alpha-1)*(Alpha-3)*pow(S,(Alpha-5)/2);
        }
        else if (DerivNum==5)
        {
            Val=X*(Alpha-1)*pow(S,(Alpha-3)/2)+pow(Y,2)*X*(Alpha-1)*(Alpha-3)*pow(S,(Alpha-5)/2);
        }
    }
    else if (FNum==2 && XFuncIndex==2)
    {
        if (DerivNum==0)
        {
            Val=Y*pow(S,(Alpha-1)/2);
        }
        else if (DerivNum==1)
        {
            Val=X*Y*(Alpha-1)*pow(S,(Alpha-3)/2);
        }
        else if (DerivNum==2)
        {
            Val=pow(S,(Alpha-1)/2)+pow(Y,2)*(Alpha-1)*pow(S,(Alpha-3)/2);
        }
        else if (DerivNum==3)
        {
            Val=Y*(Alpha-1)*pow(S,(Alpha-3)/2)+pow(X,2)*Y*(Alpha-1)*(Alpha-3)*pow(S,(Alpha-5)/2);
        }
        else if (DerivNum==4)
        {
            Val=X*(Alpha-1)*pow(S,(Alpha-3)/2)+X*pow(Y,2)*(Alpha-1)*(Alpha-3)*pow(S,(Alpha-5)/2);
        }
        else if (DerivNum==5)
        {
            Val=Y*(Alpha-1)*pow(S,(Alpha-1)/2)+2*Y*(Alpha-1)*pow(S,(Alpha-3)/2)+pow(Y,3)*(Alpha-1)*(Alpha-3)*pow(S,(Alpha-5)/2);
        }
    }
    if (FNum==4)
    {
        if (DerivNum==0)
        {
            Val=1;
        }
        else
        {
            Val=0;
        }
    }
    return Val;
}