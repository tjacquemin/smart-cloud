#include "Element.h"

Element::Element() {
}

Element::Element(const Element& orig) {
}

Element::~Element() {
}

void Element::NewElement(long ElNumber, string ElType, double* Norm)
{
    ElNum=ElNumber;
    ElementType=ElType;
    if (ElementType=="2D_LINEAR" || ElementType=="2D_LINEAR_WITH_BC" || ElementType=="2D_QUAD")
    {
        NumbNodes=2;
        Dim=2;
    }
    else if (ElementType=="3D_LINEAR" || ElType=="3D_LINEAR_WITH_BC_CAD")
    {
        NumbNodes=3;
        Dim=3;
    }
    Normal=new double[Dim];
    double NormalLen=0;
    for (int i=0; i<Dim; i++)
    {
        NormalLen+=pow(Norm[i],2);
    }
    NormalLen=pow(NormalLen,0.5);
    for (int i=0; i<Dim; i++)
    {
        Normal[i]=Norm[i]/NormalLen;
    }
    //Normalize the normal
    AttachedNodes=new Node*[NumbNodes];
}

void Element::SetBC_2D(string BC1Type, string BC1Loc, double BC1Val, string BC2Type, string BC2Loc, double BC2Val)
{
    LocalBoundaryCondition = new bool[2];
    BC_D_Bool = new bool[2];
    BC_N_Bool = new bool[2];
    BC_DOF = new int[2];
    BC_D = new double[2];
    BC_N = new double[2];
    BC_DOF[0]=0;
    BC_DOF[1]=1;
    
    
    if (BC1Type=="D" && BC1Loc=="L" && abs(Normal[1])>0.8)
    {
        BC_D_Bool[1]=true;
        BC_D_Bool[0]=false;
        BC_D[1]=BC1Val;
        BC_D[0]=0;
        BC_N_Bool[1]=false;
        BC_N_Bool[0]=true;
        BC_N[1]=0;
        BC_N[0]=BC2Val;
        BC_DOF[1]=0;
        BC_DOF[0]=1;
        LocalBoundaryCondition[0]=true;
        LocalBoundaryCondition[1]=true;
    }
    else
    {
        if (BC1Type=="D")
        {
            BC_D_Bool[0]=true;
            BC_N_Bool[0]=false;
            BC_D[0]=BC1Val;
        }
        else //if (BC1Type=="N")
        {
            BC_D_Bool[0]=false;
            BC_N_Bool[0]=true;
            BC_N[0]=BC1Val;
        }
        LocalBoundaryCondition[0]=false;
        if (BC1Loc=="L")
        {
            LocalBoundaryCondition[0]=true;
        }
        if (BC2Type=="D")
        {
            BC_D_Bool[1]=true;
            BC_N_Bool[1]=false;
            BC_D[1]=BC2Val;
            if (BC2Loc=="L")
            {
                BC_DOF[0]=1;
                BC_DOF[1]=0;
            }
        }
        else //if (BC2Type=="N")
        {
            BC_D_Bool[1]=false;
            BC_N_Bool[1]=true;
            BC_N[1]=BC2Val;
        }
        LocalBoundaryCondition[1]=false;
        if (BC2Loc=="L")
        {
            LocalBoundaryCondition[1]=true;
        }
    }
}

void Element::SetBC_3D(string BC1Type, string BC1Loc, double BC1Val, string BC2Type, string BC2Loc, double BC2Val, string BC3Type, string BC3Loc, double BC3Val)
{
    LocalBoundaryCondition = new bool[3];
    BC_D_Bool = new bool[3];
    BC_N_Bool = new bool[3];
    BC_DOF = new int[3];
    BC_D = new double[3];
    BC_N = new double[3];
    BC_DOF[0]=0;
    BC_DOF[1]=1;
    BC_DOF[2]=2;
    
    for (int i=0; i<3; i++)
    {
        BC_D_Bool[i]=false;
        BC_N_Bool[i]=false;
        LocalBoundaryCondition[i]=false;
    }
    
    //Direction 1
    if (BC1Type=="D")
    {
        BC_D_Bool[0]=true;
        BC_D[0]=BC1Val;
    }
    else
    {
        BC_N_Bool[0]=true;
        BC_N[0]=BC1Val;
    }
    if (BC1Loc=="L")
    {
        LocalBoundaryCondition[0]=true;
    }
    
    //Direction 2
    if (BC2Type=="D")
    {
        BC_D_Bool[1]=true;
        BC_D[1]=BC2Val;
    }
    else
    {
        BC_N_Bool[1]=true;
        BC_N[1]=BC2Val;
    }
    if (BC2Loc=="L")
    {
        LocalBoundaryCondition[1]=true;
    }
    
    //Direction 3
    if (BC3Type=="D")
    {
        BC_D_Bool[2]=true;
        BC_D[2]=BC3Val;
    }
    else
    {
        BC_N_Bool[2]=true;
        BC_N[2]=BC3Val;
    }
    if (BC3Loc=="L")
    {
        LocalBoundaryCondition[2]=true;
    }
    
//    if (BC1Type=="D" && BC1Loc=="L" && abs(Normal[0])>1/pow(3,0.5))
//    {
//        BC_D_Bool[0]=true;
//        BC_D_Bool[1]=false;
//        BC_D_Bool[2]=false;
//        BC_D[0]=BC1Val;
//        BC_D[1]=0;
//        BC_D[2]=0;
//        BC_N_Bool[0]=false;
//        BC_N_Bool[1]=true;
//        BC_N_Bool[2]=true;
//        BC_N[0]=0;
//        BC_N[1]=BC2Val;
//        BC_N[2]=BC3Val;
//        BC_DOF[0]=0;
//        BC_DOF[1]=1;
//        BC_DOF[2]=2;
//        LocalBoundaryCondition[0]=true;
//        LocalBoundaryCondition[1]=true;
//        LocalBoundaryCondition[2]=true;
//    }
//    else if (BC1Type=="D" && BC1Loc=="L" && abs(Normal[1])>1/pow(3,0.5))
//    {
//        BC_D_Bool[0]=false;
//        BC_D_Bool[1]=true;
//        BC_D_Bool[2]=false;
//        BC_D[0]=0;
//        BC_D[1]=BC1Val;
//        BC_D[2]=0;
//        BC_N_Bool[0]=true;
//        BC_N_Bool[1]=false;
//        BC_N_Bool[2]=true;
//        BC_N[0]=BC2Val;
//        BC_N[1]=0;
//        BC_N[2]=BC3Val;
//        BC_DOF[0]=1;
//        BC_DOF[1]=0;
//        BC_DOF[2]=2;
//        LocalBoundaryCondition[0]=true;
//        LocalBoundaryCondition[1]=true;
//        LocalBoundaryCondition[2]=true;
//    }
//    else if (BC1Type=="D" && BC1Loc=="L" && abs(Normal[2])>1/pow(3,0.5))
//    {
//        BC_D_Bool[0]=false;
//        BC_D_Bool[1]=false;
//        BC_D_Bool[2]=true;
//        BC_D[0]=0;
//        BC_D[1]=0;
//        BC_D[2]=BC1Val;
//        BC_N_Bool[0]=true;
//        BC_N_Bool[1]=true;
//        BC_N_Bool[2]=false;
//        BC_N[0]=BC2Val;
//        BC_N[1]=BC3Val;
//        BC_N[2]=0;
//        BC_DOF[0]=1;
//        BC_DOF[1]=2;
//        BC_DOF[2]=0;
//        LocalBoundaryCondition[0]=true;
//        LocalBoundaryCondition[1]=true;
//        LocalBoundaryCondition[2]=true;
//    }
//    else
//    {
//        if (BC1Type=="D")
//        {
//            BC_D_Bool[0]=true;
//            BC_N_Bool[0]=false;
//            BC_D[0]=BC1Val;
//        }
//        else
//        {
//            BC_D_Bool[0]=false;
//            BC_N_Bool[0]=true;
//            BC_N[0]=BC1Val;
//        }
//        LocalBoundaryCondition[0]=false;
//        if (BC1Loc=="L")
//        {
//            LocalBoundaryCondition[0]=true;
//        }
//        if (BC2Type=="D")
//        {
//            BC_D_Bool[1]=true;
//            BC_N_Bool[1]=false;
//            BC_D[1]=BC2Val;
//            if (BC2Loc=="L")
//            {
//                BC_DOF[0]=1;
//                BC_DOF[1]=0;
//                BC_DOF[2]=2;
//            }
//        }
//        else
//        {
//            BC_D_Bool[1]=false;
//            BC_N_Bool[1]=true;
//            BC_N[1]=BC2Val;
//        }
//        LocalBoundaryCondition[1]=false;
//        if (BC2Loc=="L")
//        {
//            LocalBoundaryCondition[1]=true;
//        }
//        
//        if (BC3Type=="D")
//        {
//            BC_D_Bool[2]=true;
//            BC_N_Bool[2]=false;
//            BC_D[2]=BC3Val;
//            if (BC2Loc=="L")
//            {
//                BC_DOF[0]=1;
//                BC_DOF[1]=2;
//                BC_DOF[2]=0;
//            }
//        }
//        else
//        {
//            BC_D_Bool[2]=false;
//            BC_N_Bool[2]=true;
//            BC_N[2]=BC3Val;
//        }
//        LocalBoundaryCondition[2]=false;
//        if (BC3Loc=="L")
//        {
//            LocalBoundaryCondition[2]=true;
//        }
//    }
}

void Element::LoadConnectionsToNodes()
{
    for (int i=0; i<NumbNodes; i++)
    {
        AttachedNodes[i]->AttachedElements.push_back(ElNum);
    }
}

void Element::LoadCenterNode()
{
    if (ElementType!="2D_QUAD")
    {
        double* X_Center=NULL;
        X_Center = new double[Dim];
        //Initialize the center coordinates
        for (int i=0; i<Dim; i++)
        {
            X_Center[i]=0;
        }
        //Calculate the average coordinates
        for (int i=0; i<Dim; i++)
        {
            for (int j=0; j<NumbNodes; j++)
            {
                X_Center[i]+=AttachedNodes[j]->X[i];
            }
            X_Center[i]=X_Center[i]/NumbNodes;
        }
        CenterNode.SetNode(0,Dim,X_Center[0],X_Center[1],X_Center[2]);
    }
}

double Element::MinElementNodesDistance()
{
    double MinDist=-1;
    vector<Vector3d> ElementEdges;
    ElementEdges.resize(3-2*(3-Dim));
    
    //Get the coordinates of the first edge
    for (int i=0; i<Dim; i++)
    {
        ElementEdges[0](i)=AttachedNodes[0]->X[i]-AttachedNodes[1]->X[i];
        if (Dim==3)
        {
            ElementEdges[1](i)=AttachedNodes[0]->X[i]-AttachedNodes[2]->X[i];
            ElementEdges[2](i)=AttachedNodes[1]->X[i]-AttachedNodes[2]->X[i];
        }
    }
    MinDist=ElementEdges[0].norm();
    if (Dim==3)
    {
        for (int i=1; i<Dim; i++)
        {
            double TempDist=ElementEdges[i].norm();
            if (TempDist<MinDist)
            {
                MinDist=TempDist;
            }
        }
    }
    return MinDist;
}