#include "Surface.h"

Surface::Surface() {
}

Surface::Surface(const Surface& orig) {
}

Surface::~Surface() {
}

void Surface::NewVertices(long VerticesNum,int Dim)
{
    VerticesNumber=VerticesNum;
    Vertices = new Vertex[VerticesNumber+1];
    for (long i=1; i<=VerticesNumber; i++)
    {
        Vertices[i].X = new double[Dim];
        Vertices[i].Normal = new double[Dim];
    }
}

void Surface::NewElements(long ElementNum,int Dim)
{
    ElementNumber=ElementNum;
    Elements = new long*[ElementNum + 1];
    ElementsNormal = new double*[ElementNum + 1];
    Centroid = new double*[ElementNum + 1];
    if ((Type=="2D_LINEAR" || Type=="2D_LINEAR_WITH_BC") && Dim==2)
        VertPerEl=2;
    if (Type=="2D_QUAD" && Dim==2)
        VertPerEl=3;
    for (long i=1; i<=ElementNumber; i++)
    {
        Elements[i]= new long[VertPerEl];
        ElementsNormal[i]= new double[Dim];
        Centroid[i]=new double[Dim];
    }
}