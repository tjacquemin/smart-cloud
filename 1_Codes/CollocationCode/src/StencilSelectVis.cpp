#include "StencilSelect.h"

void StencilSelect::LoadConnectedElements(Model* M)
{
    for (long i=0; i<M->ElementNumber; i++)
    {
        M->SurfElement[i].LoadConnectionsToNodes();
        M->SurfElement[i].LoadCenterNode();
    }
}

void StencilSelect::LoadVisibilitySupport(Model* M,Node* CollocNode,long* IndexArray,double* DistArray,int k, int SupSize)
{
    int SingularNodeInSupport=-1;
    //Initialize an array with information on the inclusion of the node in the support
    bool* SingularNode=NULL;
    SingularNode=new bool[k];
    
    bool* IncludeSupNode=NULL;
    IncludeSupNode=new bool[k];
    for(int j=0; j<k; j++)
    {
        SingularNode[j]=false;
        IncludeSupNode[j]=false;
        if (M->Nodes[IndexArray[j]].IsSingular==true)
        {
            SingularNodeInSupport=IndexArray[j];
            if (M->IncludeSingularNodes==false)
                SingularNode[j]=true;
        }
    }
    
    //Number of nodes in the support
    int CountSup=0;
    double SupRad=0;
    //Determine the nodes to be discarded from the support (singular nodes + visibility criteria)
    SelectSupportNodes(M,CollocNode,IndexArray,DistArray,IncludeSupNode,SingularNodeInSupport,SingularNode,SupSize,k,&CountSup,&SupRad);
    CollocNode->SetSupRadius(pow(SupRad*SupScaling,2));        
    CollocNode->SupSize=CountSup;
    CollocNode->SupNodes = new long[CountSup];
    int j=0;
    int index=0;
    for (j=0; j<k; j++)
    {
        if (IncludeSupNode[j]==true)
        {
            CollocNode->SupNodes[index]=IndexArray[j];
            index++;
        }
    }    
    delete[] SingularNode;
}

void StencilSelect::SelectSupportNodes(Model* M,Node* CollocNode,long* IndexArray,double* DistArray,bool* IncludeSupNode,int SingularNodeInSupport,bool* SingularNode,int SupSize, int k, int* CountSup,double* SupRad)
{
    int Count=0;
//    if (SingularNodeInSupport==-1 || VisibilityType=="NONE")
    if (M->VisibilityType=="NONE")
    {
        *SupRad=DistArray[SupSize-1]*1.001;
        for (int j=0; j<k; j++)
        {
            if(DistArray[j]<*SupRad && SingularNode[j]==false)
            {
                IncludeSupNode[j]=true;
                Count++;
            }
        }
        *CountSup=Count;
    }
    else if (M->VisibilityType=="DIRECT")
    {
        int j=0;
        CollocNode->HasSingularSupNodes=true;
        while (Count<SupSize)
        {
            if (SingularNode[j]==false && IsHidden(M,CollocNode,IndexArray[j],SingularNodeInSupport,DistArray[SupSize-1])==false)
            {
                IncludeSupNode[j]=true;
                Count++;
            }
            j++;
        }
        *SupRad=DistArray[j-1]*1.001;
        while (j<k)
        {
            if (DistArray[j]<*SupRad)
            {
                if (SingularNode[j]==false && IsHidden(M,CollocNode,IndexArray[j],SingularNodeInSupport,DistArray[SupSize-1])==false)
                {
                    IncludeSupNode[j]=true;
                    Count++;
                }
            }
            j++;
        }
        *CountSup=Count;
    }
    else if (M->VisibilityType=="DIFFRACTION")
    {
        //Create a vector pair to sort the distances
        vector<vector<double>> DistPair;
        DistPair.resize(k);
        for (int j=0; j<k; j++)
        {
            vector<double> TempVect;
            TempVect.resize(2);
            //Determine the updated distances based on the diffraction criterion
            if (IsHidden(M,CollocNode,IndexArray[j],SingularNodeInSupport,DistArray[SupSize-1])==true && SingularNode[j]==false)
            {
                double dist1=0,dist2=0;
                for (int kk=0; kk<Dimension; kk++)
                {
                    dist1+=pow(CollocNode->X[kk]-M->Nodes[SingularNodeInSupport].X[kk],2);
                    dist2+=pow(M->Nodes[SingularNodeInSupport].X[kk]-M->Nodes[IndexArray[j]].X[kk],2);
                }
                DistArray[j]=pow(dist1,0.5)+pow(dist2,0.5);
            }
            TempVect[0]=DistArray[j];
            TempVect[1]=IndexArray[j];
            DistPair[j]=TempVect;
        }
        //Sort the updated distance array
        sort(DistPair.begin(),DistPair.end(), [](const vector< double >& a, const vector< double >& b){ return a[0] < b[0]; } );
        *SupRad=DistPair[SupSize-1][0]*1.001;
        for (int j=0; j<k; j++)
        {
            long IndexVal=DistPair[j][1];
            SingularNode[j]=M->Nodes[IndexVal].IsSingular;
            if(SingularNode[j]==false && DistPair[j][0]<*SupRad)
            {
                IncludeSupNode[Count]=true;
                DistArray[Count]=DistPair[j][0];
                IndexArray[Count]=DistPair[j][1];
                Count++;
            }
            else if (SingularNode[j]==true && DistPair[j][0]<*SupRad)
            {
                IncludeSupNode[Count]=false;
                *SupRad=DistPair[SupSize][0]*1.001;
            }
        }
        *CountSup=Count;
        
        //Indicate that the collocation node has diffracted nodes on its support
        CollocNode->HasDiffractedSupNodes=true;
        CollocNode->DiffractedDist=new double[Count];
        //Load the diffracted distance array
        for (int j=0; j<Count; j++)
        {
            CollocNode->DiffractedDist[j]=DistArray[j];
        }
    }
}

void StencilSelect::LoadVisibilitySupportFromBE(Model* M,Node* CollocNode,long* IndexArray,double* DistArray,int k, int SupSize)
{
    //Get all the elements in the support
    vector<long> ElementsInSup;
    ElementsInSup.clear();
    long SupNodeNum=0;
    for (int j=0; j<k; j++)
    {
        SupNodeNum=IndexArray[j];
        if (M->Nodes[SupNodeNum].AttachedElements.size()>0)
        {
            for (int k=0; k<M->Nodes[SupNodeNum].AttachedElements.size(); k++)
            {
                ElementsInSup.push_back(M->Nodes[SupNodeNum].AttachedElements[k]);
            }
        }
    }
    //Sort the elements vector and remove the duplicates
    if (ElementsInSup.size()>0)
    {
        sort(ElementsInSup.begin(),ElementsInSup.end());
        ElementsInSup.erase(unique(ElementsInSup.begin(),ElementsInSup.end()),ElementsInSup.end());
    }
    
    Tree LocTree;
    //Load the elements centroid nodes into a tree
    int ElSupSize=ElementsInSup.size();
    std::vector<Point_3> points(ElSupSize);
    std::vector<long> indices(ElSupSize);
    
    for (long i=0; i<ElSupSize; i++)
    {
        if (Dimension==2)
            points[i]=Point_3(M->SurfElement[ElementsInSup[i]-1].CenterNode.X[0],M->SurfElement[ElementsInSup[i]-1].CenterNode.X[1],0);
        else if (Dimension==3)
            points[i]=Point_3(M->SurfElement[ElementsInSup[i]-1].CenterNode.X[0],M->SurfElement[ElementsInSup[i]-1].CenterNode.X[1],M->SurfElement[ElementsInSup[i]-1].CenterNode.X[2]);
        indices[i]=ElementsInSup[i];
    }
    
    // Insert number_of_data_points in the tree
    LocTree.insert(
    boost::make_zip_iterator(boost::make_tuple(points.begin(),indices.begin())),
    boost::make_zip_iterator(boost::make_tuple(points.end(),indices.end())));
    
    //Identify the nodes which shall be included in the support
    bool* IncludeSupNode=NULL;
    IncludeSupNode=new bool[k];
    
    //For each node of the IndexArray, assess if the nodes shall be included in the support
    bool SupLoaded=false;
    int SupNodeIndex=0;
    int CountSupNodes=0;
    double SupRad=0;
    double SupRad2=0;
    
    //Get the center of the search segment
    double* SupSegmentCenter=NULL;
    SupSegmentCenter=new double[Dimension];
    
    //List the selected nodes
    vector<long> SelectedNodesList;
    SelectedNodesList.clear();
    
    double cond=0;
    //Loop through all the support nodes until the support is loaded
    while (SupLoaded==false && SupNodeIndex<k)
    {
        IncludeSupNode[SupNodeIndex]=true;
        cond=0;
        //If the supports includes any boundary elements
        if (ElementsInSup.size()>0)
        {
            //Get a reduced vector containing the elements the closest to the support segment considered
            vector<long> ElementsInSupLoc;
            ElementsInSupLoc.clear();
            //Get the center of the search segment
            for (int l=0; l<Dimension; l++)
            {
                SupSegmentCenter[l]=(CollocNode->X[l]+M->Nodes[IndexArray[SupNodeIndex]].X[l])/2;
            }
            //Create the search query
            Point_3* query=NULL;
            if (Dimension==2)
                query= new Point_3(SupSegmentCenter[0], SupSegmentCenter[1], 0);
            else
                query= new Point_3(SupSegmentCenter[0], SupSegmentCenter[1], SupSegmentCenter[2]);
            
            //Number of selected elements
            int SupElNum=5*Dimension;
            if (ElementsInSup.size()<SupElNum)
                SupElNum=ElementsInSup.size();

            //Search the K nearest neighbours
            Distance tr_dist;
            K_neighbor_search search(LocTree, *query, SupElNum, 0.0,true,tr_dist,true);
            
            //Fill the index and distance array with the values using the iterator
            for(K_neighbor_search::iterator it = search.begin(); it != search.end(); it++)
            {
                ElementsInSupLoc.push_back(boost::get<1>(it->first));
            }
            delete query;
            
            //Loop over all boundary elments and assess if there is any intersection with the considered ray
            vector<double> CollocX;
            CollocX.resize(3);
            long CollocNodeIndex=CollocNode->NodeNum;
            for (int j=0; j<M->Dimension; j++)
            {
                CollocX[j]=CollocNode->X[j];
            }
            for (int j=0; j<ElementsInSupLoc.size(); j++)
            {
                if (IntersectsBE(M,CollocX,CollocNodeIndex,IndexArray[SupNodeIndex],ElementsInSupLoc[j])==true)
                {
                    IncludeSupNode[SupNodeIndex]=false;
                }
            }
        }
        if (IncludeSupNode[SupNodeIndex]==true)
        {
            if (SupRad==0)
            {
                CountSupNodes++;
                SelectedNodesList.push_back(IndexArray[SupNodeIndex]);
            }
            else if (DistArray[SupNodeIndex]<=SupRad)
            {
                CountSupNodes++;
                SelectedNodesList.push_back(IndexArray[SupNodeIndex]);
            }
            else if (DistArray[SupNodeIndex]>SupRad && SupRad>0)
            {
                SupLoaded=true;
            }
        }
        //Set the sup radius
        if (CountSupNodes==SupSize && SupRad==0)
        {
            SupRad=DistArray[SupNodeIndex]*1.001;
            SupRad2=SupRad;
        }
        //Assess if the matrix conditioning is below the selected threshold
        if (SupLoaded==true && StencilSelection=="COND" && SupNodeIndex+1<k && SupSize*1.2<MaxVariableSupSize)
        {
            //Get the matrix conditioning
            cond=GetSupportConditioning(M,CollocNode,IndexArray,CountSupNodes,SelectedNodesList,1,SupRad);
            if (cond>MaxSupportCond)
            {
                SupSize=SupSize*1.2;
                SupLoaded=false;
                SupRad=0;
            }
        }
        SupNodeIndex++;
    }
    delete[] SupSegmentCenter;
    
    CollocNode->SupRadiusSq=pow(SupRad2/1.001*SupScaling,2);
    CollocNode->SupRadius=SupRad2/1.001*SupScaling;
    CollocNode->SupSize=CountSupNodes;
    CollocNode->SupNodes = new long[CountSupNodes];
    CollocNode->SupNodesX = new double*[CountSupNodes];
    int j=0;
    int index=0;
    while (index<CountSupNodes)
    {
        if (IncludeSupNode[j]==true)
        {
            CollocNode->SupNodes[index]=IndexArray[j];
            CollocNode->SupNodesX[index]=M->Nodes[IndexArray[j]].X;
            index++;
        }
        //List the discarded nodes from the visibility criterion
        else if (IncludeSupNode[j]==false && DistArray[j]<SupRad)
        {
//            cout << " i=" << CollocNode->NodeNum << ", SupNode=" << IndexArray[j] << endl;
        }
        j++;
    }
}

bool StencilSelect::IsHidden(Model* M,Node* ColocNode, long SupportNodeNum, long SingularNodeNum, double Radius)
{
    bool TempResult=false;
    if (Dimension==2)
    {
        Vector3d SupVect;
        Vector3d SingVect;
        Vector3d CollocNode;
        Vector3d SingNode;
        SingularNodeNum=M->SingularNode;
        for (int i=0; i<Dimension; i++)
        {
            CollocNode(i)=ColocNode->X[i];
            SingNode(i)=M->Nodes[SingularNodeNum].X[i];

            SupVect(i)=M->Nodes[SupportNodeNum].X[i]-ColocNode->X[i];
            SingVect(i)=400*M->Nodes[SingularNodeNum].Normal[0][i];//Radius*
        }
        CollocNode(2)=0;
        SingNode(2)=0;
        SupVect(2)=0;
        SingVect(2)=0;

        double t, u;
        //Solution of the problem: Sing + t SingVect = Colloc + u SupVect  
        //                         t = (CollocNode − SingNode) × SupVect / (SingVect × SupVect)
        //                         u = (CollocNode − SingNode) × SingVect / (SingVect × SupVect)
        Vector3d P1, P2, P3, P4, P5, P6;
        P1=SingVect.cross(SupVect);
        P2=SupVect.cross(SingVect);
        P5=CollocNode - SingNode;
        P6=SingNode - CollocNode;
        P3=P5.cross(SupVect);
        P4=P6.cross(SingVect);
        
        t=P3(2)/P1(2);
        u=P4(2)/P2(2);
               
        //if (t>0 && t<1 && u>0 && u<1)
        if (t>=0 && t<=1 && u>=0 && u<=1)
        {
            TempResult=true;
        }
        if (ColocNode->NodeNum==SingularNodeNum)
        {
            TempResult=false;
        }
    }
    else if (Dimension==3)
    {
        //Test at 10 locations of the segment connecting the support node to the
        //collocation node that the point is in the L-Shape domain
        double X_Val=0;
        double Y_Val=0;
        double Z_Val=0;
        for (int i=1; i<10; i++)
        {
            X_Val=ColocNode->X[0]+(M->Nodes[SupportNodeNum].X[0]-ColocNode->X[0])/10*i;
            Y_Val=ColocNode->X[1]+(M->Nodes[SupportNodeNum].X[1]-ColocNode->X[1])/10*i;
            Z_Val=ColocNode->X[2]+(M->Nodes[SupportNodeNum].X[2]-ColocNode->X[2])/10*i;
            if (X_Val>0 && Y_Val>0 && Z_Val>0)
            {
                TempResult=true;
            }
        }
    }
    return TempResult;
}

bool StencilSelect::IntersectsBE(Model* M, vector<double> CollocX, long CollocIndex, long SupNode, long ElNum)
{
    Node** StartEndNode=NULL;
    StartEndNode=new Node*[2];
    int Last_i_Index=1;
    if (CollocIndex==0)
        Last_i_Index=2;
    
    double ThresholdValue=cos((90-M->VisibilityThresholdAngle)*pi/180);
    bool TempResult=false;
    double Epsilon2=sin(M->VisibilityThresholdAngle*pi/180);
    double Epsilon3=M->VisibilityThresholdAngle*pi/180;
    
    for (int ii=0; ii<Last_i_Index; ii++)
    {
        if (CollocIndex>0)
        {
            StartEndNode[0]=M->SurfElement[ElNum-1].AttachedNodes[0];
            StartEndNode[1]=M->SurfElement[ElNum-1].AttachedNodes[1];
            if (Dimension==3)
                StartEndNode[2]=M->SurfElement[ElNum-1].AttachedNodes[2];
        }
        else
        {
            ThresholdValue=cos((90-0)*pi/180);
            Epsilon2=0;
            Epsilon3=0;
            if (ii==0)
            {
                StartEndNode[0]=M->SurfElement[ElNum-1].AttachedNodes[0];
                StartEndNode[1]=&M->SurfElement[ElNum-1].CenterNode;
                
            }
            else if (ii==1)
            {
                StartEndNode[0]=&M->SurfElement[ElNum-1].CenterNode;
                StartEndNode[1]=M->SurfElement[ElNum-1].AttachedNodes[1];
            }
        }

        //Create and load the matrices A and C
        MatrixXd A;
        A.resize(Dimension,Dimension);
        MatrixXd C;
        C.resize(Dimension,1);
        for (int i=0; i<Dimension; i++)
        {
            A(i,0)=CollocX[i]-M->Nodes[SupNode].X[i];
            for (int j=1; j<Dimension; j++)
            {
                 A(i,j)=StartEndNode[j]->X[i]-StartEndNode[0]->X[i];
            }
            C(i,0)=CollocX[i]-StartEndNode[0]->X[i];
        }

        //Get the angle between the element and the segment
        double SegmentElAngle=GetSegmentElementAngle(M,A);

        //Create the element and support vectors
        Vector3d SupVect;
        SupVect(2)=0;
        double DistVect=0;
        //Load the element and support vectors
        for (int i=0; i<Dimension; i++)
        {
            SupVect(i)=M->Nodes[SupNode].X[i]-CollocX[i];
            DistVect=pow(SupVect(i),2);
        }
        DistVect=pow(DistVect,0.5);
                
        if (abs(A.determinant())>0)
        {
            //Define a threshold for coincident points (1000th of the distance between the colloc node and the support node)
            double Epsilon1=DistVect/1000;
            double t, u;
            double v=0.5;

            if ((abs(SegmentElAngle))>ThresholdValue)
            {            
                //Solve the linear system
                MatrixXd B=A.lu().solve(C);
                t=B(0,0);
                u=B(1,0);
                if (Dimension==3)
                {
                    v=B(2,0);
                }
                //If the support ray intersects the element in its middle section
                if (t>Epsilon1 && t<1-Epsilon1 && u>Epsilon1 && u<1-Epsilon1 && v>Epsilon1 && v<1-Epsilon1)
                {
                    bool Test1=false;
                    if (Dimension==3 && u+v<1-Epsilon1)
                    {
                        Test1=true;
                    }
                    else if (Dimension==2)
                    {
                        Test1=true;
                    }
                    
                    SupVect.normalize();
                    double ScalarProd=0;
                    for (int j=0; j<Dimension; j++)
                    {
                        ScalarProd+=M->SurfElement[ElNum-1].Normal[j]*SupVect(j);
                    }
                    if (ScalarProd<Epsilon2 && abs(acos(abs(ScalarProd))-pi/2)>Epsilon3 && Test1==true)
                        TempResult=true;
                }
                else
                {
                    double ScalarProd=0;
                    long ElNumTemp=0;
                    int OppNormalCount=0;

                    //If the support ray intersects the element at the first node of the element
                    if (abs(u)<Epsilon1 || abs(1-u)<Epsilon1)
                    {
                        SupVect.normalize();
                        OppNormalCount=0;
                        //Initialize the node index for the case where abs(u)<Epsilon1 
                        int NodeIndex=0;
                        if (abs(1-u)<Epsilon1)
                            NodeIndex=1;

                        //Test if the adjacents element normals are in line with the element or opposit to it
                        for (int i=0; i<M->SurfElement[ElNum-1].AttachedNodes[NodeIndex]->AttachedElements.size(); i++)
                        {
                            ElNumTemp=M->SurfElement[ElNum-1].AttachedNodes[NodeIndex]->AttachedElements[i];
                            ScalarProd=0;
                            for (int j=0; j<Dimension; j++)
                            {
                                ScalarProd+=M->SurfElement[ElNumTemp-1].Normal[j]*SupVect(j);
                            }
                            //Determine if the support ray exits the domain
                            if (ScalarProd>Epsilon2 && t<0.5 && abs(acos(abs(ScalarProd))-pi/2)>Epsilon3)
                                OppNormalCount++;
                            //Determine if the support ray enters into the domain
                            else if (ScalarProd<Epsilon2 && t>0.5 && abs(acos(abs(ScalarProd))-pi/2)>Epsilon3)
                                OppNormalCount++;
                            if (OppNormalCount==M->SurfElement[ElNum-1].AttachedNodes[NodeIndex]->AttachedElements.size())
                            {
                                TempResult=true;
                            }
                        }
                    }
                }
            }
        }
    }
    return TempResult;
}

double StencilSelect::GetSegmentElementAngle(Model* M,MatrixXd B)
{
    double TempResult=0;
    VectorXd Segment=B.col(0);
    Segment.normalize();
    VectorXd Element1=B.col(1);
    Element1.normalize();
    VectorXd Element2;
    if (Dimension==3)
    {
        Element2=B.col(2);
        Element2.normalize();
    }
    if (Dimension==2)
    {
        //Get the element normal
        double TempVal=Element1(0);
        Element1(0)=Element1(1);
        Element1(1)=TempVal;
        TempResult=Segment.dot(Element1);
    }
    else if (Dimension==3)
    {
        VectorXd ElementNormal;
        ElementNormal.resize(Dimension);
        ElementNormal(0)=Element1(1)*Element2(2)-Element1(2)*Element2(1);
        ElementNormal(1)=Element1(2)*Element2(0)-Element1(0)*Element2(2);
        ElementNormal(2)=Element1(0)*Element2(1)-Element1(1)*Element2(0);
        ElementNormal.normalize();
        TempResult=Segment.dot(ElementNormal);
    }
    return TempResult;
}

bool StencilSelect::InDomain(Model* M,vector<double> PointVect, bool Flag, bool NewInnerNodeTest)
{
    bool NodeInSurf=true;
    //Determine the support nodes of the candidate node
    //Create the search query
    Point_3* query=NULL;
    query= new Point_3(PointVect[0],PointVect[1],PointVect[2]);
    if (M->Nodes[0].NodeInitialized==false)
    {
        M->Nodes[0].X=new double[3];
    }
    M->Nodes[0].X[0]=PointVect[0];
    M->Nodes[0].X[1]=PointVect[1];
    M->Nodes[0].X[2]=PointVect[2];
    
//    if (M->Nodes[0].X[0]>2.94540710 && M->Nodes[0].X[0]<2.94540711 && M->Nodes[0].X[1]>-0.0313221974 && M->Nodes[0].X[1]<-0.0313221973)
//    {
//        Flag=true;
//        cout << "flag found" << endl;
//    }
    
    Distance tr_dist;
    int k=3;
    if (M->Dimension==3)
        k=6;
    
    long* SupNode=NULL;
    SupNode=new long[k];
    int i=0;

    // search K nearest neighbours
    K_neighbor_search search(M->tree, *query, k, 0.0,true,tr_dist,true);
    //Determine if a boundary node is in the K nearest neighbours
    bool HasBoundSupNode=false;
    for(K_neighbor_search::iterator it = search.begin(); it != search.end(); it++)
    {
        long SupNodeTemp=boost::get<1>(it->first);
        if (M->Nodes[SupNodeTemp].BoundaryNode==true || M->Nodes[SupNodeTemp].AttachedElements.size()>0)
        {
            HasBoundSupNode=true;
        }
    }
    if (HasBoundSupNode==false)
    {
        NodeInSurf=true;
    }
    else if (M->CAD_Model_Available==false)
    {
        vector<long> ElementsInSup;
        ElementsInSup.clear();
        //Fill the index and distance array with the values using the iterator
        for(K_neighbor_search::iterator it = search.begin(); it != search.end(); it++)
        {
            SupNode[i] = boost::get<1>(it->first);
            if (Flag==true)
            {
                cout << "SupNode[i]=" << SupNode[i] << "," << M->Nodes[SupNode[i]].X[0]*1000 << "," << M->Nodes[SupNode[i]].X[1]*1000 << endl;
            }

            //Get all the elements in the support
            for (int j=0; j<M->Nodes[SupNode[i]].AttachedElements.size(); j++)
            {
                ElementsInSup.push_back(M->Nodes[SupNode[i]].AttachedElements[j]);
                if (Flag==true)
                {
                    cout << "SupNode[i]=" << SupNode[i] << "," << M->Nodes[SupNode[i]].AttachedElements[j] << endl;
                }
            }
            i++;
        }
        
        //Sort the elements vector and remove the duplicates
        if (ElementsInSup.size()>0)
        {
            sort(ElementsInSup.begin(),ElementsInSup.end());
            ElementsInSup.erase(unique(ElementsInSup.begin(),ElementsInSup.end()),ElementsInSup.end());
        }
        bool LastSupNodeFound=false;
        int SupNodeIndex=0;

        //If the supports include any boundary elements
        if (ElementsInSup.size()>0)
        {
            NodeInSurf=true;
            while (NodeInSurf==true && LastSupNodeFound==false)
            {
                //Loop over all boundary elments and assess if there is any intersection with the considered ray
                for (int j=0; j<2; j++)
                {
                    if (M->Nodes[SupNode[SupNodeIndex]].AttachedElements.size()>0)
                    {
                        if (IntersectsBE(M,PointVect,0,SupNode[SupNodeIndex],ElementsInSup[j])==true)
                        {
                            if (Flag==true )
                            {
                                cout << "SupNode[SupNodeIndex]=" << SupNode[SupNodeIndex] << endl;
                            }
                            NodeInSurf=false;
                        }
                        else 
                        {
                            NodeInSurf=true;
                        }
                    }
                }
                SupNodeIndex++;
                if (SupNodeIndex==k)
                    LastSupNodeFound=true;
            }
        }

        delete[] SupNode;
        delete query;
    }
    else if (M->CAD_Model_Available==true)
    {
        double* NewX=NULL;
        NewX=new double[3];
        NewX[0]=PointVect[0];
        NewX[1]=PointVect[1];
        if (Dimension==2)
            NewX[2]=0;
        else
            NewX[2]=PointVect[2];
        NodeInSurf=M->ModelCAD->NodeInCAD(NewX);
        delete[] NewX;
    }
    if (HasBoundSupNode==true && NewInnerNodeTest==true)
    {
        NodeInSurf=false;
    }
    return NodeInSurf;
}