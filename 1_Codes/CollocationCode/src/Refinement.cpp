#include "Refinement.h"

Refinement::Refinement() {
}

Refinement::Refinement(const Refinement& orig) {
}

Refinement::~Refinement() {
}

void Refinement::SetIterationNumber(Model* M, string Path)
{
    //Identify if iterations have already been done
    int pos = 0;
    pos=Path.find("-It");
    if (pos>0)
    {
        //Increment the iteration number
        string ItNum=Path;
        ItNum.replace(ItNum.begin(),ItNum.begin()+pos+3,"");
        ItNum.replace(ItNum.end()-6,ItNum.end(),"");
        ItNumber=stoi(ItNum);
        if (ItNumber-2*round((ItNumber-0.5)/2)==0)
            EvenItNumber=true;
        else
            EvenItNumber=false;
        if (M->RefineEvenBOnly==false)
        {
            EvenItNumber=true;
        }
    }
}

void Refinement::NewRefinementNodes(Model* M, StencilSelect* S, ErrorEstimator* E)
{
    IdentifyRefinementNodes(M,E);
    M->Print("Refinement locations identified.",true);
    SetExistingNodesGlobalIndices(M);
    
    if (EvenItNumber==true)
    {
        FindNewBoundaryNodes(M,E);
        M->Print("New boundary nodes added.",true);
    }
    
    int StepNum=1;
    for (int i=0; i<StepNum; i++)
    {
        if (M->RefMethod=="VORO_CN" || M->RefMethod=="VORO_NN")
        {
            FindPotentialNewNodes_Voro(M,E,i);
        }
        
        IdentifyDuplicatedPoints(M,S,i);
        IdentifyNewNodesOutside(M,S,i);
    }
    
    RemoveDuplicatedPoints(M);
    SetRemainingNodesGlobalIndices(M);
    
    M->Print("New inner nodes added.",true);
    
    if ((EvenItNumber==true && M->Dimension==2) || M->Dimension==3)
        AddNewBoundaryElements(M);
    
    M->Print("New boundary elements added.",true);
    GetClosestNode(M);
}

void Refinement::IdentifyRefinementNodes(Model* M, ErrorEstimator* E)
{
    double MinVal=E->ErrorEstim(0);
    double MaxVal=MinVal;
    long MaxNodeNum=M->NodeNumber*M->AdaptFraction;
    vector<vector<double>> SortedError;
    vector<long> NodeErrorIndex;
    
    //Get the min and max errors
    if (M->AdaptThreshold>0 || M->AdaptThresholdLog>0)
    {
        for (long i=0; i<M->NodeNumber; i++)
        {
            if (E->ErrorEstim(i)<MinVal) 
                MinVal=E->ErrorEstim(i);
            if (E->ErrorEstim(i)>MaxVal) 
                MaxVal=E->ErrorEstim(i);
        }
    }
    //Sort the nodes per max error
    if (M->AdaptThresholdFrac>0)
    {
        SortedError.resize(M->NodeNumber);
        NodeErrorIndex.resize(M->NodeNumber);
        //Fill the sorted error array
        for (long i=0; i<M->NodeNumber; i++)
        {
            SortedError[i].resize(2);
            SortedError[i][0]=i;
            SortedError[i][1]=E->ErrorEstim(i);
        }
        //Sort the array by error
        std::sort(SortedError.begin(),SortedError.end(),[](const vector<double>& a,const std::vector<double>& b){return a[1]>b[1];});
        for (long i=0; i<M->NodeNumber; i++)
        {
            NodeErrorIndex[int(SortedError[i][0])]=i;
        }
    }
    
    //Calculate the error threshold based on the min and max errors
    double MinError = MinVal + (MaxVal-MinVal)*M->AdaptThreshold;
    if (M->AdaptThresholdLog>0)
    {
        MinError = exp(log(MinVal) + (log(MaxVal)-log(MinVal))*M->AdaptThresholdLog);
    }
    
    long CountRefinementNodes=0;
    Levels.clear();
    Levels.resize(M->NumLevel);
    
    bool RefinementNodesSelected=false;
    while (RefinementNodesSelected==false)
    {
        if (CountRefinementNodes>MaxNodeNum)
        {
            M->AdaptThreshold=2*M->AdaptThreshold;
            MinError = MinVal + (MaxVal-MinVal)*M->AdaptThreshold;
        }
        for (int i=0; i<M->NumLevel; i++)
        {
            Levels[i].clear();
        }
        
        //Add the nodes with an error greater than the threshold to the array
        for (long i=0; i<M->NodeNumber; i++)
        {
            if ((M->AdaptThreshold>0 || M->AdaptThresholdLog>0) && E->ErrorEstim(i) > MinError)
            {
                //Add the collocation node to the vector
                if (M->AdaptThresholdFrac>0 && NodeErrorIndex[i] < M->NodeNumber*M->AdaptThresholdFrac)
                {
                    Levels[0].push_back(i+1);
                }
                else if (M->AdaptThresholdFrac==0)
                {
                    Levels[0].push_back(i+1);
                }
            }
            else if (M->AdaptThresholdFrac>0 && NodeErrorIndex[i] < M->NodeNumber*M->AdaptThresholdFrac)
            {
                //Add the collocation node to the vector
                Levels[0].push_back(i+1);
            }
        }
        //Sort Level 0
        sort(begin(Levels[0]),end(Levels[0]));
        //Add for each collocation node  all the support nodes to the level "LevelIndex +1"
        int LevelIndex=0;
        while (LevelIndex<M->NumLevel-1)
        {
            for (int j=0; j<Levels[LevelIndex].size(); j++)
            {
                for (int k=0; k<M->Nodes[Levels[LevelIndex][j]].SupSize; k++)
                {
                    Levels[LevelIndex+1].push_back(M->Nodes[Levels[LevelIndex][j]].SupNodes[k]);
                }
            }
            //Remove the duplicates nodes (support nodes) from the vector
            sort(Levels[LevelIndex+1].begin(),Levels[LevelIndex+1].end());
            Levels[LevelIndex+1].erase(unique(Levels[LevelIndex+1].begin(),Levels[LevelIndex+1].end()),Levels[LevelIndex+1].end());
            LevelIndex++;
        }

        CountRefinementNodes=Levels[0].size();
        //Remove the duplicates nodes (support nodes) from the vectors
        for (int i=1; i<M->NumLevel; i++)
        {
            for (int j=0; j<i; j++)
            {
                Levels[i].erase(remove_if(begin(Levels[i]),end(Levels[i]),[&](auto x){return binary_search(begin(Levels[j]),end(Levels[j]),x);}),end(Levels[i]));
            }
            CountRefinementNodes+=Levels[i].size();
        }
        if (CountRefinementNodes<MaxNodeNum || M->AdaptThresholdLog>0)
            RefinementNodesSelected=true;
    }
    
    NodesNumForRefinement=CountRefinementNodes;
    NodesForRefinement = new long[NodesNumForRefinement];
    long count=0;
    for (int i=0; i<M->NumLevel; i++)
    {
        for (int j=0; j<Levels[i].size(); j++)
        {
            NodesForRefinement[count]=Levels[i][j];
            M->Nodes[NodesForRefinement[count]].MarkedForRefinement=true;
            count++;
        }
    }
}

void Refinement::SetExistingNodesGlobalIndices(Model* M)
{
    long Count=1;
    for(int i=1; i<M->NodeNumber+1; i++)
    {
        if (M->Nodes[i].BoundaryNode==true || M->Nodes[i].AttachedElements.size()>0)
        {
            M->Nodes[i].GlobalIndex=Count;
            Count++;
        }
    }
    ExistingBCNodesNum=Count-1;
}

void Refinement::FindNewBoundaryNodes(Model* M, ErrorEstimator* E)
{
    //Create node zero for the potential new nodes
    M->Nodes[0].SetNode(0, M->Dimension, 0, 0, 0);
    bool HasEdgeNodes=false;
    
    NewNodesVect.clear();
    long IndexCount=0;
    
    //Add the nodes on the boundary of the domain
    for (long i=0; i<NodesNumForRefinement; i++)
    {
        long RefNode = NodesForRefinement[i];
        //If the node for refinement is a boundary node, split the adjacent elements
        if (M->Nodes[RefNode].BoundaryNode==true || M->Nodes[RefNode].NodeInSurfaceElement==true || M->Nodes[RefNode].AttachedElements.size()>0)
        {
            for (int j=0; j<M->Nodes[RefNode].AttachedElements.size(); j++)
            {
                long ElNum=M->Nodes[RefNode].AttachedElements[j]-1;
                if (M->SurfElement[ElNum].NewNodesCreated==false)
                {
                    M->SurfElement[ElNum].NewNodesCreated=true;
                    vector<double> TempVect;
                    TempVect.resize(3);
                    bool AddMultipleNodes=false;
                    
                    long N1=M->SurfElement[ElNum].AttachedNodes[0]->NodeNum;
                    long N2=M->SurfElement[ElNum].AttachedNodes[1]->NodeNum;
                    Vector3d Normal;
                    if (M->SurfElement[ElNum].ElementType=="2D_LINEAR" || M->SurfElement[ElNum].ElementType=="2D_LINEAR_WITH_BC")
                    {
                        TempVect[0]=(M->Nodes[N1].X[0]+M->Nodes[N2].X[0])/2;
                        TempVect[1]=(M->Nodes[N1].X[1]+M->Nodes[N2].X[1])/2;
                        TempVect[2]=0;
                        if (M->CAD_Model_Available==true)
                        {
                            Vector3d ProjectedPoint(TempVect[0],TempVect[1],TempVect[2]);
                            Vector3d ElNormal(M->SurfElement[ElNum].Normal[0],M->SurfElement[ElNum].Normal[1],0);
                            Vector3d SurfacePoint=M->ModelCAD->PointProjection(ProjectedPoint,Normal,ElNormal);
                            TempVect[0]=SurfacePoint(0);
                            TempVect[1]=SurfacePoint(1);
                            TempVect[2]=SurfacePoint(2);
                        }
                    }
                    else if (M->SurfElement[ElNum].ElementType=="2D_QUAD")
                    {
                        TempVect[0]=M->SurfElement[ElNum].CenterNode.X[0];
                        TempVect[1]=M->SurfElement[ElNum].CenterNode.X[1];
                        TempVect[2]=0;
                    }
                    else if (M->SurfElement[ElNum].ElementType=="3D_LINEAR" || M->SurfElement[ElNum].ElementType=="3D_LINEAR_WITH_BC_CAD")
                    {
                        long N3=M->SurfElement[ElNum].AttachedNodes[2]->NodeNum;
                        TempVect[0]=(M->Nodes[N1].X[0]+M->Nodes[N2].X[0]+M->Nodes[N3].X[0])/3;
                        TempVect[1]=(M->Nodes[N1].X[1]+M->Nodes[N2].X[1]+M->Nodes[N3].X[1])/3;
                        TempVect[2]=(M->Nodes[N1].X[2]+M->Nodes[N2].X[2]+M->Nodes[N3].X[2])/3;
                        if (M->CAD_Model_Available==true)
                        {
                            if (M->SurfElement[ElNum].RefSurf>-1 || RefinementThreeNodes==true)
                            {
                                AddMultipleNodes=true;
                                AddThreeNewNodes(M,RefNode,ElNum);
                                HasEdgeNodes=true;
                            }
                            else
                            {
                                Vector3d ProjectedPoint(TempVect[0],TempVect[1],TempVect[2]);
                                Vector3d ElNormal(M->SurfElement[ElNum].Normal[0],M->SurfElement[ElNum].Normal[1],M->SurfElement[ElNum].Normal[2]);
                                Vector3d SurfacePoint=M->ModelCAD->PointProjection3D(ProjectedPoint,Normal,ElNormal);
                                TempVect[0]=SurfacePoint(0);
                                TempVect[1]=SurfacePoint(1);
                                TempVect[2]=SurfacePoint(2);
                            }
                        }
                    }

                    //Add thecout new nodes to the new node vector
                    if (AddMultipleNodes==false)
                    {
                        NewNodesVect.push_back(new NewNode);
                        long LastNewNode=NewNodesVect.size();
                        NewNodesVect[LastNewNode-1]->SetNewNode({TempVect[0],TempVect[1],TempVect[2]},RefNode,ElNum,pow(M->Nodes[RefNode].SupRadiusSq,0.5),true);
                        NewNodesVect[LastNewNode-1]->GlobalIndex=IndexCount+ExistingBCNodesNum+1;
                        IndexCount++;
                        
                        if (M->SurfElement[ElNum].ElementType=="2D_LINEAR_WITH_BC" || M->SurfElement[ElNum].ElementType=="3D_LINEAR" || M->SurfElement[ElNum].ElementType=="3D_LINEAR_WITH_BC_CAD")
                        {
                            NewNodesVect[LastNewNode-1]->SetBC(M->Dimension);
                            NewNodesVect[LastNewNode-1]->Normal[0]=Normal(0);
                            NewNodesVect[LastNewNode-1]->Normal[1]=Normal(1);
                            if (M->Dimension==3)
                            {
                                NewNodesVect[LastNewNode-1]->Normal[2]=Normal(2);
                            }

                            for (int k=0; k<M->Dimension; k++)
                            {
                                NewNodesVect[LastNewNode-1]->BC_D_Bool[k]=M->SurfElement[ElNum].BC_D_Bool[k];
                                NewNodesVect[LastNewNode-1]->BC_N_Bool[k]=M->SurfElement[ElNum].BC_N_Bool[k];
                                NewNodesVect[LastNewNode-1]->BC_DOF[k]=M->SurfElement[ElNum].BC_DOF[k];
                                NewNodesVect[LastNewNode-1]->LocalBoundaryCondition[k]=M->SurfElement[ElNum].LocalBoundaryCondition[k];
                                NewNodesVect[LastNewNode-1]->BC_D[k]=M->SurfElement[ElNum].BC_D[k];
                                NewNodesVect[LastNewNode-1]->BC_N[k]=M->SurfElement[ElNum].BC_N[k];
                            }
                        }
                        else if (M->SurfElement[ElNum].ElementType=="2D_LINEAR")
                        {
                            NewNodesVect[LastNewNode-1]->SetBC(M->Dimension);
                            NewNodesVect[LastNewNode-1]->Normal[0]=Normal(0);
                            NewNodesVect[LastNewNode-1]->Normal[1]=Normal(1);
                        }
                        if (M->Dimension==2)
                            NewBCNodesNum++;
                    }
                }
            }
        }
    }
    
    //Remove the duplicated edge nodes
    if (HasEdgeNodes==true && EvenItNumber==true)
    {
        RemoveDuplicatedEdgeNodes(M);
    }
}

void Refinement::AddThreeNewNodes(Model* M, long RefNode, long ElNum)
{
    long N1=M->SurfElement[ElNum].AttachedNodes[0]->NodeNum;
    long N2=M->SurfElement[ElNum].AttachedNodes[1]->NodeNum;
    long N3=M->SurfElement[ElNum].AttachedNodes[2]->NodeNum;
    
    vector<double> TempVect1;
    TempVect1.resize(3);
    vector<double> TempVect2;
    TempVect2.resize(3);
    vector<double> TempVect3;
    TempVect3.resize(3);

    TempVect1[0]=(M->Nodes[N1].X[0]+M->Nodes[N2].X[0])/2;
    TempVect1[1]=(M->Nodes[N1].X[1]+M->Nodes[N2].X[1])/2;
    TempVect1[2]=(M->Nodes[N1].X[2]+M->Nodes[N2].X[2])/2;

    TempVect2[0]=(M->Nodes[N1].X[0]+M->Nodes[N3].X[0])/2;
    TempVect2[1]=(M->Nodes[N1].X[1]+M->Nodes[N3].X[1])/2;
    TempVect2[2]=(M->Nodes[N1].X[2]+M->Nodes[N3].X[2])/2;

    TempVect3[0]=(M->Nodes[N2].X[0]+M->Nodes[N3].X[0])/2;
    TempVect3[1]=(M->Nodes[N2].X[1]+M->Nodes[N3].X[1])/2;
    TempVect3[2]=(M->Nodes[N2].X[2]+M->Nodes[N3].X[2])/2;
    
    double Dist1=0;
    double Dist2=0;
    double Dist3=0;
    Vector3d ProjectedPoint1(TempVect1[0],TempVect1[1],TempVect1[2]);
    Vector3d ProjectedPoint2(TempVect2[0],TempVect2[1],TempVect2[2]);
    Vector3d ProjectedPoint3(TempVect3[0],TempVect3[1],TempVect3[2]);
    Vector3d ProjectedPoint1Edge,ProjectedPoint2Edge,ProjectedPoint3Edge;
    if (M->SurfElement[ElNum].RefSurf>-1)
    {
        ProjectedPoint1Edge=M->ModelCAD->PointProjectionEdge(ProjectedPoint1,M->SurfElement[ElNum].RefSurf,M->SurfElement[ElNum].RefEdge);
        Vector3d TempDist1=ProjectedPoint1-ProjectedPoint1Edge;
        Dist1=TempDist1.norm();

        ProjectedPoint2Edge=M->ModelCAD->PointProjectionEdge(ProjectedPoint2,M->SurfElement[ElNum].RefSurf,M->SurfElement[ElNum].RefEdge);
        Vector3d TempDist2=ProjectedPoint2-ProjectedPoint2Edge;
        Dist2=TempDist2.norm();

        ProjectedPoint3Edge=M->ModelCAD->PointProjectionEdge(ProjectedPoint3,M->SurfElement[ElNum].RefSurf,M->SurfElement[ElNum].RefEdge);
        Vector3d TempDist3=ProjectedPoint3-ProjectedPoint3Edge;
        Dist3=TempDist3.norm();
    }
    
    Vector3d ElNormal(M->SurfElement[ElNum].Normal[0],M->SurfElement[ElNum].Normal[1],M->SurfElement[ElNum].Normal[2]);
    vector<Vector3d> NormalNew;
    NormalNew.resize(3);
    vector<Vector3d> SurfPt;
    SurfPt.resize(3);
    int EdgeNodeIndex=-1;
    if (M->SurfElement[ElNum].RefSurf>-1)
    {
        if (Dist1<=Dist2 && Dist1<=Dist3)
        {
            //Get the normal of the new node 1 and project the other nodes to the surface
            SurfPt[0]=M->ModelCAD->PointProjection3D(ProjectedPoint1,NormalNew[0],ElNormal);
            SurfPt[0]=ProjectedPoint1Edge;
            EdgeNodeIndex=0;
            SurfPt[1]=M->ModelCAD->PointProjection3D(ProjectedPoint2,NormalNew[1],ElNormal);
            SurfPt[2]=M->ModelCAD->PointProjection3D(ProjectedPoint3,NormalNew[2],ElNormal);
        }
        else if (Dist2<=Dist1 && Dist2<=Dist3)
        {
            //Get the normal of the new node 2 and project the other nodes to the surface
            SurfPt[0]=M->ModelCAD->PointProjection3D(ProjectedPoint1,NormalNew[0],ElNormal);
            SurfPt[1]=M->ModelCAD->PointProjection3D(ProjectedPoint2,NormalNew[1],ElNormal);
            SurfPt[1]=ProjectedPoint2Edge;
            EdgeNodeIndex=1;
            SurfPt[2]=M->ModelCAD->PointProjection3D(ProjectedPoint3,NormalNew[2],ElNormal);
        }
        else if (Dist3<=Dist1 && Dist3<=Dist2)
        {
            //Get the normal of the new node 3 and project the other nodes to the surface
            SurfPt[0]=M->ModelCAD->PointProjection3D(ProjectedPoint1,NormalNew[0],ElNormal);
            SurfPt[1]=M->ModelCAD->PointProjection3D(ProjectedPoint2,NormalNew[1],ElNormal);
            SurfPt[2]=M->ModelCAD->PointProjection3D(ProjectedPoint3,NormalNew[2],ElNormal);
            SurfPt[2]=ProjectedPoint3Edge;
            EdgeNodeIndex=2;
        }
    }
    else
    {
        SurfPt[0]=M->ModelCAD->PointProjection3D(ProjectedPoint1,NormalNew[0],ElNormal);
        SurfPt[1]=M->ModelCAD->PointProjection3D(ProjectedPoint2,NormalNew[1],ElNormal);
        SurfPt[2]=M->ModelCAD->PointProjection3D(ProjectedPoint3,NormalNew[2],ElNormal);
        EdgeNodeIndex=-1;
    }
    
    //Set the element
    M->SurfElement[ElNum].NewNodes.clear();
    
    //Load the new nodes
    for (int i=0; i<3; i++)
    {
        NewNodesVect.push_back(new NewNode);
        long LastNewNode=NewNodesVect.size();
        M->SurfElement[ElNum].NewNodes.push_back(LastNewNode-1);
        NewNodesVect[LastNewNode-1]->SetNewNode({SurfPt[i](0),SurfPt[i](1),SurfPt[i](2)},RefNode,ElNum,pow(M->Nodes[RefNode].SupRadiusSq,0.5),true);
        NewNodesVect[LastNewNode-1]->SetBC(M->Dimension);
        NewNodesVect[LastNewNode-1]->Normal[0]=NormalNew[i](0);
        NewNodesVect[LastNewNode-1]->Normal[1]=NormalNew[i](1);
        NewNodesVect[LastNewNode-1]->Normal[2]=NormalNew[i](2);
        
        NewNodesVect[LastNewNode-1]->NewElementEdgeNode=true;
        //The node i is an edge node
        if (i==EdgeNodeIndex)
        {
            NewNodesVect[LastNewNode-1]->NewModelEdgeNode=true;
        }
        
        //Set the Parent nodes
        NewNodesVect[LastNewNode-1]->ParentNodes.clear();
        NewNodesVect[LastNewNode-1]->ParentNodes.resize(2);
        if (i==0 || i==1)
        {
            NewNodesVect[LastNewNode-1]->ParentNodes[0]=N1;
        }
        if (i==1 || i==2)
        {
            NewNodesVect[LastNewNode-1]->ParentNodes[1]=N3;
        }
        if (i==0)
        {
            NewNodesVect[LastNewNode-1]->ParentNodes[1]=N2;
        }
        if (i==2)
        {
            NewNodesVect[LastNewNode-1]->ParentNodes[0]=N2;
        }
    }
    
}

void Refinement::RemoveDuplicatedEdgeNodes(Model* M)
{
    //Count the number of new surface nodes
    long Count=0;
    for (long i=0; i<NewNodesVect.size(); i++)
    {
        if (NewNodesVect[i]->NewElementEdgeNode==true || RefinementThreeNodes==true)
        {
            Count++;
        }
    }
    long SurfNodeNum=Count;
    //Add all the existing nodes of the model to to the tree
    SurfNodeNum+=M->NodeNumber;
    
    //Load all the nodes into a tree
    vector<Point_3> points(SurfNodeNum);
    vector<long> indices(SurfNodeNum);

    Count=0;
    for(int i=0; i<M->NodeNumber; i++)
    {
        points[Count]=Point_3(M->Nodes[i+1].X[0],M->Nodes[i+1].X[1],M->Nodes[i+1].X[2]);
        indices[Count]=i;
        Count++;
    }
    for (long i=0; i<NewNodesVect.size(); i++)
    {
        if (NewNodesVect[i]->NewElementEdgeNode==true || RefinementThreeNodes==true)
        {
            points[Count]=Point_3(NewNodesVect[i]->X[0],NewNodesVect[i]->X[1],NewNodesVect[i]->X[2]);
            indices[Count]=i+M->NodeNumber;
            Count++;
        }
    }

    //Load the EdgeNode tree
    Tree EdgeNodeTree;
    EdgeNodeTree.insert(
    boost::make_zip_iterator(boost::make_tuple(points.begin(),indices.begin())),
    boost::make_zip_iterator(boost::make_tuple(points.end(),indices.end())));

    long IndexCount=0;
    for (long i=0; i<NewNodesVect.size(); i++)
    {
        if (NewNodesVect[i]->BoundaryNode==true && NewNodesVect[i]->DuplicatedNode==false && NewNodesVect[i]->InDomain==true)
        {
            //Find the duplicated nodes
            Point_3* query=NULL;
            query= new Point_3(NewNodesVect[i]->X[0],NewNodesVect[i]->X[1],NewNodesVect[i]->X[2]);
            Distance tr_dist;
            int K=40;
            
            // search K nearest neighbours
            K_neighbor_search search(EdgeNodeTree, *query, K, 0.0,true,tr_dist,true);
            double DistThreshold=M->SurfElement[NewNodesVect[i]->ParentElement].MinElementNodesDistance();
            
            //Identify if an existing node is at the same location as the new node
            for(K_neighbor_search::iterator it = search.begin(); it != search.end(); it++)
            {
                double Dist=tr_dist.inverse_of_transformed_distance(it->second);
                int IndexVal=boost::get<1>(it->first);
                if (Dist <= DistThreshold/10)
                {
                    if (IndexVal<M->NodeNumber)
                    {
                        if (M->Nodes[IndexVal+1].BoundaryNode==true || M->Nodes[IndexVal+1].AttachedElements.size()>0)
                        {
                            NewNodesVect[i]->DuplicatedNode=true;
                            NewNodesVect[i]->GlobalIndex=M->Nodes[IndexVal+1].GlobalIndex;
                        }
                        else
                        {
                            M->Nodes[IndexVal+1].DuplicatedNode=true;
                        }
                    }
                }
            }
            
            //Set the global index
            if (NewNodesVect[i]->DuplicatedNode==false)
            {
                NewNodesVect[i]->GlobalIndex=IndexCount+ExistingBCNodesNum+1;
                IndexCount++;
            }
            
            //Identify the duplicated new nodes
            for(K_neighbor_search::iterator it = search.begin(); it != search.end(); it++)
            {
                double Dist=tr_dist.inverse_of_transformed_distance(it->second);
                int IndexVal=boost::get<1>(it->first);
                if (Dist <= DistThreshold/100)
                {
                    if (IndexVal-M->NodeNumber>i)
                    {
                        NewNodesVect[IndexVal-M->NodeNumber]->DuplicatedNode=true;
                        NewNodesVect[IndexVal-M->NodeNumber]->GlobalIndex=NewNodesVect[i]->GlobalIndex;
                    }
                }
            }
            delete query;
        }
    }
    NewBCNodesNum=IndexCount;
}

void Refinement::SetRemainingNodesGlobalIndices(Model* M)
{
    long Count=0;
    for(int i=1; i<M->NodeNumber+1; i++)
    {
        if (M->Nodes[i].BoundaryNode==false &&  M->Nodes[i].AttachedElements.size()==0 && M->Nodes[i].DuplicatedNode==false)
        {
            M->Nodes[i].GlobalIndex=ExistingBCNodesNum+NewBCNodesNum+Count+1;
            Count++;
        }
    }
    for(int i=0; i<NewNodesVect.size(); i++)
    {
        if (NewNodesVect[i]->BoundaryNode==false && NewNodesVect[i]->DuplicatedNode==false && NewNodesVect[i]->InDomain==true)
        {
            NewNodesVect[i]->GlobalIndex=ExistingBCNodesNum+NewBCNodesNum+Count+1;
            Count++;
        }
    }
}

void Refinement::FindPotentialNewNodes_Voro(Model* M, ErrorEstimator* E, int Step)
{       
    long LastNode=0;
    if (Step==0)
        LastNode=NodesNumForRefinement;
    else if (Step==1)
        LastNode=NodesNumForRefinement;//+NewNodesVect.size();
    
    //Load the combined tree with the existing and new nodes
    LoadCombinedTree(M,Step);
    
    //Add the nodes at the corner of the Vornoi cells
    for (long i=0; i<LastNode; i++)
    {
        M->PrintLoading(i,NodesNumForRefinement-1,"Finding refinement nodes: ", false);
        
        Vector3d RefNodeVect=GetRefNodeCoord(M,i);
        
        //Create a voronoi diagram on the support of each node identified for refinement
        //Get container dimensions
        double* MinContainer=NULL;MinContainer=new double[3];
        double* MaxContainer=NULL;MaxContainer=new double[3];
        
        //Initialize the min and max values
        for (int j=0; j<3; j++)
        {
            MinContainer[j]=RefNodeVect(j);
            MaxContainer[j]=RefNodeVect(j);
        }
        
        //Create the search query
        Point_3 query(RefNodeVect(0),RefNodeVect(1),RefNodeVect(2));
        Distance tr_dist;
        int SupSize=pow(15,M->Dimension/2);
        vector<long> SupportNode;
        double SupRad=0;
        SupportNode.resize(SupSize);
        K_neighbor_search search(CombinedTree, query, SupSize, 0.0,true,tr_dist,true);
        int j=0;
        //Fill the index with the values from the iterator
        for(K_neighbor_search::iterator it = search.begin(); it != search.end(); it++)
        {
            SupportNode[j] = boost::get<1>(it->first);
            double Rad=tr_dist.inverse_of_transformed_distance(it->second);
            if (Rad>SupRad)
                SupRad=Rad;
            j++;
        }
        
        //Get the extremum coordinates of the container
        for (int j=0; j<SupSize; j++)
        {
            Vector3d SupNode=GetSupNodeCoord(M,SupportNode[j]);
            for (int k=0; k<M->Dimension; k++)
            {
                if (SupNode(k) < MinContainer[k])
                    MinContainer[k] = SupNode(k);
                if (SupNode(k) > MaxContainer[k])
                    MaxContainer[k] = SupNode(k);
            }
        }

        //Enlarge the dimensions of the container
        for (int j=0; j<M->Dimension; j++)
        {
            MinContainer[j]=MinContainer[j]-0.1*(MaxContainer[j]-MinContainer[j]);
            MaxContainer[j]=MaxContainer[j]+0.1*(MaxContainer[j]-MinContainer[j]);
        }
        //Add the 3rd dimension for 2D problems
        int n_x=10,n_y=10,n_z=10;
        if (M->Dimension==2)
        {
            MinContainer[2]=0;
            MaxContainer[2]=1;
        }

        // Create a container with the geometry given above, and make it
        // non-periodic in each of the three coordinates. Allocate space for
        // eight particles within each computational block
        container* con=NULL;
        con=new container(MinContainer[0],MaxContainer[0],MinContainer[1],MaxContainer[1],MinContainer[2],MaxContainer[2],n_x,n_y,n_z,false,false,false,8);

        // Import the particles to the container
        for (int j=0; j<SupSize; j++)
        {
            Vector3d SupNode=GetSupNodeCoord(M,SupportNode[j]);
            if (M->Dimension==2)
            {
                con->put(j,SupNode(0),SupNode(1),0.5);
            }
            else if (M->Dimension==3)
            {
                con->put(j,SupNode(0),SupNode(1),SupNode(2));
            }
        }

        //Create a loop and loop over the particles of the container
        if (M->RefMethod=="VORO_CN")
        {
            FindVoroCN(M,con,SupportNode,SupRad);
        }
        else if (M->RefMethod=="VORO_NN")   
        {
            FindVoroNN(M,con,SupportNode,SupRad);
        }
        
        delete[] MinContainer;
        delete[] MaxContainer;
        delete con;
    }
}

void Refinement::FindVoroCN(Model* M, container* con, vector<long> SupportNode, double SupRad)
{
    c_loop_all cl(*con);
    voronoicell_neighbor c;
    if (cl.start())
    {
        bool LastParticFound=false;
        while (LastParticFound==false)
        {
            //Find the nodes at the corner of the voronoi cell only for the 
            //central point of index zero as the support nodes are ordered
            //by distance from the central node
            if (cl.pid()==0)
            {
                con->compute_cell(c,cl);
                vector<double> Vect;
                vector<double> TempVect;
                TempVect.resize(3);
                double cx, cy, cz;
                Vector3d SupNode2=GetSupNodeCoord(M,SupportNode[cl.pid()]);
                cx=SupNode2(0);
                cy=SupNode2(1);
                if (M->Dimension==2)
                {
                    cz=0.5;
                }
                else
                {
                    cz=SupNode2(2);
                }
                c.vertices(Vect);
                for (int j=0; j<Vect.size()/3; j++)
                {
                    TempVect[0]=Vect[3*j+0]+cx;
                    TempVect[1]=Vect[3*j+1]+cy;
                    TempVect[2]=Vect[3*j+2]+cz;
                    if (M->Dimension==3 || TempVect[2]> 0)
                    {
                        NewNodesVect.push_back(new NewNode);
                        long LastNewNode=NewNodesVect.size();
                        NewNodesVect[LastNewNode-1]->SetNewNode({TempVect[0],TempVect[1],TempVect[2]},1,0,SupRad,false);
                    }
                }
                break;
            }
            if(cl.inc()==false)
            {
                LastParticFound=true;
            }
        }
    }
}

void Refinement::FindVoroNN(Model* M, container* con, vector<long> SupportNode, double SupRad)
{
    c_loop_all cl(*con);
    voronoicell_neighbor c;
    if (cl.start())
    {
        bool LastParticFound=false;
        while (LastParticFound==false)
        {
            //Find the nodes at the corner of the voronoi cell only for the 
            //central point of index zero as the support nodes are ordered
            //by distance from the central node
            if (cl.pid()==0)
            {
                con->compute_cell(c,cl);
                
                vector<double> TempVect;
                TempVect.resize(3);
                double cx, cy, cz;
                Vector3d CollocNode=GetSupNodeCoord(M,SupportNode[cl.pid()]);
                cx=CollocNode(0);
                cy=CollocNode(1);
                if (M->Dimension==2)
                {
                    cz=0.5;
                }
                else
                {
                    cz=CollocNode(2);
                }
                vector<int> NNIndices;
                c.neighbors(NNIndices);
                for (int i=0; i<NNIndices.size(); i++)
                {
                    if (NNIndices[i]>=0)
                    {
                        //Get the coordinates of the neighbor node
                        Vector3d NNNode=GetSupNodeCoord(M,SupportNode[NNIndices[i]]);
                        //The new node is between the coolocation node and the neighbor node
                        TempVect[0]=(cx+NNNode(0))/2;
                        TempVect[1]=(cy+NNNode(1))/2;
                        TempVect[2]=(cz+NNNode(2))/2;

                        NewNodesVect.push_back(new NewNode);
                        long LastNewNode=NewNodesVect.size();
                        NewNodesVect[LastNewNode-1]->SetNewNode({TempVect[0],TempVect[1],TempVect[2]},1,0,SupRad,false);
                    }
                    
                }
                break;
            }
            if(cl.inc()==false)
            {
                LastParticFound=true;
            }
        }
    }
}

void Refinement::LoadCombinedTree(Model* M, int Step)
{
    //Count the number of new nodes
    long TreeSize=M->NodeNumber;
    for (long i=0; i<NewNodesVect.size(); i++)
    {
        if (NewNodesVect[i]->DuplicatedNode==false && NewNodesVect[i]->InDomain==true)
            TreeSize++;
    }
    
    vector<Point_3> points(TreeSize);
    vector<long> indices(TreeSize);
    vector<bool> InNewNodes(TreeSize);
    
    long Count=0;
    for (long i=0; i<M->NodeNumber; i++)
    {
        if (M->Dimension==2)
            points[Count]=Point_3(M->Nodes[i+1].X[0],M->Nodes[i+1].X[1],0.0);
        else if (M->Dimension==3)
            points[Count]=Point_3(M->Nodes[i+1].X[0],M->Nodes[i+1].X[1],M->Nodes[i+1].X[2]);
        indices[Count]=i+1;
        InNewNodes[Count]=false;
        Count++;
    }
    
    //Add the new nodes to the vectors
    for (long i=0; i<NewNodesVect.size(); i++)
    {
        if (NewNodesVect[i]->DuplicatedNode==false && NewNodesVect[i]->InDomain==true)
        {
            if (M->Dimension==2)
                points[Count]=Point_3(NewNodesVect[i]->X[0],NewNodesVect[i]->X[1],0.0);
            else if (M->Dimension==3)
                points[Count]=Point_3(NewNodesVect[i]->X[0],NewNodesVect[i]->X[1],NewNodesVect[i]->X[2]);
            indices[Count]=M->NodeNumber+1+i;
            InNewNodes[Count]=true;
            Count++;
        }
    }
    
    //Load the combined tree
    CombinedTree.insert(
    boost::make_zip_iterator(boost::make_tuple(points.begin(),indices.begin())),
    boost::make_zip_iterator(boost::make_tuple(points.end(),indices.end())));
    
    //Load the ExistingNodesTree tree
    vector<Point_3> points2(M->NodeNumber);
    vector<long> indices2(M->NodeNumber);
    Count=0;
    for (long i=0; i<M->NodeNumber; i++)
    {
        if (M->Dimension==2)
            points2[i]=Point_3(M->Nodes[i+1].X[0],M->Nodes[i+1].X[1],0.0);
        else if (M->Dimension==3)
            points2[i]=Point_3(M->Nodes[i+1].X[0],M->Nodes[i+1].X[1],M->Nodes[i+1].X[2]);
        indices2[i]=i+1;
    }
    ExistingNodesTree.insert(
    boost::make_zip_iterator(boost::make_tuple(points2.begin(),indices2.begin())),
    boost::make_zip_iterator(boost::make_tuple(points2.end()  ,indices2.end())));
}

Vector3d Refinement::GetRefNodeCoord(Model* M, long Index)
{
    Vector3d TempVect;
    if (Index<NodesNumForRefinement)
    {
        long RefNode = NodesForRefinement[Index];
        TempVect(0)=M->Nodes[RefNode].X[0];
        TempVect(1)=M->Nodes[RefNode].X[1];
        if (M->Dimension==2)
            TempVect(2)=0;
        else if (M->Dimension==3)
            TempVect(2)=M->Nodes[RefNode].X[2];
    }
    else
    {
        TempVect(0)=NewNodesVect[Index]->X[0];
        TempVect(1)=NewNodesVect[Index]->X[1];
        if (M->Dimension==2)
            TempVect(2)=0;
        else if (M->Dimension==3)
            TempVect(2)=NewNodesVect[Index]->X[2];
    }
    return TempVect;
}

Vector3d Refinement::GetSupNodeCoord(Model* M, long Index)
{
    Vector3d TempVect;
    if (Index<M->NodeNumber+1)
    {
        TempVect(0)=M->Nodes[Index].X[0];
        TempVect(1)=M->Nodes[Index].X[1];
        if (M->Dimension==2)
            TempVect(2)=0;
        else if (M->Dimension==3)
            TempVect(2)=M->Nodes[Index].X[2];
    }
    else
    {
        TempVect(0)=NewNodesVect[Index-M->NodeNumber-1]->X[0];
        TempVect(1)=NewNodesVect[Index-M->NodeNumber-1]->X[1];
        if (M->Dimension==2)
            TempVect(2)=0;
        else if (M->Dimension==3)
            TempVect(2)=NewNodesVect[Index-M->NodeNumber-1]->X[2];
    }
    return TempVect;
}

void Refinement::IdentifyDuplicatedPoints(Model* M, StencilSelect* S, int Step)
{
    double pi=3.14159265;
    
    int K=S->SupSizeInt;
    if (K<3) {K=3;}
    vector<Point_3> points(M->NodeNumber+NewNodesVect.size());
    vector<long> indices(M->NodeNumber+NewNodesVect.size());
    
    long Count=0;
    //Load the new nodes
    for (long i=0; i<NewNodesVect.size(); i++)
    {
        if (M->Dimension==2)
            points[Count]=Point_3(NewNodesVect[i]->X[0],NewNodesVect[i]->X[1],0.0);
        else if (M->Dimension==3)
            points[Count]=Point_3(NewNodesVect[i]->X[0],NewNodesVect[i]->X[1],NewNodesVect[i]->X[2]);
        indices[Count]=Count;
        Count++;
    }
    //Load the existing nodes
    for (long i=0; i<M->NodeNumber; i++)
    {
        if (M->Dimension==2)
            points[Count]=Point_3(M->Nodes[i+1].X[0],M->Nodes[i+1].X[1],0.0);
        else if (M->Dimension==3)
            points[Count]=Point_3(M->Nodes[i+1].X[0],M->Nodes[i+1].X[1],M->Nodes[i+1].X[2]);
        indices[Count]=Count;
        Count++;
    }
    
    // Insert number_of_data_points in the tree
    Tree tree(
    boost::make_zip_iterator(boost::make_tuple(points.begin(),indices.begin())),
    boost::make_zip_iterator(boost::make_tuple(points.end(),indices.end())));
    
    //Identify the duplicated nodes
    for (long i=0; i<NewNodesVect.size(); i++)
    {
        if (NewNodesVect[i]->DuplicatedNode==false)
        {
            Point_3* query=NULL;
            if (M->Dimension==2)
                query= new Point_3(NewNodesVect[i]->X[0],NewNodesVect[i]->X[1],0.0);
            else
                query= new Point_3(NewNodesVect[i]->X[0],NewNodesVect[i]->X[1],NewNodesVect[i]->X[2]);
            Distance tr_dist;
            // search K nearest neighbours
            K_neighbor_search search(tree, *query, K, 0.0,true,tr_dist,true);
            
            //Get the distance Threshold
            double DistTreshold=NewNodesVect[i]->ParentNodeRadius/10;
            double h_Loc=NewNodesVect[i]->ParentNodeRadius;

            if (NewNodesVect[i]->BoundaryNode==true)// && M->Dimension==3)
            {
                h_Loc=M->SurfElement[NewNodesVect[i]->ParentElement].MinElementNodesDistance();
                DistTreshold=h_Loc/M->DeletionFactor_Bound;
            }
            else if (NewNodesVect[i]->BoundaryNode==false)// && M->Dimension==3)
            {
                Distance tr_dist2;
                int K2=1;
                //Get the index of the closest existing node
                K_neighbor_search search2(ExistingNodesTree, *query, K2, 0.0,true,tr_dist2,true);
                for(K_neighbor_search::iterator it = search2.begin(); it != search2.end(); it++)
                {
                    long ExistingNodeIndex=boost::get<1>(it->first);
                    //Get the closest existing node
                    Distance tr_dist3;
                    Point_3* query3=NULL;
                    int K3=2;
                    if (M->Dimension==2)
                        query3= new Point_3(M->Nodes[ExistingNodeIndex].X[0],M->Nodes[ExistingNodeIndex].X[1],0.0);
                    else
                        query3= new Point_3(M->Nodes[ExistingNodeIndex].X[0],M->Nodes[ExistingNodeIndex].X[1],M->Nodes[ExistingNodeIndex].X[2]);
                    K_neighbor_search search3(ExistingNodesTree, *query3, K3, 0.0,true,tr_dist3,true);
                    for(K_neighbor_search::iterator it = search3.begin(); it != search3.end(); it++)
                    {
                        long Temp=boost::get<1>(it->first);
                        if (Temp!=ExistingNodeIndex)
                        {
                            long ClosestToExistingNodeIndex=Temp;
                            Vector3d DistNodes(0,0,0);
                            for (int j=0; j<M->Dimension; j++)
                            {
                                DistNodes(j)=M->Nodes[ExistingNodeIndex].X[j]-M->Nodes[ClosestToExistingNodeIndex].X[j];
                            }
                            h_Loc=DistNodes.norm();
                            DistTreshold=h_Loc/M->DeletionFactor_Inner;
                        }
                    }
                    delete query3;
                    if (M->Nodes[ExistingNodeIndex].BoundaryNode==true)
                    {
                        long FirstAttEl=M->Nodes[ExistingNodeIndex].AttachedElements[0];
                        h_Loc=M->SurfElement[FirstAttEl].MinElementNodesDistance();
                    }
                }
            }

            //Identify the duplicated nodes
            for(K_neighbor_search::iterator it = search.begin(); it != search.end(); it++)
            {
                int IndexVal=boost::get<1>(it->first);
                double Dist=tr_dist.inverse_of_transformed_distance(it->second);
                if (M->Dimension==2)
                {
                    if (IndexVal<NewNodesVect.size())
                    {
                        if (Dist < DistTreshold && IndexVal != i && NewNodesVect[IndexVal]->DuplicatedNode == false)
                        {
                            if (NewNodesVect[IndexVal]->BoundaryNode==false)
                            {
                                NewNodesVect[IndexVal]->DuplicatedNode=true;
                            }
                            else if (NewNodesVect[IndexVal]->BoundaryNode==true)
                            {
                                if (NewNodesVect[i]->BoundaryNode==false)
                                {
                                    NewNodesVect[i]->DuplicatedNode=true;
                                }
                                else if (NewNodesVect[i]->BoundaryNode==true && Dist < DistTreshold/1000)
                                {
                                    cout << " <!> New node addition error on boundary." << endl;
                                }
                            }
                        }
                    }
                    else if (IndexVal>=NewNodesVect.size())
                    {
                        if (Dist < DistTreshold && M->Nodes[IndexVal-NewNodesVect.size()+1].DuplicatedNode == false)
                        {
                            if (NewNodesVect[i]->BoundaryNode==false)
                            {
                                NewNodesVect[i]->DuplicatedNode=true;
                            }
                            else
                            {
                                if (M->Nodes[IndexVal-NewNodesVect.size()+1].BoundaryNode==false && M->Nodes[IndexVal-NewNodesVect.size()+1].AttachedElements.size()==0)
                                {
                                    M->Nodes[IndexVal-NewNodesVect.size()+1].DuplicatedNode=true;
                                }
                                else if (M->Nodes[IndexVal-NewNodesVect.size()+1].BoundaryNode==true && Dist < DistTreshold/1000)
                                {
                                    cout << " <!> New node addition error on boundary." << endl;
                                }
                            }
                        }
                    }
                }
                else if (M->Dimension==3)
                {
                    if (Dist <= DistTreshold && IndexVal != i)
                    {
                        if (IndexVal<NewNodesVect.size())
                        {
                            if (NewNodesVect[IndexVal]->DuplicatedNode==false)
                            {
                                if (NewNodesVect[IndexVal]->BoundaryNode==false)
                                {
                                    NewNodesVect[IndexVal]->DuplicatedNode=true;
                                }
                                else
                                {
                                    if (NewNodesVect[i]->BoundaryNode==false)
                                    {
                                        NewNodesVect[i]->DuplicatedNode=true;
                                    }
                                    else if (Dist <= DistTreshold/100)
                                    {
                                        cout << " <!> New node addition error on boundary. Error #1." << endl;
                                        cout << "Node i = [" << NewNodesVect[i]->X[0] << "," << NewNodesVect[i]->X[1] << "," << NewNodesVect[i]->X[2] << endl;
                                        cout << "Node j = [" << NewNodesVect[IndexVal]->X[0] << "," << NewNodesVect[IndexVal]->X[1] << "," << NewNodesVect[IndexVal]->X[2] << endl;
                                    }
                                }
                            }
                        }
                        else if (IndexVal>=NewNodesVect.size())
                        {
                            if (M->Nodes[IndexVal-NewNodesVect.size()+1].DuplicatedNode==false)
                            {
                                if (NewNodesVect[i]->BoundaryNode==false)
                                {
                                    NewNodesVect[i]->DuplicatedNode=true;
                                }
                                else
                                {
                                    if (M->Nodes[IndexVal-NewNodesVect.size()+1].BoundaryNode==false)
                                    {
                                        M->Nodes[IndexVal-NewNodesVect.size()+1].DuplicatedNode=true;
                                    }
                                    else  if (Dist <= DistTreshold/100)
                                    {
                                        cout << " <!> New node addition error on boundary. Error #2." << endl;
                                        cout << "Node i = [" << NewNodesVect[i]->X[0] << "," << NewNodesVect[i]->X[1] << "," << NewNodesVect[i]->X[2] << endl;
                                        cout << "Node j = [" << M->Nodes[IndexVal-NewNodesVect.size()+1].X[0] << "," << M->Nodes[IndexVal-NewNodesVect.size()+1].X[1] << "," << M->Nodes[IndexVal-NewNodesVect.size()+1].X[2] << endl;
                                    }
                                }
                            }
                        }
                    }
                }
                
                //Determine if a node is too close from the boundary of the domain
                if (NewNodesVect[i]->BoundaryNode==true && Dist<h_Loc && M->DistToBoundCriterion==true)
                {
                    //Compute the angle between the new node and the normal of the boundary node
                    Vector3d BNormal(NewNodesVect[i]->Normal[0],NewNodesVect[i]->Normal[1],0);
                    //Load the vector between the new node and the boundary node
                    Vector3d BVect(-NewNodesVect[i]->X[0],-NewNodesVect[i]->X[1],0);
                    if (M->Dimension==3)
                    {
                        BNormal(2)=NewNodesVect[i]->Normal[2];
                        BVect(2)=-NewNodesVect[i]->X[2];
                    }
                    bool LocNodeBC=false;
                    for (int j=0; j<M->Dimension; j++)
                    {
                        if (IndexVal<NewNodesVect.size())
                        {
                            BVect(j)+=NewNodesVect[IndexVal]->X[j];
                            if (NewNodesVect[IndexVal]->BoundaryNode==true)
                            {
                                LocNodeBC=true;
                            }
                        }
                        else if (IndexVal>=NewNodesVect.size())
                        {
                            BVect(j)+=M->Nodes[IndexVal-NewNodesVect.size()+1].X[j];
                            if (M->Nodes[IndexVal-NewNodesVect.size()+1].BoundaryNode==true)
                            {
                                LocNodeBC=true;
                            }
                        }
                    }
                    if (LocNodeBC==false)
                    {
                        BNormal.normalize();
                        BVect.normalize();
                        //Compute the angle between the vectors
                        double Angle=acos(BNormal.dot(BVect))*(double)180/pi;
                        if (abs(Angle)<120)
                        {
                            if (IndexVal<NewNodesVect.size())
                            {
                                NewNodesVect[IndexVal]->DuplicatedNode=true;
                            }
                            else if (IndexVal>=NewNodesVect.size())
                            {
                                NewNodesVect[i]->DuplicatedNode=true;
                            }
                            else
                            {
                                if (M->Nodes[IndexVal+1].BoundaryNode==false)
                                {
                                    M->Nodes[IndexVal+1].DuplicatedNode=true;
                                }
                                else if (M->Nodes[IndexVal+1].BoundaryNode==true && Dist < (NewNodesVect[i]->ParentNodeRadius/1000))
                                {
                                    cout << " <!> New node addition error on boundary." << endl;
                                }
                            }
                        }
                    }
                }
                else if (IndexVal>=NewNodesVect.size() && M->DistToBoundCriterion==true)
                {
                    if (M->Nodes[IndexVal-NewNodesVect.size()+1].BoundaryNode==true)
                    {
                        long FirstAttEl=M->Nodes[IndexVal-NewNodesVect.size()+1].AttachedElements[0];
                        h_Loc=M->SurfElement[FirstAttEl].MinElementNodesDistance();
                    }
                    
                    bool Flag=false;
                    if (NewNodesVect[i]->X[0]>0.9469 && NewNodesVect[i]->X[0]<0.8470 && NewNodesVect[i]->X[1]>0.5315 && NewNodesVect[i]->X[1]<0.5316 && NewNodesVect[i]->X[2]>0.5554 && NewNodesVect[i]->X[2]<0.5556)
                    {
                        Flag=true;
                        cout << "found1, " << Dist*1000*1000 << ", " << h_Loc*1000*1000 << ", " << IndexVal-NewNodesVect.size()+1 << ", " << NewNodesVect[i]->BoundaryNode << endl;
//                        cout << M->Nodes[IndexVal-NewNodesVect.size()+1].Normal[0][0]*1000 << "," << M->Nodes[IndexVal-NewNodesVect.size()+1].Normal[0][1]*1000 << "," << M->Nodes[IndexVal-NewNodesVect.size()+1].Normal[0][2] << endl;
                        cout << IndexVal-NewNodesVect.size()+1 << "," << M->Nodes[IndexVal-NewNodesVect.size()+1].BoundaryNode << endl;
                    }
                    if (M->Nodes[IndexVal-NewNodesVect.size()+1].BoundaryNode==true && NewNodesVect[i]->BoundaryNode==false && Dist<h_Loc)
                    {
                        if (Flag==true)
                        {
                            cout << "found2" << endl;
                            cout << M->Nodes[IndexVal-NewNodesVect.size()+1].Normal[0][0]*1000 << "," << M->Nodes[IndexVal-NewNodesVect.size()+1].Normal[0][1]*1000 << "," << M->Nodes[IndexVal-NewNodesVect.size()+1].Normal[0][2] << endl;
                        }
                        //Compute the angle between the new node and the normal of the boundary node
                        Vector3d BNormal(M->Nodes[IndexVal-NewNodesVect.size()+1].Normal[0][0],M->Nodes[IndexVal-NewNodesVect.size()+1].Normal[0][1],0);
                        //Load the vector between the new node and the boundary node
                        Vector3d BVect(-M->Nodes[IndexVal-NewNodesVect.size()+1].X[0],-M->Nodes[IndexVal-NewNodesVect.size()+1].X[1],0);
                        if (M->Dimension==3)
                        {
                            BNormal(2)=M->Nodes[IndexVal-NewNodesVect.size()+1].Normal[0][2];
                            BVect(2)=-M->Nodes[IndexVal-NewNodesVect.size()+1].X[2];
                        }
                        for (int j=0; j<M->Dimension; j++)
                        {
                            BVect(j)+=NewNodesVect[i]->X[j];
                        }
                        BNormal.normalize();
                        BVect.normalize();
                        //Compute the angle between the vectors
                        double Angle=acos(BNormal.dot(BVect))*(double)180/pi;
                        if (abs(Angle)<120)
                        {
                            if (NewNodesVect[IndexVal]->DuplicatedNode==false)
                            {
                                if (NewNodesVect[IndexVal]->BoundaryNode==false)
                                {
                                    NewNodesVect[IndexVal]->DuplicatedNode=true;
                                }
                                else
                                {
                                    if (NewNodesVect[i]->BoundaryNode==false)
                                    {
                                        NewNodesVect[i]->DuplicatedNode=true;
                                    }
                                    else if (Dist <= DistTreshold/100)
                                    {
                                        cout << " <!> New node addition error on boundary. Error #1." << endl;
                                        cout << "Node i = [" << NewNodesVect[i]->X[0] << "," << NewNodesVect[i]->X[1] << "," << NewNodesVect[i]->X[2] << endl;
                                        cout << "Node j = [" << NewNodesVect[IndexVal]->X[0] << "," << NewNodesVect[IndexVal]->X[1] << "," << NewNodesVect[IndexVal]->X[2] << endl;
                                    }
                                }
                            }
                        }
                        else if (IndexVal>=NewNodesVect.size())
                        {
                            if (M->Nodes[IndexVal-NewNodesVect.size()+1].DuplicatedNode==false)
                            {
                                if (NewNodesVect[i]->BoundaryNode==false)
                                {
                                    NewNodesVect[i]->DuplicatedNode=true;
                                }
                                else
                                {
                                    if (M->Nodes[IndexVal-NewNodesVect.size()+1].BoundaryNode==false)
                                    {
                                        M->Nodes[IndexVal-NewNodesVect.size()+1].DuplicatedNode=true;
                                    }
                                    else  if (Dist <= DistTreshold/100)
                                    {
                                        cout << " <!> New node addition error on boundary. Error #2." << endl;
                                        cout << "Node i = [" << NewNodesVect[i]->X[0] << "," << NewNodesVect[i]->X[1] << "," << NewNodesVect[i]->X[2] << endl;
                                        cout << "Node j = [" << M->Nodes[IndexVal-NewNodesVect.size()+1].X[0] << "," << M->Nodes[IndexVal-NewNodesVect.size()+1].X[1] << "," << M->Nodes[IndexVal-NewNodesVect.size()+1].X[2] << endl;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            delete query;
        }
    }
}

void Refinement::IdentifyNewNodesOutside(Model* M, StencilSelect* S, int Step)
{
    vector<double> TempVect;
    TempVect.resize(3);
    //For each new node which is not duplicated, identify if the node is in the domain or not
    for (long i=0; i<NewNodesVect.size(); i++)
    {
        //Determine if the new nodes, which are not duplicated, are in the domain
        if (NewNodesVect[i]->DuplicatedNode==false && NewNodesVect[i]->BoundaryNode==false)
        {
            for (int j=0; j<3; j++)
            {
                TempVect[j]=NewNodesVect[i]->X[j];
            }
            //Add the test of the new inner node for the inner node only
            bool NewInnerNodeTest=false;
            if (NewNodesVect[i]->BoundaryNode==true)
            {
                NewInnerNodeTest=false;
            }
            if (S->InDomain(M,TempVect,false,NewInnerNodeTest)==true)
            {
                NewNodesVect[i]->InDomain=true;
            }
            else
            {
                NewNodesVect[i]->InDomain=false;
            }
//            //Set all the inner nodes as outside of the domain to keep only the boundary nodes
//            NewNodesVect[i]->InDomain=false;
        }
//        else if (i>500)
//        {
//            NewNodesVect[i]->DuplicatedNode=true;
//        }
    }
}

void Refinement::RemoveDuplicatedPoints(Model* M)
{
    NewNodes = new Node[NewNodesVect.size()];
    TotalNodesNewModel=0;
    for(int i=1; i<=M->NodeNumber; i++)
    {
        if (M->Nodes[i].DuplicatedNode==false)
        {
            TotalNodesNewModel++;
        }
    }
    for(int i=0; i<NewNodesVect.size(); i++)
    {
        NewNodes[i].NodeNum=i;
        NewNodes[i].GlobalIndex=NewNodesVect[i]->GlobalIndex;
        if (NewNodesVect[i]->DuplicatedNode==false && NewNodesVect[i]->InDomain==true)
        {
            TotalNodesNewModel++;
        }
    }
}

void Refinement::AddNewBoundaryElements(Model* M)
{
    NewElements.clear();
    if (M->Dimension==2)
    {
        for (long i=0; i<NewNodesVect.size(); i++)
        {
            if (NewNodesVect[i]->BoundaryNode==true)
            {
                long ParentElNum=NewNodesVect[i]->ParentElement;
                long N1=M->SurfElement[ParentElNum].AttachedNodes[0]->NodeNum;
                long N2=M->SurfElement[ParentElNum].AttachedNodes[1]->NodeNum;

                //get the normal of the parent element
                Vector3d ParentNormal(M->SurfElement[ParentElNum].Normal[0],M->SurfElement[ParentElNum].Normal[1],0);

                //Get the normal of the new element
                NewElement TempEl2;
                TempEl2.Node(0)=NewNodesVect[i]->GlobalIndex;
                TempEl2.Node(1)=M->SurfElement[ParentElNum].AttachedNodes[1]->GlobalIndex;
                
                //Add the new node in second position of the element
                M->SurfElement[ParentElNum].AttachedNodes[1]=&NewNodes[i];
                
                TempEl2.Normal(0)= (M->Nodes[N2].X[1]-NewNodesVect[i]->X[1]);
                TempEl2.Normal(1)=-(M->Nodes[N2].X[0]-NewNodesVect[i]->X[0]);
                TempEl2.Normal(2)=0;
                if (TempEl2.Normal(0)*ParentNormal(0)+TempEl2.Normal(1)*ParentNormal(1)<0)
                {
                    TempEl2.Normal=-TempEl2.Normal;
                }
                TempEl2.Normal.normalize();
                TempEl2.RefElement=ParentElNum;

                //Load the boundary conditions to the new element
                if (M->DirectCollocRefinement==true)
                {
                    TempEl2.LocalBoundaryCondition.resize(M->Dimension);
                    TempEl2.BC_D_Bool.resize(M->Dimension);
                    TempEl2.BC_N_Bool.resize(M->Dimension);
                    TempEl2.BC_DOF.resize(M->Dimension);
                    TempEl2.BC_D.resize(M->Dimension);
                    TempEl2.BC_N.resize(M->Dimension);
                    for (int j=0; j<M->Dimension; j++)
                    {
                        TempEl2.LocalBoundaryCondition[j]=M->SurfElement[ParentElNum].LocalBoundaryCondition[j];
                        TempEl2.BC_D_Bool[j]=M->SurfElement[ParentElNum].BC_D_Bool[j];
                        TempEl2.BC_N_Bool[j]=M->SurfElement[ParentElNum].BC_N_Bool[j];
                        TempEl2.BC_DOF[j]=M->SurfElement[ParentElNum].BC_DOF[j];
                        TempEl2.BC_D[j]=M->SurfElement[ParentElNum].BC_D[j];
                        TempEl2.BC_N[j]=M->SurfElement[ParentElNum].BC_N[j];
                    }
                }
                NewElements.push_back(TempEl2);

                //Update the normal of the surface element
                Vector3d TempNormal(M->Nodes[N1].X[1]-NewNodesVect[i]->X[1],-M->Nodes[N1].X[0]+NewNodesVect[i]->X[0],0);
                if (TempNormal(0)*ParentNormal(0)+TempNormal(1)*ParentNormal(1)<0)
                {
                    TempNormal=-TempNormal;
                }
                TempNormal.normalize();
                M->SurfElement[ParentElNum].Normal[0]=TempNormal(0);
                M->SurfElement[ParentElNum].Normal[1]=TempNormal(1);
            }
        }
    }
    else if (M->Dimension==3)
    {
        for (long i=0; i<M->ElementNumber; i++)
        {
            if (M->SurfElement[i].NewNodesCreated==true && M->SurfElement[i].NewElementsCreated==false)
            {
                M->SurfElement[i].NewElementsCreated=true;

                //Get the normal of the parent element
                Vector3d ParentNormal(0,0,0);
                for (int j=0; j<M->Dimension; j++)
                {
                    ParentNormal(j)=M->SurfElement[i].Normal[j];
                }
                //get the old element nodes
                long N1=M->SurfElement[i].AttachedNodes[0]->NodeNum;
                long N2=M->SurfElement[i].AttachedNodes[1]->NodeNum;
                long N3=0;
                if (M->Dimension==3)
                {
                    N3=M->SurfElement[i].AttachedNodes[2]->NodeNum;
                }
                if (RefinementThreeNodes==true)
                {
                    Vector3d TempN;
                    //Create the 3 new elements
                    NewElement TempEl1;
                    TempEl1.Node(0)=M->SurfElement[i].AttachedNodes[1]->GlobalIndex;
                    TempEl1.Node(1)=NewNodes[M->SurfElement[i].NewNodes[2]].GlobalIndex;
                    TempEl1.Node(2)=NewNodes[M->SurfElement[i].NewNodes[0]].GlobalIndex;
                    TempN=GetNormal(NewNodesVect[M->SurfElement[i].NewNodes[2]]->X[0]-M->Nodes[N2].X[0],NewNodesVect[M->SurfElement[i].NewNodes[2]]->X[1]-M->Nodes[N2].X[1],NewNodesVect[M->SurfElement[i].NewNodes[2]]->X[2]-M->Nodes[N2].X[2],
                                    NewNodesVect[M->SurfElement[i].NewNodes[0]]->X[0]-M->Nodes[N2].X[0],NewNodesVect[M->SurfElement[i].NewNodes[0]]->X[1]-M->Nodes[N2].X[1],NewNodesVect[M->SurfElement[i].NewNodes[0]]->X[2]-M->Nodes[N2].X[2],ParentNormal);
                    TempEl1.Normal(0)=TempN(0);
                    TempEl1.Normal(1)=TempN(1);
                    TempEl1.Normal(2)=TempN(2);
                    TempEl1.RefSurf=M->SurfElement[i].RefSurf;
                    TempEl1.RefEdge=M->SurfElement[i].RefEdge;
                    TempEl1.RefElement=i;
                    if (NewNodesVect[M->SurfElement[i].NewNodes[0]]->NewModelEdgeNode==true || NewNodesVect[M->SurfElement[i].NewNodes[2]]->NewModelEdgeNode==true)
                    {
                        TempEl1.HasModelEdgeNode=true;
                    }

                    NewElement TempEl2;
                    TempEl2.Node(0)=M->SurfElement[i].AttachedNodes[2]->GlobalIndex;
                    TempEl2.Node(1)=NewNodes[M->SurfElement[i].NewNodes[1]].GlobalIndex;
                    TempEl2.Node(2)=NewNodes[M->SurfElement[i].NewNodes[2]].GlobalIndex;
                    TempN=GetNormal(NewNodesVect[M->SurfElement[i].NewNodes[1]]->X[0]-M->Nodes[N3].X[0],NewNodesVect[M->SurfElement[i].NewNodes[1]]->X[1]-M->Nodes[N3].X[1],NewNodesVect[M->SurfElement[i].NewNodes[1]]->X[2]-M->Nodes[N3].X[2],
                                    NewNodesVect[M->SurfElement[i].NewNodes[2]]->X[0]-M->Nodes[N3].X[0],NewNodesVect[M->SurfElement[i].NewNodes[2]]->X[1]-M->Nodes[N3].X[1],NewNodesVect[M->SurfElement[i].NewNodes[2]]->X[2]-M->Nodes[N3].X[2],ParentNormal);
                    TempEl2.Normal(0)=TempN(0);
                    TempEl2.Normal(1)=TempN(1);
                    TempEl2.Normal(2)=TempN(2);
                    TempEl2.RefElement=i;
                    if (NewNodesVect[M->SurfElement[i].NewNodes[1]]->NewModelEdgeNode==true || NewNodesVect[M->SurfElement[i].NewNodes[2]]->NewModelEdgeNode==true)
                    {
                        TempEl2.HasModelEdgeNode=true;
                    }

                    NewElement TempEl3;
                    TempEl3.Node(0)=NewNodes[M->SurfElement[i].NewNodes[0]].GlobalIndex;
                    TempEl3.Node(1)=NewNodes[M->SurfElement[i].NewNodes[1]].GlobalIndex;
                    TempEl3.Node(2)=NewNodes[M->SurfElement[i].NewNodes[2]].GlobalIndex;
                    TempN=GetNormal(NewNodesVect[M->SurfElement[i].NewNodes[1]]->X[0]-NewNodesVect[M->SurfElement[i].NewNodes[0]]->X[0],NewNodesVect[M->SurfElement[i].NewNodes[1]]->X[1]-NewNodesVect[M->SurfElement[i].NewNodes[0]]->X[1],NewNodesVect[M->SurfElement[i].NewNodes[1]]->X[2]-NewNodesVect[M->SurfElement[i].NewNodes[0]]->X[2],
                                    NewNodesVect[M->SurfElement[i].NewNodes[2]]->X[0]-NewNodesVect[M->SurfElement[i].NewNodes[0]]->X[0],NewNodesVect[M->SurfElement[i].NewNodes[2]]->X[1]-NewNodesVect[M->SurfElement[i].NewNodes[0]]->X[1],NewNodesVect[M->SurfElement[i].NewNodes[2]]->X[2]-NewNodesVect[M->SurfElement[i].NewNodes[0]]->X[2],ParentNormal);
                    TempEl3.Normal(0)=TempN(0);
                    TempEl3.Normal(1)=TempN(1);
                    TempEl3.Normal(2)=TempN(2);
                    TempEl3.RefElement=i;
                    TempEl3.HasModelEdgeNode=false;

    //                //Set the edge elements
    //                int RefSurf=M->SurfElement[i].RefSurf;
    //                int RefEdge=M->SurfElement[i].RefEdge;
    //                M->SurfElement[i].RefSurf=-1;
    //                M->SurfElement[i].RefEdge=-1;
    //                if (NewNodesVect[M->SurfElement[i].NewNodes[0]]->EdgeNode==true)
    //                {
    //                    M->SurfElement[i].RefSurf=RefSurf;
    //                    M->SurfElement[i].RefEdge=RefEdge;
    //                    TempEl1.RefSurf=RefSurf;
    //                    TempEl1.RefEdge=RefEdge;
    //                }
    //                else if (NewNodesVect[M->SurfElement[i].NewNodes[1]]->EdgeNode==true)
    //                {
    //                    M->SurfElement[i].RefSurf=RefSurf;
    //                    M->SurfElement[i].RefEdge=RefEdge;
    //                    TempEl2.RefSurf=RefSurf;
    //                    TempEl2.RefEdge=RefEdge;
    //                }
    //                else if (NewNodesVect[M->SurfElement[i].NewNodes[2]]->EdgeNode==true)
    //                {
    //                    TempEl1.RefSurf=RefSurf;
    //                    TempEl1.RefEdge=RefEdge;
    //                    TempEl2.RefSurf=RefSurf;
    //                    TempEl2.RefEdge=RefEdge;
    //                }

                    NewElements.push_back(TempEl1);
                    NewElements.push_back(TempEl2);
                    NewElements.push_back(TempEl3);

                    //Modify the existing element with the new node
                    M->SurfElement[i].AttachedNodes[1]=&NewNodes[M->SurfElement[i].NewNodes[0]];
                    M->SurfElement[i].AttachedNodes[2]=&NewNodes[M->SurfElement[i].NewNodes[1]];

                    TempN=GetNormal(NewNodesVect[M->SurfElement[i].NewNodes[0]]->X[0]-M->Nodes[N1].X[0],NewNodesVect[M->SurfElement[i].NewNodes[0]]->X[1]-M->Nodes[N1].X[1],NewNodesVect[M->SurfElement[i].NewNodes[0]]->X[2]-M->Nodes[N1].X[2],
                                             NewNodesVect[M->SurfElement[i].NewNodes[1]]->X[0]-M->Nodes[N1].X[0],NewNodesVect[M->SurfElement[i].NewNodes[1]]->X[1]-M->Nodes[N1].X[1],NewNodesVect[M->SurfElement[i].NewNodes[1]]->X[2]-M->Nodes[N1].X[2],ParentNormal);
                    M->SurfElement[i].Normal[0]=TempN(0);
                    M->SurfElement[i].Normal[1]=TempN(1);
                    M->SurfElement[i].Normal[2]=TempN(2);
                    if (NewNodesVect[M->SurfElement[i].NewNodes[0]]->NewModelEdgeNode==false && NewNodesVect[M->SurfElement[i].NewNodes[1]]->NewModelEdgeNode==false)
                    {
                        M->SurfElement[i].RefSurf=-1;
                        M->SurfElement[i].RefEdge=-1;
                    }
                }
            }
        }
    }
}

Vector3d Refinement::GetNormal(double V1x, double V1y, double V1z, double V2x, double V2y, double V2z, Vector3d ParentNormal)
{
    Vector3d V3;
    //Cross product
    Vector3d V1(V1x,V1y,V1z);
    Vector3d V2(V2x,V2y,V2z);
    V3=V1.cross(V2);
    if (V3.dot(ParentNormal)<0)
    {
        V3=-V3;
    }
    V3.normalize();
    return V3;
}

void Refinement::ReorderBoundaryElements(Model* M)
{    
    NewElementsOrdered.resize(M->ElementNumber+NewElements.size());
    vector<NewElement> TempNewElementsOrdered;
    TempNewElementsOrdered.resize(M->ElementNumber+NewElements.size());
    
    vector<vector<long>> AllNodes;
    AllNodes.resize(M->NodeNumber+NewNodesVect.size()+1);
    NewElement TempEl;
    
    long Count=0;
    //Load the ordered vector
    for(int i=1; i<M->ElementNumber+1; i++)
    {
        TempEl.Node(0)=M->SurfElement[i-1].AttachedNodes[0]->NodeNum;
        TempEl.Node(1)=M->SurfElement[i-1].AttachedNodes[1]->NodeNum;
        TempEl.Normal(0)=M->SurfElement[i-1].Normal[0];
        TempEl.Normal(1)=M->SurfElement[i-1].Normal[1];
        TempEl.Normal(2)=0;
                
        NewElementsOrdered[Count]=TempEl;
        //Load the AllNodes vector
        AllNodes[TempEl.Node(0)].push_back(Count);
        AllNodes[TempEl.Node(1)].push_back(Count);
        Count++;
    }
    for(int i=0; i<NewElements.size(); i++)
    {
        NewElementsOrdered[Count]=NewElements[i];
        //Load the AllNodes vector
        AllNodes[NewElements[i].Node(0)].push_back(Count);
        AllNodes[NewElements[i].Node(1)].push_back(Count);
        Count++;
    }
}

void Refinement::GetClosestNode(Model* M)
{
    //Count the number of new surface nodes
    long Count=0;
    for (long i=0; i<NewNodesVect.size(); i++)
    {
        if (NewNodesVect[i]->DuplicatedNode==false && NewNodesVect[i]->InDomain==true)
        {
            Count++;
        }
    }
    for(int i=0; i<M->NodeNumber; i++)
    {
        if (M->Nodes[i+1].DuplicatedNode==false)
        {
            Count++;
        }
    }    
    long TotalNodeNum=Count;
    
    //Load all the nodes into a tree
    vector<Point_3> points(TotalNodeNum);
    vector<long> indices(TotalNodeNum);
    
    Count=0;
    for (long i=0; i<NewNodesVect.size(); i++)
    {
        if (NewNodesVect[i]->DuplicatedNode==false && NewNodesVect[i]->InDomain==true)
        {
            if (M->Dimension==2)
                points[Count]=Point_3(NewNodesVect[i]->X[0],NewNodesVect[i]->X[1],0);
            else if (M->Dimension==3)
                points[Count]=Point_3(NewNodesVect[i]->X[0],NewNodesVect[i]->X[1],NewNodesVect[i]->X[2]);
            indices[Count]=Count;
            Count++;
        }
    }
    for(int i=0; i<M->NodeNumber; i++)
    {
        if (M->Nodes[i+1].DuplicatedNode==false)
        {
            if (M->Dimension==2)
                points[Count]=Point_3(M->Nodes[i+1].X[0],M->Nodes[i+1].X[1],0);
            else if (M->Dimension==3)
                points[Count]=Point_3(M->Nodes[i+1].X[0],M->Nodes[i+1].X[1],M->Nodes[i+1].X[2]);
            indices[Count]=Count;
            Count++;
        }
    }
    
    //Load the EdgeNode tree
    Tree EdgeNodeTree;
    EdgeNodeTree.insert(
    boost::make_zip_iterator(boost::make_tuple(points.begin(),indices.begin())),
    boost::make_zip_iterator(boost::make_tuple(points.end(),indices.end())));
    
    Count=0;
    Point_3* query=NULL;
    for (long i=0; i<NewNodesVect.size(); i++)
    {
        if (NewNodesVect[i]->DuplicatedNode==false && NewNodesVect[i]->InDomain==true)
        {
            if (M->Dimension==2)
                query= new Point_3(NewNodesVect[i]->X[0],NewNodesVect[i]->X[1],0);
            else if (M->Dimension==3)
                query= new Point_3(NewNodesVect[i]->X[0],NewNodesVect[i]->X[1],NewNodesVect[i]->X[2]);
            Distance tr_dist;
            int K=2;
                       
            // search K nearest neighbours
            K_neighbor_search search(EdgeNodeTree, *query, K, 0.0,true,tr_dist,true);
            
            //Identify if the node is a new node or a duplpicated node
            for(K_neighbor_search::iterator it = search.begin(); it != search.end(); it++)
            {
                double Dist=tr_dist.inverse_of_transformed_distance(it->second);
                int IndexVal=boost::get<1>(it->first);
                if (IndexVal != Count)
                {
                    NewNodesVect[i]->DistClosestNode=Dist;
                }
            }
            Count++;
        }
    }
    for(int i=0; i<M->NodeNumber; i++)
    {
        if (M->Nodes[i+1].DuplicatedNode==false)
        {
            if (M->Dimension==2)
                query= new Point_3(M->Nodes[i+1].X[0],M->Nodes[i+1].X[1],0);
            else if (M->Dimension==3)
                query= new Point_3(M->Nodes[i+1].X[0],M->Nodes[i+1].X[1],M->Nodes[i+1].X[2]);
            Distance tr_dist;
            int K=2;
            
            // search K nearest neighbours
            K_neighbor_search search(EdgeNodeTree, *query, K, 0.0,true,tr_dist,true);
            
            //Identify if the node is a new node or a duplpicated node
            for(K_neighbor_search::iterator it = search.begin(); it != search.end(); it++)
            {
                double Dist=tr_dist.inverse_of_transformed_distance(it->second);
                int IndexVal=boost::get<1>(it->first);
                if (IndexVal != Count)
                {
                    M->Nodes[i+1].DistClosestNode=Dist;
                }
            }
            Count++;
        }
    }
}
