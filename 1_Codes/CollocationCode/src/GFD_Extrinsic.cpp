#include "GFD_Extrinsic.h"

GFD_Extrinsic::GFD_Extrinsic() {
}

GFD_Extrinsic::GFD_Extrinsic(const GFD_Extrinsic& orig) {
}

GFD_Extrinsic::~GFD_Extrinsic() {
}

void GFD_Extrinsic::InitializeProblem(Model* M, long* TripStressTreadStartIncTemp)
{
    Dimension=M->Dimension;
    TripStressTreadStartInc=TripStressTreadStartIncTemp;
    
    //Get the terms of the Hooke’s Law
    if (M->ModelType=="2D_PLANE_STRAIN" || Dimension==3)
    {
        d1=M->E*(1-M->nu)/((1+M->nu)*(1-2*M->nu));
        d2=M->E*M->nu/((1+M->nu)*(1-2*M->nu));
        d3=M->E*(1-2*M->nu)/((1+M->nu)*(1-2*M->nu));
    }
    else if (M->ModelType=="2D_PLANE_STRESS")
    {
        d1=M->E/(1-pow(M->nu,2));
        d2=M->E*M->nu/(1-pow(M->nu,2));
        d3=M->E*(1-M->nu)/(1-pow(M->nu,2));
    }
    
    //Get the dimension of the matrix of the Taylor's expansion
    if (Dimension==2)
    {
        if (M->ApproxOrder==2)
        {
            lTayl=6;
        }
        else if (M->ApproxOrder==3)
        {
            lTayl=10;
        }
        else if (M->ApproxOrder==4)
        {
            lTayl=15;
        }
    }
    else if (Dimension==3)
    {
        lTayl=10;
    }
}

void GFD_Extrinsic::LoadDerivativeCoefficients(Model* M, Node* Nodes, long NodeNum, bool InterU, int ThreadIndex, int TreadNum)
{
    long Start, End;
    //Get the start and end node of the thread
    M->GetStartEndNode(ThreadIndex,TreadNum,Start, End,NodeNum);
    
    //Add the coefficients to the defined functions
    for (long i=Start; i<=End; i++)
    {
        if (Start==1)
        {
            M->PrintLoading(i-Start,End-Start,"Node Derivative Loading: ", false);
        }

        //Get the initial matrix C1
        MatrixXd C1=C_Matrix(M,&Nodes[i],InterU);
        
        //Fill the matrix K for each particle of the support
        int StartIndex=0;
        if (InterU==true)
        {
            StartIndex=1;
        }
        for (long j=0; j<Nodes[i].SupSize; j++)
        {
            int Col=j;
            if (InterU==true)
            {
                Nodes[i].Coeff[j][0]=0;
                if (j>0)
                {
                    Col=j+1;
                }
            }
            for (int jj=StartIndex; jj<lTayl; jj++)
            {
                Nodes[i].Coeff[j][jj]=C1(jj-StartIndex,Col);
            }
        }
    }
}

MatrixXd GFD_Extrinsic::C_Matrix(Model* M, Node* CollocNode, bool InterU)
{
    MatrixXd A(1,1);
    MatrixXd B(1,1);
    MatrixXd C;
    MatrixXd P(1,1);
    VectorXd PTemp(1);
    MatrixXd W(1,1);
    int SupSize=CollocNode->SupSize;
    int StartIndex=0;
    int MatA_Size=lTayl;
    if (InterU==true)
    {
        StartIndex=1;
    }
    
    A.resize(MatA_Size,MatA_Size);
    B.resize(MatA_Size-StartIndex,SupSize+StartIndex);
    P.resize(MatA_Size,SupSize);
    PTemp.resize(MatA_Size);
    W.resize(SupSize,SupSize);
    
    //Reduced matrices size
    MatrixXd A2(1,1);
    A2.resize(MatA_Size-StartIndex,MatA_Size-StartIndex);
    
    //Initialize the matrices P, W and B3;
    for (int i=0; i<SupSize; i++)
    {
        for (int j=0; j<SupSize; j++)
        {
            W(i,j)=0;
        }
        for (int j=0; j<MatA_Size; j++)
        {
            P(j,i)=0;
        }
        for (int j=0; j<MatA_Size-StartIndex; j++)
        {
            B(j,i)=0;
            if (InterU==true && i==SupSize-1)
            {
                B(j,SupSize)=0;
            }
        }
    }
    
    //Fill the matrices P, W and B
    for (int i=0; i<SupSize; i++)
    {
        double z_i=0;
        long LocNodeNum=CollocNode->SupNodes[i];
        
        //Fill the matrix W
        {
            //Get the distance Z_i of the support node to the reference Node
            for (int m=0; m<Dimension; m++)
            {
                z_i+=pow(M->Nodes[LocNodeNum].X[m]-CollocNode->X[m],2);
            }
            z_i=pow(z_i,0.5);
            //Get Voronoi weight if the function is selected
            double SupNodeVol=1;
            if (M->Voronoi==true && CollocNode->BoundaryNode==false)
                SupNodeVol=CollocNode->SupNodeVol[i];

            //Calculate the weight associated to the considered support node
            double SupRad=CollocNode->SupRadius;
            if (CollocNode->QuadSelection==true)
                SupRad=CollocNode->SupRadA[i];

            double W_=pow(SupNodeVol*M->w(z_i/SupRad),2);
            if (CollocNode->QuadSelection==true)
            {
                W_=CollocNode->SupNodeW[i];
            }
            W(i,i)=W_;
        }
        
        //Fill the matrix P
        {
            for (int j=0; j<lTayl; j++)
            {
                double Value1=1;
                for (int m=0; m<Dimension; m++)
                {
                    Value1=Value1*pow(M->Nodes[LocNodeNum].X[m]-CollocNode->X[m],M->Exponents(j,m))/M->factorial(M->Exponents(j,m));
                }
                P(j,i)=Value1;
            }
        }
        
        //Fill the matrix B
        {
            for (int j=0; j<MatA_Size-StartIndex; j++)
            {
                B(j,i+StartIndex)= W(i,i)*P(j+StartIndex,i);
            }
        }
    }
    
    A=P*W*P.transpose();
    //Fill the matrix A2 and fill the first column of B
    if (InterU==true)
    {
        for (int i=0; i<MatA_Size-StartIndex; i++)
        {
            for (int j=0; j<MatA_Size-StartIndex; j++)
            {
                A2(i,j)=A(i+1,j+1);
            }
            B(i,0)=-A(0,i+1);
        }
    }
    else
    {
        A2=A;
    }
    //Get the inverse of the matrix A2
    MatrixXd A2Inv=A2.inverse();
    double cond=0;
    if(M->PrintConditionNumber==1)
    {
        //Get the matrix conditioning
        cond = A2Inv.norm()*A2.norm();
        if (MinCond==0 && MaxCond==0)
        {
            MinCond=cond;
            MaxCond=cond;
        }
        if (cond>MaxCond)
        {
            MaxCond=cond;
        }
        if (cond<MinCond)
        {
            MinCond=cond;
        }
    }
    C=A2Inv*B;
    return C;
}

void GFD_Extrinsic::LoadVectors(Model* M,double* F_Vec,T* TripStiff,T* TripStress,T* TripStrain, 
        int ThreadIndex, int TreadNum)
{
    long Start, End;
    
    //Get the start and end node of the thread
    M->GetStartEndNode(ThreadIndex,TreadNum,Start, End,M->NodeNumber);
    
    long StressMatInc=TripStressTreadStartInc[ThreadIndex];
    long StiffMatInc=TripStiffTreadStartInc[ThreadIndex];
    TripStiffVect.clear();
    TripStiffVect2.clear();
    
    for (long i=Start; i<=End; i++)
    {
        int SupSize;
        long LocNodeNum;
        
        //Fill the matrix K for each particle of the suppot
        SupSize=M->Nodes[i].SupSize;
        int IndexOffset=1;
        if (M->Nodes[i].CollocationPointOffset==true)
        {
            IndexOffset=0;
        }
        for (long j=0; j<SupSize; j++)
        {
            VectorXd Coeff(1);
            Coeff.resize(lTayl);
            for (int jj=0; jj<lTayl; jj++)
            {
                Coeff(jj)=M->Nodes[i].Coeff[j][jj];
            }
            //Get the number of the support or reference node
            LocNodeNum=M->Nodes[i].SupNodes[j];
            if (Dimension==2)
            {
                LoadRows2D_Stress(M,Coeff,i,LocNodeNum,j,SupSize,TripStress,StressMatInc);
                LoadRows2D_Stiff(M,Coeff,i,LocNodeNum,j,SupSize,F_Vec,TripStiff,StiffMatInc);
            }
        }
        
        //Increment the index of the lagrange BCs
        if (M->BCTreatType=="LAGRANGE" && M->Nodes[i].BoundaryNode==true)
        {
            LagrangeBCIndex++;
        }
        
        for (int k=0; k<Dimension; k++)
        {
            if (M->Nodes[i].BoundaryNode==false)
                StiffMatInc+=SupSize*Dimension;
            else if (M->Nodes[i].BC_N_Bool[k]==true || M->Nodes[i].BC_D_Bool[k]==true)
                StiffMatInc+=SupSize*Dimension;
        }
        StressMatInc+=SupSize*(6+9*(Dimension-2));
    }
}

void GFD_Extrinsic::LoadStiffCoeff(Model* M, Node* Nodes, long NodeNum, int ThreadIndex, int TreadNum)
{
    long Start, End;
    //Get the start and end node of the thread
    M->GetStartEndNode(ThreadIndex,TreadNum,Start, End,NodeNum);
    for (long i=Start; i<=End; i++)
    {
        Nodes[i].StiffCoeff.resize(Nodes[i].SupSize*pow(M->Dimension,2));
        for (long j=0; j<Nodes[i].SupSize; j++)
        {
            if (M->Dimension==2)
            {
                Nodes[i].StiffCoeff[j*M->Dimension*M->Dimension+0]=d1*Nodes[i].Coeff[j][3]+d3/2*Nodes[i].Coeff[j][5];
                Nodes[i].StiffCoeff[j*M->Dimension*M->Dimension+1]=(d2+d3/2)*Nodes[i].Coeff[j][4];
                Nodes[i].StiffCoeff[j*M->Dimension*M->Dimension+2]=(d2+d3/2)*Nodes[i].Coeff[j][4];
                Nodes[i].StiffCoeff[j*M->Dimension*M->Dimension+3]=d1*Nodes[i].Coeff[j][5]+d3/2*Nodes[i].Coeff[j][3];
            }
        }
    }
}

void GFD_Extrinsic::LoadRows2D_Stress(Model* M,VectorXd Coeff,long i,long LocNodeNum,int j, int SupSize,T* TripStress, long& StressMatInc)
{
    //Load the triplet vectors - Method GFD
    double TempVal1,TempVal2,TempVal3,TempVal4,TempVal5,TempVal6;
    //Stress Matrix - Equation 1 - Sigma11
    TempVal1=d1*Coeff(1);
    TempVal2=d2*Coeff(2);
    TripStress[StressMatInc+SupSize*Dimension*0+j*Dimension+0]=T((i-1)*(Dimension+1)+0,(LocNodeNum-1)*Dimension+0,TempVal1);
    TripStress[StressMatInc+SupSize*Dimension*0+j*Dimension+1]=T((i-1)*(Dimension+1)+0,(LocNodeNum-1)*Dimension+1,TempVal2);
    //Stress Matrix - Equation 2 - Sigma12
    TempVal3=d3/2*Coeff(2);
    TempVal4=d3/2*Coeff(1);
    TripStress[StressMatInc+SupSize*Dimension*1+j*Dimension+0]=T((i-1)*(Dimension+1)+1,(LocNodeNum-1)*Dimension+0,TempVal3);
    TripStress[StressMatInc+SupSize*Dimension*1+j*Dimension+1]=T((i-1)*(Dimension+1)+1,(LocNodeNum-1)*Dimension+1,TempVal4);
    //Stress Matrix - Equation 3 - Sigma22
    TempVal5=d2*Coeff(1);
    TempVal6=d1*Coeff(2);
    TripStress[StressMatInc+SupSize*Dimension*2+j*Dimension+0]=T((i-1)*(Dimension+1)+2,(LocNodeNum-1)*Dimension+0,TempVal5);
    TripStress[StressMatInc+SupSize*Dimension*2+j*Dimension+1]=T((i-1)*(Dimension+1)+2,(LocNodeNum-1)*Dimension+1,TempVal6);
}

void GFD_Extrinsic::LoadRows2D_Stiff(Model* M,VectorXd Coeff,long i,long LocNodeNum,int j, int SupSize, double* F_Vec,T* TripStiff, long& StiffMatInc)
{
    double TempValU1_1,TempValU1_2,TempValU2_1,TempValU2_2;
    double u_=Coeff(0);
    //Compute the equilibrium equation
    if (M->Nodes[i].BoundaryNode==false || M->Stabilization==true)
    {
        //Equilibrium - Equation 1
        TempValU1_1=d1*Coeff(3)+d3/2*Coeff(5);
        TempValU1_2=(d2+d3/2)*Coeff(4);
        
        //Equilibrium - Equation 2
        TempValU2_1=(d2+d3/2)*Coeff(4);
        TempValU2_2=d1*Coeff(5)+d3/2*Coeff(3);
    }
    //If no boundary condition is applied to the node
    if (M->Nodes[i].BoundaryNode==false || M->BCTreatType=="LAGRANGE")
    {
        //Set the value of the force vector
        if (j==0)
        {
            F_Vec[(i-1)*Dimension+0]=0;
            F_Vec[(i-1)*Dimension+1]=0;
            for (int k=0; k<M->Nodes[i].SupNodesExRev.size(); k++)
            {
                long NodeIndex=M->Nodes[i].SupNodesExRev[k];
                for (int l=0; l<M->Dimension; l++)
                {
                    TripStiffVect.push_back(T((i-1)*Dimension+0,(NodeIndex-1)*Dimension+l,M->Nodes[i].ARow0[k*M->Dimension+l]));
                    TripStiffVect.push_back(T((i-1)*Dimension+1,(NodeIndex-1)*Dimension+l,M->Nodes[i].ARow1[k*M->Dimension+l]));
                }
            }
        }
    }
    if (M->Nodes[i].BoundaryNode==true)
    {
        //Load the triplet vectors - Method GFD
        double TempVal11_1,TempVal11_2,TempVal12_1,TempVal12_2,TempVal22_1,TempVal22_2;
        //Stress Matrix - Equation 1 - Sigma11
        TempVal11_1=d1*Coeff(1);
        TempVal11_2=d2*Coeff(2);
        
        //Stress Matrix - Equation 2 - Sigma12
        TempVal12_1=d3/2*Coeff(2);
        TempVal12_2=d3/2*Coeff(1);
        
        //Stress Matrix - Equation 3 - Sigma22
        TempVal22_1=d2*Coeff(1);
        TempVal22_2=d1*Coeff(2);
        
        int StartIndex=0;
        if (M->Inter==true || M->Nodes[i].ParentNodes.size()==0)
        {
            StartIndex=1;
        }
        int IncBound=0;
        for (int k=0; k<Dimension; k++)
        {
            //Set the Dirichlet BC if any
            if (M->Nodes[i].BC_D_Bool[k]==true)
            {
                if ((M->Inter==true || M->Nodes[i].ParentNodes.size()==0) && j==0)
                {
                    u_=1;
                }
                else if ((M->Inter==true || M->Nodes[i].ParentNodes.size()==0) && j!=0)
                {
                    u_=0;
                }
                double TempVal7=0;
                double TempVal8=0;
                if (k==0)
                {
                    TempVal7=u_;
                }
                else if (k==1)
                {
                    TempVal8=u_;
                }
                if (M->BCTreatType=="LAGRANGE")
                {
                    long Row=M->NodeNumber*Dimension+LagrangeBCIndex*Dimension+k;
                    long Col1=(LocNodeNum-1)*Dimension+0;
                    TripStiffVect.push_back(T(Row,Col1,TempVal7));
                    TripStiffVect.push_back(T(Col1,Row,TempVal7));
                    long Col2=(LocNodeNum-1)*Dimension+1;
                    TripStiffVect.push_back(T(Row,Col2,TempVal8));
                    TripStiffVect.push_back(T(Col2,Row,TempVal8));
                }
                else
                {
                    TripStiffVect.push_back(T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+0,TempVal7));
                    TripStiffVect.push_back(T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+1,TempVal8));
                }
                
                if (j==0 && M->BCTreatType!="LAGRANGE")
                {
                    F_Vec[(i-1)*Dimension+k]=M->Nodes[i].BC_D[k];
                }
                else if (j==0 && M->BCTreatType=="LAGRANGE")
                {
                    F_Vec[(i-1)*Dimension+k]=0;
                    long Row=M->NodeNumber*Dimension+LagrangeBCIndex*Dimension+k;
                    F_Vec[Row]=M->Nodes[i].BC_D[k];
                }
            }
            //Set the Neumann BC if no Dirichlet BC is present for the considered DOF
            else if (M->Nodes[i].BC_N_Bool[k]==true && M->Nodes[i].BC_D_Bool[k]==false)
            {
                double theta=0;
                if (M->Nodes[i].LocalBoundaryCondition[k]==true)
                {
                    theta=Angle(M,i,k,-1);
                }
                if (M->Nodes[i].BC_DOF[k]==0)
                {
                    double TempVal7,TempVal8;
                    if (M->Nodes[i].LocalBoundaryCondition[k]==true)
                    {
                        double TempVal11_1_loc=TempVal11_1*pow(cos(theta),2)+TempVal12_1*sin(2*theta)+TempVal22_1*pow(sin(theta),2);
                        double TempVal11_2_loc=TempVal11_2*pow(cos(theta),2)+TempVal12_2*sin(2*theta)+TempVal22_2*pow(sin(theta),2);
                        double TempVal12_1_loc=-0.5*TempVal11_1*sin(2*theta)+TempVal12_1*cos(2*theta)+0.5*TempVal22_1*sin(2*theta);
                        double TempVal12_2_loc=-0.5*TempVal11_2*sin(2*theta)+TempVal12_2*cos(2*theta)+0.5*TempVal22_2*sin(2*theta);
                        TempVal7=TempVal11_1_loc*1 + TempVal12_1_loc*0;
                        TempVal8=TempVal11_2_loc*1 + TempVal12_2_loc*0;
                    }
                    else
                    {
                        TempVal7=TempVal11_1*M->Nodes[i].Normal[k][0] + TempVal12_1*M->Nodes[i].Normal[k][1];
                        TempVal8=TempVal11_2*M->Nodes[i].Normal[k][0] + TempVal12_2*M->Nodes[i].Normal[k][1];
                    }
                    if ((M->PartialStab==false && M->Stabilization==true) || (M->PartialStab==true && M->Nodes[i].BC_N[0] + M->Nodes[i].BC_N[1] == 0))
                    {
                        TempVal7=TempVal7-0.5*M->Nodes[i].CharactLength*M->Nodes[i].ProdN*TempValU1_1;
                        TempVal8=TempVal8-0.5*M->Nodes[i].CharactLength*M->Nodes[i].ProdN*TempValU1_2;
                    }
                    
                    if (M->BCTreatType=="LAGRANGE")
                    {
                        long Row=M->NodeNumber*Dimension+LagrangeBCIndex*Dimension+k;
                        long Col1=(LocNodeNum-1)*Dimension+0;
                        TripStiffVect.push_back(T(Row,Col1,TempVal7));
                        TripStiffVect.push_back(T(Col1,Row,TempVal7));
                        long Col2=(LocNodeNum-1)*Dimension+1;
                        TripStiffVect.push_back(T(Row,Col2,TempVal8));
                        TripStiffVect.push_back(T(Col2,Row,TempVal8));
                    }
                    else
                    {
                        TripStiffVect.push_back(T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+0,TempVal7));
                        TripStiffVect.push_back(T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+1,TempVal8));
                    }
                    IncBound+=SupSize*Dimension;
                }
                else if (M->Nodes[i].BC_DOF[k]==1)
                {
                    double TempVal7,TempVal8;
                    if (M->Nodes[i].LocalBoundaryCondition[k]==true)
                    {
                        double TempVal12_1_loc=-0.5*TempVal11_1*sin(2*theta)+TempVal12_1*cos(2*theta)+0.5*TempVal22_1*sin(2*theta);
                        double TempVal12_2_loc=-0.5*TempVal11_2*sin(2*theta)+TempVal12_2*cos(2*theta)+0.5*TempVal22_2*sin(2*theta);
                        double TempVal22_1_loc=TempVal11_1*pow(sin(theta),2)-TempVal12_1*sin(2*theta)+TempVal22_1*pow(cos(theta),2);
                        double TempVal22_2_loc=TempVal11_2*pow(sin(theta),2)-TempVal12_2*sin(2*theta)+TempVal22_2*pow(cos(theta),2);
                        TempVal7=TempVal12_1_loc*1 + TempVal22_1_loc*0;
                        TempVal8=TempVal12_2_loc*1 + TempVal22_2_loc*0;
                    }
                    else
                    {
                        TempVal7=TempVal12_1*M->Nodes[i].Normal[k][0] + TempVal22_1*M->Nodes[i].Normal[k][1];
                        TempVal8=TempVal12_2*M->Nodes[i].Normal[k][0] + TempVal22_2*M->Nodes[i].Normal[k][1];
                    }
                    if ((M->PartialStab==false && M->Stabilization==true) || (M->PartialStab==true && M->Nodes[i].BC_N[0] + M->Nodes[i].BC_N[1] == 0))
                    {
                        TempVal7=TempVal7-0.5*M->Nodes[i].CharactLength*M->Nodes[i].ProdN*TempValU2_1;
                        TempVal8=TempVal8-0.5*M->Nodes[i].CharactLength*M->Nodes[i].ProdN*TempValU2_2;
                    }
                    
                    if (M->BCTreatType=="LAGRANGE")
                    {
                        long Row=M->NodeNumber*Dimension+LagrangeBCIndex*Dimension+k;
                        long Col1=(LocNodeNum-1)*Dimension+0;
                        TripStiffVect.push_back(T(Row,Col1,TempVal7));
                        TripStiffVect.push_back(T(Col1,Row,TempVal7));
                        long Col2=(LocNodeNum-1)*Dimension+1;
                        TripStiffVect.push_back(T(Row,Col2,TempVal8));
                        TripStiffVect.push_back(T(Col2,Row,TempVal8));
                    }
                    else
                    {
                        TripStiffVect.push_back(T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+0,TempVal7));
                        TripStiffVect.push_back(T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+1,TempVal8));
                    }
                }
                if (j==0 && M->BCTreatType!="LAGRANGE")
                {
                    F_Vec[(i-1)*Dimension+k]=-M->Nodes[i].BC_N[k];
                }
                else if (j==0 && M->BCTreatType=="LAGRANGE")
                {
                    F_Vec[(i-1)*Dimension+k]=0;
                    long Row=M->NodeNumber*Dimension+LagrangeBCIndex*Dimension+k;
                    F_Vec[Row]=-M->Nodes[i].BC_N[k];
                }
            }
        }
    }
}

double GFD_Extrinsic::Angle(Model* M,long CollocNode, int Dim,long SingNodes)
{
    double Value=0;
    double ValX=0;
    double ValY=0;
    if (SingNodes==-1)
    {
        ValX=M->Nodes[CollocNode].Normal[Dim][0];
        ValY=M->Nodes[CollocNode].Normal[Dim][1];
    }
    else
    {
        ValX=M->Nodes[CollocNode].X[0]-M->Nodes[SingNodes].X[0];
        ValY=M->Nodes[CollocNode].X[1]-M->Nodes[SingNodes].X[1];
    }
    double Dist=pow(pow(ValX,2)+pow(ValY,2),0.5);
    if (ValY>=0)
        Value=acos(ValX/Dist);
    else
        Value=-acos(ValX/Dist);
    return Value;
}

void GFD_Extrinsic::LoadExtendedSupports(Model* M, Node* NewCollocNodes)
{
    //Initialize the SupNodesEx vector with the normal support nodes
    for (long i=1; i<=M->NodeNumber; i++)
    {
        M->Nodes[i].SupNodesEx.clear();
        M->Nodes[i].SupNodesExRev.clear();
        //Load the direct support nodes
        for (int j=0; j<M->Nodes[i].SupSize; j++)
        {
            M->Nodes[i].SupNodesEx.push_back(M->Nodes[i].SupNodes[j]);
        }
        //Load the support nodes of the voronoi corner nodes
        long VoroCN=0;
        for (int j=0; j<M->Nodes[i].VoroCN_Index.size(); j++)
        {
            VoroCN=M->Nodes[i].VoroCN_Index[j];
            for (int k=0; k<NewCollocNodes[VoroCN].SupSize; k++)
            {
                M->Nodes[i].SupNodesEx.push_back(NewCollocNodes[VoroCN].SupNodes[k]);
            }
        }
        
        //Remove duplpicates from the extended support
        sort(M->Nodes[i].SupNodesEx.begin(),M->Nodes[i].SupNodesEx.end());
        M->Nodes[i].SupNodesEx.erase(unique(M->Nodes[i].SupNodesEx.begin(),M->Nodes[i].SupNodesEx.end()),M->Nodes[i].SupNodesEx.end());
    }
    
    //Load the reversed supports
    for (long i=1; i<=M->NodeNumber; i++)
    {
        M->Nodes[i].SupNodesExRev=M->Nodes[i].SupNodesEx;
    }
    for (long i=1; i<=M->NodeNumber; i++)
    {
        for (int j=0; j<M->Nodes[i].SupNodesEx.size(); j++)
        {
            for (int k=0; k<M->Nodes[i].SupNodesEx.size(); k++)
            {
                M->Nodes[M->Nodes[i].SupNodesEx[j]].SupNodesExRev.push_back(M->Nodes[i].SupNodesEx[k]);
            }
        }
    }
    for (long i=1; i<=M->NodeNumber; i++)
    {
        sort(M->Nodes[i].SupNodesExRev.begin(),M->Nodes[i].SupNodesExRev.end());
        M->Nodes[i].SupNodesExRev.erase(unique(M->Nodes[i].SupNodesExRev.begin(),M->Nodes[i].SupNodesExRev.end()),M->Nodes[i].SupNodesExRev.end());
        M->Nodes[i].ARow0.resize(M->Dimension*M->Nodes[i].SupNodesExRev.size());
        M->Nodes[i].ARow1.resize(M->Dimension*M->Nodes[i].SupNodesExRev.size());
        for (int j=0; j<M->Nodes[i].SupNodesExRev.size(); j++)
        {
            for (int k=0; k<M->Dimension; k++)
            {
                M->Nodes[i].ARow0[j*M->Dimension+k]=0;
                M->Nodes[i].ARow1[j*M->Dimension+k]=0;
            }
        }
    }
}

void GFD_Extrinsic::LoadStiffTriplets(Model* M)
{
    for (long i=1; i<=M->NodeNumber; i++)
    {
        int MatAH=M->Nodes[i].VoroCN_Index.size()+1;
//        int MatAH=M->Nodes[i].VoroCN_Index.size();
//        int MatAH=1;
        MatrixXd W=MatrixXd::Zero(MatAH,MatAH);
        //Get the distance of the farthest voronoi corner node and fill a distance vector
        double DistMax=0;
        vector<double> DistanceCN;
        DistanceCN.resize(M->Nodes[i].VoroCN_Index.size());
        for (int j=0; j<M->Nodes[i].VoroCN_Index.size(); j++)
        {
            long VoroCN=M->Nodes[i].VoroCN_Index[j];
            double Dist=0;
            for (int m=0; m<M->Dimension; m++)
            {
                Dist+=pow(M->Nodes[i].X[m]-M->CNodes[VoroCN].X[m],2);
            }
            Dist=pow(Dist,0.5);
            DistanceCN[j]=Dist;
            if (Dist>DistMax)
                DistMax=Dist;
        }
        //Scale the maximum distance by a factor
        DistMax=DistMax*M->WeightScaleFactor;
        //Fill the matrix W
        W(0,0)=1;
        for (int j=0; j<M->Nodes[i].VoroCN_Index.size(); j++)
        {
            if (M->WeightScaleFactor==-1)
                 W(j+1,j+1)=1;
            else
                W(j+1,j+1)=pow(M->w(DistanceCN[j]/DistMax),1);
//            W(j,j)=pow(M->w(DistanceCN[j]/DistMax),1);
        }
        
        MatrixXd A0=A_Matrix(M,i,0);
        MatrixXd A1=A_Matrix(M,i,1);
        MatrixXd A0_MLS;
        MatrixXd A1_MLS;
        
        int MehtodNum=M->NumberEnrichmentCoeff;
        
        //Compute the MLS stencil equations for the Method 2
        if (MehtodNum==2)
        {
            A0_MLS=A0.transpose()*W*A0;
            A1_MLS=A1.transpose()*W*A1;
        }
               
        //  --  Method 1: Sum of the weighted Equation on each row  --
        if (MehtodNum==1)
        {
            //Loop through all the equation enforced at the considered collocation node
            for (int j=0; j<MatAH; j++)
            {
                //Loop through all the entries of the equation
                for (int k=0; k<M->Nodes[i].SupNodesEx.size(); k++)
                {
                    long LocNodek=M->Nodes[i].SupNodesEx[k];
                    int index=getIndex(M->Nodes[i].SupNodesExRev,LocNodek);
                    for (int l=0; l<M->Dimension; l++)
                    {
                        M->Nodes[i].ARow0[index*M->Dimension+l]+=W(j,j)*A0(j,k*M->Dimension+l);
                        M->Nodes[i].ARow1[index*M->Dimension+l]+=W(j,j)*A1(j,k*M->Dimension+l);
                    }
                }
            }
        }
        
        //  --  Method 2: MLS approximation of the solution at each node  --
        if (MehtodNum==2)
        {
            //Loop through all the rows of the matrix
            for (int j=0; j<M->Nodes[i].SupNodesEx.size(); j++)
            {
                long LocNodej=M->Nodes[i].SupNodesEx[j];
                for (int k=0; k<M->Nodes[i].SupNodesEx.size(); k++)
                {
                    long LocNodek=M->Nodes[i].SupNodesEx[k];
                    //Get the index of LocNodek in the matrix row LocNodej
                    int index=getIndex(M->Nodes[LocNodej].SupNodesExRev,LocNodek);
                    for (int l=0; l<M->Dimension; l++)
                    {
                        M->Nodes[LocNodej].ARow0[index*M->Dimension+l]+=A0_MLS(j,k*M->Dimension+l);
                        M->Nodes[LocNodej].ARow1[index*M->Dimension+l]+=A1_MLS(j,k*M->Dimension+l);
                    }
                }
            }
        }
    }
}

MatrixXd GFD_Extrinsic::A_Matrix(Model* M, long i, int DOF)
{
    //Include the voronoi nodes + the collocation node
    int MatAH=M->Nodes[i].VoroCN_Index.size()+1;
    int MatAW=M->Nodes[i].SupNodesEx.size()*M->Dimension;
    MatrixXd A=MatrixXd::Zero(MatAH,MatAW);
    //Fill the matrix A for each collocation node associated to the node i
    for (int j=0; j<MatAH; j++)
    {
        if (j==0)
        {
            for (int k=0; k<M->Nodes[i].SupSize;k++)
            {
                //Get the index of the sup node in the extended support
                int index=getIndex(M->Nodes[i].SupNodesEx,M->Nodes[i].SupNodes[k]);
                for (int l=0; l<M->Dimension; l++)
                {
                    A(j,index*M->Dimension+l)=M->Nodes[i].StiffCoeff[k*pow(M->Dimension,2)+DOF*M->Dimension+l];
                }
            }
        }
        else if (j>0)
        {
            long VoroCN=M->Nodes[i].VoroCN_Index[j-1];
            for (int k=0; k<M->CNodes[VoroCN].SupSize;k++)
            {
                //Get the index of the sup node in the extended support
                int index=getIndex(M->Nodes[i].SupNodesEx,M->CNodes[VoroCN].SupNodes[k]);
                for (int l=0; l<M->Dimension; l++)
                {
                    A(j,index*M->Dimension+l)=M->CNodes[VoroCN].StiffCoeff[k*pow(M->Dimension,2)+DOF*M->Dimension+l];
                }
            }
        }
    }
    return A;
}

//www.geeksforgeeks.org/how-to-find-index-of-a-given-element-in-a-vector-in-cpp/
int GFD_Extrinsic::getIndex(vector<long> v, long K) 
{
    int index=-1;
    auto it = find(v.begin(),v.end(),K); 
    // If element was found 
    if (it != v.end()) { 
        index = distance(v.begin(),it); 
    }
    return index;
}