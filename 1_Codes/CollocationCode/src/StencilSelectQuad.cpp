#include "StencilSelect.h"

void StencilSelect::LoadAreaSupport(Model* M,Node* CollocNode,long* IndexArray,double* DistArray,int k, int SupSize,int SupScalingNum)
{
    double SupRad = DistArray[SupSize-1]*1.001;
    CollocNode->SetSupRadius(pow(SupRad/1.001*SupScaling,2));
    
    //Get the number of triangles associated to the "ideal" discretization
    double nTrianglesEqui=(1.27E-2 * pow(SupSize,2) + 1.07 * SupSize - 8.62E-1);
    double aTrianglesEqui=pow(pi*pow(SupRad,2)*4/(nTrianglesEqui*pow(3,0.5)),0.5);
    
    int CountSup=0;
    int j=0;
    while (DistArray[j]<=SupRad && j<k)
    {
        CountSup++;
        j++;
    }
    CollocNode->SupSize=CountSup;
    CollocNode->SupNodes = new long[CountSup];
    
    bool* NodeIncluded=NULL;
    NodeIncluded=new bool[SupScalingNum*SupSize];
    for (j=0;j<SupScalingNum*SupSize;j++)
    {
        NodeIncluded[j]=false;
    }
    
    j=0;
    int SelectedNode=0;
    for (j=0;j<SupScalingNum*SupSize;j++)
    {
        double MinDist=-1;
        for (int k=0; k<j; k++)
        {
            if (NodeIncluded[k]==true)
            {
                double LocDist=0;
                for (int l=0; l<Dimension; l++)
                {
                    LocDist+=pow(M->Nodes[IndexArray[j]].X[l]-M->Nodes[IndexArray[k]].X[l],2);
                }
                LocDist=pow(LocDist,0.5);
                if (k==0)
                {
                    MinDist=LocDist;
                }
                else
                {
                    if(LocDist<MinDist)
                        MinDist=LocDist;
                }
            }
        }
        if (MinDist>aTrianglesEqui*StencilSelectionThreshold || MinDist==-1)
        {
            CollocNode->SupNodes[SelectedNode]=IndexArray[j];
            NodeIncluded[j]=true;
            SelectedNode++;
        }
        else
        {
            NodeIncluded[j]=false;
            cout << "Node too close" << endl;
        }
        if (SelectedNode==CountSup)
            break;
    }
}

void StencilSelect::LoadSupportQuad(Model* M,Node* CollocNode, long* IndexArray, double* DistArray, int k, int SupSize)
{
    //Identify the number of nodes in each quadran
    vector<vector<vector<double>>> Q_All;
    Q_All.clear();
    Q_All.resize(4);
    
    //Get the start and end sector
    double StartSector=0;
    double EndSector=0;
    GetStartEndSectors2(M,CollocNode->NodeNum,IndexArray,k,&StartSector,&EndSector);
    long RefNode=-1;
    if (CollocNode->NodeNum==RefNode)
    {
        cout << "StartSector=" << StartSector << ", EndSector=" << EndSector << endl;
    }
    //Load each node in a sector
    //Start at index 1 as the first node is the collocation node
    for (int j=1; j<k; j++)
    {
        long SupIndex=IndexArray[j];
        //Get the angle in degrees
        double Angle=GetAngle(M,M->Nodes[SupIndex].X[0]-CollocNode->X[0],M->Nodes[SupIndex].X[1]-CollocNode->X[1],CollocNode->NodeNum,SupIndex,true);
        if (Angle<StartSector)
        {
            Angle+=360;
        }
        else if (Angle>EndSector)
        {
            Angle-=360;
        }
        
        vector<double> TempPair;
        TempPair.resize(2);
        TempPair[0]=M->DistanceNN(CollocNode->X,M->Nodes[SupIndex].X);
        TempPair[1]=SupIndex;
        
        if (CollocNode->NodeNum==RefNode)
        {
            cout << "j=" << SupIndex << endl;
        }
        
        if (Angle>=StartSector+0*(EndSector-StartSector)/4 && Angle<StartSector+1*(EndSector-StartSector)/4)
        {
            Q_All[0].push_back(TempPair);
        }
        else if (Angle>=StartSector+1*(EndSector-StartSector)/4 && Angle<StartSector+2*(EndSector-StartSector)/4)
        {
            Q_All[1].push_back(TempPair);
        }
        else if (Angle>=StartSector+2*(EndSector-StartSector)/4 && Angle<StartSector+3*(EndSector-StartSector)/4)
        {
            Q_All[2].push_back(TempPair);
        }
        else if (Angle>=StartSector+3*(EndSector-StartSector)/4 && Angle<=StartSector+4*(EndSector-StartSector)/4)
        {
            Q_All[3].push_back(TempPair);
        }
    }
        
    //Get the minimum number of nodes in each sector
    int MinSectorCount=Q_All[0].size();
    for (int j=1; j<4; j++)
    {
        if (Q_All[j].size()<MinSectorCount)
        {
            MinSectorCount=Q_All[j].size();
        }
    }
    
    int SupNodePerQuad=round((double)SupSize/(double)4+(double)0.49999);
    if (CollocNode->NodeNum==RefNode)
    {
        cout << "SupSize=" << SupSize << ", SupNodePerQuad=" << SupNodePerQuad << endl;
    }
    if (MinSectorCount<1.2*SupNodePerQuad || EndSector-StartSector<-1 || EndSector-StartSector<180)
    {
        //Do not consider the quadrant selection rule
        LoadNormalSupport(M,CollocNode,IndexArray,DistArray,k,SupSize);
    }
    else
    {
        //Identify that the node uses the quad selection algorithm
//        Nodes[i].QuadSelection=true;
        CollocNode->SupRadA=new double[4*SupNodePerQuad+1];
        
        //Sort the vectors of each quadrant by distance
        for (int j=0; j<4; j++)
        {
            sort(Q_All[j].begin(),Q_All[j].end(),[](const vector<double>& a,const vector<double>& b){return a[0]<b[0];});
        }
    
        CollocNode->SupSize=4*SupNodePerQuad+1;
        CollocNode->SupNodes = new long[4*SupNodePerQuad+1];
        
        //Set the collocation node (first node)
        CollocNode->SupNodes[0]=IndexArray[0];
        //The support node radius of the collocation node associated to the first node is N/A
        CollocNode->SupRadA[0]=0;
        
        vector<double> Q_Selected;
        Q_Selected.clear();
        double MaxRad=0;
        for (int l=0; l<4; l++)
        {
            if (Q_All[l][SupNodePerQuad][0]>MaxRad)
                MaxRad=Q_All[l][SupNodePerQuad-1][0];
        }
        
        int Count=1;
        double SupRad=0;
        for (int j=0; j<SupNodePerQuad; j++)
        {
            for (int l=0; l<4; l++)
            {
                CollocNode->SupNodes[Count]=Q_All[l][j][1];
                CollocNode->SupRadA[Count]=Q_All[l][SupNodePerQuad-1][0]*SupScaling;
                if (CollocNode->NodeNum==RefNode)
                    cout << "Selected Nodes: Quad" << l+1 << ", Index=" << Q_All[l][j][1] << ", Dist=" << Q_All[l][j][0]*1000 << endl;
                Count++;
                if (Q_All[l][j][0]>SupRad)
                {
                    SupRad=Q_All[l][j][0];
                }
            }
        }
        
        CollocNode->SupRadiusSq=pow(SupRad*SupScaling,2);
        CollocNode->SupRadius=SupRad*SupScaling;
    }
}

void StencilSelect::LoadSupportAnisotropic(Model* M,Node* CollocNode, long* IndexArray, double* DistArray, int k, int SupSize)
{
    //Get the anisotropy direction
    double Mx=0;
    double My=0;
    double Mxy=0;
    for (int j=1; j<k; j++)
    {
        long SupIndex=IndexArray[j];
        Mx+=pow(M->Nodes[SupIndex].X[0]-CollocNode->X[0],2);
        My+=pow(M->Nodes[SupIndex].X[1]-CollocNode->X[1],2);
        Mxy+=(M->Nodes[SupIndex].X[0]-CollocNode->X[0])*(M->Nodes[SupIndex].X[1]-CollocNode->X[1],2);
    }
    double Theta=0.5*atan(2*Mxy/(Mx-My));
    //Transformation of the stencil in a local axis system rotated by Theta
    vector<vector<double>> RotatedStencil;
    RotatedStencil.resize(k-1);
    double aPos=0;
    double aNeg=0;
    double bPos=0;
    double bNeg=0;
    for (int j=1; j<k; j++)
    {
        long SupIndex=IndexArray[j];
        RotatedStencil[j-1].resize(Dimension);
        RotatedStencil[j-1][0]=+(M->Nodes[SupIndex].X[0]-CollocNode->X[0])*cos(Theta)+(M->Nodes[SupIndex].X[1]-CollocNode->X[1])*sin(Theta);
        RotatedStencil[j-1][1]=-(M->Nodes[SupIndex].X[0]-CollocNode->X[0])*sin(Theta)+(M->Nodes[SupIndex].X[1]-CollocNode->X[1])*cos(Theta);
        
        if (RotatedStencil[j-1][0]>aPos)
            aPos=RotatedStencil[j-1][0];
        if (RotatedStencil[j-1][0]<aNeg)
            aNeg=RotatedStencil[j-1][0];
        if (RotatedStencil[j-1][1]>bPos)
            bPos=RotatedStencil[j-1][1];
        if (RotatedStencil[j-1][1]<bNeg)
            bNeg=RotatedStencil[j-1][1];
    }
    
    //Set a perfect circle
    double MaxA=aPos;
    if (abs(aNeg)>aPos)
        MaxA=abs(aNeg);
    double MaxB=bPos;
    if (abs(bNeg)>bPos)
        MaxB=abs(bNeg);
    if (MaxB>MaxA)
        MaxA=MaxB;
    aPos=MaxA;
    aNeg=MaxA;
    bPos=MaxA;
    bNeg=MaxA;
    
    //Get the coefficients c of the ellipse
    double Cpp=0;
    double Cnp=0;
    double Cnn=0;
    double Cpn=0;
    //Getting Cpp (positive aPos/bPos quadran)
    if (aPos>bPos)
        Cpp=pow(aPos,2)-pow(bPos,2);
    else
        Cpp=pow(bPos,2)-pow(aPos,2);
    double Xsi0pp=log((abs(aPos)+abs(bPos))/Cpp);
    
    //Getting Cnp (positive aNeg/bPos quadran)
    if (-aNeg>bPos)
        Cnp=pow(aNeg,2)-pow(bPos,2);
    else
        Cnp=pow(bPos,2)-pow(aNeg,2);
    double Xsi0np=log((abs(aNeg)+abs(bPos))/Cnp);
    
    //Getting Cnn (positive aNeg/bNeg quadran)
    if (-aNeg>-bNeg)
        Cnn=pow(aNeg,2)-pow(bNeg,2);
    else
        Cnn=pow(bNeg,2)-pow(aNeg,2);
    double Xsi0nn=log((abs(aNeg)+abs(bNeg))/Cnn);
    
    //Getting Cpn (positive aPos/bNeg quadran)
    if (aPos>-bNeg)
        Cpn=pow(aPos,2)-pow(bNeg,2);
    else
        Cpn=pow(bNeg,2)-pow(aPos,2);
    double Xsi0pn=log((abs(aPos)+abs(bNeg))/Cpn);
    
    vector<double> Wt;
    Wt.resize(4);
    for (int j=0; j<4; j++)
    {
        Wt[j]=0;
    }
    
    //Get the distance factor for each support node
    vector<vector<double>> EllipseFactor;
    EllipseFactor.resize(k-1);
    for (int j=1; j<k; j++)
    {
        long SupIndex=IndexArray[j];
        EllipseFactor[j-1].resize(5);
        //Get the angle i nthe local coordinate system
        double Angle=GetAngle(M,RotatedStencil[j-1][0],RotatedStencil[j-1][1],CollocNode->NodeNum,SupIndex,false);
        double dist=M->DistanceNN(CollocNode->X,M->Nodes[SupIndex].X);
        EllipseFactor[j-1][0]=SupIndex;
        EllipseFactor[j-1][1]=Angle;
        //Get the distance of the ellipse node at this angle
        double Threshold=1.0e-15;
        
        if (Angle>=0 && Angle<pi/2)
        {
            EllipseFactor[j-1][2]=pow(pow(Cpp*cosh(Xsi0pp)*cos(Angle),2)+pow(Cpp*sinh(Xsi0pp)*sin(Angle),2),0.5);
            if (abs(Cpp)<Threshold)
                EllipseFactor[j-1][2]=abs(aPos);
        }
        else if (Angle>=pi/2 && Angle<pi)
        {
            EllipseFactor[j-1][2]=pow(pow(Cnp*cosh(Xsi0np)*cos(Angle),2)+pow(Cnp*sinh(Xsi0np)*sin(Angle),2),0.5);
            if (abs(Cnp)<Threshold)
                EllipseFactor[j-1][2]=abs(aNeg);
        }
        else if (Angle>=pi && Angle<3*pi/2)
        {
            EllipseFactor[j-1][2]=pow(pow(Cnn*cosh(Xsi0nn)*cos(Angle),2)+pow(Cnn*sinh(Xsi0nn)*sin(Angle),2),0.5);
            if (abs(Cnn)<Threshold)
                EllipseFactor[j-1][2]=abs(aNeg);
        }
        else if (Angle>=3*pi/2 && Angle<2*pi)
        {
            EllipseFactor[j-1][2]=pow(pow(Cpn*cosh(Xsi0pn)*cos(Angle),2)+pow(Cpn*sinh(Xsi0pn)*sin(Angle),2),0.5);
            if (abs(Cpn)<Threshold)
                EllipseFactor[j-1][2]=abs(aPos);
        }
        EllipseFactor[j-1][3]=dist/EllipseFactor[j-1][2];
        EllipseFactor[j-1][4]=dist;
    }
    
    //Sort the support nodes by relative distance to the collocation node
    sort(EllipseFactor.begin(),EllipseFactor.end(),[](const vector<double>& a,const vector<double>& b){return a[3]<b[3];});
    
    double SupRatioEllipse = EllipseFactor[SupSize-1][3];
    long SupNode=EllipseFactor[SupSize-2][0];
    double SupRadTemp=M->DistanceNN(CollocNode->X,M->Nodes[SupNode].X);
    CollocNode->SupRadiusSq=pow(SupRadTemp*SupScaling,2);
    CollocNode->SupRadius=SupRadTemp*SupScaling;
    
    //Count the number of support nodes to be included
    int CountSup=0;
    int j=0;
    while (EllipseFactor[j][3]<=SupRatioEllipse*1.001 && j<k-1)
    {
        CountSup++;
        j++;
    }
    
    //Identify that the node uses the quad selection algorithm
    CollocNode->QuadSelection=true;
    CollocNode->SupRadA=new double[CountSup+1];
    CollocNode->SupNodeW=new double[CountSup+1];
    CollocNode->SupSize=CountSup+1;
    CollocNode->SupNodes = new long[CountSup+1];
    
    //Set the collocation node (first node)
    CollocNode->SupNodes[0]=IndexArray[0];
    CollocNode->SupRadA[0]=CollocNode->SupRadius;
    CollocNode->SupNodeW[0]=1;
    int Count=1;
    j=0;
    while (EllipseFactor[j][3]<=SupRatioEllipse*1.001 && j<k-1)
    {
        CollocNode->SupNodes[Count]=EllipseFactor[j][0];
        CollocNode->SupRadA[Count]=EllipseFactor[j][2]*SupRatioEllipse*SupScaling;
        double s=EllipseFactor[j][4]/CollocNode->SupRadA[Count];
        double LocalWeight=0;
        if (s<1)
            LocalWeight=1-6*pow(s,2)+8*pow(s,3)-3*pow(s,4);
            
        if (LocalWeight>0)
            LocalWeight=pow(LocalWeight,M->SupExp);
        else
            LocalWeight=0;
        
        if (EllipseFactor[j][1]>=0 && EllipseFactor[j][1]<pi/2)
        {
            Wt[0]+=LocalWeight;
        }
        else if (EllipseFactor[j][1]>=pi/2 && EllipseFactor[j][1]<pi)
        {
            Wt[1]+=LocalWeight;
        }
        else if (EllipseFactor[j][1]>=pi && EllipseFactor[j][1]<3*pi/2)
        {
            Wt[2]+=LocalWeight;
        }
        else if (EllipseFactor[j][1]>=3*pi/2 && EllipseFactor[j][1]<2*pi)
        {
            Wt[3]+=LocalWeight;
        }
        CollocNode->SupNodeW[Count]=LocalWeight;
        Count++;
        j++;
    }
    
    double Wavg=0;
    for (int j=0; j<4; j++)
    {
        Wavg+=Wt[j];
    }
    
    Count=1;
    j=0;
    while (EllipseFactor[j][3]<=SupRatioEllipse*1.001 && j<k-1)
    {
        if (EllipseFactor[j][1]>=0 && EllipseFactor[j][1]<pi/2)
        {
            CollocNode->SupNodeW[Count]=CollocNode->SupNodeW[Count]*Wavg/Wt[0];
        }
        else if (EllipseFactor[j][1]>=pi/2 && EllipseFactor[j][1]<pi)
        {
            CollocNode->SupNodeW[Count]=CollocNode->SupNodeW[Count]*Wavg/Wt[1];
        }
        else if (EllipseFactor[j][1]>=pi && EllipseFactor[j][1]<3*pi/2)
        {
            CollocNode->SupNodeW[Count]=CollocNode->SupNodeW[Count]*Wavg/Wt[2];
        }
        else if (EllipseFactor[j][1]>=3*pi/2 && EllipseFactor[j][1]<2*pi)
        {
            CollocNode->SupNodeW[Count]=CollocNode->SupNodeW[Count]*Wavg/Wt[3];
        }
        Count++;
        j++;
    }
}

double StencilSelect::GetAngle(Model* M,double dX, double dY, long CollocN, long SupN, bool Deg)
{
    double TempAngle=-1;
    
    //Get the distance between the collocation node and the support node
    double Dist=M->DistanceNN(M->Nodes[CollocN].X,M->Nodes[SupN].X);
    
    //Get the angle in radians
    if (Dimension==2)
    {
        if (dX>0 && dY>0)
        {
            TempAngle=acos(dX/Dist);
        }
        else if (dX==0 && dY>0)
        {
            TempAngle=pi/2;
        }
        else if (dX<0 && dY>0)
        {
            TempAngle=pi/2+acos(dY/Dist);
        }
        else if (dX<0 && dY==0)
        {
            TempAngle=pi;
        }
        else if (dX<0 && dY<0)
        {
            TempAngle=pi+acos(abs(dX)/Dist);
        }
        else if (dX==0 && dY<0)
        {
            TempAngle=3*pi/2;
        }
        else if (dX>0 && dY<0)
        {
            TempAngle=3*pi/2+acos(abs(dY)/Dist);;
        }
        else if (dX>0 && dY==0)
        {
            TempAngle=0;
        }
    }
    
    if (Deg==true)
    {
        //Conversion from radians to degrees
        TempAngle=TempAngle*180/pi;
    }
    
    //The angle is given between 0 and 2*PI or between 0° and 360°
    return TempAngle;
}

void StencilSelect::GetStartEndSectors(Model* M,long i, long* IndexArray, int k, double *StartSector, double *EndSector)
{
    //Get the angles of each ray
    vector<double> Angles;
    Angles.resize(k-1);
    for (int j=1; j<k; j++)
    {
        long SupIndex=IndexArray[j];
        //Get the angle in degrees
        Angles[j-1]=GetAngle(M,M->Nodes[SupIndex].X[0]-M->Nodes[i].X[0],M->Nodes[SupIndex].X[1]-M->Nodes[i].X[1],i,SupIndex,true);
    }
    //Sort the angles
    sort(Angles.begin(),Angles.end());
    //Get the maximum angle between two consecutive nodes
    double MaxAngle=abs(Angles[k-2]-Angles[0]-360);
    for (int j=0; j<k-2; j++)
    {
        if (abs(Angles[j+1]-Angles[j])>MaxAngle)
        {
            MaxAngle=abs(Angles[j+1]-Angles[j]);
        }
    }
        
    *StartSector=0;
    *EndSector=0;
    if (MaxAngle<90)
    {
        //Set the origin angle in the middle of the largest angle
        bool MaxAngleFound=false;
        for (int j=0; j<k-2; j++)
        {
            if (abs(Angles[j+1]-Angles[j])==MaxAngle)
            {
                *StartSector=(Angles[j]+Angles[j+1])/2+45;
                MaxAngleFound=true;
            }
        }
        if (MaxAngleFound==false)
        {
            *StartSector=(Angles[0]+(Angles[k-2]-360))/2+45;
        }
        *EndSector=*StartSector+360;
    }
    else if (MaxAngle>=90)
    {
        *StartSector=Angles[0];
        *EndSector=Angles[k-2];
        for (int j=0; j<k-2; j++)
        {
            if (abs(Angles[j+1]-Angles[j])==MaxAngle)
            {
                *StartSector=Angles[j+1];
                *EndSector=Angles[j];
            }
        }
        if (*EndSector<*StartSector)
        {
            *EndSector+=360;
        }
    }
}

void StencilSelect::GetStartEndSectors2(Model* M,long i, long* IndexArray, int k, double *StartSector, double *EndSector)
{
    //Get the average support coordinate
    double Xavg=0;
    double Yavg=0;
    for (int j=1; j<k; j++)
    {
        long SupIndex=IndexArray[j];
        Xavg+=M->Nodes[SupIndex].X[0];
        Yavg+=M->Nodes[SupIndex].X[1];
    }
    Xavg=Xavg/(k-1);
    Yavg=Yavg/(k-1);
    
    double dX=Xavg-M->Nodes[i].X[0];
    double dY=Yavg-M->Nodes[i].X[1];
    double Dist=pow(pow(dX,2)+pow(dY,2),0.5);
    
    double TempAngle=0;
    if (dX>0 && dY>0)
    {
        TempAngle=acos(dX/Dist);
    }
    else if (dX==0 && dY>0)
    {
        TempAngle=pi/2;
    }
    else if (dX<0 && dY>0)
    {
        TempAngle=pi/2+acos(dY/Dist);
    }
    else if (dX<0 && dY==0)
    {
        TempAngle=pi;
    }
    else if (dX<0 && dY<0)
    {
        TempAngle=pi+acos(abs(dX)/Dist);
    }
    else if (dX==0 && dY<0)
    {
        TempAngle=3*pi/2;
    }
    else if (dX>0 && dY<0)
    {
        TempAngle=3*pi/2+acos(abs(dY)/Dist);;
    }
    else if (dX>0 && dY==0)
    {
        TempAngle=0;
    }
//    TempAngle+=45;
//    if (TempAngle>360)
//        TempAngle=TempAngle-360;
    
    *StartSector=TempAngle;
    *EndSector=*StartSector+360;
}