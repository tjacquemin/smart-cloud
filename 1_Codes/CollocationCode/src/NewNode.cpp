#include "NewNode.h"

NewNode::NewNode() {
}

NewNode::NewNode(const NewNode& orig) {
}

NewNode::~NewNode()
{
    X.clear();
}

void NewNode::SetNewNode(vector<double> TempX, long ParentNo, long ParentEl, double ParentElSupSize, bool OnBound)
{
    X.resize(Dimension);
    for (int i=0; i<Dimension; i++)
    {
        X[i]=TempX[i];
    }
    //Parent nodes and elements
    ParentNode=ParentNo;
    ParentElement=ParentEl;
    ParentNodeRadius=ParentElSupSize;
    //New node postition
    BoundaryNode=OnBound;
}

void NewNode::CopyFromNewNode(Model* M, NewNode* NewNodeTemp)
{
    X.resize(Dimension);
    for (int i=0; i<Dimension; i++)
    {
        X[i]=NewNodeTemp->X[i];
    }
    //Parent nodes and elements
    ParentNode=NewNodeTemp->ParentNode;
    ParentElement=NewNodeTemp->ParentElement;
    ParentNodeRadius=NewNodeTemp->ParentNodeRadius;
    //New node postition
    BoundaryNode=NewNodeTemp->BoundaryNode;
    DuplicatedNode=NewNodeTemp->DuplicatedNode;
    
    //Boundary condition
    if (BoundaryNode==true && M->DirectCollocRefinement==true)
    {
        LocalBoundaryCondition = new bool[Dimension];
        BC_D_Bool = new bool[Dimension];
        BC_N_Bool = new bool[Dimension];
        BC_DOF = new int[Dimension];
        BC_D = new double[Dimension];
        BC_N = new double[Dimension];
        Normal.resize(Dimension);
        for (int i=0; i<Dimension; i++)
        {
            LocalBoundaryCondition[i]=NewNodeTemp->LocalBoundaryCondition[i];
            BC_D_Bool[i]=NewNodeTemp->BC_D_Bool[i];
            BC_N_Bool[i]=NewNodeTemp->BC_N_Bool[i];
            BC_DOF[i]=NewNodeTemp->BC_DOF[i];
            BC_D[i]=NewNodeTemp->BC_D[i];
            BC_N[i]=NewNodeTemp->BC_N[i];
            Normal[i]=NewNodeTemp->Normal[i];
        }
    }
}

void NewNode::SetBC(int Dim)
{
    BoundaryNode=true;
    LocalBoundaryCondition = new bool[Dim];
    BC_D_Bool = new bool[Dim];
    BC_N_Bool = new bool[Dim];
    BC_DOF = new int[Dim];
    BC_D = new double[Dim];
    BC_N = new double[Dim];
    Normal.resize(Dim);
}
