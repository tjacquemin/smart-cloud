#include "GFD_Problem.h"

GFD_Problem::GFD_Problem() {
}

GFD_Problem::GFD_Problem(const GFD_Problem& orig) {
}

GFD_Problem::~GFD_Problem() {
}

void GFD_Problem::InitializeProblem(Model* M, long* TripStressTreadStartIncTemp)
{
    Dimension=M->Dimension;
    TripStressTreadStartInc=TripStressTreadStartIncTemp;
    
    //Get the terms of the Hooke’s Law
    if (M->ModelType=="2D_PLANE_STRAIN" || Dimension==3)
    {
        d1=M->E*(1-M->nu)/((1+M->nu)*(1-2*M->nu));
        d2=M->E*M->nu/((1+M->nu)*(1-2*M->nu));
        d3=M->E*(1-2*M->nu)/((1+M->nu)*(1-2*M->nu));
    }
    else if (M->ModelType=="2D_PLANE_STRESS")
    {
        d1=M->E/(1-pow(M->nu,2));
        d2=M->E*M->nu/(1-pow(M->nu,2));
        d3=M->E*(1-M->nu)/(1-pow(M->nu,2));
    }
    
    //Get the dimension of the matrix of the Taylor's expansion
    if (Dimension==2)
    {
        if (M->ApproxOrder==2)
        {
            lTayl=6;
        }
        else if (M->ApproxOrder==3)
        {
            lTayl=10;
        }
        else if (M->ApproxOrder==4)
        {
            lTayl=15;
        }
    }
    else if (Dimension==3)
    {
        lTayl=10;
    }
}

void GFD_Problem::LoadDerivativeCoefficients(Model* M, Node* Nodes, long NodeNum, int ThreadIndex, int TreadNum)
{
    long Start, End;
    //Get the start and end node of the thread
    M->GetStartEndNode(ThreadIndex,TreadNum,Start, End,NodeNum);
        
    //Add the coefficients to the defined functions
    for (long i=Start; i<=End; i++)
    {
        if (Start==1)
        {
            M->PrintLoading(i-Start,End-Start,"Node Derivative Loading: ", false);
        }
        
        //Get the initial matrix C1
        MatrixXd C1=C_Matrix(M,&Nodes[i]);
        MatrixXd C2;
        MatrixXd C3;
        
        //Get the modified matrices C1, C2 and C3 in case of any enrichment
        if (M->SingularEnrichment != "")
        {
            GetModifiedMatricesC(M,i,C1,C2,C3);
        }
        
        //Fill the matrix K for each particle of the support
        
        int col=0;
        for (long j=0; j<Nodes[i].SupSize; j++)
        {
            
            if (Nodes[i].CollocationPointOffset==true || Nodes[i].CollocDOF==false)
            {
                col=j;
            }
            else
            {
                if (j<1)
                    col=j;
                else if (j>=1)
                    col=j+1;
            }
            int LastRow=lTayl-1;
            if (Nodes[i].CollocationPointOffset==true || Nodes[i].CollocDOF==false)
            {
                LastRow=lTayl;
            }
            int CoeffIndex=0;
            for (int jj=0; jj<LastRow; jj++)
            {
                CoeffIndex=jj;
                if (LastRow==lTayl && jj==0)
                    CoeffIndex=lTayl-1;
                else if (LastRow==lTayl)
                    CoeffIndex=jj-1;
                
                Nodes[i].Coeff[j][CoeffIndex]=C1(jj,col);
                if (M->SingularEnrichment != "" )
                {
                    Nodes[i].Coeff2[j][CoeffIndex]=C2(jj,col);
                }
            }
        }
    }
}

void GFD_Problem::LoadVectors(Model* M,double* F_Vec,T* TripStress,T* TripStrain, vector<vector<Trow>>& StiffRows, vector<vector<Trow>>& OpRows, vector<vector<Trow>>& StiffRowsLagrange, 
        int ThreadIndex, int TreadNum)
{
    long Start, End;
    
    //Get the start and end node of the thread
    M->GetStartEndNode(ThreadIndex,TreadNum,Start,End,M->NodeNumber);
    
    long StressMatInc=TripStressTreadStartInc[ThreadIndex];
    
    //Resize the StiffRows vector to the number of rows
    StiffRows[ThreadIndex].resize((End-Start+1)*Dimension);
    if (M->AMG_Operator=="NOBC")
    {
        OpRows[ThreadIndex].resize((End-Start+1)*Dimension);
    }
    
    //Count the number of boundary rows if Lagrange multipliers are used
    long CountBCRows=0;
    if (M->BCTreatType=="LAGRANGE")
    {
        for (long i=Start; i<=End; i++)
        {
            if (M->Nodes[i].BoundaryNode==true)
            {
                CountBCRows+=Dimension;
            }
        }
        StiffRowsLagrange[ThreadIndex].resize(CountBCRows);
    }
    
    CountBCRows=0;
    long StiffRowsLagrangeRow=0;
    for (long i=Start; i<=End; i++)
    {
        int SupSize;
        long LocNodeNum;
        
        //Fill the matrix K for each particle of the suppot
        SupSize=M->Nodes[i].SupSize;
        int IndexOffset=1;
        if (M->Nodes[i].CollocationPointOffset==true || M->SingularEnrichment != "")
        {
            IndexOffset=0;
        }
        
        //Resize the current row of the StiffRows vector to the number of triplets
        if (M->Nodes[i].BoundaryNode==false || M->BCTreatType=="LAGRANGE" || M->InterpBC==true)
        {
            for (int j=0; j<Dimension; j++)
            {
                StiffRows[ThreadIndex][Dimension*(i-Start)+j].Trip.resize(SupSize*Dimension);
            }
        }
        if (M->Nodes[i].BoundaryNode==true && M->InterpBC==false)
        {
            for (int j=0; j<Dimension; j++)
            {
                int TripSize=0;
                if (M->Nodes[i].BC_D_Bool[j]==true)
                {
                    TripSize=Dimension;
                }
                else if (M->Nodes[i].BC_N_Bool[j]==true)
                {
                    TripSize=SupSize*Dimension;
                }
                if (M->BCTreatType=="LAGRANGE")
                {
                    StiffRowsLagrange[ThreadIndex][CountBCRows].Trip.resize(TripSize);
                    CountBCRows++;
                }
                else
                {
                    StiffRows[ThreadIndex][Dimension*(i-Start)+j].Trip.resize(TripSize);
                }
            }
        }
        
        //Resize the operator triplet
        if (M->AMG_Operator=="NOBC")
        {
            for (int j=0; j<Dimension; j++)
            {
                OpRows[ThreadIndex][Dimension*(i-Start)+j].Trip.resize(SupSize*Dimension);
            }
        }
        
        for (long j=0; j<SupSize; j++)
        {
            VectorXd Coeff(1);
            Coeff.resize(lTayl-IndexOffset);
            VectorXd Coeff2(1);
            Coeff2.resize(lTayl-IndexOffset);
            for (int jj=0; jj<lTayl-IndexOffset; jj++)
            {
                Coeff(jj)=M->Nodes[i].Coeff[j][jj];
                if (M->SingularEnrichment != "")
                    Coeff2(jj)=M->Nodes[i].Coeff2[j][jj];
                else
                    Coeff2(jj)=Coeff(jj);
            }
            //Get the number of the support or reference node
            if (j==0)
            {
                LocNodeNum=i;
            }
            else
            {
                LocNodeNum=M->Nodes[i].SupNodes[j-1];
            }
            LocNodeNum=M->Nodes[i].SupNodes[j];
            
            //Load the stress and strain triplet vectors
            if (Dimension==2)
            {
                LoadRows2D_Stress(Coeff,Coeff2,i,LocNodeNum,j,SupSize,TripStress,StressMatInc);
                if (M->PrintStrain==true || M->ErrEstimator==true)
                {
                    LoadRows2D_Strain(Coeff,Coeff2,i,LocNodeNum,j,SupSize,TripStrain,StressMatInc);
                }
            }
            else if (Dimension==3)
            {
                LoadRows3D_Stress(Coeff,i,LocNodeNum,j,SupSize,TripStress,StressMatInc);
            }
            
            //Load the stiffness vectors
            for (int k=0; k<Dimension; k++)
            {
                if (Dimension==2)
                {
                    LoadRows2D_Stiff(M,Coeff,Coeff2,i,LocNodeNum,j,SupSize,F_Vec,StiffRows,OpRows,StiffRowsLagrange,ThreadIndex,i-Start,k,StiffRowsLagrangeRow+k);
                }
                else if (Dimension==3)
                {
                    LoadRows3D_Stiff(M,Coeff,i,LocNodeNum,j,SupSize,F_Vec,StiffRows,OpRows,StiffRowsLagrange,ThreadIndex,i-Start,k,StiffRowsLagrangeRow+k);
                }
            }
        }
        if (M->Nodes[i].BoundaryNode==true)
        {
            StiffRowsLagrangeRow+=Dimension;
        }
        StressMatInc+=SupSize*(6+9*(Dimension-2));
    }
    
    NormalizeTriplets(M,F_Vec,StiffRows,ThreadIndex,TreadNum);
}

void GFD_Problem::LoadRows2D_Stress(VectorXd Coeff,VectorXd Coeff2,long i,long LocNodeNum,int j, int SupSize,T* TripStress, long& StressMatInc)
{
    //Load the triplet vectors - Method GFD
    double TempVal1,TempVal2,TempVal3,TempVal4,TempVal5,TempVal6;
    //Stress Matrix - Equation 1 - Sigma11
    TempVal1=d1*Coeff(0);
    TempVal2=d2*Coeff2(1);
    TripStress[StressMatInc+SupSize*Dimension*0+j*Dimension+0]=T((i-1)*(Dimension+1)+0,(LocNodeNum-1)*Dimension+0,TempVal1);
    TripStress[StressMatInc+SupSize*Dimension*0+j*Dimension+1]=T((i-1)*(Dimension+1)+0,(LocNodeNum-1)*Dimension+1,TempVal2);
    //Stress Matrix - Equation 2 - Sigma12
    TempVal3=d3/2*Coeff(1);
    TempVal4=d3/2*Coeff2(0);
    TripStress[StressMatInc+SupSize*Dimension*1+j*Dimension+0]=T((i-1)*(Dimension+1)+1,(LocNodeNum-1)*Dimension+0,TempVal3);
    TripStress[StressMatInc+SupSize*Dimension*1+j*Dimension+1]=T((i-1)*(Dimension+1)+1,(LocNodeNum-1)*Dimension+1,TempVal4);
    //Stress Matrix - Equation 3 - Sigma22
    TempVal5=d2*Coeff(0);
    TempVal6=d1*Coeff2(1);
    TripStress[StressMatInc+SupSize*Dimension*2+j*Dimension+0]=T((i-1)*(Dimension+1)+2,(LocNodeNum-1)*Dimension+0,TempVal5);
    TripStress[StressMatInc+SupSize*Dimension*2+j*Dimension+1]=T((i-1)*(Dimension+1)+2,(LocNodeNum-1)*Dimension+1,TempVal6);
}

void GFD_Problem::LoadRows3D_Stress(VectorXd Coeff,long i,long LocNodeNum,int j, int SupSize,T* TripStress, long& StressMatInc)
{
    //Load the triplet vectors - Method GFD
    //Stress Matrix - Sigma11
    double TempVal11_1,TempVal11_2,TempVal11_3;
    TempVal11_1=d1*Coeff(0);
    TempVal11_2=d2*Coeff(1);
    TempVal11_3=d2*Coeff(2);
    TripStress[StressMatInc+SupSize*Dimension*0+j*Dimension+0]=T((i-1)*6+0,(LocNodeNum-1)*Dimension+0,TempVal11_1);
    TripStress[StressMatInc+SupSize*Dimension*0+j*Dimension+1]=T((i-1)*6+0,(LocNodeNum-1)*Dimension+1,TempVal11_2);
    TripStress[StressMatInc+SupSize*Dimension*0+j*Dimension+2]=T((i-1)*6+0,(LocNodeNum-1)*Dimension+2,TempVal11_3);
    //Stress Matrix - Sigma22
    double TempVal22_1,TempVal22_2,TempVal22_3;
    TempVal22_1=d2*Coeff(0);
    TempVal22_2=d1*Coeff(1);
    TempVal22_3=d2*Coeff(2);
    TripStress[StressMatInc+SupSize*Dimension*1+j*Dimension+0]=T((i-1)*6+1,(LocNodeNum-1)*Dimension+0,TempVal22_1);
    TripStress[StressMatInc+SupSize*Dimension*1+j*Dimension+1]=T((i-1)*6+1,(LocNodeNum-1)*Dimension+1,TempVal22_2);
    TripStress[StressMatInc+SupSize*Dimension*1+j*Dimension+2]=T((i-1)*6+1,(LocNodeNum-1)*Dimension+2,TempVal22_3);
    //Stress Matrix - Sigma33
    double TempVal33_1,TempVal33_2,TempVal33_3;
    TempVal33_1=d2*Coeff(0);
    TempVal33_2=d2*Coeff(1);
    TempVal33_3=d1*Coeff(2);
    TripStress[StressMatInc+SupSize*Dimension*2+j*Dimension+0]=T((i-1)*6+2,(LocNodeNum-1)*Dimension+0,TempVal33_1);
    TripStress[StressMatInc+SupSize*Dimension*2+j*Dimension+1]=T((i-1)*6+2,(LocNodeNum-1)*Dimension+1,TempVal33_2);
    TripStress[StressMatInc+SupSize*Dimension*2+j*Dimension+2]=T((i-1)*6+2,(LocNodeNum-1)*Dimension+2,TempVal33_3);
    //Stress Matrix - Sigma12
    double TempVal12_1,TempVal12_2;
    TempVal12_1=d3/2*Coeff(1);
    TempVal12_2=d3/2*Coeff(0);
    TripStress[StressMatInc+SupSize*Dimension*3+SupSize*2*0+j*2+0]=T((i-1)*6+3,(LocNodeNum-1)*Dimension+0,TempVal12_1);
    TripStress[StressMatInc+SupSize*Dimension*3+SupSize*2*0+j*2+1]=T((i-1)*6+3,(LocNodeNum-1)*Dimension+1,TempVal12_2);
    //Stress Matrix - Sigma13
    double TempVal13_1,TempVal13_3;
    TempVal13_1=d3/2*Coeff(2);
    TempVal13_3=d3/2*Coeff(0);
    TripStress[StressMatInc+SupSize*Dimension*3+SupSize*2*1+j*2+0]=T((i-1)*6+4,(LocNodeNum-1)*Dimension+0,TempVal13_1);
    TripStress[StressMatInc+SupSize*Dimension*3+SupSize*2*1+j*2+1]=T((i-1)*6+4,(LocNodeNum-1)*Dimension+2,TempVal13_3);
    //Stress Matrix - Sigma23
    double TempVal23_2,TempVal23_3;
    TempVal23_2=d3/2*Coeff(2);
    TempVal23_3=d3/2*Coeff(1);
    TripStress[StressMatInc+SupSize*Dimension*3+SupSize*2*2+j*2+0]=T((i-1)*6+5,(LocNodeNum-1)*Dimension+1,TempVal23_2);
    TripStress[StressMatInc+SupSize*Dimension*3+SupSize*2*2+j*2+1]=T((i-1)*6+5,(LocNodeNum-1)*Dimension+2,TempVal23_3);
}

void GFD_Problem::LoadRows2D_Strain(VectorXd Coeff,VectorXd Coeff2,long i,long LocNodeNum,int j, int SupSize,T* TripStrain, long& StressMatInc)
{
    //Load the triplet vectors - Method GFD
    double TempVal1,TempVal2,TempVal3,TempVal4,TempVal5,TempVal6;
    //Strain Matrix - Epsilon11
    TempVal1=Coeff(0);
    TempVal2=0;
    TripStrain[StressMatInc+SupSize*Dimension*0+j*Dimension+0]=T((i-1)*(Dimension+1)+0,(LocNodeNum-1)*Dimension+0,TempVal1);
    TripStrain[StressMatInc+SupSize*Dimension*0+j*Dimension+1]=T((i-1)*(Dimension+1)+0,(LocNodeNum-1)*Dimension+1,TempVal2);
    //Strain Matrix - Epsilon12
    TempVal3=Coeff(1)/2;
    TempVal4=Coeff2(0)/2;
    TripStrain[StressMatInc+SupSize*Dimension*1+j*Dimension+0]=T((i-1)*(Dimension+1)+1,(LocNodeNum-1)*Dimension+0,TempVal3);
    TripStrain[StressMatInc+SupSize*Dimension*1+j*Dimension+1]=T((i-1)*(Dimension+1)+1,(LocNodeNum-1)*Dimension+1,TempVal4);
    //Strain Matrix - Epsilon22
    TempVal5=0;
    TempVal6=Coeff2(1);
    TripStrain[StressMatInc+SupSize*Dimension*2+j*Dimension+0]=T((i-1)*(Dimension+1)+2,(LocNodeNum-1)*Dimension+0,TempVal5);
    TripStrain[StressMatInc+SupSize*Dimension*2+j*Dimension+1]=T((i-1)*(Dimension+1)+2,(LocNodeNum-1)*Dimension+1,TempVal6);
}

void GFD_Problem::LoadRows2D_Stiff(Model* M,VectorXd Coeff,VectorXd Coeff2,long i,long LocNodeNum,int j, int SupSize, double* F_Vec, vector<vector<Trow>>& StiffRows, vector<vector<Trow>>& OpRows, vector<vector<Trow>>& StiffRowsLagrange, int ThreadIndex, long ThrRowIndex, int Dim, long StiffRowsLagrangeRow)
{    
    double TempValU_1,TempValU_2;
    //Compute the equilibrium equation
    if (M->Nodes[i].BoundaryNode==false || M->Stabilization==true || M->BCTreatType=="LAGRANGE" || M->AMG_Operator=="NOBC")
    {
        //Equilibrium - Equation 1
        if (Dim==0)
        {
            TempValU_1=d1*Coeff(2)+d3/2*Coeff(4);
            TempValU_2=(d2+d3/2)*Coeff2(3);
        }
        //Equilibrium - Equation 2
        else if (Dim==1)
        {
            TempValU_1=(d2+d3/2)*Coeff(3);
            TempValU_2=d1*Coeff2(4)+d3/2*Coeff2(2);
        }
    }
    //If no boundary condition is applied to the node
    if (M->Nodes[i].BoundaryNode==false || M->BCTreatType=="LAGRANGE")
    {
        StiffRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+0]=T((i-1)*Dimension+Dim,(LocNodeNum-1)*Dimension+0,TempValU_1);
        StiffRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+1]=T((i-1)*Dimension+Dim,(LocNodeNum-1)*Dimension+1,TempValU_2);
        //Set the value of the force vector
        if (j==0)
        {
            F_Vec[(i-1)*Dimension+Dim]=0;
        }
    }
    if (M->AMG_Operator=="NOBC")
    {
        OpRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+0]=T((i-1)*Dimension+Dim,(LocNodeNum-1)*Dimension+0,TempValU_1);
        OpRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+1]=T((i-1)*Dimension+Dim,(LocNodeNum-1)*Dimension+1,TempValU_2);
    }
    if (M->Nodes[i].BoundaryNode==true)
    {
        //Determine the row at which the equation is written
        long EqRow=(i-1)*Dimension+Dim;
        if (M->BCTreatType=="LAGRANGE")
        {
            EqRow=M->NodeNumber*M->Dimension+StiffRowsLagrangeRow;
        }
        
        T TripRow1=T(0,0,0);
        T TripRow2=T(0,0,0);
        
        //Load the triplet vectors - Method GFD
        double TempVal11_1,TempVal11_2,TempVal12_1,TempVal12_2,TempVal22_1,TempVal22_2;
        //Stress Matrix - Equation 1 - Sigma11
        TempVal11_1=d1*Coeff(0);
        TempVal11_2=d2*Coeff2(1);
        //Stress Matrix - Equation 2 - Sigma12
        TempVal12_1=d3/2*Coeff(1);
        TempVal12_2=d3/2*Coeff2(0);
        //Stress Matrix - Equation 3 - Sigma22
        TempVal22_1=d2*Coeff(0);
        TempVal22_2=d1*Coeff2(1);
        
        //Set the Dirichlet BC if any
        if (M->Nodes[i].BC_D_Bool[Dim]==true && M->InterpBC==true)
        {
            if (M->Nodes[i].LocalBoundaryCondition[Dim]==false)
            {
                vector<double> ValBC{0,0};
                ValBC[Dim]=Coeff(5);
                TripRow1=T(EqRow,(LocNodeNum-1)*Dimension+0,ValBC[0]);
                TripRow2=T(EqRow,(LocNodeNum-1)*Dimension+1,ValBC[1]);
                StiffRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+0]=TripRow1;
                StiffRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+1]=TripRow2;
            }
        }
        else if (M->Nodes[i].BC_D_Bool[Dim]==true && j==0)
        {
            //For local boundary conditions add the normal terms to the boundary conditions
            if (M->Nodes[i].LocalBoundaryCondition[Dim]==true)
            {
                //The boundary is in the direction of the normal
                if (M->Nodes[i].BC_DOF[Dim]==0)
                {
                    TripRow1=T(EqRow,(i-1)*Dimension+0,-M->Nodes[i].Normal[Dim][0]);
                    TripRow2=T(EqRow,(i-1)*Dimension+1,-M->Nodes[i].Normal[Dim][1]);
                }
                //The boundary is orthogonal to the normal direction
                else if (M->Nodes[i].BC_DOF[Dim]==1)
                {
                    TripRow1=T(EqRow,(i-1)*Dimension+0,+M->Nodes[i].Normal[Dim][1]);
                    TripRow2=T(EqRow,(i-1)*Dimension+1,-M->Nodes[i].Normal[Dim][0]);
                }
            }
            else
            {
                vector<double> ValBC{0,0};
                ValBC[Dim]=1;
                TripRow1=T(EqRow,(i-1)*Dimension+0,ValBC[0]);
                TripRow2=T(EqRow,(i-1)*Dimension+1,ValBC[1]);
            }
            
            //Fill the triplets vectors
            if (M->BCTreatType=="LAGRANGE")
            {
                StiffRowsLagrange[ThreadIndex][StiffRowsLagrangeRow].Trip[0]=TripRow1;
                StiffRowsLagrange[ThreadIndex][StiffRowsLagrangeRow].Trip[1]=TripRow2;
            }
            else
            {
                StiffRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[0]=TripRow1;
                StiffRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[1]=TripRow2;
            }
        }
        if (M->Nodes[i].BC_D_Bool[Dim]==true && j==0)
        {
            if (M->SingularEnrichment != "" && M->Nodes[i].EnrichedNode==true)
                F_Vec[EqRow]=(M->Nodes[i].BC_D[Dim])/M->Nodes[i].U1[Dim];
            else
                F_Vec[EqRow]=M->Nodes[i].BC_D[Dim];
        }
        //Set the Neumann BC if no Dirichlet BC is present for the considered DOF
        else if (M->Nodes[i].BC_N_Bool[Dim]==true && M->Nodes[i].BC_D_Bool[Dim]==false)
        {
            double theta=0;
            if (M->Nodes[i].LocalBoundaryCondition[Dim]==true)
            {
                theta=Angle(M,i,Dim,-1);
            }
            if (M->Nodes[i].BC_DOF[Dim]==0)
            {
                double TempVal7,TempVal8;
                if (M->Nodes[i].LocalBoundaryCondition[Dim]==true)
                {
                    double TempVal11_1_loc=TempVal11_1*pow(cos(theta),2)+TempVal12_1*sin(2*theta)+TempVal22_1*pow(sin(theta),2);
                    double TempVal11_2_loc=TempVal11_2*pow(cos(theta),2)+TempVal12_2*sin(2*theta)+TempVal22_2*pow(sin(theta),2);
                    double TempVal12_1_loc=-0.5*TempVal11_1*sin(2*theta)+TempVal12_1*cos(2*theta)+0.5*TempVal22_1*sin(2*theta);
                    double TempVal12_2_loc=-0.5*TempVal11_2*sin(2*theta)+TempVal12_2*cos(2*theta)+0.5*TempVal22_2*sin(2*theta);
                    TempVal7=-TempVal11_1_loc*1 + TempVal12_1_loc*0;
                    TempVal8=-TempVal11_2_loc*1 + TempVal12_2_loc*0;
                }
                else
                {
                    TempVal7=TempVal11_1*M->Nodes[i].Normal[Dim][0] + TempVal12_1*M->Nodes[i].Normal[Dim][1];
                    TempVal8=TempVal11_2*M->Nodes[i].Normal[Dim][0] + TempVal12_2*M->Nodes[i].Normal[Dim][1];
                }
                if ((M->PartialStab==false && M->Stabilization==true) || (M->PartialStab==true && M->Nodes[i].BC_N[0] + M->Nodes[i].BC_N[1] == 0))
                {
                    TempVal7=TempVal7-0.5*M->Nodes[i].CharactLength*M->Nodes[i].ProdN*TempValU_1;
                    TempVal8=TempVal8-0.5*M->Nodes[i].CharactLength*M->Nodes[i].ProdN*TempValU_2;
                }
                TripRow1=T(EqRow,(LocNodeNum-1)*Dimension+0,TempVal7);
                TripRow2=T(EqRow,(LocNodeNum-1)*Dimension+1,TempVal8);
            }
            else if (M->Nodes[i].BC_DOF[Dim]==1)
            {
                double TempVal7,TempVal8;
                if (M->Nodes[i].LocalBoundaryCondition[Dim]==true)
                {
                    double TempVal12_1_loc=-0.5*TempVal11_1*sin(2*theta)+TempVal12_1*cos(2*theta)+0.5*TempVal22_1*sin(2*theta);
                    double TempVal12_2_loc=-0.5*TempVal11_2*sin(2*theta)+TempVal12_2*cos(2*theta)+0.5*TempVal22_2*sin(2*theta);
                    double TempVal22_1_loc=TempVal11_1*pow(sin(theta),2)-TempVal12_1*sin(2*theta)+TempVal22_1*pow(cos(theta),2);
                    double TempVal22_2_loc=TempVal11_2*pow(sin(theta),2)-TempVal12_2*sin(2*theta)+TempVal22_2*pow(cos(theta),2);
                    TempVal7=-TempVal12_1_loc*1 + TempVal22_1_loc*0;
                    TempVal8=-TempVal12_2_loc*1 + TempVal22_2_loc*0;
                }
                else
                {
                    TempVal7=TempVal12_1*M->Nodes[i].Normal[Dim][0] + TempVal22_1*M->Nodes[i].Normal[Dim][1];
                    TempVal8=TempVal12_2*M->Nodes[i].Normal[Dim][0] + TempVal22_2*M->Nodes[i].Normal[Dim][1];
                }
                if ((M->PartialStab==false && M->Stabilization==true) || (M->PartialStab==true && M->Nodes[i].BC_N[0] + M->Nodes[i].BC_N[1] == 0))
                {
                    TempVal7=TempVal7-0.5*M->Nodes[i].CharactLength*M->Nodes[i].ProdN*TempValU_1;
                    TempVal8=TempVal8-0.5*M->Nodes[i].CharactLength*M->Nodes[i].ProdN*TempValU_2;
                }
                TripRow1=T(EqRow,(LocNodeNum-1)*Dimension+0,TempVal7);
                TripRow2=T(EqRow,(LocNodeNum-1)*Dimension+1,TempVal8);
            }
            
            //Fill the triplets vectors
            if (M->BCTreatType=="LAGRANGE")
            {
                StiffRowsLagrange[ThreadIndex][StiffRowsLagrangeRow].Trip[j*Dimension+0]=TripRow1;
                StiffRowsLagrange[ThreadIndex][StiffRowsLagrangeRow].Trip[j*Dimension+1]=TripRow2;
            }
            else
            {
                StiffRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+0]=TripRow1;
                StiffRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+1]=TripRow2;
            }
            if (j==0)
            {
                if (M->Nodes[i].LocalBoundaryCondition[Dim]==true)
                {
                    F_Vec[EqRow]=M->Nodes[i].BC_N[Dim];
                }
                else
                {
                    F_Vec[EqRow]=-M->Nodes[i].BC_N[Dim];
                }
            }
        }
    }
}

void GFD_Problem::LoadRows3D_Stiff(Model* M,VectorXd Coeff,long i,long LocNodeNum,int j, int SupSize, double* F_Vec, vector<vector<Trow>>& StiffRows, vector<vector<Trow>>& OpRows, vector<vector<Trow>>& StiffRowsLagrange, int ThreadIndex, long ThrRowIndex, int Dim, long StiffRowsLagrangeRow)
{
    //If no boundary condition is applied to the node
    double TempVal1,TempVal2,TempVal3;
    if (M->Nodes[i].BoundaryNode==false || M->AMG_Operator=="NOBC")
    {
        //Equilibrium - Equation 1
        if (Dim==0)
        {
            TempVal1=d1*Coeff(3)+d3/2*(Coeff(6)+Coeff(8));
            TempVal2=(d2+d3/2)*Coeff(4);
            TempVal3=(d2+d3/2)*Coeff(5);
        }
        //Equilibrium - Equation 2
        else if (Dim==1)
        {
            TempVal1=(d2+d3/2)*Coeff(4);
            TempVal2=d1*Coeff(6)+d3/2*(Coeff(3)+Coeff(8));
            TempVal3=(d2+d3/2)*Coeff(7);
        }
        //Equilibrium - Equation 3
        else if (Dim==2)
        {
            TempVal1=(d2+d3/2)*Coeff(5);
            TempVal2=(d2+d3/2)*Coeff(7);
            TempVal3=d1*Coeff(8)+d3/2*(Coeff(3)+Coeff(6));
        }
    }
    if (M->Nodes[i].BoundaryNode==false)
    {
        StiffRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+0]=T((i-1)*Dimension+Dim,(LocNodeNum-1)*Dimension+0,TempVal1);
        StiffRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+1]=T((i-1)*Dimension+Dim,(LocNodeNum-1)*Dimension+1,TempVal2);
        StiffRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+2]=T((i-1)*Dimension+Dim,(LocNodeNum-1)*Dimension+2,TempVal3);
        
        //Set the value of the force vector
        if (j==0)
        {
            F_Vec[(i-1)*Dimension+0]=0;
            F_Vec[(i-1)*Dimension+1]=0;
            F_Vec[(i-1)*Dimension+2]=0;
        }
    }
    if (M->AMG_Operator=="NOBC")
    {
        OpRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+0]=T((i-1)*Dimension+Dim,(LocNodeNum-1)*Dimension+0,TempVal1);
        OpRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+1]=T((i-1)*Dimension+Dim,(LocNodeNum-1)*Dimension+1,TempVal2);
        OpRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+2]=T((i-1)*Dimension+Dim,(LocNodeNum-1)*Dimension+2,TempVal3);
    }
    if (M->Nodes[i].BoundaryNode==true)
    {
        //Load the triplet vectors - Method GFD
        //Stress Matrix - Sigma11
        double TempVal11_1,TempVal11_2,TempVal11_3;
        TempVal11_1=d1*Coeff(0);
        TempVal11_2=d2*Coeff(1);
        TempVal11_3=d2*Coeff(2);
        //Stress Matrix - Sigma22
        double TempVal22_1,TempVal22_2,TempVal22_3;
        TempVal22_1=d2*Coeff(0);
        TempVal22_2=d1*Coeff(1);
        TempVal22_3=d2*Coeff(2);
        //Stress Matrix - Sigma33
        double TempVal33_1,TempVal33_2,TempVal33_3;
        TempVal33_1=d2*Coeff(0);
        TempVal33_2=d2*Coeff(1);
        TempVal33_3=d1*Coeff(2);
        //Stress Matrix - Sigma12
        double TempVal12_1,TempVal12_2;
        double TempVal12_3=0;
        TempVal12_1=d3/2*Coeff(1);
        TempVal12_2=d3/2*Coeff(0);
        //Stress Matrix - Sigma13
        double TempVal13_1,TempVal13_3;
        double TempVal13_2=0;
        TempVal13_1=d3/2*Coeff(2);
        TempVal13_3=d3/2*Coeff(0);
        //Stress Matrix - Sigma23
        double TempVal23_2,TempVal23_3;
        double TempVal23_1=0;
        TempVal23_2=d3/2*Coeff(2);
        TempVal23_3=d3/2*Coeff(1);
        
        double* Normal=NULL;
        Normal=new double[Dimension];
        double TempLocalValue=0;
        
        //Set the Dirichlet BC if any
        if (M->Nodes[i].BC_D_Bool[Dim]==true && j==0)
        {
            //Get the local normal associated with the boundary direction
            Get3DLocalNormals(M->Nodes[i].Normal[Dim],M->Nodes[i].BC_DOF[Dim],Normal);
            //For local boundary conditions add the normal terms to the boundary conditions
            if (M->Nodes[i].LocalBoundaryCondition[Dim]==true)
            {
                //The boundary is in the direction of the normal
                StiffRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+0]=T((i-1)*Dimension+Dim,(i-1)*Dimension+0,-M->Nodes[i].Normal[Dim][0]);
                StiffRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+1]=T((i-1)*Dimension+Dim,(i-1)*Dimension+1,-M->Nodes[i].Normal[Dim][1]);
                StiffRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+2]=T((i-1)*Dimension+Dim,(i-1)*Dimension+2,-M->Nodes[i].Normal[Dim][2]);
//                StiffRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+0]=T((i-1)*Dimension+Dim,(i-1)*Dimension+0, Normal[0]);
//                StiffRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+1]=T((i-1)*Dimension+Dim,(i-1)*Dimension+1, Normal[1]);
//                StiffRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+2]=T((i-1)*Dimension+Dim,(i-1)*Dimension+2, Normal[2]);
            }
            else
            {
                vector<double> ValBC{0,0,0};
                ValBC[Dim]=1;
                StiffRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+0]=T((i-1)*Dimension+Dim,(i-1)*Dimension+0,ValBC[0]);
                StiffRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+1]=T((i-1)*Dimension+Dim,(i-1)*Dimension+1,ValBC[1]);
                StiffRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+2]=T((i-1)*Dimension+Dim,(i-1)*Dimension+2,ValBC[2]);
            }
            F_Vec[(i-1)*Dimension+Dim]=M->Nodes[i].BC_D[Dim];
        }
        //Set the Neumann BC if no Dirichlet BC is present for the considered DOF
        else if (M->Nodes[i].BC_N_Bool[Dim]==true && M->Nodes[i].BC_D_Bool[Dim]==false)
        {
            double TempVal1,TempVal2,TempVal3;
            //Get the TempVal for the local boundary conditions
            if (M->Nodes[i].LocalBoundaryCondition[Dim]==true)
            {
                Get3DRotatedTerms(M->Nodes[i].Normal[Dim],M->Nodes[i].BC_DOF[Dim],TempLocalValue,TempVal11_1,TempVal12_1,TempVal13_1,TempVal22_1,TempVal23_1,TempVal33_1,M->Nodes[i].BC_DOF[Dim]);
                TempVal1=TempLocalValue;
                Get3DRotatedTerms(M->Nodes[i].Normal[Dim],M->Nodes[i].BC_DOF[Dim],TempLocalValue,TempVal11_2,TempVal12_2,TempVal13_2,TempVal22_2,TempVal23_2,TempVal33_2,M->Nodes[i].BC_DOF[Dim]);
                TempVal2=TempLocalValue;
                Get3DRotatedTerms(M->Nodes[i].Normal[Dim],M->Nodes[i].BC_DOF[Dim],TempLocalValue,TempVal11_3,TempVal12_3,TempVal13_3,TempVal22_3,TempVal23_3,TempVal33_3,M->Nodes[i].BC_DOF[Dim]);
                TempVal3=TempLocalValue;
            }
            if (M->Nodes[i].BC_DOF[Dim]==0)
            {
                //Neumann BC DOF1
                if (M->Nodes[i].LocalBoundaryCondition[Dim]==false)
                {
                    TempVal1=TempVal11_1*M->Nodes[i].Normal[Dim][0] + TempVal12_1*M->Nodes[i].Normal[Dim][1] + TempVal13_1*M->Nodes[i].Normal[Dim][2];
                    TempVal2=TempVal11_2*M->Nodes[i].Normal[Dim][0] + TempVal12_2*M->Nodes[i].Normal[Dim][1];
                    TempVal3=TempVal11_3*M->Nodes[i].Normal[Dim][0] +                                        TempVal13_3*M->Nodes[i].Normal[Dim][2];
                }
            }
            else if (M->Nodes[i].BC_DOF[Dim]==1)
            {
                //Neumann BC DOF2
                if (M->Nodes[i].LocalBoundaryCondition[Dim]==false)
                {
                    TempVal1=TempVal12_1*M->Nodes[i].Normal[Dim][0] + TempVal22_1*M->Nodes[i].Normal[Dim][1];
                    TempVal2=TempVal12_2*M->Nodes[i].Normal[Dim][0] + TempVal22_2*M->Nodes[i].Normal[Dim][1] + TempVal23_2*M->Nodes[i].Normal[Dim][2];
                    TempVal3=                                     + TempVal22_3*M->Nodes[i].Normal[Dim][1] + TempVal23_3*M->Nodes[i].Normal[Dim][2];
                }
            }
            else if (M->Nodes[i].BC_DOF[Dim]==2)
            {
                //Neumann BC DOF3
                if (M->Nodes[i].LocalBoundaryCondition[Dim]==false)
                {
                    TempVal1=TempVal13_1*M->Nodes[i].Normal[Dim][0]                                        + TempVal33_1*M->Nodes[i].Normal[Dim][2];
                    TempVal2=                                       TempVal23_2*M->Nodes[i].Normal[Dim][1] + TempVal33_2*M->Nodes[i].Normal[Dim][2];
                    TempVal3=TempVal13_3*M->Nodes[i].Normal[Dim][0] + TempVal23_3*M->Nodes[i].Normal[Dim][1] + TempVal33_3*M->Nodes[i].Normal[Dim][2];
                }
            }
            StiffRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+0]=T((i-1)*Dimension+Dim,(LocNodeNum-1)*Dimension+0,TempVal1);
            StiffRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+1]=T((i-1)*Dimension+Dim,(LocNodeNum-1)*Dimension+1,TempVal2);
            StiffRows[ThreadIndex][ThrRowIndex*Dimension+Dim].Trip[j*Dimension+2]=T((i-1)*Dimension+Dim,(LocNodeNum-1)*Dimension+2,TempVal3);
            if (j==0)
            {
                F_Vec[(i-1)*Dimension+Dim]=-M->Nodes[i].BC_N[Dim];
            }
        }
        
        delete[] Normal;
    }
}

void GFD_Problem::Get3DLocalNormals(double* n1, int Dim, double* LocNormal)
{
    if (n1[1]==0 && n1[2]==0)
    {
        if (Dim==0)
        {
            LocNormal[0]=n1[0];
            LocNormal[1]=n1[1];
            LocNormal[2]=n1[2];
        }
        else if (Dim==1)
        {
            LocNormal[0]=0;
            LocNormal[1]=1;
            LocNormal[2]=0;
            if (n1[0]<0)
                LocNormal[1]=-1;
        }
        else if (Dim==2)
        {
            LocNormal[0]=0;
            LocNormal[1]=0;
            LocNormal[2]=1;
        }
    }
    else if (Dim==0)
    {
        LocNormal[0]=n1[0];
        LocNormal[1]=n1[1];
        LocNormal[2]=n1[2];
    }
    else if (Dim==1 || Dim==2)
    {
        //Gram Schmidt Process with U1 = N and U2= (1,0,0)
        double ScalarProd=pow(n1[0],2)+pow(n1[1],2)+pow(n1[2],2);
        double n20=1-1*n1[0]/ScalarProd*n1[0];
        double n21=-1*n1[0]/ScalarProd*n1[1];
        double n22=-1*n1[0]/ScalarProd*n1[2];
        if (Dim==1)
        {
            LocNormal[0]=n20;
            LocNormal[1]=n21;
            LocNormal[2]=n22;
        }
        else if (Dim==2)
        {
            LocNormal[0]=n1[1]*n22-n1[2]*n21;
            LocNormal[1]=n1[2]*n20-n1[0]*n22;
            LocNormal[2]=n1[0]*n21-n1[1]*n20;
        }
    }
    double Norm=0;
    for (int i=0; i<3; i++)
    {
        Norm+=pow(LocNormal[i],2);
    }
    Norm=pow(Norm,0.5);
    for (int i=0; i<3; i++)
    {
        LocNormal[i]=LocNormal[i]/Norm;
    }
}

void GFD_Problem::Get3DRotatedTerms(double* n1, int Dim, double& TempVal, double S11, double S12, double S13, double S22, double S23, double S33, int DOFDim)
{
    MatrixXd R(1,1);
    R.resize(3,3);
    MatrixXd S(1,1);
    S.resize(3,3);
    S(0,0)=S11;S(0,1)=S12;S(0,2)=S13;
    S(1,0)=S12;S(1,1)=S22;S(1,2)=S23;
    S(2,0)=S13;S(2,1)=S23;S(2,2)=S33;
    
    //Get the max direction of the normal
    int MaxDir=0;
    double MaxVal=abs(n1[0]);
    for (int i=1; i<3; i++)
    {
        if (abs(n1[i])>MaxVal)
        {
            MaxDir=i;
            MaxVal=abs(n1[i]);
        }
    }
    
    //Get the orthogonal vectors coordinates coordinates
    Vector3d VectTemp(0,0,0);
    Vector3d n1Vect(n1[0],n1[1],n1[2]);
    n1Vect.normalize();
    Vector3d OrtNormal1;
    Vector3d OrtNormal2;
    if (MaxDir==0)
        VectTemp(2)=1;
    else if (MaxDir==1)
        VectTemp(2)=1;
    else if (MaxDir==2)
        VectTemp(1)=1;
    OrtNormal1=n1Vect.cross(VectTemp);
    OrtNormal2=n1Vect.cross(OrtNormal1);
    
    //Load the rotation matrix R
    for (int i=0; i<3; i++)
    {
        for (int j=0; j<3; j++)
        {
            if (i==0)
            {
                R(j,i)=n1Vect[j];
            }
            else if (i==1)
            {
                R(j,i)=OrtNormal1[j];
            }
            else if (i==2)
            {
                R(j,i)=OrtNormal2[j];
            }
        }
    }
    MatrixXd S2=R.transpose()*S*R;
    TempVal=-S2(DOFDim,0);
}

MatrixXd GFD_Problem::C_Matrix(Model* M, Node* CollocNode)
{
    MatrixXd A(1,1);
    MatrixXd B(1,1);
    MatrixXd C;
    MatrixXd I(1,1);
    int SupSize=CollocNode->SupSize;
    int MatA_Size=lTayl-1;
    int StartIndex=1;
    
    if (CollocNode->CollocationPointOffset==true || CollocNode->CollocDOF==false)
    {
        MatA_Size=lTayl;
        StartIndex=0;
    }
    A.resize(MatA_Size,MatA_Size);
    B.resize(MatA_Size,SupSize+StartIndex);
    
    //Matrix for QR decomposition
    MatrixXd A2(1,1);
    MatrixXd B2(1,1);
    MatrixXd C2;
    if (M->StencilSol=="QR")
    {
        A2.resize(SupSize-1,MatA_Size);
        B2.resize(SupSize-1,SupSize);
    }
    
    //Initialize the matrices A and B;
    for (int i=0; i<MatA_Size; i++)
    {
        for (int j=0; j<MatA_Size; j++)
        {
            A(i,j)=0;
        }
        B(i,0)=0;
    }
    
    for (int i=0; i<SupSize; i++)
    {
        double z_i=0;
        long LocNodeNum=CollocNode->SupNodes[i];
                
        //Get the distance Z_i of the support node to the reference Node
        if (CollocNode->HasDiffractedSupNodes==false)
        {
            for (int m=0; m<Dimension; m++)
            {
                if (CollocNode->CollocationPointOffset==true)
                {
                    z_i+=pow(M->Nodes[LocNodeNum].X[m]-CollocNode->CollocX[m],2);
                }
                else
                {
                    z_i+=pow(M->Nodes[LocNodeNum].X[m]-CollocNode->X[m],2);
                }
            }
            z_i=pow(z_i,0.5);
        }
        else
        {
            z_i=CollocNode->DiffractedDist[i];
        }
        //Get Voronoi weight if the function is selected
        double SupNodeVol=1;
        if (M->Voronoi==true && CollocNode->BoundaryNode==false)
            SupNodeVol=CollocNode->SupNodeVol[i];
        
        //Calculate the weight associated to the considered support node
        double SupRad=CollocNode->SupRadius;
        if (CollocNode->QuadSelection==true)
            SupRad=CollocNode->SupRadA[i];
        
        double W_=pow(SupNodeVol*M->w(z_i/SupRad),2);
        if (CollocNode->QuadSelection==true)
        {
            W_=CollocNode->SupNodeW[i];
        }
        
        //Calculate the zero moments
        if (CollocNode->CollocationPointOffset==false && CollocNode->CollocDOF==true)
        {
            for (int j=StartIndex; j<lTayl; j++)
            {
                double Value=1;
                for (int m=0; m<Dimension; m++)
                {
                    Value= Value*pow(M->Nodes[LocNodeNum].X[m]-CollocNode->X[m],M->Exponents(j,m))/M->factorial(M->Exponents(j,m)); 
                }
                B(j-StartIndex,0)=B(j-StartIndex,0)-W_*Value;
            }
        }
        
        //Calculte the other moments
        for (int j=StartIndex; j<lTayl; j++)
        {
            double Value1=1;
            for (int m=0; m<Dimension; m++)
            {
                if (CollocNode->CollocationPointOffset==true)
                {
                    Value1=Value1*pow(M->Nodes[LocNodeNum].X[m]-CollocNode->CollocX[m],M->Exponents(j,m))/M->factorial(M->Exponents(j,m));
                }
                else
                {
                    Value1=Value1*pow(M->Nodes[LocNodeNum].X[m]-CollocNode->X[m],M->Exponents(j,m))/M->factorial(M->Exponents(j,m));
                }
            }
            
            //Fill the matrices A2
            if (i>0 && M->StencilSol=="QR")
            {
                A2(i-1,j-StartIndex)=W_*Value1;
            }
            
            for (int jj=j; jj<lTayl; jj++)
            {
                double Value2=1;
                for (int m=0; m<Dimension; m++)
                {
                    if (CollocNode->CollocationPointOffset==true)
                    {
                        Value2= Value2*pow(M->Nodes[LocNodeNum].X[m]-CollocNode->CollocX[m],M->Exponents(jj,m))/M->factorial(M->Exponents(jj,m)); 
                    }
                    else
                    {
                        Value2= Value2*pow(M->Nodes[LocNodeNum].X[m]-CollocNode->X[m],M->Exponents(jj,m))/M->factorial(M->Exponents(jj,m)); 
                    }
                }
                double Value=W_*Value1*Value2;
                A(j-StartIndex,jj-StartIndex)+=Value;

                if (j != jj)
                {
                    //Fill the symmetric part of the matrix A
                    A(jj-StartIndex,j-StartIndex)+=Value;
                }
                else
                {
                    //Fill the moment components
                    B(j-StartIndex,i+StartIndex)=W_*Value2;
                }
            }
        }
        
        //Fill the matrices B2
        if (i>0 && M->StencilSol=="QR")
        {
            B2(i-1,0)=-W_;
            for (int ii=1; ii<SupSize; ii++)
            {
                if (i==ii)
                    B2(i-1,ii)=W_;
                else
                    B2(i-1,ii)=0;
            }
        }
    }
    
    //Get the inverse of the matrix A2
    MatrixXd AInv=A.inverse();
    double cond=0;
    if(M->PrintConditionNumber==1)
    {
        //Get the matrix conditioning
        cond = AInv.norm()*A.norm();
        CollocNode->cond=cond;
        if (MinCond==0 && MaxCond==0)
        {
            MinCond=cond;
            MaxCond=cond;
        }
        if (cond<MinCond)
            MinCond=cond;
        if (cond>MaxCond)
            MaxCond=cond;
    }
    
    if (M->StencilSol != "QR")
    {
        C=AInv*B;
    }
    else
    {
        C.resize(MatA_Size,SupSize+1);
    }
    
    if (M->StencilSol=="QR")
    {
        //Solve the overdetermined linear system dy QR decomposition
        C2=A2.householderQr().solve(B2);
        //Fill the matrix C using the same form as the matrix calculated based on the direct method
        for (int i=0; i<SupSize; i++)
        {  
            int col=0;
            if (i>0)
                col=i+1;
            for (int j=StartIndex; j<lTayl; j++)
            {
                C(j-StartIndex,col)=C2(j-StartIndex,i);
            }
        }
    }
        
    return C;
}

void GFD_Problem::NormalizeTriplets(Model* M,double* F_Vec, vector<vector<Trow>>& StiffRows, int ThreadIndex, int TreadNum)
{
    //Get the start and end node of the thread
    long Start, End;
    M->GetStartEndNode(ThreadIndex,TreadNum,Start,End,M->NodeNumber);
    
    //Normalize each row of the matrix
    for (long i=Start; i<=End; i++)
    {
        for (int k=0; k<Dimension; k++)
        {
            //Get the sum of the row
            double Max=StiffRows[ThreadIndex][(i-Start)*Dimension+k].Trip[0].value();
            for (int j=0; j<StiffRows[ThreadIndex][(i-Start)*Dimension+k].Trip.size(); j++)
            {
                double Val=StiffRows[ThreadIndex][(i-Start)*Dimension+k].Trip[j].value();
                if (Val>Max)
                    Max=Val;
            }
            //Normalize the Force vector
//            F_Vec[(i-1)*Dimension+k]=F_Vec[(i-1)*Dimension+k]/Max;
//            //Normalize the triplets
//            for (int j=0; j<StiffRows[ThreadIndex][(i-Start)*Dimension+k].Trip.size(); j++)
//            {
//                StiffRows[ThreadIndex][(i-Start)*Dimension+k].Trip[j]=T(StiffRows[ThreadIndex][(i-Start)*Dimension+k].Trip[j].row(),StiffRows[ThreadIndex][(i-Start)*Dimension+k].Trip[j].col(),StiffRows[ThreadIndex][(i-Start)*Dimension+k].Trip[j].value()/Max);
//            }
        }
    }
}
