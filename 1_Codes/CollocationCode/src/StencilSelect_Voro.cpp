#include "StencilSelect.h"

void StencilSelect::LoadVoronoi_AllNodes(Model* M)
{
    //Loop over all nodes of the model
    for (long i=1; i<=M->NodeNumber; i++)
    {
        M->Nodes[i].FindVoroCN();
    }
    if (M->Method=="GFD_XE")
    {
        LoadUniqueVoronoiCN(M);
    }
}

void StencilSelect::LoadUniqueVoronoiCN(Model* M)
{
    //Count the number of corner nodes
    double CountAllCN=0;
    for (long i=1; i<=M->NodeNumber; i++)
    {
        CountAllCN+=M->Nodes[i].VoroCN.size();
    }
    
    //Put all the voronoi corner nodes in a tree
    vector<Point_3> points(CountAllCN);
    vector<long> indices(CountAllCN);
    vector<vector<long>> ParentNodes(CountAllCN);
    vector<bool> DuplicatedPoint(CountAllCN);
    vector<vector<long>> IndexToNode;
    IndexToNode.resize(CountAllCN);
    long count=0;
    for (long i=1; i<=M->NodeNumber; i++)
    {
        for (int j=0;j<M->Nodes[i].VoroCN.size();j++)
        {
            if (Dimension==2)
                points[count]=Point_3(M->Nodes[i].VoroCN[j][0],M->Nodes[i].VoroCN[j][1],0.0);
            else if (Dimension==3)
                points[count]=Point_3(M->Nodes[i].VoroCN[j][0],M->Nodes[i].VoroCN[j][1],M->Nodes[i].VoroCN[j][2]);
            indices[count]=count;
            DuplicatedPoint[count]=false;
            IndexToNode[count]={i,j};
            ParentNodes[count].clear();
            ParentNodes[count].push_back(i);
            count++;
        }
    }
    // Insert number_of_data_points in the tree
    Tree tree(
    boost::make_zip_iterator(boost::make_tuple(points.begin(),indices.begin())),
    boost::make_zip_iterator(boost::make_tuple(points.end(),indices.end())));
    
    //Initialize the vectors and loop variables
    count=0;
    int K=M->SupSizeIntM;
    if (K<3) {K=3;}
    vector<long> RefNodeIndex;
    RefNodeIndex.resize(CountAllCN);
    //Initialize all the ref nodes to -1
    for (long i=0; i<CountAllCN; i++)
    {
        RefNodeIndex[i]=-1;
    }
    //Identify the duplicated nodes
    for (long i=1; i<=M->NodeNumber; i++)
    {
        M->Nodes[i].VoroCN_Index.clear();
        double ParentNodeRadius=M->Nodes[i].SupRadius;
        for (int j=0;j<M->Nodes[i].VoroCN.size();j++)
        {
            if (DuplicatedPoint[count]==false)
            {
                RefNodeIndex[count]=count;
                Point_3* query=NULL;
                if (Dimension==2)
                    query=new Point_3(M->Nodes[i].VoroCN[j][0],M->Nodes[i].VoroCN[j][1],0.0);
                else
                    query=new Point_3(M->Nodes[i].VoroCN[j][0],M->Nodes[i].VoroCN[j][1],M->Nodes[i].VoroCN[j][2]);
                Distance tr_dist;
                // search K nearest neighbours
                K_neighbor_search search(tree, *query, K, 0.0,true,tr_dist,true);
                for(K_neighbor_search::iterator it = search.begin(); it != search.end(); it++)
                {
                    double Dist=tr_dist.inverse_of_transformed_distance(it->second);
                    int IndexVal=boost::get<1>(it->first);
                    if (Dist < ParentNodeRadius/100 && IndexVal != count && DuplicatedPoint[IndexVal]==false)
                    {
                        DuplicatedPoint[IndexVal]=true;
                        RefNodeIndex[IndexVal]=count;
                        long ParentNode=ParentNodes[IndexVal][0];
                        ParentNodes[count].push_back(ParentNode);
                    }
                }
            }
            count++;
        }
    }
    
    //Count the number of unique voronoi corner nodes
    long UniqueVoroCNNum=0;
    for (long i=0; i<CountAllCN; i++)
    {
        if (DuplicatedPoint[i]==false)
        {
            //Identify the nodes in the domain
            long Ni=IndexToNode[i][0];
            int Nj=IndexToNode[i][1];
            vector<double> TempVect=M->Nodes[Ni].VoroCN[Nj];
            if (InDomain(M,TempVect,false,false)==true)
            {
                UniqueVoroCNNum++;
            }
            else
            {
                DuplicatedPoint[i]=true;
            }
        }
    }
    M->CNodesNum=UniqueVoroCNNum;
    M->CNodes = new Node[UniqueVoroCNNum+1];
    count=1;
    for (long i=0; i<CountAllCN; i++)
    {
        if (DuplicatedPoint[i]==false)
        {
            long Ni=IndexToNode[i][0];
            int Nj=IndexToNode[i][1];
            double X1=M->Nodes[Ni].VoroCN[Nj][0];
            double X2=M->Nodes[Ni].VoroCN[Nj][1];
            double X3=0;
            if (M->Dimension==3)
            {
                X3=M->Nodes[Ni].VoroCN[Nj][2];
            }
            M->CNodes[count].SetNode(count, M->Dimension, X1, X2, X3);
            M->CNodes[count].ParentNodes=ParentNodes[i];
//            cout << count << ", " << X1*1000 << ", " << X2*1000 << endl;
            count++;
        }
    }
    
    //Load the vornoi CN indices of each collocation node
    for (long i=0; i< M->CNodesNum; i++)
    {
        for (int j=0; j<M->CNodes[i].ParentNodes.size(); j++)
        {
            M->Nodes[M->CNodes[i].ParentNodes[j]].VoroCN_Index.push_back(i);
        }
    }
}
