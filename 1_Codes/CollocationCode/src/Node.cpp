#include "Node.h"

Node::Node() {
}

Node::Node(const Node& orig) {
}

Node::~Node() {
    delete [] X;
    if (BoundaryNode==true)
    {
        delete [] BC_D_Bool;
        delete [] BC_N_Bool;
        delete [] BC_D;
        delete [] BC_N;
        delete [] BC_DOF;
        delete [] LocalBoundaryCondition;
        for (int i=0; i<Dimension;i++)
        {
            delete [] Normal[i];
        }
        delete [] Normal;
    }
    if (HasExactS==true)
        delete [] ExactS;
    if (HasExactU==true)
        delete [] ExactU;
    delete [] SupNodes;
}

void Node::SetNode(long Number, int Dim, double X_1, double X_2, double X_3=0)
{
    Dimension=Dim;
    X = new double[Dim];
    X[0]=X_1;
    X[1]=X_2;
    if (Dim==3) 
    {
        X[2]=X_3;
    }
    NodeNum=Number;
    BoundaryNode=false;
    BC_D_Init=false;
    BC_N_Init=false;
    AttachedElements.clear();
}

long Node::NodeNumber()
{
    return NodeNum;
}

void Node::SetBoundNode(bool IsBound, int Dim, int BCDOF, int BCIndex, string BCDOFStr, double BC_Val, double NormalX1=0, double NormalX2=0, double NormalX3=0, string prefix="", string BC_Axis="")
{
    //Nodes[NodeNum].SetBoundNode(true,Dimension,BCDOF,BCIndex,BCDOFStr,BC_Val,NX1,NX2,NX3,"D",BC_Axis);
    if (NodeInitialized==false)
    {
        BC_D_Bool = new bool[Dim];
        BC_N_Bool = new bool[Dim];
        BC_D = new double[Dim];
        BC_N = new double[Dim];
        BC_DOF = new int[Dim];
        Normal = new double*[Dim];
        LocalBoundaryCondition = new bool[Dim];

        for (int i=0; i<Dim; i++)
        {
            BC_D_Bool[i]=false;
            BC_N_Bool[i]=false;
            LocalBoundaryCondition[i]=false;
            Normal[i] = new double[Dim];
        }
        NodeInitialized=true;
    }
    if (BC_Axis=="L")
    {
        LocalBoundaryCondition[BCIndex]=true;
    }

    if (IsBound == true)
    {
        BoundaryNode=true;
        if (prefix=="D")
        {
            BC_D[BCIndex]=BC_Val;
            BC_D_Bool[BCIndex]=true;
        }
        else if (prefix=="N")
        {
            BC_N[BCIndex]=BC_Val;
            BC_N_Bool[BCIndex]=true;
        }
    }
    Normal[BCIndex][0]=NormalX1;
    Normal[BCIndex][1]=NormalX2;
    if (Dim==3)
    {
        Normal[BCIndex][2]=NormalX3;
    }
    if (BC_Axis!="L" && BCDOF==0)
    {
        for (int i=0; i<Dim; i++)
        {
            Normal[i][0]=NormalX1;
            Normal[i][1]=NormalX2;
            if (Dim==3)
            {
                Normal[i][2]=NormalX3;
            }
        }
        
    }
    BC_DOF[BCIndex]=BCDOF;
}

void Node::SetSupRadius(double RadiusValue)
{
    SupRadiusSq=RadiusValue;
    SupRadius=pow(SupRadiusSq,0.5);
}

void Node::FreeCoeff()
{
    for (int i=0; i<SupSize; i++)
    {
        delete [] Coeff[i];
    }
    delete [] Coeff;
}

void Node::FindVoroCN()
{
    //Load the coordiates of the node into a vector
    Vector3d RefNodeVect;
    RefNodeVect(2)=0;
    for (int i=0; i<Dimension; i++)
    {
        RefNodeVect(i)=X[i];
    }
    
    //Create a voronoi diagram on the support of each node identified for refinement
    //Get container dimensions
    double* MinContainer=NULL;MinContainer=new double[3];
    double* MaxContainer=NULL;MaxContainer=new double[3];

    //Initialize the min and max values
    for (int i=0; i<3; i++)
    {
        MinContainer[i]=RefNodeVect(i);
        MaxContainer[i]=RefNodeVect(i);
    }
    
    //Get the extremum coordinates of the container
    for (int i=0; i<SupSize; i++)
    {
        for (int j=0; j<Dimension; j++)
        {
            if (SupNodesX[i][j] < MinContainer[j])
                MinContainer[j] = SupNodesX[i][j];
            if (SupNodesX[i][j] > MaxContainer[j])
                MaxContainer[j] = SupNodesX[i][j];
        }
    }

    //Enlarge the dimensions of the container
    for (int j=0; j<Dimension; j++)
    {
        MinContainer[j]=MinContainer[j]-0.1*(MaxContainer[j]-MinContainer[j]);
        MaxContainer[j]=MaxContainer[j]+0.1*(MaxContainer[j]-MinContainer[j]);
    }
    //Add the 3rd dimension for 2D problems
    int n_x=10,n_y=10,n_z=10;
    if (Dimension==2)
    {
        MinContainer[2]=0;
        MaxContainer[2]=1;
    }

    // Create a container with the geometry given above, and make it
    // non-periodic in each of the three coordinates. Allocate space for
    // eight particles within each computational block
    container* con=NULL;
    con=new container(MinContainer[0],MaxContainer[0],MinContainer[1],MaxContainer[1],MinContainer[2],MaxContainer[2],n_x,n_y,n_z,false,false,false,8);

    // Import the particles to the container
    for (int i=0; i<SupSize; i++)
    {
        if (Dimension==2)
        {
            con->put(i,SupNodesX[i][0],SupNodesX[i][1],0.5);
        }
        else if (Dimension==3)
        {
            con->put(i,SupNodesX[i][0],SupNodesX[i][1],SupNodesX[i][2]);
        }
    }

    //Create a loop and loop over the particles of the container
    c_loop_all cl(*con);
    voronoicell_neighbor c;
    if (cl.start())
    {
        bool LastParticFound=false;
        while (LastParticFound==false)
        {
            //Find the nodes at the corner of the voronoi cell only for the 
            //central point of index zero as the support nodes are ordered
            //by distance from the central node
            if (cl.pid()==0)
            {
                con->compute_cell(c,cl);
                vector<double> Vect;
                vector<double> TempVect;
                TempVect.resize(3);
                double cx, cy, cz;
                cx=X[0];
                cy=X[1];
                if (Dimension==2)
                {
                    cz=0.5;
                }
                else
                {
                    cz=X[2];
                }
                c.vertices(Vect);
                VoroCN.clear();
                for (int j=0; j<Vect.size()/3; j++)
                {
                    TempVect[0]=Vect[3*j+0]+cx;
                    TempVect[1]=Vect[3*j+1]+cy;
                    TempVect[2]=Vect[3*j+2]+cz;
                    if (Dimension==3 || TempVect[2]> 0)
                    {
                        VoroCN.push_back(TempVect);
                    }
                }
                break;
            }
            if(cl.inc()==false)
            {
                LastParticFound=true;
            }
        }
    }
    
    //For each voronoi corner node, get the distance to the reference node
    VoroCN_DistToRef=new double[VoroCN.size()];
    for (int i=0; i<VoroCN.size(); i++)
    {
        double DistSq=0;
        for (int j=0; j<Dimension; j++)
        {
            DistSq+=pow(X[j]-VoroCN[i][j],2);
        }
        VoroCN_DistToRef[i]=pow(DistSq,0.5);
    }
    
    delete[] MinContainer;
    delete[] MaxContainer;  
    delete con;
}