#include "OutputFile.h"

OutputFile::OutputFile() {
}

OutputFile::OutputFile(const OutputFile& orig) {
}

OutputFile::~OutputFile() {
}
void OutputFile::PrintOutputFile(Model* M,StencilSelect* S, LinearProblem* L, ErrorEstimator* E, string Path)
{
    std::ofstream *outfile;
    outfile= new std::ofstream(Path.replace(Path.end()-3,Path.end(),"out"));
        
    PrintOutputFileHeader(M,S,L,outfile);
    PrintErrorNorms(M,L,E,outfile);
    PrintApproxSolution(M,L,outfile);
    PrintEstimExactSolution(M,L,E,outfile);
        
    outfile->close();
    delete outfile;
}

void OutputFile::PrintOutputFileHeader(Model* M,StencilSelect* S, LinearProblem* L, std::ofstream* outfile)
{
    time_t now = time(0);
    *outfile << "********************************" << endl;
    *outfile << "**     COLLOCATION v1.0.9     **" << endl;
    *outfile << "**            ----            **" << endl;
    *outfile << "**      by  T. JACQUEMIN      **" << endl;
    *outfile << "**         Legato Team        **" << endl;
    *outfile << "**  University of Luxembourg  **" << endl;
    *outfile << "********************************" << endl;
    *outfile << "" << endl;
    *outfile << "********************************" << endl;
    *outfile << "**     ANALYSIS COMPLETED     **" << endl;
    *outfile << "********************************" << endl;
    *outfile << ctime(&now) << endl;
    
    *outfile << "#PROCESSES_USED=" << M->ProcessNumber << endl;
    *outfile << "#THREAD_USED=" << M->ThreadNumber << endl;
    *outfile << "#RUNTIME=" << M->RunTime[4] << endl;
    if (M->TimeSplit==true)
    {
        *outfile << " PREP.=" << M->RunTime[0] << endl;
        *outfile << " BUIL.=" << M->RunTime[1] << endl;
        *outfile << " SOLV.=" << M->RunTime[2] << endl;
        *outfile << " PPRO.=" << M->RunTime[3] << endl;
    }
    
    *outfile << "" << endl;
    *outfile << "    PROBLEM INFORMATION:" << endl;
    *outfile << "    ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾" << endl;
    *outfile << "#ANALYSIS_TYPE=" << M->ModelType << endl;
    *outfile << "#DIMENSION=" << M->Dimension << endl;
    *outfile << "#NODE_NUMBER=" << M->NodeNumber << endl;
    *outfile << "#DOF_NUMBER=" << M->NodeNumber*M->Dimension+L->AdditionalDOFs+L->AdditionalForceVectDOFs << endl;
    
    *outfile << "" << endl;
    *outfile << "    METHOD INFORMATION:" << endl;
    *outfile << "    ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾" << endl;
    *outfile << "#METHOD=" << M->Method << endl;
    *outfile << "#APPROX_ORDER=" << M->ApproxOrder << endl;
    *outfile << "#WINDOW_TYPE=" << M->WindowFunctionType << "," << M->SupRatio << endl;
    *outfile << "#SUP_SCALING=" << S->SupScaling << endl;
    if (M->MinWeight>0)
    {
        *outfile << "#MIN_WEIGHT=" << M->MinWeight << endl;
    }
    if (S->StencilSelection!="")
    {
        *outfile << "#STENCIL_SEL=" << S->StencilSelection << endl;
    }
    else
    {
        *outfile << "#STENCIL_SEL=DIST" << endl;
    }
        
    *outfile << "#STABILIZATION=";
    if (M->Stabilization == true) {
        *outfile << "TRUE" << endl;}
    else{
        *outfile << "FALSE" << endl;}
    if (M->VisibilityType != "")
    {
        *outfile << "#VISIBILITY_TYPE=" << M->VisibilityType << endl;
        *outfile << "#VISIBILITY_THRESHOLD=" << M->VisibilityThresholdAngle << endl;
    }
    if (M->BCTreatType != "") *outfile << "#BC_TYPE=" << M->BCTreatType << endl;
    *outfile << "#REQ_SUP_SIZE=" << S->SupSizeInt <<"," << S->SupSizeBound << endl;
    *outfile << "#AVERAGE_INNER_SUP=" << GetAverageInnerNodeSupport(M) << endl;
    *outfile << "#AVERAGE_BC_SUP=" << GetAverageBCNodeSupport(M) << endl;
    
    *outfile << "" << endl;
    *outfile << "    SOLVER INFORMATION:" << endl;
    *outfile << "    ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾" << endl;
    *outfile << "#SOLVER_TYPE=" << M->SolverType << endl;
    
    //Print additional informations for the KSP solver
    if (M->SolverType=="KSP")
    {
        *outfile << "#SOLVER_SPEC=" << M->SolverPC << "," << M->SolverKSP << endl;
        if (M->SolverPC=="GAMG" || M->SolverPC=="BOOMERAMG" )
        {
            *outfile << "#PC_AMG_SPEC=" << M->AMG_Threshold;
            if (M->AMG_Levels>0) *outfile << "," << M->AMG_Levels;
            else  *outfile << ",default";
            if (M->AMG_SmoothN>0) *outfile << "," << M->AMG_SmoothN;
            else  *outfile << ",default";
            *outfile << endl;
        }
        *outfile << "#SOLVER_TOL=" << M->SolverTol << endl;
        *outfile << "#SOLVER_ADDITIONAL_INFORMATION" << endl;
        *outfile << " " << L->SolverConvergenceStatus << endl;
        *outfile << "#SOLVER_ITERATION_NUMBER=" << L->SolverIterationNum << endl;
        *outfile << "#ITERATION_STOP_REASON"  << endl;
        *outfile << " " << L->IterationStopReason_Str << endl;
    }
    
    if (M->ErrEstimator==true)
    {
        *outfile << "" << endl;
        *outfile << "    ERROR INDICATOR INFORMATION:" << endl;
        *outfile << "    ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾" << endl;
        *outfile << "#INDICATOR_TYPE=" << M->EstimatorType << endl;
        *outfile << "#INDICATOR_VAR=" << M->EstimVar << endl;
        if (M->EstimatorType=="GR")
        {
            *outfile << "#INDICATOR_WEIGHT=" << M->EstimWeight << endl;
            *outfile << "#INDICATOR_DIRECT_VMS=";
            if (M->ZZ_DirectVMS==true)
            {
                *outfile << "TRUE" << endl;
            }
            else
            {
                *outfile << "FALSE" << endl;
            }
        }
    }
    
    *outfile << "" << endl;
    *outfile << "    SOLUTION INFORMATION:" << endl;
    *outfile << "    ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾" << endl;
    if(M->PrintConditionNumber==1)
    {
        *outfile << "#GLOBAL_CONDITION_NUMBER=" << L->ConditionNumber << endl;
        *outfile << "#MIN_LOC_CONDITION_NUMBER=" << L->MinCond << endl;
        *outfile << "#MAX_LOC_CONDITION_NUMBER=" << L->MaxCond << endl;
        *outfile << "#AVG_LOC_CONDITION_NUMBER=" << L->AvgCond << endl;
    }
}

void OutputFile::PrintErrorNorms(Model* M, LinearProblem* L, ErrorEstimator* E, std::ofstream* outfile)
{
    //Calculate and print the exact error norms is the solution is available
    if (M->ExactSolutionAvailable==true)
    {
        CalculateErrorNorms(M,L,outfile);
    }
    //Calculate and print the error norms if available
    if (M->ErrEstimator==true)
    {
        CalculateEstimatorErrorNorms(M,L,E,outfile);
    }
}

void OutputFile::PrintApproxSolution(Model* M, LinearProblem* L, std::ofstream* outfile)
{
    int Dim=M->Dimension;
    string Line;
    *outfile << "" << endl;
    *outfile << "    SOLUTION:" << endl;
    *outfile << "    ‾‾‾‾‾‾‾‾" << endl;
    *outfile << "#APPROXIMATED_SOLUTION" << endl;
    if (Dim==2)
    {
        if (M->PrintStrain==true)
        {
            *outfile << " NodeNum   X1             X2             U1             U2             S11            S12            S22            E11            E12            E22" << std::endl;
        }
        else
        {
            *outfile << " NodeNum   X1             X2             U1             U2             S11            S12            S22" << std::endl;
        }
    }
    else if (Dim==3)
    {
        *outfile << " NodeNum   X1             X2             X3             U1             U2             U3             S11            S12            S13            S22            S23            S33" << std::endl;
    }
    for (long i=0; i<M->NodeNumber; i++)
    {
        double TempU1, TempU2, TempU3;
        double TempS1, TempS2, TempS3, TempS4, TempS5, TempS6;
        double TempE1, TempE2, TempE3, TempE4, TempE5, TempE6;
        long RowU1=Dim*i;long RowU2=Dim*i+1;
        long RowU3;
        long RowS1=i*3*(Dim-1)+0;long RowS2=i*3*(Dim-1)+1;long RowS3=i*3*(Dim-1)+2;
        long RowS4, RowS5, RowS6;
        TempU1=L->U(RowU1);TempU2=L->U(RowU2);
        TempS1=L->S_Ord(RowS1);TempS2=L->S_Ord(RowS2);TempS3=L->S_Ord(RowS3);
        if (Dim==3)
        {
            RowU3=Dim*i+2;
            RowS4=3*(Dim-1)*i+3;
            RowS5=3*(Dim-1)*i+4;
            RowS6=3*(Dim-1)*i+5;
            TempS4=L->S_Ord(RowS4);TempS5=L->S_Ord(RowS5);TempS6=L->S_Ord(RowS6);
            TempU3=L->U(RowU3);
        }
        
        if (Dim==2)
        {
            string LastStr;
            if (M->PrintStrain==true)
            {
                TempE1=L->Epsilon(RowS1);TempE2=L->Epsilon(RowS2);TempE3=L->Epsilon(RowS3);
                LastStr=M->ToString(TempS3,5,15,false,0) + M->ToString(TempE1,5,15,false,0) + M->ToString(TempE2,5,15,false,0) + M->ToString(TempE3,5,15,true,0);
            }
            else
            {
                LastStr=M->ToString(TempS3,5,15,true,0);
            }
            Line= M->ToString(M->Nodes[i+1].NodeNum,0,15,false,0) + M->ToString(M->Nodes[i+1].X[0],5,15,false,0) + M->ToString(M->Nodes[i+1].X[1],5,15,false,0)
                 + M->ToString(TempU1,5,15,false,0)+ M->ToString(TempU2,5,15,false,0)
                 //+ ToString(abs(ErrorEstimU(i,0)),5,false)+ ToString(abs(ErrorEstimU(i,1)),5,false) 
                 + M->ToString(TempS1,5,15,false,0)+ M->ToString(TempS2,5,15,false,0)+ LastStr;
        }
        if (Dim==3)
        {
            Line= M->ToString(M->Nodes[i+1].NodeNum,0,15,false,0) + M->ToString(M->Nodes[i+1].X[0],5,15,false,0) + M->ToString(M->Nodes[i+1].X[1],5,15,false,0) + M->ToString(M->Nodes[i+1].X[2],5,15,false,0)
                 + M->ToString(TempU1,5,15,false,0)+ M->ToString(TempU2,5,15,false,0) + M->ToString(TempU3,5,15,false,0) 
                 + M->ToString(TempS1,5,15,false,0)+ M->ToString(TempS2,5,15,false,0)+ M->ToString(TempS3,5,15,false,0)
                 + M->ToString(TempS4,5,15,false,0)+ M->ToString(TempS5,5,15,false,0)+ M->ToString(TempS6,5,15,true,0);
        }
        *outfile << Line << std::endl;
    }
}

void OutputFile::PrintEstimExactSolution(Model* M, LinearProblem* L, ErrorEstimator* E, std::ofstream* outfile)
{
    string Line;
    int Dim=M->Dimension;
    
    if (M->ErrEstimator==true)
    {
        *outfile << "#ERROR_ESTIMATION" << endl;
        if (Dim==2)
        {
            if (M->EstimVar=="STRAIN")
                *outfile << " NodeNum   ERR_E11        ERR_E12        ERR_E22 " << std::endl;
            else if (M->EstimVar=="STRESS")
                *outfile << " NodeNum   ERR_S11        ERR_S12        ERR_S22        ERR_VMS" << std::endl;
            else if (M->EstimVar=="DISPLACEMENT")
                *outfile << " NodeNum   ERR_U1         ERR_U2" << std::endl;
        }
        if (Dim==3)
        {
            if (M->EstimVar=="STRAIN")
                *outfile << " NodeNum   ERR_E11        ERR_E12        ERR_E13        ERR_E22        ERR_E23        ERR_E33" << std::endl;
            else if (M->EstimVar=="STRESS")
                *outfile << " NodeNum   ERR_S11        ERR_S12        ERR_S13        ERR_S22        ERR_S23        ERR_S33        ERR_VMS" << std::endl;
            else if (M->EstimVar=="DISPLACEMENT")
                *outfile << " NodeNum   ERR_U1         ERR_U2         ERR_U3" << std::endl;
        }
        for (int i=0; i<M->NodeNumber; i++)
        {
            Line= M->ToString(M->Nodes[i+1].NodeNum,0,15,false,0);
            if (M->EstimVar=="STRESS")
            {
                Line+=M->ToString(abs(E->Estim(i*3*(Dim-1)+0)-L->S(i*3*(Dim-1)+0)),5,15,false,0)
                    + M->ToString(abs(E->Estim(i*3*(Dim-1)+1)-L->S(i*3*(Dim-1)+1)),5,15,false,0)
                    + M->ToString(abs(E->Estim(i*3*(Dim-1)+2)-L->S(i*3*(Dim-1)+2)),5,15,false,0)
                    + M->ToString(E->ErrorEstim(i),5,15,true,0);
            }
            else if (M->EstimVar=="STRAIN")
            {
                Line+=M->ToString(abs(E->Estim(i*3*(Dim-1)+0)-L->Epsilon(i*3*(Dim-1)+0)),5,15,false,0)
                    + M->ToString(abs(E->Estim(i*3*(Dim-1)+1)-L->Epsilon(i*3*(Dim-1)+1)),5,15,false,0)
                    + M->ToString(abs(E->Estim(i*3*(Dim-1)+2)-L->Epsilon(i*3*(Dim-1)+2)),5,15,true,0);
            }
            else if (M->EstimVar=="DISPLACEMENT")
            {
                Line+=M->ToString(abs(E->Estim(i*Dim+0)),5,15,false,0)
                    + M->ToString(abs(E->Estim(i*Dim+1)),5,15,true,0);
            }
            *outfile << Line << std::endl;
        }
    }
    if (M->ExactSolutionAvailable==true)
    {
        *outfile << "#EXACT_SOLUTION" << endl;
        if (Dim==2)
        {
            *outfile << " NodeNum   U1             U2             S11            S12            S22" << std::endl;
        }
        else if (Dim==3)
        {
            *outfile << " NodeNum   U1             U2             U3             S11            S12            S13            S22            S23            S33" << std::endl;
        }
        for (int i=0; i<M->NodeNumber; i++)
        {
            string LineU, LineS;
            LineU="";
            for (int j=0; j<Dim; j++)
            {
                if (M->Nodes[i+1].ExactU!=NULL)
                {
                    LineU= LineU+M->ToString(M->Nodes[i+1].ExactU[j],5,15,false,0);
                }
                else
                {
                    LineU= LineU +"           ~,  ";
                }
            }
            LineS="";
            for (int j=0; j<3*(Dim-1)-1; j++)
            {
                if (M->Nodes[i+1].ExactS!=NULL)
                {
                    LineS= LineS+M->ToString(M->Nodes[i+1].ExactS[j],5,15,false,0);
                }
                else
                {
                    LineS= LineS+ "           ~,  ";
                }
            }
            //Last stress of the row
            if (M->Nodes[i+1].ExactS!=NULL)
            {
                LineS= LineS+M->ToString(M->Nodes[i+1].ExactS[3*(Dim-1)-1],5,15,true,0);
            }
            else
            {
                LineS= LineS+ "             ~";
            }
            Line= M->ToString(M->Nodes[i+1].NodeNum,0,15,false,0) + LineU + LineS;
            *outfile << Line << std::endl;
        }
    }
}

void OutputFile::CalculateErrorNorms(Model* M, LinearProblem* L, std::ofstream* outfile)
{
    bool ExactUAvailable=false, ExactSAvailable=false;
    if (M->Nodes[1].ExactS != NULL){ExactSAvailable=true;}
    if (M->Nodes[1].ExactU != NULL){ExactUAvailable=true;}
    
    if (ExactSAvailable==true || ExactUAvailable==true)
    {
        *outfile << "#ERROR_NORMS" << endl;
    }
    
    //Calculate the error norms
    if (ExactSAvailable==true)
    {
        M->ExactS.resize(M->NodeNumber*(3*(M->Dimension-1)));
        for (long i=0; i<M->NodeNumber; i++)
        {
            for (int j=0; j<3*(M->Dimension-1); j++)
            {
                M->ExactS[i*3*(M->Dimension-1)+j]=M->Nodes[i+1].ExactS[j];
            }
        }
        CalculatePrintExactErrorS(M,L,outfile);
    }
    if (ExactUAvailable==true)
    {
        M->ExactU.resize(M->NodeNumber*M->Dimension);
        for (int i=0; i<M->NodeNumber; i++)
        {
            for (int j=0; j<M->Dimension; j++)
            {
                M->ExactU[i*M->Dimension+j]=M->Nodes[i+1].ExactU[j];
            }
        }
        CalculatePrintExactErrorU(M,L,outfile);
    }
}

void OutputFile::CalculatePrintExactErrorS(Model* M, LinearProblem* L, std::ofstream* outfile)
{
    int Dim=M->Dimension;
    VectorXd ExactEpsilon;
    VectorXd NodeNum;
    NodeNum.resize(3*(Dim-1)+1);

    //Fill the ExactEpsilon vector
    if (M->EstimVar=="STRAIN")
    {
        ExactEpsilon.resize(M->NodeNumber*3*(Dim-1));
        for (long i=0; i<M->NodeNumber; i++)
        {
            if (Dim==2)
            {
                ExactEpsilon(3*(Dim-1)+0)=(M->Nodes[i+1].ExactS[0]-L->d2/L->d1*M->Nodes[i+1].ExactS[2])/(L->d1-pow(L->d2,2)/L->d1);
                ExactEpsilon(3*(Dim-1)+2)=(M->Nodes[i+1].ExactS[0]-L->d1*ExactEpsilon(0))/L->d2;
                ExactEpsilon(3*(Dim-1)+1)=M->Nodes[i+1].ExactS[1]/L->d3;
            }
        }
    }
    string Line1="", Line2="", Line3="", Line4="", Line5="", Line6="";
    bool LastItem=false;
    
    for (int j=0; j<3*(Dim-1)+1; j++)
    {
        if (j==3*(Dim-1)) {LastItem=true;}
        if (M->EstimVar=="STRAIN")
        {
            Line1=Line1 + M->ToString(M->ErrorNorm("L1_ERROR","",ExactEpsilon,L->Epsilon,j,3*(Dim-1),0),5,15,LastItem,0);
            Line2=Line2 + M->ToString(M->ErrorNorm("L2_ERROR","",ExactEpsilon,L->Epsilon,j,3*(Dim-1),0),5,15,LastItem,0);
            Line3=Line3 + M->ToString(M->ErrorNorm("L1_ERROR","WEIGHTED",ExactEpsilon,L->Epsilon,j,3*(Dim-1),0),5,15,LastItem,0);
            Line4=Line4 + M->ToString(M->ErrorNorm("L2_ERROR","WEIGHTED",ExactEpsilon,L->Epsilon,j,3*(Dim-1),0),5,15,LastItem,0);
            Line5=Line5 + M->ToString(M->ErrorNorm("L2_ERROR","RELATIVE",ExactEpsilon,L->Epsilon,j,3*(Dim-1),0),5,15,LastItem,0);
            Line6=Line6 + M->ToString(M->ErrorNorm("LInf_ERROR","",ExactEpsilon,L->Epsilon,j,3*(Dim-1),0),5,15,LastItem,0);
        }
        else
        {
            Line1=Line1 + M->ToString(M->ErrorNorm("L1_ERROR","",M->ExactS,L->S_Ord,j,3*(Dim-1),0),5,15,LastItem,0);
            Line2=Line2 + M->ToString(M->ErrorNorm("L2_ERROR","",M->ExactS,L->S_Ord,j,3*(Dim-1),0),5,15,LastItem,0);
            Line3=Line3 + M->ToString(M->ErrorNorm("L1_ERROR","WEIGHTED",M->ExactS,L->S_Ord,j,3*(Dim-1),0),5,15,LastItem,0);
            Line4=Line4 + M->ToString(M->ErrorNorm("L2_ERROR","WEIGHTED",M->ExactS,L->S_Ord,j,3*(Dim-1),0),5,15,LastItem,0);
            Line5=Line5 + M->ToString(M->ErrorNorm("L2_ERROR","RELATIVE",M->ExactS,L->S_Ord,j,3*(Dim-1),0),5,15,LastItem,0);
            Line6=Line6 + M->ToString(M->ErrorNorm("LInf_ERROR","",M->ExactS,L->S_Ord,j,3*(Dim-1),0),5,15,LastItem,0);
        }
    }
    if (Dim==2)
    {
        if (M->EstimVar=="STRAIN")
            *outfile << " ERROR_TYPE               E11            E12            E22" << endl;
        else
            *outfile << " ERROR_TYPE               S11            S12            S22            VMS" << endl;
    }
    else if (Dim==3)
    {
        if (M->EstimVar=="STRAIN")
            *outfile << " ERROR_TYPE               E11            E12            E13            E22            E23            E33" << endl;
        else
            *outfile << " ERROR_TYPE               S11            S12            S13            S22            S23            S33            VMS" << endl;
    }

    *outfile << " L1_ERROR                " << Line1 << endl;
    *outfile << " L2_ERROR                " << Line2 << endl;
    *outfile << " L1_ERROR_WEIGHTED       " << Line3 << endl;
    *outfile << " L2_ERROR_WEIGHTED       " << Line4 << endl;
    *outfile << " L2_ERROR_RELATIVE       " << Line5 << endl;
    *outfile << " LInf_ERROR              " << Line6 << endl;
    *outfile << "" << endl;
}

void OutputFile::CalculatePrintExactErrorU(Model* M, LinearProblem* L, std::ofstream* outfile)
{
    int Dim=M->Dimension;
    string Line1="", Line2="", Line3="";
    bool LastItem=false;
    for (int j=0; j<Dim+1; j++)
    {
        if (j==Dim) {LastItem=true;}
        Line1=Line1 + M->ToString(M->ErrorNorm("L2_ERROR","WEIGHTED",M->ExactU,L->U,j,Dim,0),5,15,LastItem,0);
        Line2=Line2 + M->ToString(M->ErrorNorm("L2_ERROR","RELATIVE",M->ExactU,L->U,j,Dim,0),5,15,LastItem,0);
        Line3=Line3 + M->ToString(M->ErrorNorm("LInf_ERROR","",M->ExactU,L->U,j,Dim,0),5,15,LastItem,0);
    }
    if (Dim==2)
    {
        *outfile << " ERROR_TYPE               U1             U2             U" << endl;
    }
    else if (Dim==3)
    {
        *outfile << " ERROR_TYPE               U1             U2             U3             U" << endl;
    }
    *outfile << " L2_ERROR_WEIGHTED       " << Line1 << endl;
    *outfile << " L2_ERROR_RELATIVE       " << Line2 << endl;
    *outfile << " LInf_ERROR              " << Line3 << endl;
    *outfile << "" << endl;
}

void OutputFile::CalculateEstimatorErrorNorms(Model* M, LinearProblem* L, ErrorEstimator* E, std::ofstream* outfile)
{
    if (M->ErrEstimator==true)
    {
        int Dim=M->Dimension;
        *outfile << "#ESTIMATED_ERROR_NORMS" << endl;
        
        string Line1="", Line2="", Line3="";
        bool LastItem=false;
        
        int LastEstimatorIndex=3*(Dim-1)+1;
        if (M->EstimVar=="DISPLACEMENT")
            LastEstimatorIndex=Dim+1;
        for (int j=0; j<LastEstimatorIndex; j++)
        {
            if (j==LastEstimatorIndex-1) {LastItem=true;}
            if (M->EstimVar=="STRAIN")
            {
                Line1=Line1 + M->ToString(M->ErrorNorm("L2_ERROR","WEIGHTED",E->Estim,L->Epsilon,j,3*(Dim-1),1),5,15,LastItem,0);
                Line2=Line2 + M->ToString(M->ErrorNorm("L2_ERROR","RELATIVE",E->Estim,L->Epsilon,j,3*(Dim-1),1),5,15,LastItem,0);
                Line3=Line3 + M->ToString(M->ErrorNorm("LInf_ERROR","",E->Estim,L->Epsilon,j,3*(Dim-1),1),5,15,LastItem,0);
            }
            else if (M->EstimVar=="STRESS")
            {
                if (M->ZZ_DirectVMS==true && LastItem==true)
                {
                    Line1=Line1 + M->ToString(M->ErrorNorm("L2_ERROR","WEIGHTED",E->EstimVMS,L->S,j,3*(Dim-1),2),5,15,LastItem,0);
                    Line2=Line2 + M->ToString(M->ErrorNorm("L2_ERROR","RELATIVE",E->EstimVMS,L->S,j,3*(Dim-1),2),5,15,LastItem,0);
                    Line3=Line3 + M->ToString(M->ErrorNorm("LInf_ERROR","",E->EstimVMS,L->S,j,3*(Dim-1),2),5,15,LastItem,0);
                }
                else
                {
                    Line1=Line1 + M->ToString(M->ErrorNorm("L2_ERROR","WEIGHTED",E->Estim,L->S,j,3*(Dim-1),1),5,15,LastItem,0);
                    Line2=Line2 + M->ToString(M->ErrorNorm("L2_ERROR","RELATIVE",E->Estim,L->S,j,3*(Dim-1),1),5,15,LastItem,0);
                    Line3=Line3 + M->ToString(M->ErrorNorm("LInf_ERROR","",E->Estim,L->S,j,3*(Dim-1),1),5,15,LastItem,0);
                }
                
            }
            else if (M->EstimVar=="DISPLACEMENT")
            {
                Line1=Line1 + M->ToString(M->ErrorNorm("L2_ERROR","WEIGHTED",E->Estim,L->U,j,Dim,1),5,15,LastItem,0);
                Line2=Line2 + M->ToString(M->ErrorNorm("L2_ERROR","RELATIVE",E->Estim,L->U,j,Dim,1),5,15,LastItem,0);
                Line3=Line3 + M->ToString(M->ErrorNorm("LInf_ERROR","",E->Estim,L->U,j,Dim,1),5,15,LastItem,0);
            }
        }
        if (Dim==2)
        {
            if (M->EstimVar=="STRAIN")
                *outfile << " ERROR_TYPE               EST_E11        EST_E12        EST_E22" << endl;
            else if (M->EstimVar=="STRESS")
                *outfile << " ERROR_TYPE               EST_S11        EST_S12        EST_S22        EST_VMS" << endl;
            else if (M->EstimVar=="DISPLACEMENT")
                *outfile << " ERROR_TYPE               EST_U1         EST_U2         EST_U" << endl;
        }
        else if (Dim==3)
        {
            if (M->EstimVar=="STRAIN")
                *outfile << " ERROR_TYPE               EST_E11        EST_E12        EST_E13        EST_E22        EST_E23        EST_E33" << endl;
            if (M->EstimVar=="STRESS")
                *outfile << " ERROR_TYPE               EST_S11        EST_S12        EST_S13        EST_S22        EST_S23        EST_S33        EST_VMS" << endl;
        }
        *outfile << " L2_ERROR_WEIGHTED       " << Line1 << endl;
        *outfile << " L2_ERROR_RELATIVE       " << Line2 << endl;
        *outfile << " LInf_ERROR              " << Line3 << endl;
        *outfile << "" << endl;
    }
}

double OutputFile::GetAverageBCNodeSupport(Model* M)
{
    double AverageSupSize=0;
    long BCNodeNum=0;
    for (long i=1; i<=M->NodeNumber; i++)
    {
        if (M->Nodes[i].BoundaryNode == true)
        {
            BCNodeNum++;
            AverageSupSize=AverageSupSize+M->Nodes[i].SupSize;
        }
    }
    return AverageSupSize/BCNodeNum;
}

double OutputFile::GetAverageInnerNodeSupport(Model* M)
{
    double AverageSupSize=0;
    long IntNodeNum=0;
    for (long i=1; i<=M->NodeNumber; i++)
    {
        if (M->Nodes[i].BoundaryNode == false)
        {
            IntNodeNum++;
            AverageSupSize=AverageSupSize+M->Nodes[i].SupSize;
        }
    }
    return AverageSupSize/IntNodeNum;
}
