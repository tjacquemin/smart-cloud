#include "FileReader.h"

FileReader::FileReader() {
}

FileReader::FileReader(const FileReader& orig) {
}

FileReader::~FileReader() {
}

void FileReader::LoadInputFileArray(string Value,Model* M)
{
    string str;
    long Count=0;
    char Path[Value.length()+1];
    strncpy(Path, Value.c_str(),Value.length()+1);
    M->InputFilePath=Value;
    ifstream File;
    //Open the selected file
    File.open (Path, ios::in);
    //Add all the lines of the file in a vector
    while (getline(File, str))
    { 
        if (str!="")
           Count++;
    }
    InpArraySize=Count;
    File.close();
    M->AnalysisFile = new string[InpArraySize];
    File.open (Path, ios::in);
    //Add all the lines of the file in a vector
    long i=0;
    while (getline(File, str))
    {
        if (str!="")
        {
            M->AnalysisFile[i]=str;
            i++;
        }
    }
    File.close();
}

void FileReader::LoadInputs(Model* M,StencilSelect* S)
{
    long i = 0;
    //Read all the lines of the input file vector to find the identifiers
    //For each identifier, load the input data to the associated variables
    while (i <= InpArraySize-1)
    {
        if (M->AnalysisFile[i].substr(0,1) == "#")
        {
            //General model parameters
            {
                if (M->split(M->AnalysisFile[i],'=',0)=="#ANALYSIS_TYPE")
                {
                    M->ModelType=M->split(M->AnalysisFile[i],'=',1);
                    if (M->ModelType=="2D_PLANE_STRESS" || M->ModelType=="2D_PLANE_STRAIN")
                        M->Dimension=2;
                    else if (M->ModelType=="3D_ELASTICITY")
                        M->Dimension=3;
                }
                else if (M->split(M->AnalysisFile[i],'=',0)=="#PARAMETERS")
                {
                    i++;
                    M->E=stod(M->split(M->AnalysisFile[i],'=',1));
                    i++;
                    M->nu=stod(M->split(M->AnalysisFile[i],'=',1));
                }
                else if ( M->split(M->AnalysisFile[i],'=',0)=="#METHOD")
                {
                    M->Method=M->split(M->AnalysisFile[i],'=',1);
                }
                else if ( M->split(M->AnalysisFile[i],'=',0)=="#APPROX_SOL")
                {
                    M->StencilSol=M->split(M->AnalysisFile[i],'=',1);
                }
                else if ( M->split(M->AnalysisFile[i],'=',0)=="#APPROX_ORDER")
                {
                    M->ApproxOrder=stoi(M->split(M->AnalysisFile[i],'=',1));
                }
                else if ( M->split(M->AnalysisFile[i],'=',0)=="#BASIS")
                {
                    M->FunctionBasis=M->split(M->AnalysisFile[i],'=',1);
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#VOLUME")
                {
                    M->Volume = stod(M->split(M->AnalysisFile[i],'=',1));
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#THREAD")
                {
                    M->ThreadNumber=stoi(M->split(M->AnalysisFile[i],'=',1));
                }
            }
            //Boundary considerations
            {
                if ( M->split(M->AnalysisFile[i],'=',0)=="#STABILIZATION")
                {
                    if (M->split(M->AnalysisFile[i],'=',1)=="TRUE")
                    {
                        M->Stabilization=true;
                    }
                    if (M->split(M->AnalysisFile[i],'=',1)=="PARTIAL")
                    {
                        M->Stabilization=true;
                        M->PartialStab=true;
                    }
                }
                else if ( M->split(M->AnalysisFile[i],'=',0)=="#BC_TREATMENT")
                {
                    M->BCTreatType=M->split(M->AnalysisFile[i],'=',1);
                    if (M->BCTreatType=="INTERPOLATION")
                    {
                        M->HasCollocationOffset=true;
                        M->InterpBC=true;
                    }
                }
                else if ( M->split(M->AnalysisFile[i],'=',0)=="#LAGRANGE_SYM")
                {
                    if (M->split(M->AnalysisFile[i],'=',1)=="TRUE") 
                        M->LagrangeSym=true;
                }
                else if ( M->split(M->AnalysisFile[i],'=',0)=="#WEAK_BC")
                {
                    if (M->split(M->AnalysisFile[i],'=',1)=="TRUE") 
                        M->WeakBC=true;
                }
            }
            //Stencil selection
            {
                if (M->split(M->AnalysisFile[i],'=',0) == "#SUP_SIZE")
                {
                    S->SupRadiusGiven=false;
                    S->SupSizeInt = stoi(M->split(M->split(M->AnalysisFile[i],'=',1),',',0));
                    S->SupSizeBound=stoi(M->split(M->split(M->AnalysisFile[i],'=',1),',',1));
                    M->SupSizeIntM=S->SupSizeInt;
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#SUP_SCALING")
                {
                    S->SupScaling=stod(M->split(M->AnalysisFile[i],'=',1));
                    M->SupScaling=S->SupScaling;
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#STENCIL_SEL")
                {
                    S->StencilSelection=M->split(M->split(M->AnalysisFile[i],'=',1),',',0);
                    if (S->StencilSelection=="AREA")
                        S->StencilSelectionThreshold=stod(M->split(M->split(M->AnalysisFile[i],'=',1),',',1));
                    else if (S->StencilSelection=="COND")
                    {
                        S->MaxVariableSupSize=stol(M->split(M->split(M->AnalysisFile[i],'=',1),',',1));
                        S->MaxSupportCond=stod(M->split(M->split(M->AnalysisFile[i],'=',1),',',2));
                    }
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#VISIBILITY_TYPE")
                {
                    M->VisibilityType=M->split(M->AnalysisFile[i],'=',1);
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#VISIBILITY_THRESHOLD")
                {
                    M->VisibilityThresholdAngle=stod(M->split(M->AnalysisFile[i],'=',1));
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#SUP_INNER_THRESHOLD")
                {
                    S->SupInnerThreshold=stod(M->split(M->AnalysisFile[i],'=',1));
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#SUP_RADIUS")
                {
                    S->SupRadiusGiven=true;
                    S->RadiusInteriorNodes=stod(M->split(M->split(M->AnalysisFile[i],'=',1),',',0));
                    S->RadiusBoundaryNodes= stod(M->split(M->split(M->AnalysisFile[i],'=',1),',',1));
                }
            }
            //Stencil weight function
            {
                if ( M->split(M->AnalysisFile[i],'=',0)=="#WINDOW_TYPE")
                {
                    M->WindowFunctionType=M->split(M->split(M->AnalysisFile[i],'=',1),',',0);
                    M->SupRatio=stod(M->split(M->split(M->AnalysisFile[i],'=',1),',',1));
                    M->SupExp=M->SupRatio;
                }
                else if ( M->split(M->AnalysisFile[i],'=',0)=="#VORONOI")
                {
                    if (M->split(M->split(M->AnalysisFile[i],'=',1),',',0)=="TRUE")
                        M->Voronoi=true;
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#MIN_WEIGHT")
                {
                    M->MinWeight=stod(M->split(M->AnalysisFile[i],'=',1));
                }
            }
            //Singular model parameters
            {
                if (M->split(M->AnalysisFile[i],'=',0) == "#INCLUDE_SINGULAR")
                {
                    if (M->split(M->AnalysisFile[i],'=',1) == "TRUE")
                        M->IncludeSingularNodes=true;
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#SINGULAR_NODES")
                {
                    M->SingularNodeNumber=stol(M->split(M->AnalysisFile[i],'=',1));
                    i=LoadSingularNodes(M,i);
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#SINGULAR_ENRICHEMENT")
                {
                    M->SingularEnrichment=M->split(M->split(M->AnalysisFile[i],'=',1),',',0);
                    if (M->split(M->split(M->AnalysisFile[i],'=',1),',',1)=="TRUE")
                    {
                        M->FullEnrichment=true;
                    }
                    else
                    {
                        M->FullEnrichment=false;
                    }
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#SINGULAR_THRESHOLD")
                {
                    M->DistThreshold=stod(M->split(M->split(M->AnalysisFile[i],'=',1),',',0));
                    M->ScaleFactor=stod(M->split(M->split(M->AnalysisFile[i],'=',1),',',1));
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#HAS_COLLOC_OFFSET")
                {
                    if (M->trim(M->split(M->AnalysisFile[i],'=',1))=="TRUE")
                        M->HasCollocationOffset=true;
                }
            }
            {
                if (M->split(M->AnalysisFile[i],'=',0) == "#XCOEFF_NUMBER")
                {
                    M->NumberEnrichmentCoeff=stoi(M->split(M->AnalysisFile[i],'=',1));
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#XFUNCTIONS_INDEX")
                {
                    M->FNum=stoi(M->split(M->AnalysisFile[i],'=',1));
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#XINTER")
                {
                    if (M->split(M->AnalysisFile[i],'=',1)=="FALSE")
                    {
                        M->Inter=false;
                    }
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#XWEIGHT_SCALE")
                {
                    M->WeightScaleFactor=stod(M->split(M->AnalysisFile[i],'=',1));
                }
            }
            //Solver options
            {
                if (M->split(M->AnalysisFile[i],'=',0) == "#SOLVER")
                {
                    M->SolverType = M->split(M->AnalysisFile[i],'=',1);
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#SOLVER_PACKAGE")
                {
                    M->SolverPackage = M->split(M->AnalysisFile[i],'=',1);
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#SOLVER_SPEC")
                {
                    M->SolverPC = M->split(M->split(M->AnalysisFile[i],'=',1),',',0);
                    M->SolverKSP = M->split(M->split(M->AnalysisFile[i],'=',1),',',1);
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#SOLVER_TOL")
                {
                    M->SolverTol = stod(M->split(M->split(M->AnalysisFile[i],'=',1),',',0));
                    M->SolverMaxIt=stol(M->split(M->split(M->AnalysisFile[i],'=',1),',',1));
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#AMG_SPEC")
                {
                    M->AMG_Threshold = stod(M->split(M->split(M->AnalysisFile[i],'=',1),',',0));
                    M->AMG_Levels=stoi(M->split(M->split(M->AnalysisFile[i],'=',1),',',1));
                    M->AMG_SmoothN=stoi(M->split(M->split(M->AnalysisFile[i],'=',1),',',2));
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#AMG_SPEC_GAMG")
                {
                    while (M->AnalysisFile[i+1].substr(0,1)  != "#")
                    {
                        i++;
                        if (M->split(M->AnalysisFile[i],'=',0) == "AGG_THRESHOLD")
                        {
                            M->AMG_Threshold=stod(M->split(M->AnalysisFile[i],'=',1));
                        }
                        else if (M->split(M->AnalysisFile[i],'=',0) == "MAX_LEVELS")
                        {
                            M->AMG_Levels=stoi(M->split(M->AnalysisFile[i],'=',1));
                        }
                        else if (M->split(M->AnalysisFile[i],'=',0) == "SMOOTH_NUM")
                        {
                            M->AMG_SmoothN=stoi(M->split(M->AnalysisFile[i],'=',1));
                        }
                        else if (M->split(M->AnalysisFile[i],'=',0) == "CYCLE_NUM")
                        {
                            M->AMG_CycleN=stoi(M->split(M->AnalysisFile[i],'=',1));
                        }
                        else if (M->split(M->AnalysisFile[i],'=',0) == "OPERATOR")
                        {
                            M->AMG_Operator=M->split(M->AnalysisFile[i],'=',1);
                        }
                    }
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#AMG_SPEC_HYPRE")
                {
                    while (M->AnalysisFile[i+1].substr(0,1)  != "#")
                    {
                        i++;
                        if (M->split(M->AnalysisFile[i],'=',0) == "COARSEN_TYPE")
                        {
                            M->H_CoarsenType=stoi(M->split(M->AnalysisFile[i],'=',1));
                        }
                        else if (M->split(M->AnalysisFile[i],'=',0) == "STRONG_THRESHOLD")
                        {
                            M->H_StrongThreshold=stod(M->split(M->AnalysisFile[i],'=',1));
                        }
                        else if (M->split(M->AnalysisFile[i],'=',0) == "AGG_NUM_LEVEL")
                        {
                            M->H_AggNumLevel=stoi(M->split(M->AnalysisFile[i],'=',1));
                        }
                        else if (M->split(M->AnalysisFile[i],'=',0) == "MAX_COARSE_SIZE")
                        {
                            M->H_MaxCoarseSize=stoi(M->split(M->AnalysisFile[i],'=',1));
                        }
                        else if (M->split(M->AnalysisFile[i],'=',0) == "MAX_AMG_LEVELS")
                        {
                            M->H_MaxLevels=stoi(M->split(M->AnalysisFile[i],'=',1));
                        }
                        else if (M->split(M->AnalysisFile[i],'=',0) == "MAX_AMG_ITER")
                        {
                            M->H_MaxAMGIter=stoi(M->split(M->AnalysisFile[i],'=',1));
                        }
                        else if (M->split(M->AnalysisFile[i],'=',0) == "RELAX_TYPE")
                        {
                            M->H_RelaxType=stoi(M->split(M->AnalysisFile[i],'=',1));
                        }
                        else if (M->split(M->AnalysisFile[i],'=',0) == "INTERP_TYPE")
                        {
                            M->H_InterpType=stoi(M->split(M->AnalysisFile[i],'=',1));
                        }
                        else if (M->split(M->AnalysisFile[i],'=',0) == "SMOOTH_TYPE")
                        {
                            M->H_SmoothType=stoi(M->split(M->AnalysisFile[i],'=',1));
                        }
                        else if (M->split(M->AnalysisFile[i],'=',0) == "NUM_SWEEPS")
                        {
                            M->H_NumSweeps=stoi(M->split(M->AnalysisFile[i],'=',1));
                        }
                        else if (M->split(M->AnalysisFile[i],'=',0) == "AMG_TOL")
                        {
                            M->H_AMGTol=stod(M->split(M->AnalysisFile[i],'=',1));
                        }
                        else if (M->split(M->AnalysisFile[i],'=',0) == "NON_GALERK_TOL")
                        {
                            M->H_NonGalerkTol=stod(M->split(M->AnalysisFile[i],'=',1));
                        }
                        else if (M->split(M->AnalysisFile[i],'=',0) == "CYCLE_TYPE")
                        {
                            M->H_CycleType=stoi(M->split(M->AnalysisFile[i],'=',1));
                        }
                    }
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#ILU_SPEC")
                {
                    M->ILU_Levels = stoi(M->split(M->split(M->AnalysisFile[i],'=',1),',',0));
                    M->ILU_DiagFill=stod(M->split(M->split(M->AnalysisFile[i],'=',1),',',1));
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#MAT_ORDERING")
                {
                    if(M->split(M->AnalysisFile[i],'=',1)=="FALSE")
                        M->Mat_Ordering = 0;
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#MAT_PARTITIONING")
                {
                    if(M->split(M->AnalysisFile[i],'=',1)=="TRUE")
                        M->Mat_Partitioning = 1;
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#MAT_NULLSPACE")
                {
                    if(M->split(M->AnalysisFile[i],'=',1)=="TRUE")
                        M->Mat_NullSpace = 1;
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#ADJ_TRESHOLD")
                {
                    M->AdjThreshold=stod(M->split(M->AnalysisFile[i],'=',1));
                }
            }
            //Error estimation
            {
                if ( M->split(M->AnalysisFile[i],'=',0)=="#ERR_ESTIMATOR")
                {
                    if (M->split(M->split(M->AnalysisFile[i],'=',1),',',0)=="TRUE")
                    {
                        M->ErrEstimator=true;
                        M->EstimatorType=M->split(M->split(M->AnalysisFile[i],'=',1),',',1);
                        if (M->EstimatorType=="GR" || M->EstimatorType=="BEN" || M->EstimatorType=="BEN_RES" || M->EstimatorType=="RES")
                        {
                            M->EstimVar=M->split(M->split(M->AnalysisFile[i],'=',1),',',2);
                        }
                    }
                }
                else if (M->split(M->AnalysisFile[i],'=',0)=="#ERR_ESTIMATOR_ZZ_DIRECT_VMS")
                {
                   if (M->split(M->AnalysisFile[i],'=',1)=="TRUE")
                   {
                       M->ZZ_DirectVMS=true;
                   }                       
                }
                else if (M->split(M->AnalysisFile[i],'=',0)=="#ERR_ESTIMATOR_ZZ_STENCIL_SCALING")
                {
                    M->StencilScaling=stod(M->split(M->AnalysisFile[i],'=',1));
                }
                else if (M->split(M->AnalysisFile[i],'=',0)=="#ERR_ESTIMATOR_WEIGHT")
                {
                    M->EstimWeight=M->split(M->AnalysisFile[i],'=',1);
                }
                else if (M->split(M->AnalysisFile[i],'=',0)=="#ERR_ESTIMATOR_SUP_SIZE")
                {
                    M->EstimSupSize=stoi(M->split(M->AnalysisFile[i],'=',1));
                }
                else if (M->split(M->AnalysisFile[i],'=',0)=="#BEN_EST_ORDER")
                {
                    M->BenEstOrder=stoi(M->split(M->AnalysisFile[i],'=',1));
                }
                else if (M->split(M->AnalysisFile[i],'=',0)=="#RES_VORO_CORNER")
                {
                    if (M->split(M->AnalysisFile[i],'=',1)=="TRUE")
                   {
                       M->ResVoroCorner=true;
                   }   
                }
            }
            //Discretization and boundary loading
            {
                if (M->trim(M->AnalysisFile[i]) == "#NODE_DATA")
                {
                    i=LoadNodeData(M,i);
                }
                else if (M->trim(M->AnalysisFile[i]) == "#BOUNDARY_DIRICHLET")
                {
                    i=LoadBC_D(M,i);
                }
                else if (M->trim(M->AnalysisFile[i]) == "#BOUNDARY_NEUMANN")
                {
                    i=LoadBC_N(M,i);
                }
                else if (M->trim(M->AnalysisFile[i]) == "#BOUNDARY")
                {
                    i=LoadBC_All(M,i);
                }
                else if (M->trim(M->AnalysisFile[i]) == "#EXACT_SOLUTION")
                {
                    M->ExactSolutionAvailable=true;
                    i=LoadExactSolution(M,i);
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#SURFACE_NUMBER")
                {
                    M->SurfaceNumber=stoi(M->split(M->AnalysisFile[i],'=',1));
                    if (M->Surfaces==NULL){
                        M->Surfaces=new Surface[M->SurfaceNumber];}
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#SURFACE_TYPE")
                {
                    M->SurfaceDefined=true;
                    int SurfaceIndex=stoi(M->split(M->split(M->AnalysisFile[i],'=',1),',',0))-1;
                    if (M->split(M->split(M->AnalysisFile[i],'=',1),',',1)=="INNER")
                        M->Surfaces[SurfaceIndex].InnerSurface=true;
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#SURFACE_VERTICES")
                {
                    int SurfaceIndex=stoi(M->split(M->split(M->AnalysisFile[i],'=',1),',',0))-1;
                    long VerticeNumber=stol(M->split(M->split(M->AnalysisFile[i],'=',1),',',1));
                    M->Surfaces[SurfaceIndex].NewVertices(VerticeNumber,M->Dimension);
                    i=LoadSurfaceVertices(M,i,SurfaceIndex);
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#SURFACE_EL")
                {
                    int SurfNumber=stoi(M->split(M->split(M->AnalysisFile[i],'=',1),',',0));
                    M->ElementNumber=stol(M->split(M->split(M->AnalysisFile[i],'=',1),',',1));
                    string ElType=M->split(M->split(M->AnalysisFile[i],'=',1),',',2);
                    i=LoadSurfaceElements(M,i,M->ElementNumber,ElType);
                    
                    if (ElType=="2D_LINEAR_WITH_BC" || ElType=="3D_LINEAR_WITH_BC_CAD")
                    {
                        M->DirectCollocRefinement=true;
                    }
                }
            }
            //Adaptivity
            {
                if (M->split(M->AnalysisFile[i],'=',0) == "#CAD_MODEL_AVAILABLE")
                {
                    if (M->trim(M->split(M->AnalysisFile[i],'=',1))=="TRUE")
                    {
                        M->CAD_Model_Available=true;
                        M->ModelCAD = new CAD_Model;
                        i++;
                        if (M->split(M->AnalysisFile[i],'=',0)=="PATH")
                        {
                            M->PathToCAD=M->split(M->AnalysisFile[i],'=',1);
                            M->ModelCAD->LoadCAD_Model(M->PathToCAD,M->Dimension);
                        }
                    }
                }
                else if (M->split(M->AnalysisFile[i],'=',0) == "#ADAPTIVITY")
                {
                    if (M->trim(M->split(M->AnalysisFile[i],'=',1))=="TRUE")
                        M->Adaptivity=true;
                    while (M->AnalysisFile[i+1].substr(0,1)  != "#")
                    {
                        i++;
                        if (M->split(M->AnalysisFile[i],'=',0)=="THRESHOLD")
                            M->AdaptThreshold=stod(M->split(M->AnalysisFile[i],'=',1));
                        if (M->split(M->AnalysisFile[i],'=',0)=="THRESHOLD_LOG")
                            M->AdaptThresholdLog=stod(M->split(M->AnalysisFile[i],'=',1));
                        if (M->split(M->AnalysisFile[i],'=',0)=="THRESHOLD_FRAC")
                            M->AdaptThresholdFrac=stod(M->split(M->AnalysisFile[i],'=',1));
                        if (M->split(M->AnalysisFile[i],'=',0)=="MAX_ADAPT_FRAC")
                            M->AdaptFraction=stod(M->split(M->AnalysisFile[i],'=',1));
                        if (M->split(M->AnalysisFile[i],'=',0)=="LEVEL")
                        {
                            M->NumLevel=stoi(M->split(M->AnalysisFile[i],'=',1));
                            M->NumLevel++;
                        }
                        if (M->split(M->AnalysisFile[i],'=',0)=="REFINEMENT_TYPE")
                        {
                            M->RefType=M->split(M->AnalysisFile[i],'=',1);
                        }
                        if (M->split(M->AnalysisFile[i],'=',0)=="REFINEMENT_METHOD")
                        {
                            M->RefMethod=M->split(M->AnalysisFile[i],'=',1);
                        }
                        if (M->split(M->AnalysisFile[i],'=',0)=="NEW_NODES_DELETION_FAC")
                        {
                            M->DeletionFactor_Bound=stof(M->split(M->split(M->AnalysisFile[i],'=',1),',',0));
                            M->DeletionFactor_Inner=stof(M->split(M->split(M->AnalysisFile[i],'=',1),',',1));
                        }
                        if (M->split(M->AnalysisFile[i],'=',0)=="B_REF_EVEN")
                        {
                            if (M->split(M->AnalysisFile[i],'=',1) =="TRUE")
                            {
                                M->RefineEvenBOnly=true;
                            }
                        }
                        if (M->split(M->AnalysisFile[i],'=',0)=="DIST_BOUND")
                        {
                            if (M->split(M->AnalysisFile[i],'=',1) =="TRUE")
                            {
                                M->DistToBoundCriterion=true;
                            }
                        }
                    }
                }
            }
            //Print options
            {
                if (M->split(M->AnalysisFile[i],'=',0) == "#PRINT_OPTIONS")
                {
                    while (M->AnalysisFile[i+1].substr(0,1)  != "#")
                    {
                        i++;
                        if (M->split(M->AnalysisFile[i],'=',0) == "VIEW_MAT")
                        {
                            if (M->split(M->AnalysisFile[i],'=',1)=="TRUE")
                                M->PrintMat=1;
                        }
                        else if (M->split(M->AnalysisFile[i],'=',0) == "CONDITION_NUM")
                        {
                            if (M->split(M->AnalysisFile[i],'=',1) =="TRUE")
                                M->PrintConditionNumber=1;
                            else if (M->split(M->AnalysisFile[i],'=',1) =="FALSE")
                                M->PrintConditionNumber=0;
                        }
                        else if (M->split(M->AnalysisFile[i],'=',0) == "TIME_SPLIT")
                        {
                            if(M->split(M->AnalysisFile[i],'=',1)=="TRUE")
                                M->TimeSplit = true;
                        }
                        else if (M->split(M->AnalysisFile[i],'=',0) == "PRINT_STRAIN")
                        {
                            if(M->split(M->AnalysisFile[i],'=',1)=="TRUE")
                                M->PrintStrain = true;
                        }
                        else if (M->split(M->AnalysisFile[i],'=',0) == "PRINT_VTK")
                        {
                            if(M->split(M->AnalysisFile[i],'=',1)=="TRUE")
                                M->PrintVTK = true;
                        }
                        else if (M->split(M->AnalysisFile[i],'=',0) == "PRINT_LOCAL_RESIDUAL")
                        {
                            if(M->split(M->AnalysisFile[i],'=',1)=="TRUE")
                                M->PrintLocResidual = true;
                        }
                    }
                    /** \page PagePrintOptions Print Options
                     *   Additional print options can be added to the output file with the keyword \b #PRINT_OPTIONS. \n
                     *   - \b #PRINT_OPTIONS \n
                     *    *VIEW_MAT=TRUE* \n
                     *    *CONDITION_NUM=TRUE* \n
                     *    *TIME_SPLIT=TRUE* \n
                     *    *PRINT_STRAIN=TRUE* \n
                     *   
                    */
                }
            }
            if (M->trim(M->AnalysisFile[i]) == "#END")
            {
                i=InpArraySize+10;
            }
        }
        i++;
    }
    //Keep the file in the memory for the adaptivity section
    if (M->Adaptivity==false)
    {
        delete[] M->AnalysisFile;
    }
}

long FileReader::LoadNodeData(Model* M,long FirstRow)
{   
    long i=FirstRow+1;
    long Count=0;
    double X1, X2, X3=0;
    long NodeNum;
    //Count the number of nodes
    while (i <= InpArraySize && M->AnalysisFile[i].substr(0,1)  != "#")
    {
        if (M->AnalysisFile[i].empty()==false)
        {
            Count++;
        }
        i++;
    }
    i=FirstRow+1;
    //Create the array of nodes
    M->Nodes = new Node[Count+1];
    M->NodeNumber=Count;
    //Once the node vector of the appropriate dimension, fill it with the node data
    while (i <= InpArraySize)
    {
        if (M->AnalysisFile[i].empty()==false)
        {
            NodeNum=stol(M->split(M->AnalysisFile[i],',',0));
            X1=stod(M->split(M->AnalysisFile[i],',',1));
            X2=stod(M->split(M->AnalysisFile[i],',',2));
            if (M->Dimension==3)
            {
                X3=stod(M->split(M->AnalysisFile[i],',',3));
            }
            M->Nodes[NodeNum].SetNode(NodeNum, M->Dimension, X1, X2, X3);
            if (M->HasCollocationOffset==true)
            {
//                if (M->split(M->AnalysisFile[i],',',M->Dimension+1)!="")
                {
                    M->Nodes[NodeNum].CollocationPointOffset=true;
                    M->Nodes[NodeNum].CollocX = new double[M->Dimension];
                    for (int j=0; j<M->Dimension; j++)
                    {
                        if (M->InterpBC==true)
                        {
                            M->Nodes[NodeNum].CollocX[j]=M->Nodes[NodeNum].X[j];
                        }
                        else
                        {
                            M->Nodes[NodeNum].CollocX[j]=stod(M->split(M->AnalysisFile[i],',',M->Dimension+1+j));
                        }
                    }
                }
            }
        }
        i++;
        //Exit the loop if a new identifer is found or if the end of the file is reached
        if (M->AnalysisFile[i].substr(0,1)  == "#")
        {
            break;
        }
    }
    return i-1;
}

long FileReader::LoadBC_D(Model* M,long FirstRow)
{
    long i=FirstRow+1;
    double Value;
    long NodeNum;
    int DOF;
    //Once the BC vector of the appropriate dimension, fill it with the node data
    while (i <= InpArraySize-1)
    {
        if (M->AnalysisFile[i].empty()==false)
        {
            NodeNum=stol(M->split(M->AnalysisFile[i],',',0));
            DOF=stoi(M->split(M->AnalysisFile[i],',',1));
            Value=stod(M->split(M->AnalysisFile[i],',',2)); 
            M->Nodes[NodeNum].SetBoundNode(true,M->Dimension,DOF,DOF-1,"*",Value,0,0,0,"D","G");
        }
        i++;
        //Exit the loop if a new identifer is found or if the end of the file is reached
        if (M->AnalysisFile[i].substr(0,1)  == "#" || i == InpArraySize-1)
            break;
    }
    return i-1;
}

long FileReader::LoadBC_N(Model* M,long FirstRow)
{
    long i=FirstRow+1;
    double Value,NX1,NX2;
    double NX3=0;
    long NodeNum;
    int DOF;
    i=FirstRow+1;
    //Once the BC vector of the appropriate dimension, fill it with the node data
    while (i <= InpArraySize-1)
    {
        if (M->AnalysisFile[i].empty()==false)
        {
            NodeNum=stol(M->split(M->trim(M->AnalysisFile[i]),',',0));
            NX1=stod(M->split(M->AnalysisFile[i],',',1));
            NX2=stod(M->split(M->AnalysisFile[i],',',2));
            int inc=3;
            if (M->Dimension==3)
            {
                NX3=stod(M->trim(M->split(M->AnalysisFile[i],',',3)));
                inc=4;
            }
            string DOF_Val="";
            DOF_Val=M->split(M->AnalysisFile[i],',',inc);
            while (DOF_Val != "")
            {
                DOF=stoi(M->split(M->AnalysisFile[i],',',inc));
                Value=stod(M->split(M->AnalysisFile[i],',',inc+1));
                M->Nodes[NodeNum].SetBoundNode(true,M->Dimension,DOF,DOF-1,"*",Value,NX1,NX2,NX3,"N","G");
                inc=inc+2;
                DOF_Val=M->split(M->AnalysisFile[i],',',inc);
            }
        }
        i++;
        //Exit the loop if a new identifer is found or if the end of the file is reached
        if (i == InpArraySize-1)
            break;
        if (M->AnalysisFile[i].substr(0,1)  == "#" || i == InpArraySize)
            break;
    }
    return i-1;
}

long FileReader::LoadBC_All(Model* M,long FirstRow)
{
    long i=FirstRow+1;
    double NX1,NX2;
    double NX3=0;
    long NodeNum;
    string BC_Type="";
    string BC_Axis="";
    double BC_Val=0;
    i=FirstRow+1;
    //Once the BC vector of the appropriate dimension, fill it with the node data
    while (i <= InpArraySize-1)
    {
        if (M->AnalysisFile[i].empty()==false)
        {
            int BCNodeBlockSize=1;
            int NumBCPerRow=1;
            int BCDOF=-1;
            int BCIndex=-1;
            string BCDOFStr=M->split(M->AnalysisFile[i],',',1);
            if (BCDOFStr!="*")
            {
                BCNodeBlockSize=M->Dimension;
                NumBCPerRow=1;
            }
            else
            {
                BCNodeBlockSize=1;
                NumBCPerRow=M->Dimension;
            }
            for (int ii=0;ii<BCNodeBlockSize;ii++)
            {
                NodeNum=stol(M->split(M->trim(M->AnalysisFile[i]),',',0));
                if (BCNodeBlockSize>1)
                {
                    M->Nodes[NodeNum].MultipleBCRows=true;
                }
                
                BCDOFStr=M->split(M->AnalysisFile[i],',',1);
                NX1=stod(M->split(M->AnalysisFile[i],',',2));
                NX2=stod(M->split(M->AnalysisFile[i],',',3));
                if (M->Dimension==3)
                {
                    NX3=stod(M->trim(M->split(M->AnalysisFile[i],',',4)));
                }
                for (int j=0;j<NumBCPerRow;j++)
                {
                    if (BCDOFStr=="*")
                    {
                        BCDOF=j;
                        BCIndex=j;
                    }
                    else
                    {
                        BCDOF=stoi(BCDOFStr)-1;
                        BCIndex=ii;
                    }
                    BC_Type=M->split(M->AnalysisFile[i],',',M->Dimension+2+3*j);
                    BC_Axis=M->split(M->AnalysisFile[i],',',M->Dimension+3+3*j);
                    BC_Val=stod(M->split(M->AnalysisFile[i],',',M->Dimension+4+3*j));
                                            
                    if (BC_Type=="D")
                        M->Nodes[NodeNum].SetBoundNode(true,M->Dimension,BCDOF,BCIndex,BCDOFStr,BC_Val,NX1,NX2,NX3,"D",BC_Axis);
                    else if (BC_Type=="N")
                        M->Nodes[NodeNum].SetBoundNode(true,M->Dimension,BCDOF,BCIndex,BCDOFStr,BC_Val,NX1,NX2,NX3,"N",BC_Axis);
                }
                if (BCNodeBlockSize>1 && ii<BCNodeBlockSize-1)
                    i++;
            }
        }
        i++;
        
        //Exit the loop if a new identifer is found or if the end of the file is reached
        if (i == InpArraySize-1)
            break;
        if (M->AnalysisFile[i].substr(0,1)  == "#" || i == InpArraySize)
            break;
    }
    return i-1;
}

long FileReader::LoadExactSolution(Model* M,long FirstRow)
{
    long i=FirstRow+1;
    string ExactValue;
    int StressLastIndex=5;
    long NodeNum;
    if (M->Dimension==3)
    {
        StressLastIndex=9;
    }
    while (i <= InpArraySize-1)
    {
        if (M->AnalysisFile[i].empty()==false)
        {
            NodeNum=stol(M->split(M->trim(M->AnalysisFile[i]),',',0));
            for (int j=1; j<=M->Dimension; j++)
            {
                ExactValue=M->trim(M->split(M->AnalysisFile[i],',',j));
                if (ExactValue != "~")
                {
                    if (j==1)
                    {
                        M->Nodes[NodeNum].ExactU=new double[M->Dimension];
                        M->Nodes[NodeNum].HasExactU=true;
                    }
                    M->Nodes[NodeNum].ExactU[j-1]=stod(ExactValue);
                }
            }
            for (int j=M->Dimension+1; j<=StressLastIndex; j++)
            {
                ExactValue=M->split(M->AnalysisFile[i],',',j);
                if (ExactValue != "~")
                {
                    if (j==M->Dimension+1)
                    {
                        M->Nodes[NodeNum].ExactS=new double[3*(M->Dimension-1)];
                        M->Nodes[NodeNum].HasExactS=true;
                    }
                    if (M->trim(ExactValue) != "Inf")
                    {
                        M->Nodes[NodeNum].ExactS[j-M->Dimension-1]=stod(ExactValue);
                    }
                    else
                    {
                        M->Nodes[NodeNum].ExactS[j-M->Dimension-1]=9.9999E30;
                    }
                }
            }
        }
        i++;
        
        //Exit the loop if a new identifer is found or if the end of the file is reached
        if (i == InpArraySize)
        {
            break;
        }
        else if (M->AnalysisFile[i].substr(0,1)  == "#")
        {
            break;
        }
    }
    return i-1;
}

long FileReader::LoadSurfaceVertices(Model* M,long FirstRow, int SurfaceIndex)
{
    long i=FirstRow+1;
    long VertexNum=0;
    
    while (i <= InpArraySize-1)
    {
        if (M->AnalysisFile[i].empty()==false)
        {
            VertexNum=stol(M->split(M->trim(M->AnalysisFile[i]),',',0));
            for (int j=0; j<M->Dimension; j++)
            {
                M->Surfaces[SurfaceIndex].Vertices[VertexNum].X[j]=stod(M->split(M->trim(M->AnalysisFile[i]),',',j+1));                
            }
        }
        i++;
        //Exit the loop if a new identifer is found or if the end of the file is reached
        if (M->AnalysisFile[i].substr(0,1)  == "#")
        {
            break;
        }
    }
    return i-1;
}

long FileReader::LoadSurfaceElements(Model* M,long FirstRow, long NumElements, string ElType)
{
    //Create the element Array
    M->SurfElement= new Element[NumElements];
    long i=FirstRow+1;
    long ElNum=0;
    long ElCount=0;
    double* ElNomal=NULL;
    ElNomal=new double[M->Dimension];
    long LocNodeNum=0;
    while (i <= InpArraySize-1)
    {
        if (M->AnalysisFile[i].empty()==false)
        {
            //Get the element number
            ElNum=stol(M->split(M->AnalysisFile[i],',',0));
            //Get the element normal
            if (ElType=="2D_LINEAR" || ElType=="2D_LINEAR_WITH_BC" || ElType=="2D_QUAD" || ElType=="3D_LINEAR" || ElType=="3D_LINEAR_WITH_BC_CAD")
            {
                for (int j=0; j<M->Dimension; j++)
                {
                    ElNomal[j]=stod(M->split(M->AnalysisFile[i],',',j+M->Dimension+1));
                }
            }
            //Create the element and load the element type and the normal
            M->SurfElement[ElCount].NewElement(ElNum,ElType,ElNomal);
            
            
            
            //Load the nodes connected to the element
            if (ElType=="2D_LINEAR" || ElType=="2D_LINEAR_WITH_BC" || ElType=="2D_QUAD" || ElType=="3D_LINEAR" || ElType=="3D_LINEAR_WITH_BC_CAD")
            {
                for (int j=0; j<M->Dimension; j++)
                {
                    LocNodeNum=stol(M->split(M->AnalysisFile[i],',',j+1));
                    M->Nodes[LocNodeNum].NodeInSurfaceElement=true;
                    M->SurfElement[ElCount].AttachedNodes[j]=&M->Nodes[LocNodeNum];
                }
                if (ElType=="2D_QUAD")
                {
                    M->SurfElement[ElCount].CenterNode.SetNode(0,M->Dimension,stod(M->split(M->AnalysisFile[i],',',5)),stod(M->split(M->AnalysisFile[i],',',6)),0);
                }
                else if (ElType=="2D_LINEAR_WITH_BC")
                {
                    M->SurfElement[ElCount].SetBC_2D(M->split(M->AnalysisFile[i],',',5),M->split(M->AnalysisFile[i],',',6),stod(M->split(M->AnalysisFile[i],',',7)),M->split(M->AnalysisFile[i],',',8),M->split(M->AnalysisFile[i],',',9),stod(M->split(M->AnalysisFile[i],',',10)));
                }
                else if (ElType=="3D_LINEAR_WITH_BC_CAD")
                {
                    M->SurfElement[ElCount].SetBC_3D(M->split(M->AnalysisFile[i],',',7),M->split(M->AnalysisFile[i],',',8),stod(M->split(M->AnalysisFile[i],',',9)),
                                                     M->split(M->AnalysisFile[i],',',10),M->split(M->AnalysisFile[i],',',11),stod(M->split(M->AnalysisFile[i],',',12)),
                                                     M->split(M->AnalysisFile[i],',',13),M->split(M->AnalysisFile[i],',',14),stod(M->split(M->AnalysisFile[i],',',15)));
                    if (stod(M->split(M->AnalysisFile[i],',',16))>-0.5)
                    {
                        M->SurfElement[ElCount].RefSurf=stol(M->split(M->AnalysisFile[i],',',16));
                    }
                    if (stod(M->split(M->AnalysisFile[i],',',17))>-0.5)
                    {
                        M->SurfElement[ElCount].RefEdge=stol(M->split(M->AnalysisFile[i],',',17));
                    }
                    if (stod(M->split(M->AnalysisFile[i],',',18))>-0.5)
                    {
                        M->SurfElement[ElCount].RefSurfOfEl=stol(M->split(M->AnalysisFile[i],',',18));
                    }
                }
            }
            
            ElCount++;
        }
        i++;
        //Exit the loop if a new identifer is found or if the end of the file is reached
        if (M->AnalysisFile[i].substr(0,1)  == "#")
        {
            break;
        }
    }
        
    delete[] ElNomal;
    return i-1;
}

long FileReader::LoadSingularNodes(Model* M,long FirstRow)
{
    long i=FirstRow+1;
    double NX1,NX2;
    double NX3=0;
    long NodeNum;
    
    //Once the BC vector of the appropriate dimension, fill it with the node data
    while (i <= InpArraySize-1)
    {
        if (M->AnalysisFile[i].empty()==false)
        {
            NodeNum=stol(M->split(M->trim(M->AnalysisFile[i]),',',0));
            NX1=stod(M->split(M->AnalysisFile[i],',',1));
            NX2=stod(M->split(M->AnalysisFile[i],',',2));
            if (M->Dimension==3)
            {
                NX3=stod(M->trim(M->split(M->AnalysisFile[i],',',3)));
            }
            M->Nodes[NodeNum].IsSingular=true;
            M->Nodes[NodeNum].SetBoundNode(false,M->Dimension,0,0,"*",0,NX1,NX2,NX3,"N","G");
            M->SingularNode=NodeNum;
        }
        i++;
        //Exit the loop if a new identifer is found or if the end of the file is reached
        if (M->AnalysisFile[i].substr(0,1)  == "#" || i == InpArraySize-1)
            break;
    }
    if (M->IncludeSingularNodes==false)
    {
        M->NodeNumber-=M->SingularNodeNumber;
    }
    return i-1;
}
