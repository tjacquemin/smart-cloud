#include "MPI_Solver.h"

MPI_Solver::MPI_Solver() {
}

MPI_Solver::MPI_Solver(const MPI_Solver& orig) {
}

MPI_Solver::~MPI_Solver() {
}

void MPI_Solver::Solve(Model* M,LinearProblem* L, int rankTemp, int sizeTemp)
{
    //Set the Values of the rank and size
    rank=rankTemp;
    size=sizeTemp;
    comm=MPI_COMM_WORLD;
    if (rankTemp==0)
    {
        M->Print("Load data to each processor.",true);
    }
    PetscLogStageRegister("Initialization", &stage1);
    PetscLogStagePush(stage1);
    
    //Load the model information to each process and create the matrices
    LoadProcessData(M,L);
    InitializeMatrices();
    
//    //Get the initial matrix range before partitioning
    GetMatrixRanges();
    PetscLogStagePop();
    
    PetscLogStageRegister("Assembly", &stage2);
    PetscLogStagePush(stage2);
    
    if (rankTemp==0)
    {
        M->Print("Split the stiffness matrix triplet vectors.",true);
    }
//    SplitNodeCoordinates(M);
    SplitForceVector(L);
    SplitTriplet(M,L,&LocTripletLength);
        
    if (SolverPackage=="HYPRE")
    {
        LoadHypreParameters(M);
        CreateHypreMatrix();
        LoadHypreMatrix();
        CreateLoadHypreForce();
        SolveHypre(M);
        ReturnSolverInfoHypre(L);
    }
    
    if (SolverPackage=="PETSC")
    {
        //Compute the adjacency matrix
        if (Mat_Partitioning==1)
        {
            ComputeAdjacencyTriplets(M);
            LoadAdjacencyMatrix();
        }
        if (rankTemp==0)
        {
            M->Print("Start filling Matrices",true);
        }
        FillMatrices();
        
        //Split and load the Null Space Matrix
        if (Mat_NullSpace==1)
        {
            SplitCoordVector(L);
        }

        PetscLogStagePop();

        PetscLogStageRegister("Solution", &stage3);
        PetscLogStagePush(stage3);
        if (rankTemp==0)
        {
            M->Print("Matrices Filled",true);
        }
        if (SolverType=="KSP")
        {
            SolveGMRES(M,L);
        }
        else if (SolverType=="DIRECT_MUMPS")
        {
            SolveMUMPS();
        }
        FillUVectLoc();
        PetscLogStagePop();
    }
    ReturnResult(L);
}

void MPI_Solver::LoadProcessData(Model* M, LinearProblem* L)
{
    //Send-Receive the triplet arrays to each processor
    MPI_Status status;
    if (rank==0)
    {
        Dimension=M->Dimension;NodeNumber=M->NodeNumber;
        SolverPackage=M->SolverPackage;SolverType=M->SolverType;SolverPC=M->SolverPC;
        SolverKSP=M->SolverKSP;SolverTol=M->SolverTol;
        SolverMaxIt=M->SolverMaxIt;
        AMG_Threshold=M->AMG_Threshold;AMG_Levels=M->AMG_Levels;AMG_SmoothN=M->AMG_SmoothN;AMG_CycleN=M->AMG_CycleN;
        Mat_Partitioning=M->Mat_Partitioning;Mat_Ordering=M->Mat_Ordering;Mat_NullSpace=M->Mat_NullSpace;
        AdjThreshold=M->AdjThreshold;
        FilePath=M->InputFilePath;PrintMat=M->PrintMat;PrintConditionNumber=M->PrintConditionNumber;
        for (int DestRank=1;DestRank<size;DestRank++)
        {
            MPI_Send(&M->Dimension, 1, MPI_INT, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(&M->NodeNumber, 1, MPI_LONG, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(&TripStiffLength, 1, MPI_LONG, DestRank, 123, MPI_COMM_WORLD);
            
            int StrLength;
            StrLength=SolverPackage.size();
            MPI_Send(&StrLength, 1, MPI_INT, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(SolverPackage.c_str(), StrLength, MPI_CHAR, DestRank, 123, MPI_COMM_WORLD);
            StrLength=SolverType.size();
            MPI_Send(&StrLength, 1, MPI_INT, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(SolverType.c_str(), StrLength, MPI_CHAR, DestRank, 123, MPI_COMM_WORLD);
            StrLength=SolverPC.size();
            MPI_Send(&StrLength, 1, MPI_INT, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(SolverPC.c_str(), StrLength, MPI_CHAR, DestRank, 123, MPI_COMM_WORLD);
            StrLength=SolverKSP.size();
            MPI_Send(&StrLength, 1, MPI_INT, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(SolverKSP.c_str(), StrLength, MPI_CHAR, DestRank, 123, MPI_COMM_WORLD);

            MPI_Send(&SolverTol, 1, MPI_DOUBLE, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(&SolverMaxIt, 1, MPI_LONG, DestRank, 123, MPI_COMM_WORLD);
            
            MPI_Send(&AMG_Threshold, 1, MPI_DOUBLE, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(&AMG_Levels, 1, MPI_INT, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(&AMG_SmoothN, 1, MPI_INT, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(&AMG_CycleN, 1, MPI_INT, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(&Mat_Partitioning, 1, MPI_INT, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(&Mat_Ordering, 1, MPI_INT, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(&Mat_NullSpace, 1, MPI_INT, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(&AdjThreshold, 1, MPI_DOUBLE, DestRank, 123, MPI_COMM_WORLD);
            
            StrLength=FilePath.size();
            MPI_Send(&StrLength, 1, MPI_INT, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(FilePath.c_str(), StrLength, MPI_CHAR, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(&PrintMat, 1, MPI_INT, DestRank, 123, MPI_COMM_WORLD);
            MPI_Send(&PrintConditionNumber, 1, MPI_INT, DestRank, 123, MPI_COMM_WORLD);
        }
    }
    else
    {
        MPI_Recv(&Dimension, 1, MPI_INT, 0, 123, MPI_COMM_WORLD,&status);
        MPI_Recv(&NodeNumber, 1, MPI_LONG, 0, 123, MPI_COMM_WORLD,&status);
        MPI_Recv(&TripStiffLength, 1, MPI_LONG, 0, 123, MPI_COMM_WORLD,&status);
        
        int StrLength;
        MPI_Recv(&StrLength, 1, MPI_INT, 0, 123, MPI_COMM_WORLD,&status);
        char* TempChar0;TempChar0= new char[StrLength];
        MPI_Recv(TempChar0, StrLength, MPI_CHAR, 0, 123, MPI_COMM_WORLD,&status);
        SolverPackage=string(TempChar0,StrLength);
        delete[] TempChar0;
        MPI_Recv(&StrLength, 1, MPI_INT, 0, 123, MPI_COMM_WORLD,&status);
        char* TempChar1;TempChar1= new char[StrLength];
        MPI_Recv(TempChar1, StrLength, MPI_CHAR, 0, 123, MPI_COMM_WORLD,&status);
        SolverType=string(TempChar1,StrLength);
        delete[] TempChar1;
        MPI_Recv(&StrLength, 1, MPI_INT, 0, 123, MPI_COMM_WORLD,&status);
        char* TempChar2;TempChar2= new char[StrLength];
        MPI_Recv(TempChar2, StrLength, MPI_CHAR, 0, 123, MPI_COMM_WORLD,&status);
        SolverPC=string(TempChar2,StrLength);
        delete[] TempChar2;
        MPI_Recv(&StrLength, 1, MPI_INT, 0, 123, MPI_COMM_WORLD,&status);
        char* TempChar3;TempChar3= new char[StrLength];
        MPI_Recv(TempChar3, StrLength, MPI_CHAR, 0, 123, MPI_COMM_WORLD,&status);
        SolverKSP=string(TempChar3,StrLength);
        delete[] TempChar3;
        
        MPI_Recv(&SolverTol, 1, MPI_DOUBLE, 0, 123, MPI_COMM_WORLD,&status);
        MPI_Recv(&SolverMaxIt, 1, MPI_LONG, 0, 123, MPI_COMM_WORLD,&status);
        
        MPI_Recv(&AMG_Threshold, 1, MPI_DOUBLE, 0, 123, MPI_COMM_WORLD,&status);
        MPI_Recv(&AMG_Levels, 1, MPI_INT, 0, 123, MPI_COMM_WORLD,&status);
        MPI_Recv(&AMG_SmoothN, 1, MPI_INT, 0, 123, MPI_COMM_WORLD,&status);
        MPI_Recv(&AMG_CycleN, 1, MPI_INT, 0, 123, MPI_COMM_WORLD,&status);
        MPI_Recv(&Mat_Partitioning, 1, MPI_INT, 0, 123, MPI_COMM_WORLD,&status);
        MPI_Recv(&Mat_Ordering, 1, MPI_INT, 0, 123, MPI_COMM_WORLD,&status);
        MPI_Recv(&Mat_NullSpace, 1, MPI_INT, 0, 123, MPI_COMM_WORLD,&status);
        MPI_Recv(&AdjThreshold, 1, MPI_DOUBLE, 0, 123, MPI_COMM_WORLD,&status);
        
        MPI_Recv(&StrLength, 1, MPI_INT, 0, 123, MPI_COMM_WORLD,&status);
        char* TempChar4;TempChar4= new char[StrLength];
        MPI_Recv(TempChar4, StrLength, MPI_CHAR, 0, 123, MPI_COMM_WORLD,&status);
        FilePath=string(TempChar4,StrLength);
        delete[] TempChar4;
        MPI_Recv(&PrintMat, 1, MPI_INT, 0, 123, MPI_COMM_WORLD,&status);
        MPI_Recv(&PrintConditionNumber, 1, MPI_INT, 0, 123, MPI_COMM_WORLD,&status);
    }
}

void MPI_Solver::InitializeMatrices()
{   
    PetscInt SizeStiff=Dimension*NodeNumber;
    PetscMPIInt SizeMPI=SizeStiff;
    MPI_Comm_size(comm,&SizeMPI);
    
    //Set the block size
    PetscInt bs=1;
    
    //Create the striffness matrix
    MatCreate(comm,&StiffMat);
    
    //Block matrices are not supported by AMG methods (GAMG at least)
    MatSetType(StiffMat,MATMPIAIJ);
    
    MatSetBlockSize(StiffMat,bs);
    MatSetSizes(StiffMat,PETSC_DECIDE,PETSC_DECIDE,SizeStiff,SizeStiff);
    MatSetFromOptions(StiffMat);
    MatSetUp(StiffMat);
    
    PetscInt IstartTemp,IendTemp;
    MatGetOwnershipRange(StiffMat,&IstartTemp,&IendTemp);
    //Convert PetscInt into long
    Istart=IstartTemp;Iend=IendTemp;
    
    //Create the vectors
    PetscInt LocSize=Iend-Istart;
    VecCreate(comm,&U);
    VecSetType(U,VECMPI);
    VecSetSizes(U,LocSize,SizeStiff);
    VecSetBlockSize(U,bs);
    VecSetFromOptions(U);
    
    VecCreate(comm,&b);
    VecSetType(b,VECMPI);
    VecSetSizes(b,LocSize,SizeStiff);
    VecSetBlockSize(b,bs);
    VecSetFromOptions(b);
    
    if (Mat_NullSpace==1)
    {
        VecCreate(comm,&Coord);
        VecSetType(Coord,VECMPI);
        VecSetSizes(Coord,LocSize,Dimension*NodeNumber);
        VecSetBlockSize(Coord,bs);
        VecSetFromOptions(Coord);
    }
}

void MPI_Solver::GetMatrixRanges()
{
    MPI_Status status;
    if (rank==0)
    {
        StartRangeArray = new long[size];
        EndRangeArray = new long[size];
        StartRangeArray[0]=Istart;
        EndRangeArray[0]=Iend;
        for (int IncRank=1;IncRank<size;IncRank++)
        {           
            MPI_Recv(&StartRangeArray[IncRank], 1, MPI_LONG, IncRank, 123, MPI_COMM_WORLD,&status);
            MPI_Recv(&EndRangeArray[IncRank], 1, MPI_LONG, IncRank, 123, MPI_COMM_WORLD,&status);
        }
    }
    else
    {
        MPI_Send(&Istart, 1, MPI_LONG, 0, 123, MPI_COMM_WORLD);
        MPI_Send(&Iend, 1, MPI_LONG, 0, 123, MPI_COMM_WORLD);
    }
}

void MPI_Solver::SplitForceVector(LinearProblem* L)
{
    MPI_Status status;
    if (rank==0)
    {
        //Resize the MPI triplet vector for the master node
        F_VecAll=L->F_Vec;
        //Send the portion of the main triplet vector to each process
        for (int i=1;i<size;i++)
        {
            MPI_Send(L->F_Vec, Dimension*NodeNumber, MPI_DOUBLE, i, 123, MPI_COMM_WORLD);
        }
    }
    else
    {
        F_VecAll=new double[Dimension*NodeNumber];
        MPI_Recv(F_VecAll, Dimension*NodeNumber, MPI_DOUBLE, 0, 123, MPI_COMM_WORLD,&status);
    }
}

void MPI_Solver::SplitCoordVector(LinearProblem* L)
{
    MPI_Status status;
    if (rank==0)
    {
        //Resize the MPI triplet vector for the master node
        CoordAll=L->Coord_Vec;
        //Send the portion of the main triplet vector to each process
        for (int i=1;i<size;i++)
        {
            MPI_Send(L->Coord_Vec, Dimension*NodeNumber, MPI_DOUBLE, i, 123, MPI_COMM_WORLD);
        }
    }
    else
    {
        CoordAll=new double[Dimension*NodeNumber];
        MPI_Recv(CoordAll, Dimension*NodeNumber, MPI_DOUBLE, 0, 123, MPI_COMM_WORLD,&status);
    }
}

void MPI_Solver::FillMatrices()
{      
    //Fill the triplet vector
    PetscInt* RowVect;
    PetscInt* ColumnVect;
    PetscScalar* ValVect;
    
    RowVect = new PetscInt[LocTripletLength];
    ColumnVect = new PetscInt[LocTripletLength];
    ValVect = new PetscScalar[LocTripletLength];
    
    for (PetscInt i=0; i<LocTripletLength; i++)
    {
        RowVect[i]=TripStiff[i].row()-Istart;
        ColumnVect[i]=TripStiff[i].col();
        ValVect[i]=TripStiff[i].value();
    }
    
    Mat ProcessStiffMat;
    MatCreateSeqAIJFromTriple(MPI_COMM_SELF,Iend-Istart,Dimension*NodeNumber,RowVect,ColumnVect,ValVect,&ProcessStiffMat,LocTripletLength,PETSC_FALSE);
    MatSetFromOptions(ProcessStiffMat);
    MatSetUp(ProcessStiffMat);
    MatAssemblyBegin(ProcessStiffMat,MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(ProcessStiffMat,MAT_FINAL_ASSEMBLY);
    
    MatCreateMPIMatConcatenateSeqMat(comm,ProcessStiffMat,PETSC_DECIDE,MAT_INITIAL_MATRIX,&StiffMat);
    
    MatAssemblyBegin(StiffMat,MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(StiffMat,MAT_FINAL_ASSEMBLY);  
    
    MatDestroy(&ProcessStiffMat);
    delete [] TripStiff;
    delete[] RowVect;
    delete[] ColumnVect;
    delete[] ValVect;
}

void MPI_Solver::SplitTriplet(Model* M, LinearProblem* L, long* LocTripSize)
{
    //Create the MPI triplet struct
    MPI_Datatype MPI_Triplet;
    const int nitems=3;
    int blocklengths[3] = {1,1,1};
    MPI_Aint offsets[3];
    offsets[0] = offsetof(Triplets, row);
    offsets[1] = offsetof(Triplets, col);
    offsets[2] = offsetof(Triplets, val);
    MPI_Datatype types[3] = {MPI_INT,MPI_INT,MPI_DOUBLE};
    
    // Call the data type constructor
    MPI_Type_struct(nitems, blocklengths, offsets, types, &MPI_Triplet);
    MPI_Type_commit(&MPI_Triplet);
    
    MPI_Status status;
    long* StartTripRange=NULL;
    long* EndTripRange=NULL;
    if (rank==0)
    {
        StartTripRange=new long[size];
        EndTripRange=new long[size];
        
        vector<vector<int>> StartRow;
        vector<vector<int>> EndRow;
        StartRow.resize(size);
        EndRow.resize(size);
        for (int i=0; i<size; i++)
        {
            StartRow[i].resize(2);
            EndRow[i].resize(2);
        }
        
        //Get for each process the start and end triplet row
        int ThreadIndex=0;
        long TripIndex=0;
        for (int i=0; i<size; i++)
        {
            //Get the rows owned by the process TempRank (based on Hypre ex11.c)
            long TempNrows = (NodeNumber*Dimension)/size;
            long extra = (NodeNumber*Dimension) - TempNrows*size;
            long Tempilower = TempNrows*i;
            Tempilower += my_min(i, extra);
            long Tempiupper = TempNrows*(i+1);
            Tempiupper += my_min(i+1, extra);
            Tempiupper = Tempiupper - 1;
            TempNrows=Tempiupper-Tempilower+1;
                        
            bool Found=false;
            while (Found==false)
            {
                if (L->StiffRows[ThreadIndex][TripIndex].Trip[0].row()==Tempilower)
                {
                    StartRow[i][0]=ThreadIndex;
                    StartRow[i][1]=TripIndex;
                }
                if (L->StiffRows[ThreadIndex][TripIndex].Trip[0].row()==Tempiupper)
                {
                    EndRow[i][0]=ThreadIndex;
                    EndRow[i][1]=TripIndex;
                    Found=true;
                }
                TripIndex++;
                if (TripIndex>=L->StiffRows[ThreadIndex].size())
                {
                    ThreadIndex++;
                    TripIndex=0;
                }
            }
        }     
        
        //Load the process triplet vectors
        for (int Proi=0;Proi<size;Proi++)
        {
            //Get the size of the triplet
            long TempLocTripSize=0;
            
            //Loop through all the threads associated to the considered process
            for (int Thrj=StartRow[Proi][0]; Thrj<=EndRow[Proi][0]; Thrj++)
            {
                //Get the start row for the considered thread
                int FirstRow=0;
                if (Thrj==StartRow[Proi][0])
                    FirstRow=StartRow[Proi][1];
                
                //Get the last row for the considered thread
                int LastRow=L->StiffRows[Thrj].size()-1;
                if (Thrj==EndRow[Proi][0])
                    LastRow=EndRow[Proi][1];
                
                //Count the number of triplets in the considered range
                for (int k=FirstRow; k<=LastRow; k++)
                {
                    TempLocTripSize+=L->StiffRows[Thrj][k].Trip.size();
                }
            }
            
            //Resize the MPI triplet vector
            T* TempTrip;
            if (Proi==0)
            {
                //Resize the MPI triplet vector for the master node
                *LocTripSize=TempLocTripSize;
                TripStiff= new T[TempLocTripSize];
            }
            else
            {
                TempTrip= new T[TempLocTripSize];
            }
            
            //Fill the triple vectors
            long count=0;
            for (int Thrj=StartRow[Proi][0]; Thrj<=EndRow[Proi][0]; Thrj++)
            {
                //Get the start row for the considered thread
                int FirstRow=0;
                if (Thrj==StartRow[Proi][0])
                    FirstRow=StartRow[Proi][1];
                
                //Get the last row for the considered thread
                int LastRow=L->StiffRows[Thrj].size()-1;
                if (Thrj==EndRow[Proi][0])
                    LastRow=EndRow[Proi][1];
                
                //Count the number of triplets in the considered range
                for (int k=FirstRow; k<=LastRow; k++)
                {
                    for (int l=0; l<L->StiffRows[Thrj][k].Trip.size(); l++)
                    {
                        if (Proi==0)
                            TripStiff[count]=L->StiffRows[Thrj][k].Trip[l];
                        else
                            TempTrip[count]=L->StiffRows[Thrj][k].Trip[l];
                        count++;
                    }
                }
            }
            
            //Send the portion of the main triplet vector to each process
            if (Proi>0)
            {
                MPI_Send(&TempLocTripSize, 1, MPI_LONG, Proi, 123, MPI_COMM_WORLD);
                MPI_Send(TempTrip, TempLocTripSize, MPI_Triplet, Proi, 123, MPI_COMM_WORLD);
                delete [] TempTrip;
            }
        }
        
        //Clear the stifness triplet
        M->Print("Starting clearing the stiffness triplets.",true);
        L->StiffRows.clear();
        L->StiffRows.shrink_to_fit();
        M->Print("Stiffness  cleared.",true);
    }
    else
    {
        long TempSize;
        MPI_Recv(&TempSize, 1, MPI_LONG, 0, 123, MPI_COMM_WORLD,&status);
        *LocTripSize=TempSize;
        TripStiff=new T[*LocTripSize];
        MPI_Recv(TripStiff, *LocTripSize, MPI_Triplet, 0, 123, MPI_COMM_WORLD,&status);
    }
}

void MPI_Solver::SolveGMRES(Model* M, LinearProblem* L)
{
    KSP ksp;
    PC pc;
    
    //Permutation variables
    IS rowperm=NULL,colperm=NULL;
    Mat StiffMat_Petsc_perm;
    
    //Partitioning variables
    MatPartitioning part;
    IS is,isn,isrows;
    Vec x,u;
    
    if(Mat_Partitioning==1)
    {        
        MatPartitioningCreate(comm, &part);
        MatPartitioningSetAdjacency(part, AdjMatrix);
        MatPartitioningSetType(part,MATPARTITIONINGHIERARCH);
        MatPartitioningHierarchicalSetNcoarseparts(part,2);
        MatPartitioningHierarchicalSetNfineparts(part,2);
        MatPartitioningSetFromOptions(part);
        MatPartitioningApply(part, &is);
        
        //Get new global number of each old global number
        ISPartitioningToNumbering(is,&isn);
        ISBuildTwoSided(is,NULL,&rowperm);
        MatCreateSubMatrix(StiffMat,rowperm,rowperm,MAT_INITIAL_MATRIX,&StiffMat_Petsc_perm);
        MatCreateVecs(StiffMat_Petsc_perm,&b,NULL);
        VecSetFromOptions(b);
        VecDuplicate(b,&U);
                
        MatDestroy(&StiffMat);
        StiffMat = StiffMat_Petsc_perm;
                
        //Get the indices resulting from the ordering
//        const PetscInt *nindices;
//        ISGetIndices(colperm,&nindices);
        //Get the indices resulting from the partitioning
        const PetscInt* nindices_Temp;
        ISGetIndices(isn,&nindices_Temp);
        PetscInt LocalSize;
        ISGetLocalSize(isn,&LocalSize);
//        nindices_Ordering = new long[Iend-Istart];
        nindices_Partitioning = new long[Iend-Istart];
        for (long i=0; i<Iend-Istart; i++)
        {
//            nindices_Ordering[i]=nindices[i];
            nindices_Partitioning[i]=nindices_Temp[i];
        }
        
        //Split the force vector based on the new indices
        ShareNewOrdering(L);
        
        //Get the new range owned by each processes
        PetscInt IstartTemp,IendTemp;
//        MatGetOwnershipRange(StiffMat,&IstartTemp,&IendTemp);
//        //Convert PetscInt into long
//        Istart=IstartTemp;Iend=IendTemp;
        //Update the matrix range after matrix partitioning
        GetMatrixRanges();
    }
    
    if (Mat_Ordering==1)
    {
        //Set ordering
        char ordering[256] = MATORDERINGRCM;
        MatGetOrdering(StiffMat,ordering,&rowperm,&colperm);
        MatPermute(StiffMat,rowperm,colperm,&StiffMat_Petsc_perm);
        MatDestroy(&StiffMat);
        StiffMat = StiffMat_Petsc_perm;
    }
    
    //Fill the vector b based on the current partitioning
    FillVector_b();
    
    //Permut the vector b if matrix partitioning has not been applied
    if (Mat_Ordering==1)
    {
        VecPermute(b,colperm,PETSC_FALSE);
    }
    
    //Create the solver
    KSPCreate(comm,&ksp);
    KSPSetOperators(ksp,StiffMat,StiffMat);
    
    MatNullSpace nullspace;
    if (Mat_NullSpace==1)
    {
        MatNullSpaceCreateRigidBody(Coord,&nullspace);
        MatSetNearNullSpace(StiffMat,nullspace);
        MatNullSpaceDestroy(&nullspace);
    }
    
    KSPGetPC(ksp,&pc);   
    if (rank==0)
    {
        M->Print("Setting the preconditioner.",true);
    }
    SetPreconditioner(&pc);
    if (rank==0)
    {
        M->Print("Solving the Linear system ...",true);
    }
    if (SolverKSP=="GMRES") 
    {
        KSPSetType(ksp,KSPGMRES);
    }
    else if (SolverKSP=="BCGS") 
    {
        KSPSetType(ksp,KSPBCGS);
    }
    else if (SolverKSP=="BCGSL") 
    {
        KSPSetType(ksp,KSPBCGSL);
    }
    
    if (PrintConditionNumber==1)
    {
        KSPSetComputeSingularValues(ksp, PETSC_TRUE);
    }
    
    KSPSetTolerances(ksp,SolverTol,PETSC_DEFAULT,PETSC_DEFAULT,SolverMaxIt);
    KSPSetUp(ksp);
    KSPSetFromOptions(ksp);
    
    //Print the output to a file
    fpos_t pos;
    fgetpos(stdout, &pos);
    int fd = dup(fileno(stdout));
    if (rank == 0)
    {
        string Path=M->InputFilePath;
        Path.replace(Path.end()-3,Path.end(),"txt");
        char* PathChar=new char[Path.length()+1];
        strcpy(PathChar,Path.c_str());
        std::freopen(PathChar, "w", stdout);
    }
    KSPView(ksp,PETSC_VIEWER_STDOUT_WORLD);
             
    KSPSolve(ksp,b,U);
    
    if (rank == 0)
    {
        fflush(stdout);
        //Close file and restore standard output to stdout - which should be the terminal
        dup2(fd, fileno(stdout));
        close(fd);
        clearerr(stdout);
        fsetpos(stdout, &pos);
    }
    
    //Permute the solution vector
    if (Mat_Ordering==1)
    {
        VecPermute(U,rowperm,PETSC_TRUE);
    }
    
    //Create two viewers and print the solver info in them
    char kspinfo[120];
    PetscViewer viewer;
    //Open a string viewer; then write info to it.
    PetscViewerStringOpen(comm,kspinfo,120,&viewer);
//    KSPView(ksp,viewer);
//    PetscViewerDestroy(&viewer);
//    
//    //The second viewer is used to print the results into a file
//    PetscViewer viewer2;
//    PetscViewerCreate(PETSC_COMM_WORLD, &viewer2);
//    PetscViewerSetType(viewer2, PETSCVIEWERASCII); 
//    PetscViewerFileSetMode(viewer2, FILE_MODE_WRITE);
//    //Set the path of the output file
//    string PathViewer= FilePath;
//    PathViewer.replace(PathViewer.end()-3,PathViewer.end(),"vwr");
//    char* PathViewerChar=new char[PathViewer.length()+1];
//    strcpy(PathViewerChar,PathViewer.c_str());
//    PetscViewerFileSetName(viewer2, PathViewerChar);
//    KSPView(ksp,viewer2);
//    PetscViewerDestroy(&viewer2);
//    
    if (rank==0)
    {
        L->SolverConvergenceStatus=kspinfo;
        KSPGetIterationNumber(ksp,&L->SolverIterationNum);
        KSPConvergedReason IterationStopReason;
        KSPGetConvergedReason(ksp,&IterationStopReason);
        L->SetConvergenceReason(IterationStopReason);
        
        //Get the condition number of the matrix
        if (PrintConditionNumber==1)
        {
            PetscReal emax, emin;
            KSPComputeExtremeSingularValues(ksp, &emax, &emin);
            L->ConditionNumber=emax / emin;
        }
    }
//    
//    if (PrintMat==1)
//    {
//        PetscViewer viewer2;
//        PetscDraw draw; 
//        string Path= FilePath;
//        Path.replace(Path.end()-3,Path.end(),"ppm");
//        char* PathChar=new char[Path.length()+1];
//        strcpy(PathChar,Path.c_str());
//        PetscViewerDrawOpen(comm,0,"",0,0,600,600,&viewer2);
//        MatView(StiffMat,viewer2);
//        PetscViewerDrawGetDraw(viewer2,0,&draw);
//        PetscDrawSetSaveFinalImage(draw,PathChar);
//        PetscDrawSetSave(draw,PathChar);
//        PetscViewerDestroy(&viewer2);
//    }
    
    VecAssemblyBegin(U);
    VecAssemblyEnd(U);
    KSPDestroy(&ksp);
    VecDestroy(&b);
    ISDestroy(&rowperm);
    ISDestroy(&colperm);
    MatDestroy(&StiffMat);
}

void MPI_Solver::SolveMUMPS()
{
    KSP ksp;
    PC pc;
    
    KSPCreate(PETSC_COMM_WORLD,&ksp);
    KSPSetOperators(ksp,StiffMat,StiffMat);
    KSPSetType(ksp,KSPPREONLY);
    KSPGetPC(ksp,&pc);
    SetPreconditioner(&pc);
    PCSetType(pc,PCLU);
    
    PCFactorSetMatSolverType(pc,MATSOLVERMUMPS);
    PCFactorSetUpMatSolverType(pc);
    
    KSPSetNormType(ksp,KSP_NORM_UNPRECONDITIONED);
    KSPSetFromOptions(ksp); 
    KSPSetUp(ksp);
    
    PCFactorGetMatrix(pc,&F);
    MatMumpsSetCntl(F,3,1.e-6);
    MatMumpsSetIcntl(F,8,77);
    //Threshhold for row pivot detection
    MatMumpsSetIcntl(F,24,1);
    //Parallel Ordering
    MatMumpsSetIcntl(F,28,2);
    MatMumpsSetIcntl(F,29,0);
    
    FillVector_b(); 
    KSPSolve(ksp,b,U);
    
    VecAssemblyBegin(U);
    VecAssemblyEnd(U);
    
//    PCDestroy(&pc);
    KSPDestroy(&ksp);
}

void MPI_Solver::SetPreconditioner(PC* pc)
{
    if (SolverPC=="JACOBI")
    {
        PCSetType(*pc,PCBJACOBI);
//        PCSetType(pc,PCGASM);
    }
    else if (SolverPC=="SOR")
    {
        PCSetType(*pc,PCSOR);
    }
    else if (SolverPC=="ML")
    {
        PCSetType(*pc,PCML);
        PetscOptionsSetValue(NULL,"-pc_ml_cycles","2");
        PetscOptionsSetValue(NULL,"-pc_ml_Threshold","0.0001");
        PetscOptionsSetValue(NULL,"-pc_ml_maxNlevels","10");
        PetscOptionsSetValue(NULL,"-pc_ml_maxCoarseSize","2");
    }
    else if (SolverPC=="BOOMERAMG")
    {
//        MatNullSpace nullspace;
//        MatNullSpaceCreate(PETSC_COMM_WORLD,PETSC_TRUE,0,0,&nullspace);
//        MatSetNullSpace(StiffMat,nullspace);
        
        PCSetType(*pc,PCHYPRE);
        PCHYPRESetType(*pc,"boomeramg");
        
//        char ThresholdStr[10];
//        AMG_Threshold=0.0001;
//        sprintf(ThresholdStr, "%f", (double)0.25);
//        PetscOptionsSetValue(NULL,"-pc_hypre_boomeramg_strong_threshold","0.000001");
//        PetscOptionsSetValue(NULL,"-pc_hypre_boomeramg_agg_nl","4");
//        PetscOptionsSetValue(NULL,"-pc_hypre_boomeramg_max_iter","10");
//        PetscOptionsSetValue(NULL,"-pc_hypre_boomeramg_tol","0.000001");
//        PetscOptionsSetValue(NULL,"-pc_hypre_boomeramg_coarsen_type","Ruge-Stueben");
//        PetscOptionsSetValue(NULL,"-pc_hypre_boomeramg_measure_type","global");
//        PetscOptionsSetValue(NULL,"-pc_hypre_boomeramg_nodal_coarsen","3");
//        PetscOptionsSetValue(NULL,"-pc_hypre_boomeramg_eu_level","2");
//        PetscOptionsSetValue(NULL,"-pc_hypre_boomeramg_max_levels","25");
//        PetscOptionsSetValue(NULL,"-pc_hypre_boomeramg_max_row_sum","0.2");
//        PetscOptionsSetValue(NULL,"-pc_hypre_boomeramg_relax_type_all","Jacobi");
//        PetscOptionsSetValue(NULL,"-pc_hypre_boomeramg_coarsen_type","Jacobi");
//        PetscOptionsSetValue(NULL,"-pc_hypre_boomeramg_vec_interp_variant","1");
//        
////        PetscOptionsSetValue(NULL,"-ksp_max_it","100");
//        PetscOptionsSetValue(NULL,"-ksp_rtol","1.0e-7");
//        
//        PetscReal dvalue;
//        PetscBool  set;
//        PetscOptionsGetReal(NULL,NULL,"-pc_hypre_boomeramg_strong_threshold",&dvalue,&set);
//        cout << "SetThreshold=" << dvalue << endl; 
//        PetscOptionsGetReal(NULL,NULL,"-pc_hypre_boomeramg_max_iter",&dvalue,&set);
//        cout << "Max Iteration=" << dvalue << endl; 
//        PetscOptionsGetReal(NULL,NULL,"-pc_hypre_boomeramg_tol",&dvalue,&set);
//        cout << "Solver Tol.=" << dvalue << endl;
    }
    else if (SolverPC=="GAMG")
    {
//        PetscInt ThresholdInt=1;
//        PCSetType(*pc,PCGAMG);
        PCSetType(*pc,PCGAMG);
        PCGAMGSetSymGraph(*pc,PETSC_TRUE);
        
        //Set the block size
        if (Dimension==2)
            PetscOptionsSetValue(NULL,"–mat_block_size","2");
        else if (Dimension==3)
            PetscOptionsSetValue(NULL,"–mat_block_size","3");
        
        PetscReal* TempThresholdArray;
        PetscInt ThresholdInt=1;
        TempThresholdArray = new PetscReal[ThresholdInt];
        TempThresholdArray[0]=AMG_Threshold;
        PCGAMGSetThreshold(*pc,TempThresholdArray,ThresholdInt);
        delete[] TempThresholdArray;
        
        // PCMGMultiplicativeSetCycles(*pc,5); => this command does not work and shall be applied at run time using -pc_mg_multiplicative_cycles 5
        string TempCyclNumStr= to_string(AMG_CycleN);  
        char const *TempCyclNum = TempCyclNumStr.c_str();
        PetscOptionsSetValue(NULL,"-pc_mg_multiplicative_cycles",TempCyclNum);
        
        if (AMG_Levels>0) PCGAMGSetNlevels(*pc,AMG_Levels);
        if (AMG_SmoothN > 0) PCGAMGSetNSmooths(*pc,AMG_SmoothN);
        
//        PCGAMGSetType(*pc,PCGAMGAGG); 
//        PCGAMGSetSymGraph(*pc,PETSC_TRUE);
//        PCGAMGSetThreshold(*pc,&AMG_Threshold,ThresholdInt);
//        PCGAMGSetSquareGraph(*pc,1);
//        PCGAMGASMSetUseAggs(*pc,PETSC_TRUE);
//        PCMGSetLevels(*pc,AMG_Levels,NULL);
//        if (AMG_Levels>0) PCGAMGSetNlevels(*pc,AMG_Levels);
//        if (AMG_SmoothN > 0) PCGAMGSetNSmooths(*pc,AMG_SmoothN);
//        PetscInt levels=20;
//        PCMGSetLevels(*pc,levels,NULL); 
//        PCMGSetCycleType(*pc,PC_MG_CYCLE_W);
//        PCGAMGSetRepartition(pc,PETSC_FALSE);
//        PCGAMGSetReuseInterpolation(pc,PETSC_FALSE);
    }
    else if (SolverPC=="SOR")
    {
        PCSetType(*pc,PCSOR);
    }
    else if (SolverPC=="ILU")
    {
        PCSetType(*pc,PCHYPRE);
        PCHYPRESetType(*pc,"pilut");
        PetscOptionsSetValue(NULL,"-pc_hypre_pilut_factorrowsize","350");
    }
    
    PCSetFromOptions(*pc);
    PCSetUp(*pc);
}

void MPI_Solver::ShareNewOrdering(LinearProblem* L)
{
    MPI_Status status;
//    nindices_OrdGlob = new long[NodeNumber*Dimension];
    nindices_PartGlob = new long[NodeNumber*Dimension];
    
    //Receive all the local values on the master process
    if (rank==0)
    {
        for (PetscInt i=0; i<Iend-Istart; i++)
        {
//            nindices_OrdGlob[Istart+i]=nindices_Ordering[i];
            nindices_PartGlob[Istart+i]=nindices_Partitioning[i];
        }
        for (PetscInt IncRank=1;IncRank<size;IncRank++)
        {  
//            long* TempVect1;
            long* TempVect2;
//            TempVect1=new long[EndRangeArray[IncRank]-StartRangeArray[IncRank]];
            TempVect2=new long[EndRangeArray[IncRank]-StartRangeArray[IncRank]];
//            MPI_Recv(TempVect1,EndRangeArray[IncRank]-StartRangeArray[IncRank], MPI_LONG, IncRank, 123, MPI_COMM_WORLD,&status);
            MPI_Recv(TempVect2,EndRangeArray[IncRank]-StartRangeArray[IncRank], MPI_LONG, IncRank, 123, MPI_COMM_WORLD,&status);
            for (long i=StartRangeArray[IncRank]; i<EndRangeArray[IncRank]; i++)
            {
//                nindices_OrdGlob[i]=TempVect1[i-StartRangeArray[IncRank]];
                nindices_PartGlob[i]=TempVect2[i-StartRangeArray[IncRank]];
            }
//            delete [] TempVect1;
            delete [] TempVect2;
        }
    }
    //Send the local values to the master process
    else
    {
//        MPI_Send(nindices_Ordering, Iend-Istart, MPI_LONG, 0, 123, MPI_COMM_WORLD);
        MPI_Send(nindices_Partitioning, Iend-Istart, MPI_LONG, 0, 123, MPI_COMM_WORLD);
    }

    //Send the array of all indices to all processes
    if (rank==0)
    {
        for (PetscInt IncRank=1;IncRank<size;IncRank++)
        {
//            MPI_Send(nindices_OrdGlob, Dimension*NodeNumber, MPI_LONG, IncRank, 123, MPI_COMM_WORLD);
            MPI_Send(nindices_PartGlob, Dimension*NodeNumber, MPI_LONG, IncRank, 123, MPI_COMM_WORLD);
        }
    }
    else
    {
//        MPI_Recv(nindices_OrdGlob, Dimension*NodeNumber, MPI_LONG, 0, 123, MPI_COMM_WORLD,&status);
        MPI_Recv(nindices_PartGlob, Dimension*NodeNumber, MPI_LONG, 0, 123, MPI_COMM_WORLD,&status);
    }
    //Load the global opposite matrix
//    nindices_OrdOpp = new long[Dimension*NodeNumber];
    nindices_PartOpp = new long[Dimension*NodeNumber];
    for (long i=0; i<Dimension*NodeNumber; i++)
    {
//        nindices_OrdOpp[nindices_OrdGlob[i]]=i;
        nindices_PartOpp[nindices_PartGlob[i]]=i;
    }
}

void MPI_Solver::FillVector_b()
{
    //Insert the values 
    if (Mat_Partitioning==1)
    {
        for (PetscInt i=Istart; i<Iend; i++)
        {
            //VecSetValues(b,1,&i,&F_VecAll[nindices_OrdGlob[nindices_PartOpp[i]]],INSERT_VALUES);
            VecSetValues(b,1,&i,&F_VecAll[nindices_PartOpp[i]],INSERT_VALUES);
        }
    }
    else
    {
        for (PetscInt i=Istart; i<Iend; i++)
        {
            VecSetValues(b,1,&i,&F_VecAll[i],INSERT_VALUES);
        }
    }
    //Assemble the vector
    VecAssemblyBegin(b);
    VecAssemblyEnd(b);
    
    if (Mat_NullSpace==1)
    {
        //Insert the values 
        if (Mat_Partitioning==1)
        {
            for (PetscInt i=Istart; i<Iend; i++)
            {
                VecSetValues(Coord,1,&i,&CoordAll[nindices_PartOpp[i]],INSERT_VALUES);
            }
        }
        else
        {
            for (PetscInt i=Istart; i<Iend; i++)
            {
                VecSetValues(Coord,1,&i,&CoordAll[i],INSERT_VALUES);
            }
        }
        //Assemble the vector
        VecAssemblyBegin(Coord);
        VecAssemblyEnd(Coord);
    }
}

void MPI_Solver::FillUVectLoc()
{
    UVectLoc=new double[Iend-Istart];
    PetscScalar TempUVal;
    for (PetscInt i=Istart; i<Iend; i++)
    {
        VecGetValues(U,1,&i,&TempUVal);
        UVectLoc[i-Istart]=TempUVal;
    }
}

void MPI_Solver::ReturnResult(LinearProblem* L)
{
    MPI_Status status;
    if (rank==0)
    {
        U_Out.resize(NodeNumber*Dimension);
        for (PetscInt i=Istart; i<Iend; i++)
        {
            if (Mat_Partitioning==1)
            {
                //U_Out[nindices_OrdGlob[nindices_PartOpp[i]]]=TempUVal;
                U_Out[nindices_PartOpp[i]]=UVectLoc[i-Istart];
            }
            else
            {
                U_Out[i]=UVectLoc[i-Istart];
            }
        }
        delete [] UVectLoc;
        for (PetscInt IncRank=1;IncRank<size;IncRank++)
        {
            double* TempUVect;
            TempUVect=new double[EndRangeArray[IncRank]-StartRangeArray[IncRank]];
            MPI_Recv(TempUVect,EndRangeArray[IncRank]-StartRangeArray[IncRank], MPI_DOUBLE, IncRank, 123, MPI_COMM_WORLD,&status);
            for (long i=StartRangeArray[IncRank]; i<EndRangeArray[IncRank]; i++)
            {
                if (Mat_Partitioning==1)
                {
                    //U_Out[nindices_OrdGlob[nindices_PartOpp[i]]]=TempUVect[i-StartRangeArray[IncRank]];
                    U_Out[nindices_PartOpp[i]]=TempUVect[i-StartRangeArray[IncRank]];
                }
                else
                {
                    U_Out[i]=TempUVect[i-StartRangeArray[IncRank]];
                }
            }
        }
        L->U=U_Out;
    }
    else
    {
        MPI_Send(UVectLoc, Iend-Istart, MPI_DOUBLE, 0, 123, MPI_COMM_WORLD);
        delete [] UVectLoc;
    }
    VecDestroy(&b);
    VecDestroy(&U);
    MatDestroy(&StiffMat);
}

void MPI_Solver::ComputeAdjacencyTriplets(Model* M)
{
    //Create the MPI triplet struct
    MPI_Datatype MPI_Triplet;
    const int nitems=3;
    int blocklengths[3] = {1,1,1};
    MPI_Aint offsets[3];
    offsets[0] = offsetof(Triplets, row);
    offsets[1] = offsetof(Triplets, col);
    offsets[2] = offsetof(Triplets, val);
    MPI_Datatype types[3] = {MPI_INT,MPI_INT,MPI_DOUBLE};
    
    // Call the data type constructor
    MPI_Type_struct(nitems, blocklengths, offsets, types, &MPI_Triplet);
    MPI_Type_commit(&MPI_Triplet);
    
    //Create the adacency matrix if partitioning is used
    MatCreate(comm,&AdjMatrix);
    MatSetType(AdjMatrix,MATMPIAIJ);
    MatSetSizes(AdjMatrix,PETSC_DECIDE,PETSC_DECIDE,NodeNumber*Dimension,NodeNumber*Dimension);
    MatSetBlockSize(AdjMatrix,1);
    MatSetFromOptions(AdjMatrix);
    MatSetUp(AdjMatrix);
    
    //Get the matrix ownership
    PetscInt IstartTemp,IendTemp;
    MatGetOwnershipRange(AdjMatrix,&IstartTemp,&IendTemp);
    IstartAdj=IstartTemp;IendAdj=IendTemp;
    
    //Send ownership to the master process
    MPI_Status status;
    if (rank==0)
    {
        StartRangeArrayAdj = new long[size];
        EndRangeArrayAdj = new long[size];
        StartTripRangeArrayAdj = new long[size];
        EndTripRangeArrayAdj = new long[size];
        
        StartRangeArrayAdj[0]=Istart;
        EndRangeArrayAdj[0]=Iend;
        for (int IncRank=1;IncRank<size;IncRank++)
        {           
            MPI_Recv(&StartRangeArrayAdj[IncRank], 1, MPI_LONG, IncRank, 123, MPI_COMM_WORLD,&status);
            MPI_Recv(&EndRangeArrayAdj[IncRank], 1, MPI_LONG, IncRank, 123, MPI_COMM_WORLD,&status);
        }
    }
    else
    {
        MPI_Send(&IstartAdj, 1, MPI_LONG, 0, 123, MPI_COMM_WORLD);
        MPI_Send(&IendAdj, 1, MPI_LONG, 0, 123, MPI_COMM_WORLD);
    }  
    
    //Compute the global triplets array
    if (rank==0)
    {
        //Get the size of the triplet vector
        long TripIndex=0;
        for (long i=0; i<NodeNumber; i++)
        {
            for (int j=0; j<M->Dimension; j++)
            {
                if (M->Nodes[i+1].BoundaryNode==false)
                {
                    TripIndex+=M->Nodes[i+1].SupSize*Dimension;
                }
                else
                {
                    if (M->Nodes[i+1].BC_D_Bool[j]==true && M->Method=="GFD")
                    {
                        TripIndex+=1;
                    }
                    else
                    {
                        TripIndex+=M->Nodes[i+1].SupSize*Dimension;
                    }
                }
            }
        }
        TripLengthAdj=TripIndex;
        
        //Load a global temporary Duplet vector
        Triplets* TempTripGlobal;
        TempTripGlobal=new Triplets[TripLengthAdj];
        
        TripIndex=0;
        Triplets TempTrip;
        double* TempWeight;
        TempWeight=new double[M->Nodes[1].SupSize*Dimension*3];
        for (long i=0; i<NodeNumber; i++)
        {
            //Get the weights associated to each nodes
            for (int j=0; j<M->Nodes[i+1].SupSize; j++)
            {
                TempWeight[j]=0;
                for (int k=0; k<M->Dimension; k++)
                {
                    TempWeight[j]+=pow(M->Nodes[i+1].X[k]-M->Nodes[M->Nodes[i+1].SupNodes[j]].X[k],2);
                }
                TempWeight[j]=pow(TempWeight[j]/M->Nodes[i+1].SupRadiusSq,0.5);
                
            }
            //Fill the triplet per row
            for (int k=0; k<M->Dimension; k++)
            {
                if (M->Nodes[i+1].BoundaryNode==false)
                {
                    for (int j=0; j<M->Nodes[i+1].SupSize; j++)
                    {
                        for (int l=0; l<Dimension; l++)
                        {
                            TempTrip.row=i*Dimension+k;
                            TempTrip.col=(M->Nodes[i+1].SupNodes[j]-1)*Dimension+l;
                            TempTrip.val=TempWeight[j];
                            TempTripGlobal[TripIndex]=TempTrip;
                            TripIndex++;
                        }
                    }
                }
                else
                {
                    if (M->Nodes[i+1].BC_D_Bool[k]==true && M->Method=="GFD")
                    {
                        TempTrip.row=i*Dimension+k;
                        TempTrip.col=i*Dimension+k;
                        TempTrip.val=1;
                        TempTripGlobal[TripIndex]=TempTrip;
                        TripIndex++;
                    }
                    else
                    {
                        for (int j=0; j<M->Nodes[i+1].SupSize; j++)
                        {
                            for (int l=0; l<Dimension; l++)
                            {
                                TempTrip.row=i*Dimension+k;
                                TempTrip.col=(M->Nodes[i+1].SupNodes[j]-1)*Dimension+l;
                                TempTrip.val=TempWeight[j];
                                TempTripGlobal[TripIndex]=TempTrip;
                                TripIndex++;
                            }
                        }
                    }
                }
            }
        }
        delete[] TempWeight;
        TripLengthAdj=TripIndex;
        
        //Get the start and end indices of the triplet owned by each process
        int ProcIndex=0;
        for (long i=0; i<TripLengthAdj; i++)
        {
            if (ProcIndex<size)
            {
                if (TempTripGlobal[i].row>=StartRangeArrayAdj[ProcIndex])
                {
                    StartTripRangeArrayAdj[ProcIndex]=i;
                    if (ProcIndex>0)
                        EndTripRangeArrayAdj[ProcIndex-1]=i-1;
                    ProcIndex++;
                }
            }
        }
        EndTripRangeArrayAdj[ProcIndex-1]=TripLengthAdj-1;
        
        //Resize the MPI triplet vector for the master node
        TripLengthAdj=(EndTripRangeArrayAdj[0]-StartTripRangeArrayAdj[0]+1);
        TripletVectAdj= new Triplets[TripLengthAdj];
        for (long j=0; j<TripLengthAdj; j++)
        {
            TripletVectAdj[j]=TempTripGlobal[StartTripRangeArrayAdj[0]+j];
        }
        //Send the portion of the main triplet vector to each process
        for (int i=1;i<size;i++)
        {
            //Create a temporary triplet vector of the appropriate size for the destination process
            Triplets* TempTrip;
            long TempTripSize=(EndTripRangeArrayAdj[i]-StartTripRangeArrayAdj[i]+1);
            TempTrip= new Triplets[TempTripSize];
            for (long j=0; j<TempTripSize; j++)
            {
                TempTrip[j]=TempTripGlobal[StartTripRangeArrayAdj[i]+j];
            }            
            MPI_Send(&TempTripSize, 1, MPI_LONG, i, 123, MPI_COMM_WORLD);
            MPI_Send(TempTrip, TempTripSize, MPI_Triplet, i, 123, MPI_COMM_WORLD);
            delete [] TempTrip;
        }
    }
    else
    {
        //Receive the triplet length and array from the master process
        MPI_Recv(&TripLengthAdj, 1, MPI_LONG, 0, 123, MPI_COMM_WORLD,&status);
        TripletVectAdj=new Triplets[TripLengthAdj];
        MPI_Recv(TripletVectAdj, TripLengthAdj, MPI_Triplet, 0, 123, MPI_COMM_WORLD,&status);
    }
}

void MPI_Solver::LoadAdjacencyMatrix()
{
    //Create the triplet vector
    PetscInt* RowVect;
    PetscInt* ColumnVect;
    PetscScalar* ValVect;
    
    long TempSize=0;
    //Get the final size of the local triplet vector
    for (PetscInt i=0; i<TripLengthAdj; i++)
    {
        if (TripletVectAdj[i].val>=AdjThreshold*0.99)
        {
            TempSize++;
        }
    }
    
    long InitSize=TripLengthAdj;
    TripLengthAdj=TempSize;
    
    //Resize the triplet vectors
    RowVect = new PetscInt[TripLengthAdj];
    ColumnVect = new PetscInt[TripLengthAdj];
    ValVect = new PetscScalar[TripLengthAdj];

    //Fill the triplet vector    
    long Index=0;
    for (PetscInt i=0; i<InitSize; i++)
    {
        if (TripletVectAdj[i].val>=AdjThreshold*0.99)
        {
            RowVect[Index]=TripletVectAdj[i].row-IstartAdj;
            ColumnVect[Index]=TripletVectAdj[i].col;
            ValVect[Index]=TripletVectAdj[i].val;
            Index++;
        }
    }
    
    //Create a sequencial matrix for each process in order to fill the matrix in one go
    Mat TempProcessAdj;
    MatCreateSeqAIJFromTriple(MPI_COMM_SELF,IendAdj-IstartAdj,Dimension*NodeNumber,RowVect,ColumnVect,ValVect,&TempProcessAdj,TripLengthAdj,PETSC_FALSE);
    MatSetFromOptions(TempProcessAdj);
    MatSetUp(TempProcessAdj);
    MatAssemblyBegin(TempProcessAdj,MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(TempProcessAdj,MAT_FINAL_ASSEMBLY);
    
    //Fill and assemble the multiprocess adjacency matrix
    MatCreateMPIMatConcatenateSeqMat(comm,TempProcessAdj,PETSC_DECIDE,MAT_INITIAL_MATRIX,&AdjMatrix);
    MatAssemblyBegin(AdjMatrix,MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(AdjMatrix,MAT_FINAL_ASSEMBLY);  
    
    //Delete the local matrix and the vectors
    MatDestroy(&TempProcessAdj);
    delete [] TripletVectAdj;
    delete[] RowVect;
    delete[] ColumnVect;
    delete[] ValVect;
}