#include "VTKOutFile.h"

VTKOutFile::VTKOutFile() {
}

VTKOutFile::VTKOutFile(const VTKOutFile& orig) {
}

VTKOutFile::~VTKOutFile() {
}

void VTKOutFile::PrintVTKOutputFile(Model* M, LinearProblem* L, ErrorEstimator* E, string Path)
{
    std::ofstream *outfile;
    outfile= new std::ofstream(Path.replace(Path.end()-3,Path.end(),"vtk"));
    
    *outfile << "# vtk DataFile Version 1.0" << endl;
    *outfile << Path << endl;
    *outfile << "ASCII" << endl;
    *outfile << endl;
    
    //Print the node coordinates
    *outfile << "DATASET UNSTRUCTURED_GRID" << endl;
    long NumNodesVTK=M->NodeNumber;
    
    if (M->ResVoroCorner==true)
        NumNodesVTK=M->NodeNumber+E->VoroNodeCount;
        
    *outfile << "POINTS " << NumNodesVTK << " float" << endl;
    for (int i=0; i<M->NodeNumber; i++)
    {
        *outfile << "  " << M->ToString(M->Nodes[i+1].X[0],5,15,true,0) << " " << M->ToString(M->Nodes[i+1].X[1],5,15,true,0);
        if (M->Dimension==2)
            *outfile << " 0" << endl;
        else
            *outfile << " " << M->ToString(M->Nodes[i+1].X[2],5,15,true,0) << endl;
    }
    
    if (M->ResVoroCorner==true)
    {
        for (int i=0; i<E->VoroNodeCount; i++)
        {
            *outfile << "  " << M->ToString(E->VoronoiNodes[i+1].X[0],5,15,true,0) << " " << M->ToString(E->VoronoiNodes[i+1].X[1],5,15,true,0);
            if (M->Dimension==2)
                *outfile << " 0" << endl;
            else
                *outfile << " " << M->ToString(E->VoronoiNodes[i+1].X[2],5,15,true,0) << endl;
        }
    }
    
    //Print the cell data
    *outfile << "CELLS " << M->NodeNumber << " " << M->NodeNumber*2 << endl;
    for (int i=0; i<M->NodeNumber; i++)
    {
        *outfile << " 1 " << i << endl;
    }
    *outfile << "CELL_TYPES " << M->NodeNumber << endl;
    for (int i=0; i<M->NodeNumber; i++)
    {
        *outfile << " 1" << endl;
    }
    
    //Print point data
    *outfile << endl;
    *outfile << "POINT_DATA " << NumNodesVTK << endl;
    
    if (M->ResVoroCorner==false)
    {
        //Print the values approx, exact and the error
        int LastIndex=1;
        if (M->ExactSolutionAvailable==true)
        {
            LastIndex=3;
        }
        for (int ii=0; ii<LastIndex; ii++)
        {
            //Get the suffix
            string Suffix="";
            if (ii==0)
                Suffix="_Approx";
            else if (ii==1)
                Suffix="_Exact";
            else if (ii==2)
                Suffix="_Error";

            //Print the displacement field
            *outfile << "VECTORS U" << Suffix << " float" << endl;  
            for (int i=0; i<M->NodeNumber; i++)
            {
                long RowU1=M->Dimension*i;
                long RowU2=M->Dimension*i+1;
                long RowU3=M->Dimension*i+2;
                double Val1=L->U(RowU1);
                double Val2=L->U(RowU2);
                double Val3=0;
                double Val1Ex=0;
                double Val2Ex=0;
                if (M->ExactSolutionAvailable==true)
                {
                    Val1Ex=M->ExactU(RowU1);
                    Val2Ex=M->ExactU(RowU2);
                }
                double Val3Ex=0;
                double ValPrint1=0;
                double ValPrint2=0;
                double ValPrint3=0;

                if (M->Dimension==3)
                {
                    Val3=L->U(RowU3);
                    if (M->ExactSolutionAvailable==true)
                    {
                        Val3Ex=M->ExactU(RowU3);
                    }
                }
                if (ii==0)
                {
                    ValPrint1=Val1;
                    ValPrint2=Val2;
                    ValPrint3=Val3;
                }
                else if (ii==1)
                {
                    ValPrint1=Val1Ex;
                    ValPrint2=Val2Ex;
                    ValPrint3=Val3Ex;
                }
                else if (ii==2)
                {
                    ValPrint1=abs(Val1-Val1Ex);
                    ValPrint2=abs(Val2-Val2Ex);
                    ValPrint3=abs(Val3-Val3Ex);
                }

                *outfile << "  " << M->ToString(ValPrint1,5,15,true,0) << "  " << M->ToString(ValPrint2,5,15,true,0);
                if (M->Dimension==2)
                    *outfile << "  0";
                else if (M->Dimension==3)
                    *outfile << "  " << M->ToString(ValPrint3,5,15,true,0);
                *outfile << endl;
            } 

            //Print all the stress components
            for (int i=0; i<3*(M->Dimension-1)+1; i++)
            {
                *outfile << endl;
                string ComponentTitle="";
                long RowS=0;

                if (i==0)
                    ComponentTitle="S11";
                else if (i==1)
                    ComponentTitle="S12";
                else if (i==3*(M->Dimension-1))
                        ComponentTitle="vonMises_Stress";
                else if (M->Dimension==2)
                {
                    if (i==2)
                        ComponentTitle="S22";
                }
                else if (M->Dimension==3)
                {
                    if (i==2)
                        ComponentTitle="S13";
                    else if (i==3)
                        ComponentTitle="S22";
                    else if (i==4)
                        ComponentTitle="S23";
                    else if (i==5)
                        ComponentTitle="S33";
                }
                ComponentTitle+=Suffix;

                *outfile << endl;
                *outfile << "SCALARS " << ComponentTitle << " float" << endl;
                *outfile << "LOOKUP_TABLE default" << endl;
                for (long j=0; j<M->NodeNumber; j++)
                {
                    if (i<3*(M->Dimension-1))
                    {
                        RowS=j*3*(M->Dimension-1)+i;
                        double Val=L->S_Ord(RowS);
                        double ValEx=0;
                        if (M->ExactSolutionAvailable==true)
                            ValEx=M->ExactS(RowS);
                        double PrintVal=0;
                        if (ii==0)
                            PrintVal=Val;
                        else if (ii==1)
                            PrintVal=ValEx;
                        else if (ii==2)
                            PrintVal=abs(Val-ValEx);
                        *outfile << "  " << M->ToString(PrintVal,5,15,true,0) << endl;
                    }
                    else if (i==3*(M->Dimension-1))
                    {
                        double ValVMS=0;
                        double ValVMSEx=0;
                        double PrintVal=0;
                        if (M->Dimension==2)
                        {
                            ValVMS=pow(pow(L->S_Ord(j*3*(M->Dimension-1)+0),2)+pow(L->S_Ord(j*3*(M->Dimension-1)+2),2)-L->S_Ord(j*3*(M->Dimension-1)+0)*L->S_Ord(j*3*(M->Dimension-1)+2)+3*pow(L->S_Ord(j*3*(M->Dimension-1)+1),2),0.5);
                            if (M->ExactSolutionAvailable==true)
                                ValVMSEx=pow(pow(M->ExactS(j*3*(M->Dimension-1)+0),2)+pow(M->ExactS(j*3*(M->Dimension-1)+2),2)-M->ExactS(j*3*(M->Dimension-1)+0)*M->ExactS(j*3*(M->Dimension-1)+2)+3*pow(M->ExactS(j*3*(M->Dimension-1)+1),2),0.5);
                        }
                        else if (M->Dimension==3)
                        {
                            ValVMS=pow(0.5*(pow(L->S_Ord(j*3*(M->Dimension-1)+0)-L->S_Ord(j*3*(M->Dimension-1)+3),2)+pow(L->S_Ord(j*3*(M->Dimension-1)+0)-L->S_Ord(j*3*(M->Dimension-1)+5),2)+pow(L->S_Ord(j*3*(M->Dimension-1)+3)-L->S_Ord(j*3*(M->Dimension-1)+5),2)
                                +6*(pow(L->S_Ord(j*3*(M->Dimension-1)+1),2)+pow(L->S_Ord(j*3*(M->Dimension-1)+2),2)+pow(L->S_Ord(j*3*(M->Dimension-1)+4),2))),0.5);
                            if (M->ExactSolutionAvailable==true)
                                ValVMSEx=pow(0.5*(pow(M->ExactS(j*3*(M->Dimension-1)+0)-M->ExactS(j*3*(M->Dimension-1)+3),2)+pow(M->ExactS(j*3*(M->Dimension-1)+0)-M->ExactS(j*3*(M->Dimension-1)+5),2)+pow(M->ExactS(j*3*(M->Dimension-1)+3)-M->ExactS(j*3*(M->Dimension-1)+5),2)
                                +6*(pow(M->ExactS(j*3*(M->Dimension-1)+1),2)+pow(M->ExactS(j*3*(M->Dimension-1)+2),2)+pow(M->ExactS(j*3*(M->Dimension-1)+4),2))),0.5);
                        }
                        if (ii==0)
                            PrintVal=ValVMS;
                        else if (ii==1)
                            PrintVal=ValVMSEx;
                        else if (ii==2)
                            PrintVal=abs(ValVMS-ValVMSEx);
                        *outfile << "  " << M->ToString(PrintVal,5,15,true,0) << endl;
                    }
                }
            }
        }
    
        //Print the error estimator
        if (M->ErrEstimator==true)
        {
            //Get the number of err or components
            int ErrorComp=3*(M->Dimension-1);
            if (M->EstimatorType=="BEN" || M->EstimatorType=="BEN_RES" || M->EstimatorType=="RES")
                ErrorComp=M->Dimension;

            for (int i=0; i<ErrorComp+1; i++)
            {
                string ComponentTitle="";
                if (M->EstimatorType=="BEN" || M->EstimatorType=="BEN_RES" || M->EstimatorType=="RES")
                {
                    if (i==0)
                        ComponentTitle="U1";
                    else if (i==1)
                        ComponentTitle="U2";
                    else if (i==2)
                        ComponentTitle="U3";
                    if (i==ErrorComp)
                        ComponentTitle="UResult";
                }
                else
                {
                    if (i==0)
                        ComponentTitle="S11";
                    else if (i==1)
                        ComponentTitle="S12";
                    else if (i==3*(M->Dimension-1))
                            ComponentTitle="vonMises_Stress";
                    else if (M->Dimension==2)
                    {
                        if (i==2)
                            ComponentTitle="S22";
                    }
                    else if (M->Dimension==3)
                    {
                        if (i==2)
                            ComponentTitle="S13";
                        else if (i==3)
                            ComponentTitle="S22";
                        else if (i==4)
                            ComponentTitle="S23";
                        else if (i==5)
                            ComponentTitle="S33";
                    }
                }
                ComponentTitle+="_ErrorEstim";

                *outfile << endl;
                *outfile << "SCALARS " << ComponentTitle << " float" << endl;
                *outfile << "LOOKUP_TABLE default" << endl;
                for (long j=0; j<M->NodeNumber; j++)
                {
                    double PrintVal=0;
                    if (i<ErrorComp)
                    {
                        if (M->EstimatorType=="BEN" || M->EstimatorType=="BEN_RES" || M->EstimatorType=="RES")
                        {
                            PrintVal=abs(E->Estim(j*M->Dimension+i));
                        }
                        else
                        {
                            PrintVal=abs(E->Estim(j*3*(M->Dimension-1)+i)-L->S_Ord(j*3*(M->Dimension-1)+i));
                        }
                    }
                    else
                    {
                        if (M->EstimatorType=="BEN" || M->EstimatorType=="BEN_RES" || M->EstimatorType=="RES")
                        {
                            PrintVal=pow(pow(E->Estim(j*M->Dimension+0),2)+pow(E->Estim(j*M->Dimension+1),2),0.5);
                        }
                        else
                        {
                            PrintVal=abs(E->ErrorEstim(j));
                        }
                    }
                    *outfile << "  " << M->ToString(PrintVal,5,15,true,0) << endl;
                }
            }
        }
        
        //Print the local residual of the PDE
        if (M->PrintLocResidual==true)
        {
            *outfile << endl;
            *outfile << "VECTORS PDE_Residual float" << endl;
            for (long i=0; i<M->NodeNumber; i++)
            {
                long RowRes1=M->Dimension*i;
                long RowRes2=M->Dimension*i+1;
                long RowRes3=M->Dimension*i+2;
                double Val1=L->LocResidual(RowRes1);
                double Val2=L->LocResidual(RowRes2);
                double Val3=0;
                if (M->Dimension==3)
                {
                    Val3=L->LocResidual(RowRes3);
                }
                *outfile << "  " << M->ToString(Val1,5,15,true,0) << "  " << M->ToString(Val2,5,15,true,0);
                if (M->Dimension==2)
                    *outfile << "  0";
                else if (M->Dimension==3)
                    *outfile << "  " << M->ToString(Val3,5,15,true,0);
                *outfile << endl;
            }
        }
        
        //Print the normals
        for (int ii=0 ;ii<M->Dimension; ii++)
        {
            *outfile << endl;
            *outfile << "VECTORS NormalsDOF" << ii+1 << " float" << endl;
            for (long i=0; i<M->NodeNumber; i++)
            {
                if (M->Nodes[i+1].BoundaryNode==false)
                {
                    *outfile << "  0 0 0" << endl;
                }
                else
                {
                    *outfile << "  " << M->ToString(M->Nodes[i+1].Normal[ii][0],5,15,true,0) << " " << M->ToString(M->Nodes[i+1].Normal[ii][1],5,15,true,0);
                    if (M->Dimension==2)
                        *outfile << " 0" << endl;
                    else
                        *outfile << " " << M->ToString(M->Nodes[i+1].Normal[ii][2],5,15,true,0) << endl;
                }
            }
        }
    }
    else
    {
        double PrintVal=1e-15;
        *outfile << endl;
        *outfile << "SCALARS ResAtVoroCorners float" << endl;
        *outfile << "LOOKUP_TABLE default" << endl;
        for (int i=0; i<M->NodeNumber; i++)
        {
            *outfile << "  " << M->ToString(PrintVal,5,15,true,0) << endl;
        }

        if (M->ResVoroCorner==true)
        {
            for (int i=0; i<E->VoroNodeCount; i++)
            {
                PrintVal=E->ErrorEstimVoro(i);
                *outfile << "  " << M->ToString(PrintVal,5,15,true,0) << endl;
            }
        }
    }
    
    outfile->close();
    delete outfile;
}