#include "Seq_Solver.h"

Seq_Solver::Seq_Solver() {
}

Seq_Solver::Seq_Solver(const Seq_Solver& orig) {
}

Seq_Solver::~Seq_Solver() {
}

void Seq_Solver::Solve(Model* M, LinearProblem* L)
{
    FillStiffMat_Petsc(M,L);
       
    int Dimension=M->Dimension;
    long NodeNumber=M->NodeNumber;
    long SizeStiff=Dimension*NodeNumber+L->AdditionalForceVectDOFs;
    //Set the vectors
    VecCreate(PETSC_COMM_SELF,&U_Petsc);
    VecSetType(U_Petsc, VECSEQ);
    VecSetSizes(U_Petsc,PETSC_DECIDE,SizeStiff);
    VecSetFromOptions(U_Petsc);
    VecDuplicate(U_Petsc,&b);
    
    VecAssemblyBegin(b);
    VecAssemblyEnd(b);
    
    //Create the residual vector if needed
    if (M->PrintLocResidual==true)
    {
        VecCreate(PETSC_COMM_SELF,&Res_Petsc);
        VecSetType(Res_Petsc, VECSEQ);
        VecSetSizes(Res_Petsc,PETSC_DECIDE,SizeStiff);
        VecSetFromOptions(Res_Petsc);
    }
    
    //Fill the  force vector
    for (PetscInt i=0; i<SizeStiff; i++)
    {
        VecSetValues(b,1,&i,&L->F_Vec[i],INSERT_VALUES);
    }
    
    //Compute the permutation
    IS rowperm=NULL,colperm=NULL;
    Mat StiffMat_Petsc_perm;
    char ordering[256] = MATORDERINGRCM;
    if (M->Mat_Ordering==1)
    {
        MatGetOrdering(StiffMat,ordering,&rowperm,&colperm);
        MatPermute(StiffMat,rowperm,colperm,&StiffMat_Petsc_perm);
        VecPermute(b,colperm,PETSC_FALSE);
        MatDestroy(&StiffMat);
        StiffMat = StiffMat_Petsc_perm;
    }
    
    if (M->SolverType=="DIRECT_MUMPS")
        SolveMUMPS_LU(M,L);
    else if (M->SolverType=="DIRECT_SUPER_LU")
        SolveSuperLU(M,L);
    else if (M->SolverType=="KSP")
        SolveKSP(M,L);
    
    VecAssemblyBegin(U_Petsc);
    VecAssemblyEnd(U_Petsc);
    
    //Get the local residual of the PDE
    if (M->PrintLocResidual==true)
    {
        MatMult(StiffMat,U_Petsc,Res_Petsc);
        //Assemble the residual vector
        VecAssemblyBegin(Res_Petsc);
        VecAssemblyEnd(Res_Petsc);
    }
    
    //Permute the solution vector
    if (M->Mat_Ordering==1)
    {
        VecPermute(U_Petsc,colperm,PETSC_TRUE);
    }
    
    MatDestroy(&StiffMat);
    ISDestroy(&rowperm);
    ISDestroy(&colperm);
    
    if (M->Method == "GFD_XI" || M->Method == "GFD_XE")
    {
        for (PetscInt i=0; i<Dimension*NodeNumber; i++)
        {
            PetscScalar TempU;
            VecGetValues(U_Petsc,1,&i,&TempU);
            L->U_DOF(i)=TempU;
            L->U(i)=TempU;
        }
    }
    
    if ((M->Method == "GFD_XI" || M->Method == "GFD_XE") && M->Inter==false)
    {
//        for (PetscInt i=0; i<NodeNumber; i++)
//        {
//            long NodeNum=i+1;
//            double Dist=0;
//            if (M->Method == "GFD_XI")
//            {
//                for (int m=0; m<Dimension; m++)
//                {
//                    Dist+=pow(M->Nodes[NodeNum].X[m],2);
//                }
//                Dist=pow(Dist,0.5);
//            }
//            
//            long SupSize=M->Nodes[NodeNum].SupSize;
//            for (PetscInt j=0; j<Dimension; j++)
//            {
//                double Val=0;
//                for (int k=0; k<SupSize; k++)
//                {
//                    PetscInt LocIndex=(M->Nodes[NodeNum].SupNodes[k]-1)*Dimension+j;
//                    PetscScalar LocVal;
//                    VecGetValues(U_Petsc,1,&LocIndex,&LocVal);
//                    double C_u=M->Nodes[NodeNum].Coeff[k][0];
//                    if (M->Method == "GFD_XI")
//                    {
//                        if (M->SingProd==true && M->Inter==false)
//                        {
//                            Val+=LocVal*(C_u*M->GetEnrichmentFunctionVal(NodeNum,M->FNum,1,0));
//                        }
//                        else
//                        {
//                            double C_a=M->Nodes[NodeNum].CoeffEnrich[k][0];
//                            Val+=LocVal*(C_u+C_a*M->GetEnrichmentFunctionVal(NodeNum,M->FNum,1,0));
//                        }
//                        if (M->FNum==2)
//                        {
//                            double C_b=M->Nodes[NodeNum].CoeffEnrich[k][1];
//                            Val+=LocVal*(C_b*M->GetEnrichmentFunctionVal(NodeNum,M->FNum,2,0));
//                        }
//                    }
//                    else
//                    {
//                        Val+=LocVal*C_u;
//                    }
//                }
//                L->U(i*Dimension+j)=Val;
//            }
//        }
    }
    else if (M->Method == "GFD_XI" && M->Inter==true && M->SingProd==true)
    {
        for (PetscInt i=0; i<NodeNumber; i++)
        {
            for (PetscInt j=0; j<Dimension; j++)
            {
                PetscInt LocIndex=i*Dimension+j;
                PetscScalar LocVal;
                VecGetValues(U_Petsc,1,&LocIndex,&LocVal);
                L->U(i*Dimension+j)=LocVal*M->GetEnrichmentFunctionVal(i+1,M->FNum,1,0);
            }
        }
    }
    else
    {
        for (PetscInt i=0; i<Dimension*NodeNumber; i++)
        {
            PetscScalar TempU;
            VecGetValues(U_Petsc,1,&i,&TempU);
            L->U(i)=TempU;
            
            if (M->PrintLocResidual==true)
            {
                PetscScalar TempRes;
                VecGetValues(Res_Petsc,1,&i,&TempRes);
                PetscScalar Tempb;
                VecGetValues(b,1,&i,&Tempb);
                L->LocResidual(i)=TempRes-Tempb;
            }
        }
    }
    VecDestroy(&b);
}

void Seq_Solver::SolveMUMPS_LU(Model* M, LinearProblem* L)
{   
    MPI_Comm comm = MPI_COMM_SELF;
    KSP solver;
    PC pc;
    M->Print("Create Solver.",true);
    KSPCreate(comm,&solver);
    KSPSetOperators(solver,StiffMat,StiffMat);
    
    KSPGetPC(solver,&pc);
    M->Print("KSP Preconditioning.",true);
    KSPSetType(solver,KSPPREONLY);
    M->Print("Set LU.",true);
    PCSetType(pc,PCLU);
    M->Print("Solver MUMPS.",true);
    PCFactorSetMatSolverType(pc,MATSOLVERMUMPS);
    PCFactorSetUpMatSolverType(pc);
    
    Mat F;
    PCFactorGetMatrix(pc,&F);
    //sequential ordering
    MatMumpsSetIcntl(F,7,3);
    MatMumpsSetIcntl(F,8,77);   
    
    M->Print("Solving the system of equations.",true);
    
    //Print the PC
    if (M->PrintMat==1)
    {
        string Path=M->InputFilePath;
        Path.replace(Path.end()-3,Path.end(),"txt");
        char* PathChar=new char[Path.length()+1];
        strcpy(PathChar,Path.c_str());
        PetscViewer  viewer;
        PetscViewerCreate(PETSC_COMM_WORLD, &viewer);
        PetscViewerSetType(viewer, PETSCVIEWERASCII);
        PetscViewerFileSetMode(viewer, FILE_MODE_APPEND);
        PetscViewerFileSetName(viewer, PathChar);
        PCView(pc,viewer);
        PetscViewerASCIIOpen(PETSC_COMM_WORLD, NULL, &viewer);
        PetscViewerPushFormat(viewer, PETSC_VIEWER_ASCII_INFO);
        PetscViewerDestroy(&viewer);
    }
    
    //Solve the system
    KSPSolve(solver,b,U_Petsc);
    
    //Get the condition number
    if (M->PrintConditionNumber==1)
    {
        PetscReal emax, emin;
        KSPSetComputeSingularValues(solver, PETSC_TRUE);
        KSPComputeExtremeSingularValues(solver, &emax, &emin);
        L->ConditionNumber=emax / emin;
    }
    
    KSPDestroy(&solver);
}

void Seq_Solver::SolveSuperLU(Model* M, LinearProblem* L)
{
    MPI_Comm comm = MPI_COMM_SELF;
    KSP solver;
    PC pc;
    Mat Aperm;
    IS rowperm=NULL, colperm=NULL;
    char ordering[256]=MATORDERINGRCM;
    MatGetOrdering(StiffMat,ordering,&rowperm,&colperm);
    MatPermute(StiffMat,rowperm,colperm,&Aperm);
    VecPermute(b,colperm,PETSC_FALSE);
    MatDestroy(&StiffMat);
    StiffMat=Aperm;

    M->Print("Create Solver.",true);
    KSPCreate(comm,&solver);
        
    KSPSetOperators(solver,StiffMat,StiffMat);
    
    KSPGetPC(solver,&pc);
    M->Print("KSP Preconditioning.",true);
    KSPSetType(solver,KSPPREONLY);
    M->Print("Set LU.",true);
    PCSetType(pc,PCLU);
    M->Print("Solver SuperLU.",true);
    
    PCFactorSetMatSolverType(pc,MATSOLVERSUPERLU);
    PCFactorSetUpMatSolverType(pc);
    
//    Mat F;
//    PCFactorGetMatrix(pc,&F);
//    const MatFactorInfo *info;
//    MatLUFactorSymbolic(F,StiffMat,rowperm,colperm,info);
//    MatLUFactorNumeric(F,StiffMat,info);  
    
    M->Print("Solving the system of equations.",true);
    KSPSetFromOptions(solver);
    
    //Solve the system
    KSPSolve(solver,b,U_Petsc);
    KSPDestroy(&solver);
//    VecPermute(U_Petsc,rowperm,PETSC_TRUE);
}

void Seq_Solver::SolveKSP(Model* M, LinearProblem* L)
{      
    MPI_Comm comm = MPI_COMM_SELF;
    KSP solver;
    PC pc;
    M->Print("Create KSP Solver and load preconditionner.",true);
    KSPCreate(comm,&solver);
    
    if (M->PrintConditionNumber==1)
    {
        KSPSetComputeSingularValues(solver, PETSC_TRUE);
    }
    
    KSPSetOperators(solver,StiffMat,StiffMat);
    
    if (M->SolverKSP=="GMRES") 
    {
        KSPSetType(solver,KSPGMRES);
    }
    else if (M->SolverKSP=="FGMRES") 
    {
        KSPSetType(solver,KSPFGMRES);
    }
    else if (M->SolverKSP=="BCGS") 
    {
        KSPSetType(solver,KSPBCGS);
    }
    else if (M->SolverKSP=="BCGSL") 
    {
        KSPSetType(solver,KSPBCGSL);
    }
    
    //Set preconditioner
    KSPGetPC(solver,&pc);
    if (M->SolverPC=="JACOBI") 
    {
        PCSetType(pc,PCJACOBI);
    }
    else if (M->SolverPC=="ILU") 
    {
        PCSetType(pc,PCILU);
        PCFactorSetLevels(pc,M->ILU_Levels);
        PCFactorSetMatOrderingType(pc,M->ILU_Ordering);
    }
    else if (M->SolverPC=="GAMG")
    {
        M->Print("Setting PC.",true);
        PCSetType(pc,PCGAMG);
        PCGAMGSetType(pc,PCGAMGAGG);
        //Set the block size
        if (M->Dimension==2)
            PetscOptionsSetValue(NULL,"–mat_block_size","2");
        else if (M->Dimension==3)
            PetscOptionsSetValue(NULL,"–mat_block_size","3");
        
        PetscReal* TempThresholdArray;
        PetscInt ThresholdInt=1;
        TempThresholdArray = new PetscReal[ThresholdInt];
        TempThresholdArray[0]=M->AMG_Threshold;
        PCGAMGSetThreshold(pc,TempThresholdArray,ThresholdInt);
        delete[] TempThresholdArray;
        
        // PCMGMultiplicativeSetCycles(*pc,5); => this command does not work and shall be applied at run time using -pc_mg_multiplicative_cycles 5
        string TempCyclNumStr= to_string(M->AMG_CycleN);  
        char const *TempCyclNum = TempCyclNumStr.c_str();
        PetscOptionsSetValue(NULL,"-pc_mg_multiplicative_cycles",TempCyclNum);
        
        if (M->AMG_Levels>0) PCGAMGSetNlevels(pc,M->AMG_Levels);
        if (M->AMG_SmoothN > 0) PCGAMGSetNSmooths(pc,M->AMG_SmoothN);
        
        //Set the oeprator considered for the PC
        if (M->AMG_Operator=="NOBC")
        {
            PCSetOperators(pc,StiffMat,OpMat);
        }
        else
        {
            PCSetOperators(pc,StiffMat,StiffMat);
        }
    }
    else if (M->SolverPC=="BOOMERAMG")
    {
        M->Print("Setting PC BoomerAMG.",true);
        PCSetType(pc,PCHYPRE);
        PCHYPRESetType(pc,"boomeramg");
        PetscOptionsSetValue(NULL,"-pc_hypre_boomeramg_strong_threshold","0.05");
    }
    else if (M->SolverPC=="BLOCK") 
    {
        PCSetType(pc,PCFIELDSPLIT);
        PCFieldSplitSetType(pc,PC_COMPOSITE_SCHUR);
        IS is;
        PetscInt n=M->Dimension*M->NodeNumber;
        PetscInt first=0;
        PetscInt step=1;
        ISCreateStride(PETSC_COMM_SELF,n,first,step,&is);
        PCFieldSplitSetIS(pc,"MatK",is);
        PetscInt min;
        PetscInt max;   
        ISGetMinMax(is,&min,&max);
        cout << "Min=" << min << ", Max=" << max << endl;
    }
    
    
    PCSetFromOptions(pc);
    M->Print("Setting Ordering.",true);
    PCFactorSetMatOrderingType(pc,MATORDERINGRCM);
    PCSetUp(pc);

    M->Print("PC Set-Up Complete.",true);
    
    //Print the output to a file
    fpos_t pos;
    fgetpos(stdout, &pos);
    int fd = dup(fileno(stdout));
    string Path=M->InputFilePath;
    Path.replace(Path.end()-3,Path.end(),"txt");
    char* PathChar=new char[Path.length()+1];
    strcpy(PathChar,Path.c_str());
    std::freopen(PathChar, "w", stdout);
    
    KSPSetTolerances(solver,M->SolverTol,PETSC_DEFAULT,PETSC_DEFAULT,M->SolverMaxIt);
    KSPSetFromOptions(solver);
    KSPSetUp(solver);
    M->Print("Solving the system of equations.",true);
    
    //Print the PC
    if (M->PrintMat==1)
    {
        string Path=M->InputFilePath;
        Path.replace(Path.end()-3,Path.end(),"txt");
        char* PathChar=new char[Path.length()+1];
        strcpy(PathChar,Path.c_str());
        PetscViewer  viewer;
        PetscViewerCreate(PETSC_COMM_WORLD, &viewer);
        PetscViewerSetType(viewer, PETSCVIEWERASCII);
        PetscViewerFileSetMode(viewer, FILE_MODE_APPEND);
        PetscViewerFileSetName(viewer, PathChar);
        PCView(pc,viewer);
        PetscViewerASCIIOpen(PETSC_COMM_WORLD, NULL, &viewer);
        PetscViewerPushFormat(viewer, PETSC_VIEWER_ASCII_INFO);
        PetscViewerDestroy(&viewer);
    }
    
    //Solve the system
    KSPView(solver,PETSC_VIEWER_STDOUT_WORLD);
    KSPSolve(solver,b,U_Petsc);
    
    fflush(stdout);
    //Close file and restore standard output to stdout - which should be the terminal
    dup2(fd, fileno(stdout));
    close(fd);
    clearerr(stdout);
    fsetpos(stdout, &pos);
    
    //Create a viewer and print the solver info in it
//    char kspinfo[120];
//    PetscViewer viewer;
//    //Open a string viewer; then write info to it.
//    PetscViewerStringOpen(PETSC_COMM_SELF,kspinfo,120,&viewer);
//    KSPView(solver,viewer);
//    L->SolverConvergenceStatus=kspinfo;
    KSPGetIterationNumber(solver,&L->SolverIterationNum);
    KSPConvergedReason IterationStopReason;
    KSPGetConvergedReason(solver,&IterationStopReason);
    L->SetConvergenceReason(IterationStopReason);
    
    //Get the condition number
    if (M->PrintConditionNumber==1)
    {
        PetscReal emax, emin;
        KSPComputeExtremeSingularValues(solver, &emax, &emin);
        L->ConditionNumber=emax / emin;
    }
    
    KSPDestroy(&solver);    
}

void Seq_Solver::FillStiffMat_Petsc(Model* M, LinearProblem* L)
{
    PetscInt SizeStiff=M->Dimension*M->NodeNumber+L->AdditionalForceVectDOFs;
    
    MatCreate(PETSC_COMM_SELF, &StiffMat);
    MatSetType(StiffMat,MATSEQBAIJ);
    MatSetSizes(StiffMat, PETSC_DECIDE, PETSC_DECIDE, SizeStiff, SizeStiff);
    MatSetFromOptions(StiffMat);
    MatSetUp(StiffMat);
    
    Mat array[4];
    //Count the number of entries in the matrix array[0]
    long Counta0=0;
    if (M->BCTreatType=="LAGRANGE" && M->LagrangeSym==true)
    {
        //Create the matrix array[0]
        MatCreate(PETSC_COMM_SELF, &array[0]);
        MatSetType(array[0],MATSEQBAIJ);
        MatSetSizes(array[0],PETSC_DECIDE,PETSC_DECIDE,SizeStiff,SizeStiff);
        MatSetFromOptions(array[0]);
        MatSetUp(array[0]);
        
        //Count the number of entries in the matrix array[0]
        for (int j=0; j<M->ThreadNumber; j++)
        {
            long NumRows=L->StiffRows[j].size();
            for (long k=0; k<NumRows; k++)
            {
                Counta0+=L->StiffRows[j][k].Trip.size();
            }
        }
        
        //Create the triplet vectors
        PetscInt *RowVecta0;
        PetscInt *ColumnVecta0;
        PetscScalar *ValVecta0;
        PetscMalloc1(Counta0,&RowVecta0);
        PetscMalloc1(Counta0,&ColumnVecta0);
        PetscMalloc1(Counta0,&ValVecta0);
        
        //Fill the triplet vectors
        Counta0=0;
        for (int j=0; j<M->ThreadNumber; j++)
        {
            long NumRows=L->StiffRows[j].size();
            for (long k=0; k<NumRows; k++)
            {
                long NumVals=L->StiffRows[j][k].Trip.size();
                for (int l=0; l<NumVals; l++)
                {
                    RowVecta0[Counta0]=L->StiffRows[j][k].Trip[l].row();
                    ColumnVecta0[Counta0]=L->StiffRows[j][k].Trip[l].col();
                    ValVecta0[Counta0]=L->StiffRows[j][k].Trip[l].value();
                    Counta0++;
                }
            }
        }
        //Create the matrices
        MatCreateSeqAIJFromTriple(PETSC_COMM_WORLD,SizeStiff,SizeStiff,RowVecta0,ColumnVecta0,ValVecta0,&array[0],Counta0,PETSC_FALSE);
    }

            
    //Get the size of the triplet vector
    PetscInt size=0;
    if (M->Method!="GFD_XE")
    {
        for (int j=0; j<M->ThreadNumber; j++)
        {
            long NumRows=L->StiffRows[j].size();
            for (long k=0; k<NumRows; k++)
            {
                if (M->BCTreatType=="LAGRANGE" && M->LagrangeSym==true)
                {
                    //Nothing
                }
                else
                {
                    size+=L->StiffRows[j][k].Trip.size();
                }
            }
            if (M->BCTreatType=="LAGRANGE")
            {
                long NumRows2=L->StiffRowsLagrange[j].size();
                for (long k=0; k<NumRows2; k++)
                {
                    //Compute the number of triplets from the additional DOFs - *2 because of the transpose of the BCs matrix
                    size+=L->StiffRowsLagrange[j][k].Trip.size()*2;
                }
            }
        }
    }
    else if (M->Method=="GFD_XE")
    {
        size=L->TripStiffVect.size();
    }
    
    PetscInt *RowVect;
    PetscInt *ColumnVect;
    PetscScalar *ValVect;
    PetscMalloc1(size,&RowVect);
    PetscMalloc1(size,&ColumnVect);
    PetscMalloc1(size,&ValVect);
    
    long i=0;
    if (M->Method=="GFD_XE")
    {
        while (i<size)
        {
            RowVect[i]=L->TripStiffVect[i].row();
            ColumnVect[i]=L->TripStiffVect[i].col();
            ValVect[i]=L->TripStiffVect[i].value();
            i++;
        }
    }
    else
    {
        for (int j=0; j<M->ThreadNumber; j++)
        {
            if (M->BCTreatType=="LAGRANGE" && M->LagrangeSym==true)
            {
                //Nothing
            }
            else
            {
                long NumRows=L->StiffRows[j].size();
                for (long k=0; k<NumRows; k++)
                {
                    long NumVals=L->StiffRows[j][k].Trip.size();
                    for (int l=0; l<NumVals; l++)
                    {
                        RowVect[i]=L->StiffRows[j][k].Trip[l].row();
                        ColumnVect[i]=L->StiffRows[j][k].Trip[l].col();
                        ValVect[i]=L->StiffRows[j][k].Trip[l].value();
                        i++;
                    }
                }
            }
            if (M->BCTreatType=="LAGRANGE")
            {
                long NumRows2=L->StiffRowsLagrange[j].size();
                for (long k=0; k<NumRows2; k++)
                {
                    long NumVals2=L->StiffRowsLagrange[j][k].Trip.size();
                    for (int l=0; l<NumVals2; l++)
                    {
                        //Set the boundary value
                        RowVect[i]=L->StiffRowsLagrange[j][k].Trip[l].row();
                        ColumnVect[i]=L->StiffRowsLagrange[j][k].Trip[l].col();
                        ValVect[i]=L->StiffRowsLagrange[j][k].Trip[l].value();
                        i++;
                        //Set the transpose
                        RowVect[i]=L->StiffRowsLagrange[j][k].Trip[l].col();
                        ColumnVect[i]=L->StiffRowsLagrange[j][k].Trip[l].row();
                        ValVect[i]=L->StiffRowsLagrange[j][k].Trip[l].value();
                        i++;
                    }
                }
            }
        }
    }
    
    //Fill the system matrix
    M->Print("Starting filling Stiffness matrix.",true);
    if (M->BCTreatType=="LAGRANGE" && M->LagrangeSym==true)
    {
        MatCreateSeqAIJFromTriple(PETSC_COMM_SELF,SizeStiff,SizeStiff,RowVect,ColumnVect,ValVect,&StiffMat,size,PETSC_FALSE);
        MatAssemblyBegin(array[0],MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd(array[0],MAT_FINAL_ASSEMBLY);
        MatTranspose(array[0],MAT_INITIAL_MATRIX,&array[1]);
        MatAssemblyBegin(array[1],MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd(array[1],MAT_FINAL_ASSEMBLY);
        MatMatMult(array[1],array[0],MAT_INITIAL_MATRIX,PETSC_DEFAULT,&array[2]);
        MatAssemblyBegin(array[2],MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd(array[2],MAT_FINAL_ASSEMBLY);
        PetscScalar a=1;     
        MatAXPY(StiffMat,a,array[2],DIFFERENT_NONZERO_PATTERN);
    }
    else
    {
        MatCreateSeqAIJFromTriple(PETSC_COMM_SELF,SizeStiff,SizeStiff,RowVect,ColumnVect,ValVect,&StiffMat,size,PETSC_FALSE);
    }
    
    M->Print("Stiffness matrix filled.",true);
    MatAssemblyBegin(StiffMat,MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(StiffMat,MAT_FINAL_ASSEMBLY);
    M->Print("Stiffness matrix assembled.",true);
    
    L->StiffRows.clear();
    L->StiffRows.shrink_to_fit();
    
    PetscScalar v;
    for (PetscInt i=0; i<SizeStiff; i++)
    {
        MatGetValues(StiffMat,1,&i,1,&i,&v);
        if (v==0 && i<M->Dimension*M->NodeNumber)
            cout << "<!> Error row " << i << ", there is a zero on the diagonal." << endl;
    }
    
    PetscFree(RowVect);
    PetscFree(ColumnVect);
    PetscFree(ValVect);
    
    //Fill the operator matrix if needed
    if (M->AMG_Operator=="NOBC")
    {
        MatCreate(PETSC_COMM_SELF, &OpMat);
        MatSetType(OpMat,MATSEQBAIJ);
        MatSetSizes(OpMat, PETSC_DECIDE, PETSC_DECIDE, SizeStiff, SizeStiff);
        MatSetFromOptions(OpMat);
        MatSetUp(OpMat);
        
        PetscInt sizeOp=0;
        for (int j=0; j<M->ThreadNumber; j++)
        {
            long NumRows=L->OpRows[j].size();
            for (long k=0; k<NumRows; k++)
            {
                sizeOp+=L->OpRows[j][k].Trip.size();
            }
        }
        PetscInt *RowVectOp;
        PetscInt *ColumnVectOp;
        PetscScalar *ValVectOp;
        PetscMalloc1(sizeOp,&RowVectOp);
        PetscMalloc1(sizeOp,&ColumnVectOp);
        PetscMalloc1(sizeOp,&ValVectOp);
        long iOp=0;
        for (int j=0; j<M->ThreadNumber; j++)
        {
            long NumRows=L->OpRows[j].size();
            for (long k=0; k<NumRows; k++)
            {
                long NumVals=L->OpRows[j][k].Trip.size();
                for (int l=0; l<NumVals; l++)
                {
                    RowVectOp[iOp]=L->OpRows[j][k].Trip[l].row();
                    ColumnVectOp[iOp]=L->OpRows[j][k].Trip[l].col();
                    ValVectOp[iOp]=L->OpRows[j][k].Trip[l].value();
                    iOp++;
                }
            }
        }
         M->Print("Starting filling Operator matrix.",true);
        MatCreateSeqAIJFromTriple(PETSC_COMM_SELF,SizeStiff,SizeStiff,RowVectOp,ColumnVectOp,ValVectOp,&OpMat,size,PETSC_FALSE);
        M->Print("Operator matrix filled.",true);
        MatAssemblyBegin(OpMat,MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd(OpMat,MAT_FINAL_ASSEMBLY);
        M->Print("Operator matrix assembled.",true);
        L->OpRows.clear();
        L->OpRows.shrink_to_fit();

        PetscFree(RowVectOp);
        PetscFree(ColumnVectOp);
        PetscFree(ValVectOp);
    }
}
