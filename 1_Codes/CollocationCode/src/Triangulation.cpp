#include "Triangulation.h"

Triangulation::Triangulation() {
}

Triangulation::Triangulation(const Triangulation& orig) {
}

Triangulation::~Triangulation() {
}

void Triangulation::ComputeTriangulation(Model* M)
{
    //Create the triangulation
    const int Dim=M->Dimension;
    t = new TriangulationC(Dim);
    
    //Add the points to a vector of Points
    vector<TriangulationC::Point> points;
    std::vector<unsigned> indices;
    long NodeNumber=M->NodeNumber;
    points.resize(NodeNumber);
    indices.resize(NodeNumber);
    for (long i=0; i<NodeNumber; i++)
    {        
        points[i]=TriangulationC::Point(&M->Nodes[i+1].X[0],&M->Nodes[i+1].X[Dim]);
        indices[i]=i+1;
    }
    //Insert the nodes in the triangulation and calculate it
    CGAL_assertion(t->empty());
    t->insert(points.begin(), points.end());
    CGAL_assertion(t->is_valid());
    
    //Add the node index to the vertices of the triangulation
    long i=1;
    for (VertexIt It=t->vertices_begin(); It !=t->vertices_end(); ++It)
    {
        It->data()=i;
        i++;
    }
    i=1;
    for (FullCellsIt It=t->full_cells_begin(); It != t->full_cells_end(); ++It)
    {
//        std::cout << "Cell[" <<i << "]= " << It->vertex(0)->data()<< "," << *(It->vertex(0)) << ",";
//        std::cout << It->vertex(1)->data() << ","  << *(It->vertex(1) )<< ",";
//        std::cout << It->vertex(2)->data() << ","  << *(It->vertex(2)) << "\n";
        i++;
    }    
}