#include "Refinement.h"

void Refinement::PrintRefinedModelInpFile(Model* M, string Path)
{
    string Line;
    std::ofstream *outfile;
    
    //Identify if iterations have already been done
    int pos = 0;
    pos=Path.find("-It");
    if (pos==-1)
    {
        outfile= new std::ofstream(Path.replace(Path.end()-4,Path.end(),"-It1.inp"));
    }
    else if (pos>0)
    {
        //Increment the iteration number
        string NameStart=Path;
        NameStart.replace(NameStart.begin()+pos,NameStart.end(),"");
        stringstream ss;
        ss << ItNumber+1;
        string NewName=NameStart + "-It" + ss.str() + ".inp";
        outfile= new std::ofstream(NewName);
    }
    
    *outfile << "*NODE" << endl;
    long Count=1;
    
    //Print the boundary nodes
    for(int i=1; i<M->NodeNumber+1; i++)
    {
        if ((M->Nodes[i].BoundaryNode==true || M->Nodes[i].AttachedElements.size()>0) && M->Nodes[i].DuplicatedNode==false)
        {
            Line = "," + M->ToString(M->Nodes[i].X[0],10,19,false,0);
            if (M->Dimension==2)
            {
                Line += M->ToString(M->Nodes[i].X[1],10,19,true,0);
                Line += ", 0";
            }
            else if (M->Dimension==3)
            {
                Line += M->ToString(M->Nodes[i].X[1],10,19,false,0) + M->ToString(M->Nodes[i].X[2],10,19,true,0);
            }
            Line += ", B";
            if (M->Nodes[i].MarkedForRefinement==true)
            {
                Line += ", F";
            }
            else
            {
                Line += ", C";
            }
            *outfile << M->Nodes[i].GlobalIndex << Line << endl;
            Count++;
        }
    }
    for(int i=0; i<NewNodesVect.size(); i++)
    {
        if (NewNodesVect[i]->BoundaryNode==true && NewNodesVect[i]->DuplicatedNode==false && NewNodesVect[i]->InDomain==true)
        {
            Line = "," + M->ToString(NewNodesVect[i]->X[0],10,19,false,0);
            if (M->Dimension==2)
            {
                Line += M->ToString(NewNodesVect[i]->X[1],10,19,true,0);
                Line += ", 0";
            }
            else if (M->Dimension==3)
            {
                Line += M->ToString(NewNodesVect[i]->X[1],10,19,false,0) + M->ToString(NewNodesVect[i]->X[2],10,19,true,0);
            }
            Line += ", B, F";
            *outfile << NewNodesVect[i]->GlobalIndex << Line << endl;
            Count++;
        }
    }
    
    //Print the interior nodes
    for(int i=1; i<M->NodeNumber+1; i++)
    {
        if (M->Nodes[i].BoundaryNode==false && M->Nodes[i].AttachedElements.size()==0 && M->Nodes[i].DuplicatedNode==false)
        {
            Line = "," + M->ToString(M->Nodes[i].X[0],10,19,false,0);
            if (M->Dimension==2)
            {
                Line += M->ToString(M->Nodes[i].X[1],10,19,true,0);
                Line += ", 0";
            }
            else if (M->Dimension==3)
            {
                Line += M->ToString(M->Nodes[i].X[1],10,19,false,0) + M->ToString(M->Nodes[i].X[2],10,19,true,0);
            }
            Line += ", I";
            if (M->Nodes[i].MarkedForRefinement==true)
            {
                Line += ", F";
            }
            else
            {
                Line += ", C";
            }
            *outfile << Count << Line << endl;
            Count++;
        }
    }
    for(int i=0; i<NewNodesVect.size(); i++)
    {
        if (NewNodesVect[i]->BoundaryNode==false && NewNodesVect[i]->DuplicatedNode==false && NewNodesVect[i]->InDomain==true)
        {
            Line = "," + M->ToString(NewNodesVect[i]->X[0],10,19,false,0);
            if (M->Dimension==2)
            {
                Line += M->ToString(NewNodesVect[i]->X[1],10,19,true,0);
                Line += ", 0";
            }
            else if (M->Dimension==3)
            {
                Line += M->ToString(NewNodesVect[i]->X[1],10,19,false,0) + M->ToString(NewNodesVect[i]->X[2],10,19,true,0);
            }
            Line += ", I, F";
            *outfile << Count << Line << endl;
            Count++;
        }
    }
    
    if (M->Dimension==2)
    {
        *outfile << "*ELEMENT, type=T3D2, ELSET=Line" << endl;
        long Count=1;
        for(int i=1; i<M->ElementNumber+1; i++)
        {
            *outfile << Count << ", " << M->SurfElement[i-1].AttachedNodes[0]->GlobalIndex << ", " <<  M->SurfElement[i-1].AttachedNodes[1]->GlobalIndex << endl;
            Count++;
        }
        for(int i=0; i<NewElements.size(); i++)
        {
            *outfile << Count << ", " << NewElements[i].Node(0) << ", " <<  NewElements[i].Node(1) << endl;
            Count++;
        }
        *outfile << "*ELEMENT, type=CPS3, ELSET=Surface1" << endl;
    }
    else if (M->Dimension==3)
    {
        *outfile << "*ELEMENT, type=CPS3, ELSET=Surface1" << endl;
        long Count=1;
        for(int i=1; i<M->ElementNumber+1; i++)
        {
            *outfile << Count << ", " << M->SurfElement[i-1].AttachedNodes[0]->GlobalIndex << ", " << M->SurfElement[i-1].AttachedNodes[1]->GlobalIndex << ", " << M->SurfElement[i-1].AttachedNodes[2]->GlobalIndex;
            if (M->SurfElement[i-1].RefSurf==-1)
            {
                *outfile << ", -, -" << endl;
            }
            else
            {
                *outfile << ", " << M->SurfElement[i-1].RefSurf << ", " << M->SurfElement[i-1].RefEdge << endl;
            }
            Count++;
        }
        for(int i=0; i<NewElements.size(); i++)
        {
            *outfile << Count << ", " << NewElements[i].Node(0) << ", " << NewElements[i].Node(1) << ", " << NewElements[i].Node(2);
            if (NewElements[i].RefSurf==-1)
            {
                *outfile << ", -, -" << endl;
            }
            else
            {
                *outfile << ", " << NewElements[i].RefSurf << ", " << NewElements[i].RefEdge << endl;
            }
            Count++;
        }
        *outfile << "*END" << endl;
    }
}

void Refinement::PrintRefinedModelVtkFile(Model* M, string Path)
{
    std::ofstream *outfile;
    
    //Identify if iterations have already been done
    int pos = 0;
    pos=Path.find("-It");
    if (pos==-1)
    {
        outfile= new std::ofstream(Path.replace(Path.end()-4,Path.end(),"-It1.vtk"));
    }
    else if (pos>0)
    {
        //Increment the iteration number
        string NameStart=Path;
        NameStart.replace(NameStart.begin()+pos,NameStart.end(),"");
        stringstream ss;
        ss << ItNumber+1;
        string NewName=NameStart + "-It" + ss.str() + ".vtk";
        outfile= new std::ofstream(NewName);
    }
    
    *outfile << "# vtk DataFile Version 2.0" << endl;
    *outfile << "SphereInclusion-10000-0.30-c.vtk" << endl;
    *outfile << "ASCII" << endl;
    *outfile << "" << endl;
    *outfile << "DATASET UNSTRUCTURED_GRID" << endl;
    *outfile << "POINTS " << TotalNodesNewModel << " float" << endl;
    
    //Print the boundary nodes
    long TotalNodeCount=0;
    for(int i=1; i<M->NodeNumber+1; i++)
    {
        if ((M->Nodes[i].BoundaryNode==true || M->Nodes[i].AttachedElements.size()>0) && M->Nodes[i].DuplicatedNode==false)
        {
            if (M->Dimension==2)
                *outfile << "  " << M->Nodes[i].X[0] << " " << M->Nodes[i].X[1] << " " << 0 << endl;
            else if (M->Dimension==3)
                *outfile << "  " << M->Nodes[i].X[0] << " " << M->Nodes[i].X[1] << " " << M->Nodes[i].X[2] << endl;
            TotalNodeCount++;
        }
    }
    for(int i=0; i<NewNodesVect.size(); i++)
    {
        if (NewNodesVect[i]->BoundaryNode==true && NewNodesVect[i]->DuplicatedNode==false && NewNodesVect[i]->InDomain==true)
        {
            if (M->Dimension==2)
                *outfile << "  " << NewNodesVect[i]->X[0] << " " << NewNodesVect[i]->X[1] << " " << 0 << endl;
            else if (M->Dimension==3)
                *outfile << "  " << NewNodesVect[i]->X[0] << " " << NewNodesVect[i]->X[1] << " " << NewNodesVect[i]->X[2] << endl;
            TotalNodeCount++;
        }
    }
    
    //Print the inner nodes
    for(int i=1; i<M->NodeNumber+1; i++)
    {
        if (M->Nodes[i].BoundaryNode==false && M->Nodes[i].AttachedElements.size()==0 && M->Nodes[i].DuplicatedNode==false)
        {
            if (M->Dimension==2)
                *outfile << "  " << M->Nodes[i].X[0] << " " << M->Nodes[i].X[1] << " " << 0 << endl;
            else if (M->Dimension==3)
                *outfile << "  " << M->Nodes[i].X[0] << " " << M->Nodes[i].X[1] << " " << M->Nodes[i].X[2] << endl;
            TotalNodeCount++;
        }
    }
    for(int i=0; i<NewNodesVect.size(); i++)
    {
        if (NewNodesVect[i]->BoundaryNode==false && NewNodesVect[i]->DuplicatedNode==false && NewNodesVect[i]->InDomain==true)
        {
            if (M->Dimension==2)
                *outfile << "  " << NewNodesVect[i]->X[0] << " " << NewNodesVect[i]->X[1] << " " << 0 << endl;
            else if (M->Dimension==3)
                *outfile << "  " << NewNodesVect[i]->X[0] << " " << NewNodesVect[i]->X[1] << " " << NewNodesVect[i]->X[2] << endl;
            TotalNodeCount++;
        }
    }
    *outfile << endl;
    
    //Print the surface element cells
    *outfile << "CELLS " << M->ElementNumber+NewElements.size()+TotalNodeCount << " " << (M->ElementNumber+NewElements.size())*(M->Dimension+1)+TotalNodeCount*2 << endl;
    for(int i=0; i<TotalNodeCount; i++)
    {
        *outfile << "1 " << i << endl;
    }
    for(int i=1; i<M->ElementNumber+1; i++)
    {
        if (M->Dimension==2)
            *outfile << "2 ";
        else if (M->Dimension==3)
            *outfile << "3 ";
        *outfile <<  M->SurfElement[i-1].AttachedNodes[0]->GlobalIndex-1 << " " <<  M->SurfElement[i-1].AttachedNodes[1]->GlobalIndex-1 << " ";
        if (M->Dimension==3)
            *outfile << M->SurfElement[i-1].AttachedNodes[2]->GlobalIndex-1 << " ";
        *outfile << endl;
    }
    for(int i=0; i<NewElements.size(); i++)
    {
        if (M->Dimension==2)
            *outfile << "2 ";
        else if (M->Dimension==3)
            *outfile << "3 ";
        *outfile <<  NewElements[i].Node(0)-1 << " " <<  NewElements[i].Node(1)-1 << " ";
        if (M->Dimension==3)
            *outfile <<  NewElements[i].Node(2)-1 << " ";
        *outfile << endl;
    }

    *outfile << "CELL_TYPES " << M->ElementNumber+NewElements.size()+TotalNodeCount << endl;
    for(int i=0; i<TotalNodeCount; i++)
    {
        *outfile << "1" << endl;
    }
    for (long j=0; j<M->ElementNumber+NewElements.size(); j++)
    {
        if (M->Dimension==2)
            *outfile << "3" << endl;
        else if (M->Dimension==3)
            *outfile << "5" << endl;
    }
    
    //Print the node density
    *outfile << endl;
    *outfile << "POINT_DATA " << TotalNodesNewModel << endl;
    *outfile << "SCALARS NodeDensity float" << endl;
    *outfile << "LOOKUP_TABLE default" << endl;
    //Print the boundary nodes
    for(int i=1; i<M->NodeNumber+1; i++)
    {
        if ((M->Nodes[i].BoundaryNode==true || M->Nodes[i].AttachedElements.size()>0) && M->Nodes[i].DuplicatedNode==false)
        {
            *outfile << "  " << 1/M->Nodes[i].DistClosestNode << endl;
        }
    }
    for(int i=0; i<NewNodesVect.size(); i++)
    {
        if (NewNodesVect[i]->BoundaryNode==true && NewNodesVect[i]->DuplicatedNode==false && NewNodesVect[i]->InDomain==true)
        {
            *outfile << "  " << 1/NewNodesVect[i]->DistClosestNode << endl;
        }
    }
    //Print the inner nodes
    for(int i=1; i<M->NodeNumber+1; i++)
    {
        if (M->Nodes[i].BoundaryNode==false && M->Nodes[i].AttachedElements.size()==0 && M->Nodes[i].DuplicatedNode==false)
        {
            *outfile << "  " << 1/M->Nodes[i].DistClosestNode << endl;
        }
    }
    for(int i=0; i<NewNodesVect.size(); i++)
    {
        if (NewNodesVect[i]->BoundaryNode==false && NewNodesVect[i]->DuplicatedNode==false && NewNodesVect[i]->InDomain==true)
        {
            *outfile << "  " << 1/NewNodesVect[i]->DistClosestNode << endl;
        }
    }
    *outfile << endl;
}

void Refinement::PrintRefinedCollocInpFile(Model* M, string Path)
{
    string Line;
    ofstream *outfile;
    
    //Identify if iterations have already been done
    int pos = 0;
    pos=Path.find("-It");
    if (pos==-1)
    {
        outfile= new ofstream(Path.replace(Path.end()-4,Path.end(),"-It1-c.inp"),ofstream::out | ofstream::trunc);
    }
    else if (pos>0)
    {
        //Increment the iteration number
        string NameStart=Path;
        NameStart.replace(NameStart.begin()+pos,NameStart.end(),"");
        stringstream ss;
        ss << ItNumber+1;
        string NewName=NameStart + "-It" + ss.str() + "-c.inp";
        outfile= new ofstream(NewName,ofstream::out | ofstream::trunc);
    }
    
    //Print the header
    string str;
    char Path2[M->InputFilePath.length()+1];
    strncpy(Path2, M->InputFilePath.c_str(),M->InputFilePath.length()+1);
    ifstream InpFile;
    //Open the selected file
    InpFile.open (Path2, ios::in);
    //Add all the lines of the file in a vector
    while (getline(InpFile, str))
    {
        if (str=="#NODE_DATA")
        {
            break;
        }
        else
        {
            *outfile << str << endl;
        }
    }
    InpFile.close();
    
    *outfile << "#NODE_DATA" << endl;
    {
        for (int ii=0; ii<2; ii++)
        {
            //Print the existing nodes
            for(int i=1; i<M->NodeNumber+1; i++)
            {
                if ((((M->Nodes[i].BoundaryNode==true || M->Nodes[i].AttachedElements.size()>0) && ii==0) || (M->Nodes[i].BoundaryNode==false && ii>0)) && M->Nodes[i].DuplicatedNode==false)
                {                
                    Line = M->ToString(M->Nodes[i].X[0],8,17,false,0);
                    if (M->Dimension==2)
                    {
                        Line += M->ToString(M->Nodes[i].X[1],8,17,true,0);
                    }
                    else if (M->Dimension==3)
                    {
                        Line += M->ToString(M->Nodes[i].X[1],8,17,false,0) + M->ToString(M->Nodes[i].X[2],8,17,true,0);
                    }
                    *outfile << M->ToString(M->Nodes[i].GlobalIndex,0,15,false,0) << Line << endl;
                }
            }

            //Print the new nodes
            for(int i=0; i<NewNodesVect.size(); i++)
            {
                if (((NewNodesVect[i]->BoundaryNode==true && ii==0) || (NewNodesVect[i]->BoundaryNode==false && ii>0)) && NewNodesVect[i]->DuplicatedNode==false && NewNodesVect[i]->InDomain==true)
                {
                    Line = M->ToString(NewNodesVect[i]->X[0],8,17,false,0);
                    if (M->Dimension==2)
                    {
                        Line += M->ToString(NewNodesVect[i]->X[1],8,17,true,0);
                    }
                    else if (M->Dimension==3)
                    {
                        Line += M->ToString(NewNodesVect[i]->X[1],8,17,false,0) + M->ToString(NewNodesVect[i]->X[2],8,17,true,0);
                    }
                    *outfile << M->ToString(NewNodesVect[i]->GlobalIndex,0,15,false,0) << Line << endl;
                }
            }
        }
    }
    
    *outfile << "#BOUNDARY" << endl;    
    //Determine, for 3D problems, the boundary condition of which node shall be considered for the new nodes
    if (M->Dimension==3)
    {
        LoadOldNodesPosition(M);
    }
    
    //Print the BCs of the initial nodes of the model
    for(int i=1; i<M->NodeNumber+1; i++)
    {
        if (M->Nodes[i].BoundaryNode==true && M->Nodes[i].DuplicatedNode==false)
        {
            if (M->Dimension==2)
            {
                PrintBCRow2D(M,false,i,outfile);
            }
            else if (M->Dimension==3)
            {
                PrintBCRow3D(M,false,i,outfile);
            }
        }
    }
    
    //Print the BCs of the new nodes of the model
    for(int i=0; i<NewNodesVect.size(); i++)
    {
        if (NewNodesVect[i]->BoundaryNode==true && NewNodesVect[i]->DuplicatedNode==false && NewNodesVect[i]->InDomain==true)
        {
            if (M->Dimension==2)
            {
                PrintBCRow2D(M,true,i,outfile);
            }
            else if (M->Dimension==3)
            {
                PrintBCRow3D(M,true,i,outfile);
            }
        }
    }
    
    //Print the elements
    PrintElements(M,outfile);
}

void Refinement::PrintBCRow2D(Model* M, bool NewNode, long NodeIndex, ofstream* File)
{
    double Nx=0;
    double Ny=0;
    vector<bool> BC_D_Bool;BC_D_Bool.resize(2);
    vector<bool> BC_N_Bool;BC_N_Bool.resize(2);
    vector<int> BC_DOF;BC_DOF.resize(2);
    vector<bool> LocalBoundaryCondition;LocalBoundaryCondition.resize(2);
    vector<double> BC_D;BC_D.resize(2);
    vector<double> BC_N;BC_N.resize(2);
    long NodeIndexPrint=0;
    if (NewNode==false)
    {
        Nx=M->Nodes[NodeIndex].Normal[0][0];
        Ny=M->Nodes[NodeIndex].Normal[0][1];
        for (int j=0; j<2; j++)
        {
            BC_D_Bool[j]=M->Nodes[NodeIndex].BC_D_Bool[j];
            BC_N_Bool[j]=M->Nodes[NodeIndex].BC_N_Bool[j];
            BC_DOF[j]=M->Nodes[NodeIndex].BC_DOF[j];
            LocalBoundaryCondition[j]=M->Nodes[NodeIndex].LocalBoundaryCondition[j];
            BC_D[j]=M->Nodes[NodeIndex].BC_D[j];
            BC_N[j]=M->Nodes[NodeIndex].BC_N[j];
        }
        NodeIndexPrint=M->Nodes[NodeIndex].GlobalIndex;
    }
    else if (NewNode==true)
    {
        Nx=NewNodesVect[NodeIndex]->Normal[0];
        Ny=NewNodesVect[NodeIndex]->Normal[1];
        for (int j=0; j<2; j++)
        {
            BC_D_Bool[j]=NewNodesVect[NodeIndex]->BC_D_Bool[j];
            BC_N_Bool[j]=NewNodesVect[NodeIndex]->BC_N_Bool[j];
            BC_DOF[j]=NewNodesVect[NodeIndex]->BC_DOF[j];
            LocalBoundaryCondition[j]=NewNodesVect[NodeIndex]->LocalBoundaryCondition[j];
            BC_D[j]=NewNodesVect[NodeIndex]->BC_D[j];
            BC_N[j]=NewNodesVect[NodeIndex]->BC_N[j];
        }
        NodeIndexPrint=NewNodesVect[NodeIndex]->GlobalIndex;
    }
    
    
    if (BC_D_Bool[1]==true && BC_DOF[1]==0 && LocalBoundaryCondition[1]==true)
    {
        *File << " " << NodeIndexPrint << ", 2, " << M->ToString(Nx,8,17,false,0) << M->ToString(Ny,8,17,false,0) << "N, L, " << M->ToString(BC_N[0],8,17,true,0) << endl;
        *File << " " << NodeIndexPrint << ", 1, " << M->ToString(Nx,8,17,false,0) << M->ToString(Ny,8,17,false,0) << "D, L, " << M->ToString(BC_D[1],8,17,true,0) << endl;
    }
    else
    {
        *File << " " << NodeIndexPrint << ", *, " << M->ToString(Nx,8,17,false,0) << M->ToString(Ny,8,17,false,0);
        for (int k=0; k<M->Dimension; k++)
        {
            if (BC_D_Bool[k]==true)
                *File << "D, ";
            else if (BC_N_Bool[k]==true)
                *File << "N, ";
            if (LocalBoundaryCondition[k]==true)
                *File << "L, ";
            else if (LocalBoundaryCondition[k]==false)
                *File << "G, ";
            bool Last=false;
            if (k==M->Dimension-1)
                Last=true;
            if (BC_D_Bool[k]==true)
                *File << M->ToString(BC_D[k],8,17,Last,0);
            else if (BC_N_Bool[k]==true)
                *File << M->ToString(BC_N[k],8,17,Last,0);
        }
        *File << endl;
    }
}

void Refinement::LoadOldNodesPosition(Model* M)
{
    for(int i=1; i<=M->NodeNumber; i++)
    {
        if (M->Nodes[i].BoundaryNode==true || M->Nodes[i].AttachedElements.size()>0)
        {
            vector<int> NodeSurf;
            NodeSurf.clear();
            //Add the surface index of all the elements attached to a node
            for (int j=0; j<M->Nodes[i].AttachedElements.size(); j++)
            {
                long ElNum=M->Nodes[i].AttachedElements[j]-1;
                if (M->SurfElement[ElNum].RefSurfOfEl>-0.5)
                {
                    NodeSurf.push_back(M->SurfElement[ElNum].RefSurfOfEl);
                }
            }
            //Remove the duplicated surface indices of the vector
            std::sort(NodeSurf.begin(),NodeSurf.end());
            NodeSurf.erase(std::unique(NodeSurf.begin(),NodeSurf.end()),NodeSurf.end());
            //Determine the position of the node on the geometry based o nthe number of attached surfaces
            if (NodeSurf.size()==1)
            {
                M->Nodes[i].FaceNode=true;
            }
            else if (NodeSurf.size()==2)
            {
                M->Nodes[i].EdgeNode=true;
            }
            else if (NodeSurf.size()>2)
            {
                M->Nodes[i].CornerNode=true;
            }
        }
    }
}

void Refinement::PrintBCRow3D(Model* M, bool NewNode, long NodeIndex, ofstream* File)
{
    //Identify the reference node
    long RefNode=NodeIndex;
    long GlobalIndex=0;
    
    if (NewNode==true)
    {
        if (M->Nodes[NewNodesVect[NodeIndex]->ParentNodes[0]].CornerNode==true)
        {
            RefNode=NewNodesVect[NodeIndex]->ParentNodes[1];
        }
        else if (M->Nodes[NewNodesVect[NodeIndex]->ParentNodes[1]].CornerNode==true)
        {
            RefNode=NewNodesVect[NodeIndex]->ParentNodes[0];
        }
        else if (M->Nodes[NewNodesVect[NodeIndex]->ParentNodes[0]].EdgeNode==true && M->Nodes[NewNodesVect[NodeIndex]->ParentNodes[1]].CornerNode==false)
        {
            RefNode=NewNodesVect[NodeIndex]->ParentNodes[1];
        }
        else if (M->Nodes[NewNodesVect[NodeIndex]->ParentNodes[1]].EdgeNode==true && M->Nodes[NewNodesVect[NodeIndex]->ParentNodes[0]].CornerNode==false)
        {
            RefNode=NewNodesVect[NodeIndex]->ParentNodes[0];
        }
        else
        {
            RefNode=NewNodesVect[NodeIndex]->ParentNodes[0];
        }
    }
    
    //Set the boundary conditions of the node
    vector<vector<double>> AttachedNormals;
    AttachedNormals.resize(3);
    vector<bool> BC_D_Bool(3);
    vector<bool> BC_N_Bool(3);
    vector<bool> LocalBoundaryCondition(3);
    vector<double> BC_D(3);
    vector<double> BC_N(3);
    vector<int> BC_DOF(3);
    vector<vector<double>> AttachedElementsNormals;
    
    //Initialize the vectors
    for (int j=0; j<3; j++)
    {
        AttachedNormals[j].resize(3);
        BC_D_Bool[j]=false;
        BC_N_Bool[j]=false;
        LocalBoundaryCondition[j]=false;
        BC_D[j]=0;
        BC_N[j]=0;
        BC_DOF[j]=j;
    }
    
    if (NewNode==false)
    {
        //Set the global index
        GlobalIndex=M->Nodes[RefNode].GlobalIndex;
        //GlobalIndex=M->Nodes[RefNode].NodeNum;
        //Set the boundary conditions
        for (int j=0; j<3; j++)
        {
            BC_D_Bool[j]=M->Nodes[RefNode].BC_D_Bool[j];
            BC_N_Bool[j]=M->Nodes[RefNode].BC_N_Bool[j];
            LocalBoundaryCondition[j]=M->Nodes[RefNode].LocalBoundaryCondition[j];
            BC_D[j]=M->Nodes[RefNode].BC_D[j];
            BC_N[j]=M->Nodes[RefNode].BC_N[j];
            BC_DOF[j]=M->Nodes[RefNode].BC_DOF[j];
            for (int k=0; k<3; k++)
            {
                AttachedNormals[j][k]=M->Nodes[RefNode].Normal[j][k];
            }
        }
    }
    else
    {
        //Set the global index
        GlobalIndex=NewNodesVect[NodeIndex]->GlobalIndex;
        
        //Get the corrdinates of the new node
        Vector3d NewNodeCoord;
        if (NewNode==true)
        {
            NewNodeCoord(0)=NewNodesVect[NodeIndex]->X[0];
            NewNodeCoord(1)=NewNodesVect[NodeIndex]->X[1];
            NewNodeCoord(2)=NewNodesVect[NodeIndex]->X[2];
        }

        //Get the normals at the nodes in the surfaces of all the attached elements
        AttachedElementsNormals.resize(M->Nodes[RefNode].AttachedElements.size());
        
        for (int j=0; j<M->Nodes[RefNode].AttachedElements.size(); j++)
        {
            //Compute normal from the CAD model only if the ref surface is different
            AttachedElementsNormals[j].resize(3);
            long RefEl=M->Nodes[RefNode].AttachedElements[j]-1;
            if (j==0)
            {
                Vector3d NormalToElSurf=M->ModelCAD->GetNormal(NewNodeCoord,M->SurfElement[RefEl].RefSurfOfEl);
                for (int k=0; k<3; k++)
                {
                    AttachedElementsNormals[j][k]=NormalToElSurf(k);
                }
            }
            else if (j>0)
            {
                if(M->SurfElement[RefEl].RefSurfOfEl==M->SurfElement[M->Nodes[RefNode].AttachedElements[j-1]].RefSurfOfEl)
                {
                    for (int k=0; k<3; k++)
                    {
                        AttachedElementsNormals[j][k]=AttachedElementsNormals[j-1][k];
                    }
                }
                else
                {
                    Vector3d NormalToElSurf=M->ModelCAD->GetNormal(NewNodeCoord,M->SurfElement[RefEl].RefSurfOfEl);
                    for (int k=0; k<3; k++)
                    {
                        AttachedElementsNormals[j][k]=NormalToElSurf(k);
                    }
                }
            }
        }
        
        //Set the stress loading
        for (int j=0; j<M->Nodes[RefNode].AttachedElements.size(); j++)
        {
            long ElementIndex=M->Nodes[RefNode].AttachedElements[j]-1;
            bool HasStressLoading=false;
            int LoadingDir=0;
            for (int k=0; k<3; k++)
            {
                if (abs(M->SurfElement[ElementIndex].BC_N[k])>0)
                {
                    HasStressLoading=true;
                    LoadingDir=k;
                }
            }
            if (HasStressLoading==true)
            {
                for (int k=0; k<3; k++)
                {
                    //Set all the normals to the normals of the set
                    for (int kk=0; kk<3; kk++)
                    {
                        AttachedNormals[k][kk]=AttachedElementsNormals[j][kk];
                    }
                    BC_N_Bool[k]=true;
                    //Convert local BC to global BC
                    LocalBoundaryCondition[k]=false;
                    if (M->SurfElement[ElementIndex].LocalBoundaryCondition[LoadingDir]==true)
                    {
                        BC_N[k]=round(AttachedElementsNormals[j][k]*pow(10,10))/pow(10,10)*M->SurfElement[ElementIndex].BC_N[LoadingDir];
                    }
                    else
                    {
                        BC_N[k]=M->SurfElement[ElementIndex].BC_N[k];
                    }
                }
            }
        }
        
        //Set the BCD conditions
        for (int j=0; j<M->Nodes[RefNode].AttachedElements.size(); j++)
        {
            int ElementIndex=M->Nodes[RefNode].AttachedElements[j]-1;
            bool HasBCD=false;
            for (int k=0; k<3; k++)
            {
                if (M->SurfElement[ElementIndex].BC_D_Bool[k]==true)
                {
                    HasBCD=true;
                    //if (M->SurfElement[ElementIndex].LocalBoundaryCondition[k]==true)
                    {
                        //Identify the direction of the normal
                        double MaxVal=0;
                        int MaxDir=0;
                        for (int kk=0; kk<3; kk++)
                        {
                            if (abs(AttachedElementsNormals[j][kk])>MaxVal)
                            {
                                MaxVal=abs(AttachedElementsNormals[j][kk]);
                                MaxDir=kk;
                            }
                        }
                        BC_D_Bool[MaxDir]=true;
                        BC_DOF[MaxDir]=0;
                        LocalBoundaryCondition[MaxDir]=true;
                        BC_D[MaxDir]=M->SurfElement[ElementIndex].BC_D[k];
                        //Set the normal
                        for (int kk=0; kk<3; kk++)
                        {
                            AttachedNormals[MaxDir][kk]=AttachedElementsNormals[j][kk];
                        }
                        int CountBCDOF=1;
                        //Set the normals and BCN in the other directions
                        for (int kk=0; kk<3; kk++)
                        {
                            if (BC_D_Bool[kk]==false && BC_N_Bool[kk]==false)
                            {
                                //Set the normal
                                for (int kkk=0; kkk<3; kkk++)
                                {
                                    AttachedNormals[kk][kkk]=AttachedElementsNormals[j][kkk];
                                }
                                BC_N_Bool[kk]=true;
                                BC_N[kk]=0;
                                BC_DOF[kk]=CountBCDOF;
                                LocalBoundaryCondition[kk]=true;
                                CountBCDOF++;
                            }
                        }
                    }
                }
            }
        }
        
        //Set the stress free boundary conditions
        for (int j=0; j<M->Nodes[RefNode].AttachedElements.size(); j++)
        {
            for (int k=0; k<3; k++)
            {
                if (BC_D_Bool[k]==false && BC_N_Bool[k]==false)
                {
                    for (int kk=0; kk<3; kk++)
                    {
                        AttachedNormals[k][kk]=AttachedElementsNormals[j][kk];
                    }
                    BC_N_Bool[k]=true;
                    BC_N[k]=0;
                }
            }
        }
    }
    
    //Determine if there are multiple normals
    bool MultipleNormals=false;
    for (int j=0; j<3; j++)
    {
        if (BC_DOF[j] != j || LocalBoundaryCondition[j]==true)
        {
            MultipleNormals=true;
            break;
        }
    }
    if (MultipleNormals==false)
    {
        for (int j=1; j<3; j++)
        {
            if (AttachedNormals[j][0]!=AttachedNormals[j-1][0])
            {
                MultipleNormals=true;
                break;
            }
        }
    }
    if (NewNode==false && MultipleNormals==false)
    {
        if (M->Nodes[RefNode].MultipleBCRows==true)
        {
            MultipleNormals=true;
        }
    }
    if (abs(BC_N[0])>0 || abs(BC_N[1])>0 || abs(BC_N[2])>0)
    {
        MultipleNormals=true;
    }
    
    for (int j=0; j<M->Dimension; j++)
    {
        if (MultipleNormals==false && j==0)
        {
            *File << " " << GlobalIndex << ", *, " << M->ToString(AttachedNormals[0][0],8,17,false,10) << M->ToString(AttachedNormals[0][1],8,17,false,10) << M->ToString(AttachedNormals[0][2],8,17,false,10);
        }
        else if (MultipleNormals==true)
        {
            *File << " " << GlobalIndex << ", " << BC_DOF[j]+1 << ", " << M->ToString(AttachedNormals[j][0],8,17,false,10) << M->ToString(AttachedNormals[j][1],8,17,false,10) << M->ToString(AttachedNormals[j][2],8,17,false,10);
        }
        
        if (BC_D_Bool[j]==true)
            *File << "D, ";
        else if (BC_N_Bool[j]==true)
            *File << "N, ";
        if (LocalBoundaryCondition[j]==true)
            *File << "L, ";
        else if (LocalBoundaryCondition[j]==false)
            *File << "G, ";
        bool Last=false;
        if (j==M->Dimension-1 || MultipleNormals==true)
            Last=true;
        if (BC_D_Bool[j]==true)
            *File << M->ToString(BC_D[j],8,17,Last,0);
        else if (BC_N_Bool[j]==true)
            *File << M->ToString(BC_N[j],8,17,Last,0);
        
        if (MultipleNormals==false && j==M->Dimension-1)
        {
            *File << endl;
        }
        else if (MultipleNormals==true)
        {
            *File << endl;
        }
    }
}

void Refinement::PrintElements(Model* M, ofstream* outfile)
{
    *outfile << "#SURFACE_NUMBER=1" << endl;
    *outfile << "#SURFACE_TYPE=1,OUTER" << endl;
    if (M->Dimension==2)
    {
        *outfile << "#SURFACE_EL=1," << M->ElementNumber+NewElements.size() << ",2D_LINEAR_WITH_BC" << endl;
    }
    else if (M->Dimension==3)
    {
        *outfile << "#SURFACE_EL=1," << M->ElementNumber+NewElements.size() << ",3D_LINEAR_WITH_BC_CAD" << endl;
    }
    
    long Count=1;
    //Print the "old" surface elements
    for(int i=1; i<M->ElementNumber+1; i++)
    {
        *outfile << Count << ", " << M->SurfElement[i-1].AttachedNodes[0]->GlobalIndex << ", " << M->SurfElement[i-1].AttachedNodes[1]->GlobalIndex;
        if (M->Dimension==3)
        {
            *outfile << ", " << M->SurfElement[i-1].AttachedNodes[2]->GlobalIndex;
        }
        //Print the normal
        *outfile << ", " << M->ToString(M->SurfElement[i-1].Normal[0],8,17,false,10);
        if (M->Dimension==2)
        {
            *outfile << M->ToString(M->SurfElement[i-1].Normal[1],8,17,true,10);
        }
        else if (M->Dimension==3)
        {
            *outfile << M->ToString(M->SurfElement[i-1].Normal[1],8,17,false,10) << M->ToString(M->SurfElement[i-1].Normal[2],8,17,true,10);
        }
        PrintElementBCs(M,i-1,outfile,true);
        Count++;
    }
    
    //Print the "new" surface elements
    if ((EvenItNumber==true && M->Dimension==2) || M->Dimension==3)
    {
        for(int i=0; i<NewElements.size(); i++)
        {
            *outfile << " " << Count << ", " << NewElements[i].Node(0) << ", " << NewElements[i].Node(1);
            if (M->Dimension==3)
            {
                *outfile << ", " << NewElements[i].Node(2);
            }
            //Print the normal
            *outfile << ", " << M->ToString(NewElements[i].Normal(0),10,19,false,10);
            if (M->Dimension==2)
            {
                *outfile << M->ToString(NewElements[i].Normal(1),8,17,true,10);
            }
            else if (M->Dimension==3)
            {
                *outfile << M->ToString(NewElements[i].Normal(1),8,17,false,10) << M->ToString(NewElements[i].Normal(2),8,17,true,10);
            }
            PrintElementBCs(M,NewElements[i].RefElement,outfile,NewElements[i].HasModelEdgeNode);
            Count++;
        }
    }
    *outfile << "#END" << endl;
}

void Refinement::PrintElementBCs(Model* M, long ElIndex, ofstream* outfile, bool HasModelEdge)
{
    //Print the boundary conditions associated to the element
    *outfile << ", ";
    for (int j=0; j<M->Dimension; j++)
    {
        if (M->SurfElement[ElIndex].BC_D_Bool[j]==true)
        {
            *outfile << "D, ";
        }
        else
        {
            *outfile << "N, ";
        }
        if (M->SurfElement[ElIndex].LocalBoundaryCondition[j]==true)
        {
            *outfile << "L, ";
        }
        else
        {
            *outfile << "G, ";
        }
        if (M->SurfElement[ElIndex].BC_D_Bool[j]==true)
        {
            *outfile << M->SurfElement[ElIndex].BC_D[j];
        }
        else
        {
            *outfile << M->SurfElement[ElIndex].BC_N[j];
        }
        if (j==M->Dimension-1)
        {
            if (HasModelEdge==true)
            {
                *outfile << ", " << M->SurfElement[ElIndex].RefSurf << ", " << M->SurfElement[ElIndex].RefEdge << ", " << M->SurfElement[ElIndex].RefSurfOfEl << endl;
            }
            else
            {
                *outfile << ", -1, -1, " << M->SurfElement[ElIndex].RefSurfOfEl << endl;
            }
        }
        else
        {
            *outfile << ", ";
        }
    }
}