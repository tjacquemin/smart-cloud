#include "ErrorEstimator.h"

void ErrorEstimator::ComputeErrorEstimator_RES(Model* M,StencilSelect* S,LinearProblem* L)
{
    //Get all the voronoi corner nodes
    GetAllVoronoiNodes(M,S);
    M->Print("Voronoi corner nodes identified.",true);
    
    //Load the supports of the voronoi nodes
    S->LoadSupportCGAL(M,VoronoiNodes,VoroNodeCount,1);
    M->Print("Voronoi corner nodes support loaded.",true);
    
    //Load the derivative coefficients
    GFD_Problem* M_Temp;
    M_Temp=new GFD_Problem;
    long* TripStressTreadStartIncTemp;
    M_Temp->InitializeProblem(M,TripStressTreadStartIncTemp);
    //Load the derivative coefficients
    #pragma omp parallel num_threads(M->ThreadNumber) 
    {
        M_Temp->LoadDerivativeCoefficients(M,VoronoiNodes,VoroNodeCount,omp_get_thread_num(),M->ThreadNumber);
    }
    //Get the residual at each voronoi node
    M->Print("Loading the coefficients.",true);
    #pragma omp parallel num_threads(M->ThreadNumber) 
    {
        GetVoronoiResiduals(M,M_Temp,L,VoroNodeCount,omp_get_thread_num(),M->ThreadNumber);
    }
    
    //Get the residual at each collocation node
    GetVoronoiErrors(M);
}

//Get all the voronoi nodes and identify the associated collocation nodes
void ErrorEstimator::GetAllVoronoiNodes(Model* M,StencilSelect* S)
{
    long TreeSize=0;
    for (long i=1; i<=M->NodeNumber; i++)
    {
        M->Nodes[i].FindVoroCN();
        TreeSize+=M->Nodes[i].VoroCN.size();
        M->Nodes[i].VoroCN_Index.clear();
    }
    
    //Load all the voronoi nodes into a tree
    Tree VoroAllTree;
    
    //Load the combined tree
    std::vector<Point_3> Points(TreeSize);
    std::vector<long> Indices(TreeSize);
    vector<double> DistToRef(TreeSize);
    vector<bool> IncludeVoroNode(TreeSize);
    vector<vector<long>> RefCollocNodes(TreeSize);
    vector<vector<long>> AttachedCollocNodesTemp(TreeSize);
    
    long Count=0;
    for (long i=1; i<=M->NodeNumber; i++)
    {
        for (int j=0; j<M->Nodes[i].VoroCN.size(); j++)
        {
            if (M->Dimension==2)
                Points[Count]=Point_3(M->Nodes[i].VoroCN[j][0],M->Nodes[i].VoroCN[j][1],0.0);
            else if (M->Dimension==3)
                Points[Count]=Point_3(M->Nodes[i].VoroCN[j][0],M->Nodes[i].VoroCN[j][1],M->Nodes[i].VoroCN[j][2]);
            Indices[Count]=Count;            
            DistToRef[Count]=M->Nodes[i].VoroCN_DistToRef[j];
            IncludeVoroNode[Count]=true;
            RefCollocNodes[Count].resize(2);
            RefCollocNodes[Count][0]=i;
            RefCollocNodes[Count][1]=j;
            Count++;
        }
    }
    
    VoroAllTree.insert(
    boost::make_zip_iterator(boost::make_tuple(Points.begin(),Indices.begin())),
    boost::make_zip_iterator(boost::make_tuple(Points.end(),Indices.end())));
    
    //Identify all the voronoi nodes to be kept
    for (long i=0; i<TreeSize; i++)
    {
        AttachedCollocNodesTemp[i].clear();
        //Locate all the voronoi nodes at the same location than the one marked true and set them to false
        if (IncludeVoroNode[i]==true)
        {
            //Create the search query
            Distance tr_dist;
            //Search K nearest neighbours. The number of selected neighbors is the normal number of support nodes of a collocation node.
            //It should be much larger than the number of natural neighbors.
            int k;
            if (Dimension==2)
                k=15;
            else if (Dimension==3)
                k=58;
            Point_3 query(Points[i].x(),Points[i].y(),Points[i].z());
            K_neighbor_search search(VoroAllTree,query,k);
            for(K_neighbor_search::iterator it = search.begin(); it != search.end(); it++)
            {
                long VoroNodeIndex=boost::get<1>(it->first);
                double VoroNodeDist=tr_dist.inverse_of_transformed_distance(it->second);
                if (VoroNodeDist<DistToRef[i]/1000 && VoroNodeIndex!=i)
                {
                    //Load the AttachedCollocNodes
                    AttachedCollocNodesTemp[i].push_back(RefCollocNodes[VoroNodeIndex][0]);
                    IncludeVoroNode[VoroNodeIndex]=false;
                }
                else if (VoroNodeIndex==i)
                {
                    AttachedCollocNodesTemp[i].push_back(RefCollocNodes[VoroNodeIndex][0]);
                }
            }
        }
    }
    
    //Clear the vectors
    Points.clear();
    Indices.clear();
    
    //Count the number of unique Voronoi nodes
    VoroNodeCount=0;
    //Load the connectivities Voronoi Nodes / Collocation Nodes
    AttachedVoroNodes.resize(M->NodeNumber+1);
    //Identify the nodes which are outside of the domain
    vector<double> TempVect;
    TempVect.resize(3);
    for (long i=0; i<TreeSize; i++)
    {
        if (IncludeVoroNode[i]==true)
        {
            long RefCollocNode=RefCollocNodes[i][0];
            long RefVoroNode=RefCollocNodes[i][1];
            TempVect[0]=M->Nodes[RefCollocNode].VoroCN[RefVoroNode][0];
            TempVect[1]=M->Nodes[RefCollocNode].VoroCN[RefVoroNode][1];
            TempVect[2]=0;
            if (M->Dimension==3)
            {
                TempVect[2]=M->Nodes[RefCollocNode].VoroCN[RefVoroNode][2];
            }
            if (S->InDomain(M,TempVect,false,false)==true)
            {
                IncludeVoroNode[i]=true;
                for (int j=0; j<AttachedCollocNodesTemp[i].size(); j++)
                {
                    AttachedVoroNodes[AttachedCollocNodesTemp[i][j]].push_back(VoroNodeCount+1);
                }
                VoroNodeCount++;
            }
            else
            {
                IncludeVoroNode[i]=false;
            }
        }
    }
    
    //Create an array of Voronoi nodes
    VoronoiNodes=new Node[VoroNodeCount+1];
    
    //Load the Voronoi nodes
    Count=1;
    for (long i=0; i<TreeSize; i++)
    {
        if (IncludeVoroNode[i]==true)
        {
            long RefCollocNode=RefCollocNodes[i][0];
            long RefVoroNode=RefCollocNodes[i][1];
            double X1=M->Nodes[RefCollocNode].VoroCN[RefVoroNode][0];
            double X2=M->Nodes[RefCollocNode].VoroCN[RefVoroNode][1];
            if (M->Dimension==2)
            {
                VoronoiNodes[Count].SetNode(Count,M->Dimension,X1,X2,0);
            }
            else if (M->Dimension==3)
            {
                double X3=M->Nodes[RefCollocNode].VoroCN[RefVoroNode][2];
                VoronoiNodes[Count].SetNode(Count,M->Dimension,X1,X2,X3);
            }
            VoronoiNodes[Count].CollocDOF=false;
            Count++;
        }
    }
}

void ErrorEstimator::GetVoronoiResiduals(Model* M, GFD_Problem* G,LinearProblem* L,long NodeNum,int ThreadIndex,int TreadNum)
{
    //Resize the Voro error estimator arrays
    EstimVoro.resize(VoroNodeCount*Dimension);
    ErrorEstimVoro.resize(VoroNodeCount);
    
    long Start, End;
    //Get the start and end node of the thread
    M->GetStartEndNode(ThreadIndex,TreadNum,Start,End,NodeNum);
    
    //Add the coefficients to the defined functions
    for (long i=Start; i<=End; i++)
    {
        double Eqn1=0;
        double Eqn2=0;
        for (int j=0; j<VoronoiNodes[i].SupSize; j++)
        {
            double Coeff2=VoronoiNodes[i].Coeff[j][2];
            double Coeff3=VoronoiNodes[i].Coeff[j][3];
            double Coeff4=VoronoiNodes[i].Coeff[j][4];
            
            long SupNode=VoronoiNodes[i].SupNodes[j];
            long RowU1=Dimension*(SupNode-1);
            long RowU2=Dimension*(SupNode-1)+1;
            double U1=L->U(RowU1);
            double U2=L->U(RowU2);
            //Equilibrium - Equation 1
            Eqn1+=U1*(G->d1*Coeff2+G->d3/2*Coeff4)+U2*((G->d2+G->d3/2)*Coeff3);
            //Equilibrium - Equation 2
            Eqn2+=U1*((G->d2+G->d3/2)*Coeff3)+U2*(G->d1*Coeff4+G->d3/2*Coeff2);
        }
        //The body forces are considered to be equal to zero
        EstimVoro((i-1)*Dimension+0)=Eqn1-0;
        EstimVoro((i-1)*Dimension+1)=Eqn2-0;
        ErrorEstimVoro(i-1)=pow(pow(EstimVoro((i-1)*Dimension+0),2)+pow(EstimVoro((i-1)*Dimension+1),2),0.5);
        if (i==5)
        {
            cout << VoronoiNodes[i].X[0]*1000 << "," << VoronoiNodes[i].X[1]*1000 << "," << EstimVoro((i-1)*Dimension+0)*1000 << "," << EstimVoro((i-1)*Dimension+1)*1000 << endl;
            for (int j=0; j<VoronoiNodes[i].SupSize; j++)
            {
                long SupNode=VoronoiNodes[i].SupNodes[j];
                cout << j << "," << M->Nodes[SupNode].X[0]*1000 << "," << M->Nodes[SupNode].X[1]*1000 << endl;
            }
        }
            
    }
}

void ErrorEstimator::GetVoronoiErrors(Model* M)
{
    for (long i=1; i<=M->NodeNumber; i++)
    {
        double ErrorEstimTemp=0;
        for (int m=0; m<Dimension; m++)
        {
            Estim((i-1)*Dimension+m)=0;
            for (int j=0; j<AttachedVoroNodes[i].size(); j++)
            {
                long VoroCNIndex=AttachedVoroNodes[i][j];
                Estim((i-1)*Dimension+m)+=EstimVoro((VoroCNIndex-1)*Dimension+m);
            }
            //Use the average error of the closest nodes if the considered colocation node has no voro corner node in the domain
            if (AttachedVoroNodes[i].size()==0)
            {
                int Count=0;
                for (int j=0; j<4*(Dimension-1); j++)
                {
                    //Get the closest node
                    long LocNode=M->Nodes[i].SupNodes[j+1];
                    if (AttachedVoroNodes[LocNode].size()>0)
                    {
                        Count++;
                        for (int k=0; k<AttachedVoroNodes[LocNode].size(); k++)
                        {
                            long VoroCNIndex=AttachedVoroNodes[LocNode][k];
                            Estim((i-1)*Dimension+m)+=EstimVoro((VoroCNIndex-1)*Dimension+m);
                        }
                    }
                }
                Estim((i-1)*Dimension+m)=Estim((i-1)*Dimension+m)/Count;
            }
            if (AttachedVoroNodes[i].size()>0)
            {
                Estim((i-1)*Dimension+m)=Estim((i-1)*Dimension+m)/AttachedVoroNodes[i].size();
            }
            ErrorEstimTemp+=pow(Estim((i-1)*Dimension+m),2);
        }
        ErrorEstimTemp=pow(ErrorEstimTemp,0.5);
        ErrorEstim(i-1)=ErrorEstimTemp;
    }
}
