#ifndef MAIN_H
#define MAIN_H

#include <string>
#include <iostream>
#include <iomanip> 
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#include <omp.h>
#include <time.h>
#include <ctime>
#include <mpi.h>
#include <list>
#include <cmath>
#include <cstdlib>

#include "CgalLib.h"
#include "EigenLib.h"
#include "Petsc.h"
#include <unistd.h>
#include <utility>
//#include "slepceps.h"

using namespace std;
using namespace Eigen;

struct Trow{
        long row;
        int dof;
        vector<T> Trip;
    };

#endif /* MAIN_H */

