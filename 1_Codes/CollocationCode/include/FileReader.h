#ifndef FILEREADER_H
#define FILEREADER_H

#include "Model.h"
#include "StencilSelect.h"
#include "LinearProblem.h"

class FileReader {
public:
    FileReader();
    FileReader(const FileReader& orig);
    virtual ~FileReader();
    
    //Load input file functions
    long InpArraySize=0;
    void LoadInputFileArray(string,Model*);
    void LoadInputs(Model*,StencilSelect*);
    
private:
    
    //Load function
    long LoadNodeData(Model*,long);
    long LoadBC_D(Model*,long);
    long LoadBC_N(Model*,long);
    long LoadBC_All(Model*,long);
    long LoadExactSolution(Model*,long);
    long LoadSurfaceVertices(Model*,long,int);  
    long LoadSurfaceElements(Model*,long,long,string);
    long LoadSingularNodes(Model*,long);

};

#endif /* FILEREADER_H */

