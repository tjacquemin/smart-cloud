#ifndef GFD_INTRINSIC_H
#define GFD_INTRINSIC_H

#include "Model.h"

class GFD_Intrinsic {
public:
    GFD_Intrinsic();
    GFD_Intrinsic(const GFD_Intrinsic& orig);
    virtual ~GFD_Intrinsic();
    void InitializeProblem(Model*,long*);
    void LoadDerivativeCoefficients(Model*,Node*,long,int,int);
    void LoadVectors(Model*,double*,vector<vector<Trow>>&,T*,T*,int,int);
    
    //Public variables
    double d1=0, d2=0, d3=0;
    
    //Conditioning
    double MinCond=0;
    double MaxCond=0;
    bool MaxCondFound=false;
private:
    MatrixXd C_Matrix(Model*,Node*,bool);
    void LoadRows2D_Stress(Model* M,VectorXd,long,long,int,int,T*,long&);
    void LoadRows2D_Stiff(Model*,VectorXd,long,long,int,int,double*,vector<vector<Trow>>&,long&,int,long);
    
    int Dimension=2;
    double w(Model*,double,double*,double*,double);
    double factorial(int);
    int lTayl=0;
    long *TripStressTreadStartInc;
    long *TripStiffTreadStartInc;
    
    double Angle(Model*,long,int,long);
    

};

#endif /* GFD_INTRINSIC_H */

