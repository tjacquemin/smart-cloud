#ifndef TRIANGULATION_H
#define TRIANGULATION_H

#include "Model.h"

typedef CGAL::Epick_d<CGAL::Dynamic_dimension_tag> K2;
typedef CGAL::Triangulation_data_structure<CGAL::Dynamic_dimension_tag,CGAL::Triangulation_vertex<K2,int>,CGAL::Triangulation_full_cell<K2>> Tds;
typedef CGAL::Triangulation<K2,Tds> TriangulationC;
typedef TriangulationC::Full_cell_iterator FullCellsIt;
typedef TriangulationC::Vertex_iterator VertexIt;

class Triangulation {
public:
    Triangulation();
    Triangulation(const Triangulation& orig);
    virtual ~Triangulation();
    
    void ComputeTriangulation(Model*);
    TriangulationC* t;
private:
    
};

#endif /* TRIANGULATION_H */

