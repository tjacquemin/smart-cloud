#ifndef OCCTLIB_H
#define OCCTLIB_H

//Open Cascade includes
#include <BRep_Tool.hxx>
#include <BRepTools.hxx> 
#include <BRepBuilderAPI_MakeVertex.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx> 
#include <BRepGProp.hxx>
#include <GProp_GProps.hxx>
#include <GCPnts_UniformAbscissa.hxx>
#include <GCPnts_AbscissaPoint.hxx>

//Reading STEP file
#include <STEPControl_Reader.hxx> 
#include <TColgp_HArray1OfPnt.hxx>

//Topology includes
#include <TopoDS.hxx>
#include <TopoDS_Shape.hxx> 
#include <TopoDS_Vertex.hxx>
#include <TopoDS_Edge.hxx> 
#include <TopoDS_Face.hxx>
#include <TopoDS_Iterator.hxx>
#include <TopExp.hxx>
#include <TopTools_IndexedMapOfShape.hxx>
#include <TColgp_HArray1OfPnt.hxx>

//Geometry includes
#include <gp_Pnt.hxx> 
#include <gp_Pnt2d.hxx>
#include <gp_Pln.hxx>
#include <GeomAPI.hxx>
#include <Geom_Line.hxx>
#include <Geom_Ellipse.hxx>
#include <Geom2dAPI_ProjectPointOnCurve.hxx>
#include <GeomAPI_ProjectPointOnCurve.hxx>
#include <GeomAPI_ProjectPointOnSurf.hxx>
#include <BRepAdaptor_Surface.hxx>
#include <ShapeAnalysis_Edge.hxx>
#include <ShapeAnalysis_Surface.hxx>
#include <BRepClass3d_SolidClassifier.hxx>

//Intersection
#include <BOPAlgo_Builder.hxx>
#include <BRepExtrema_DistShapeShape.hxx>

#endif /* OCCTLIB_H */

