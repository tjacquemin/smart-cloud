#ifndef REFINEMENTL_H
#define REFINEMENT_H

#include "Model.h"
#include "StencilSelect.h"
#include "NewNode.h"
#include "ErrorEstimator.h"
#include "voro++.hh"
#include <v_compute.hh>
#include <cell.hh>

using namespace voro;

class Refinement {
public:
    Refinement();
    Refinement(const Refinement& orig);
    virtual ~Refinement();
    
    void SetIterationNumber(Model*,string);
    int ItNumber=0;
    bool EvenItNumber=false;
    
    void NewRefinementNodes(Model*,StencilSelect*,ErrorEstimator*);
    void PrintRefinedModelInpFile(Model*,string);
    void PrintRefinedModelVtkFile(Model*,string);
    void PrintRefinedCollocInpFile(Model*,string);
    void PrintBCRow2D(Model*,bool,long,ofstream*);
    
    void PrintElements(Model*,ofstream*);
    void PrintElementBCs(Model*,long,ofstream*,bool);
    
    //Get the new nodes boundary condition
    void LoadOldNodesPosition(Model*);
    void PrintBCRow3D(Model*,bool,long,ofstream*);
    
private:
    
    void IdentifyRefinementNodes(Model*,ErrorEstimator*);
    void FindNewBoundaryNodes(Model*, ErrorEstimator*);
    void AddThreeNewNodes(Model*,long,long);
    void RemoveDuplicatedEdgeNodes(Model*);
    
    //Find new nodes
    void FindPotentialNewNodes_Voro(Model*, ErrorEstimator*,int);
    void FindVoroCN(Model*, container*, vector<long>, double);
    void FindVoroNN(Model*, container*, vector<long>, double);
    void LoadCombinedTree(Model*,int);
    Tree CombinedTree;
    Tree ExistingNodesTree;
    Vector3d GetRefNodeCoord(Model*,long);
    Vector3d GetSupNodeCoord(Model*,long);
    void SetExistingNodesGlobalIndices(Model*);
    void SetRemainingNodesGlobalIndices(Model*);
    long ExistingBCNodesNum=-1;
    long NewBCNodesNum=0;
    void GetClosestNode(Model*);
    
    void IdentifyDuplicatedPoints(Model*,StencilSelect*,int);
    void IdentifyNewNodesOutside(Model*,StencilSelect*,int);
    void RemoveDuplicatedPoints(Model*);
    void AddNewBoundaryElements(Model*);
    void ReorderBoundaryElements(Model*);
        
    long* NodesForRefinement=NULL;
    long NodesNumForRefinement=0;
    bool RefinementThreeNodes=true;
    
    
    
    long NewBoundaryNodesNum=0;
    
    struct NewElement
    {
        Vector3d Node;
        Vector3d Normal;
        long** AttachedNode=NULL;
        vector<bool> LocalBoundaryCondition;
        vector<bool> BC_D_Bool;
        vector<bool> BC_N_Bool;
        vector<int> BC_DOF;
        vector<double> BC_D;
        vector<double> BC_N;
        //Reference to CAD geometry
        int RefSurf=-1;
        int RefEdge=-1;
        long RefElement=-1;
        bool HasModelEdgeNode=false;
    };
    vector<NewElement> NewElements;
    vector<NewElement> NewElementsOrdered;
    
//    vector<vector<long>> NewElements;
//    vector<vector<long>> NewElementsOrdered;
    vector<NewNode*> NewNodesVect;
    
    Vector3d GetNormal(double,double,double,double,double,double,Vector3d);
    
    Node* NewNodes=NULL;
    vector<vector<long>> Levels;
    long TotalNodesNewModel=0;
};

#endif /* REFINEMENT_H */