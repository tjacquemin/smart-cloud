#ifndef VTKOUTFILE_H
#define VTKOUTFILE_H

#include "Model.h"
#include "LinearProblem.h"
#include "ErrorEstimator.h"

class VTKOutFile {
public:
    VTKOutFile();
    VTKOutFile(const VTKOutFile& orig);
    virtual ~VTKOutFile();
    
    void PrintVTKOutputFile(Model*,LinearProblem*,ErrorEstimator*,string);
private:

};

#endif /* VTKOUTFILE_H */

