#ifndef SURFACE_H
#define SURFACE_H

#include "Main.h"
#include "CgalLib.h"
#include "Vertex.h"

class Surface {
public:
    Surface();
    Surface(const Surface& orig);
    virtual ~Surface();
    
    void NewVertices(long,int);
    void NewElements(long,int);
    
    Vertex* Vertices=NULL;
    long** Elements=NULL;
    double** ElementsNormal=NULL;
    Point* points=NULL;
    double** Centroid=NULL;
    
    long VerticesNumber=0;
    long ElementNumber=0;
    int VertPerEl=0;
    string Type;
    
    bool InnerSurface=false;
private:

};

#endif /* SURFACE_H */