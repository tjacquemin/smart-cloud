#ifndef GFD_EXTRINSIC_H
#define GFD_EXTRINSIC_H

#include "Model.h"

class GFD_Extrinsic {
public:
    GFD_Extrinsic();
    GFD_Extrinsic(const GFD_Extrinsic& orig);
    virtual ~GFD_Extrinsic();
    void InitializeProblem(Model*,long*);
    void LoadDerivativeCoefficients(Model*,Node*,long,bool,int,int);
    void LoadStiffCoeff(Model*,Node*,long,int,int);
    void LoadVectors(Model*,double*,T*,T*,T*,int,int);
    
    //Public variables
    double d1=0, d2=0, d3=0;
    
    //Conditioning
    double MinCond=0;
    double MaxCond=0;
    bool MaxCondFound=false;
    
    //Extended supports
    void LoadExtendedSupports(Model*,Node*);
    void LoadStiffTriplets(Model*);
    vector<T> TripStiffVect;
    vector<T> TripStiffVect2;
    long LagrangeBCIndex=0;
private:
    MatrixXd C_Matrix(Model*,Node*,bool);
    void LoadRows2D_Stress(Model* M,VectorXd,long,long,int,int,T*,long&);
    void LoadRows2D_Stiff(Model*,VectorXd,long,long,int,int,double*,T*,long&);
    MatrixXd A_Matrix(Model*,long,int);
    
    int Dimension=2;
    double w(Model*,double,double*,double*,double);
    double factorial(int);
    int lTayl=0;
    long *TripStressTreadStartInc;
    long *TripStiffTreadStartInc;
    
    double Angle(Model*,long,int,long);
    int getIndex(vector<long>,long);
    
};

#endif /* GFD_EXTRINSIC_H */

