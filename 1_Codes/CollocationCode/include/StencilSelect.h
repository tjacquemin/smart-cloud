#ifndef STENCILSELECT_H
#define STENCILSELECT_H

#include "Main.h"
#include "Model.h"
#include "CgalLib.h"
#include "Node.h"
#include "Element.h"
#include "Surface.h"
#include "voro++.hh"
#include <v_compute.hh>
using namespace voro;

class StencilSelect {
public:
    StencilSelect();
    StencilSelect(const StencilSelect& orig);
    virtual ~StencilSelect();
    
    //General parameters
    int Dimension=2;
    double pi=3.1415;
    int lTayl=6;
    
    //Stencil selection parameters
    int SupSizeInt=0;
    int SupSizeBound=0;
    bool SupRadiusGiven=false;
    double RadiusBoundaryNodes=0;
    double RadiusInteriorNodes=0;
    double SupScaling=1.001;
    double StencilSelectionThreshold=-1;
    double MaxVariableSupSize=20;
    double MaxSupportCond=1e10;
    double SupInnerThreshold=0;
    string StencilSelection="";
    
    //Initialization of the stencil search
    void LoadSupportCGAL(Model*,Node*,long,int);
    void GetNeighbourNodes(Model*,double,Node*,int*,int*,long*,double*);
    
    //Stencil selection variables
    bool InDomain(Model*,vector<double> PointVect,bool,bool);
    void LoadVoronoi_AllNodes(Model*);
    
    //Identification of the voronoi corner nodes
private:

    //Visibility functions
    void LoadConnectedElements(Model*);
    void LoadVisibilitySupport(Model*,Node*,long*,double*,int,int);
    void LoadVisibilitySupportFromBE(Model*,Node*,long*,double*,int,int);
    void SelectSupportNodes(Model*,Node*,long*,double*,bool*,int,bool*,int,int,int*,double*);
    bool IsHidden(Model*,Node*,long,long,double);
    bool IntersectsBE(Model*,vector<double>,long,long,long);
    double GetSegmentElementAngle(Model*,MatrixXd);
    
    //Node selection algorithms
    void LoadNormalSupport(Model*,Node*,long*,double*,int,int);
    void LoadNormalSupportEstim(Model*,Node*,long*,double*,int,int);
    void LoadAreaSupport(Model*,Node*,long*,double*,int,int,int);
    void LoadSupportQuad(Model*,Node*,long*,double*,int,int);
    void LoadSupportAnisotropic(Model*,Node*,long*,double*,int,int);
    void LoadSupportConditionNumber(Model*,Node*,long*,double*,int,int);
    double GetSupportConditioning(Model*,Node*,long*,int,vector<long>,int,double);
    
    double GetAngle(Model*,double,double,long,long,bool);
    void GetStartEndSectors(Model*,long,long*,int,double*,double*);
    void GetStartEndSectors2(Model*,long,long*,int,double*,double*);
    void LoadCoefficients(Model*,Node*,long);
    
    //Voronoi CN selection
    void LoadUniqueVoronoiCN(Model*);
};

#endif /* STENCILSELECT_H */

