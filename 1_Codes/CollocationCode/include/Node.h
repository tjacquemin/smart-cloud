#ifndef NODE_H
#define NODE_H

#include "Main.h"
#include "CgalLib.h"
#include "voro++.hh"
#include <v_compute.hh>
#include <cell.hh>
using namespace voro;

class Node {
public:
    Node();
    Node(const Node& orig);
    virtual ~Node();
    
    long NodeNum=0;   
    double* X = NULL;
    int Dimension;
    bool NodeInitialized=false;
    
    //Support node information
    long* SupNodes=NULL;
    long* SupNodesEstim=NULL;
    double** SupNodesX=NULL;
    int SupSize=0;
    int SupSizeEstim=0;
    double SupRadiusSq=0;
    double SupRadius=0;
    double SupRadiusSqEstim=0;
    double SupRadiusEstim=0;
    double* SupRadA=NULL;
    bool QuadSelection=false;
    vector<vector<double>> VoCN;
    double cond=0;
    
    //Derivative information (used for error estomation and weak form)
    double** Coeff = NULL;
    double** Coeff2 = NULL;
    double** CoeffEnrich = NULL;
    
    //Weak form additional parameters
    int WeakSupSize=0;
    long* WeakSupport = NULL;
    long* LagrangeMultCol = NULL;
    
    //Boundary node additional information
    bool BoundaryNode=false;
    bool NodeInSurfaceElement=false;
    bool* LocalBoundaryCondition = NULL;
    bool* BC_D_Bool = NULL;
    bool* BC_N_Bool = NULL;
    double* BC_D = NULL;
    double* BC_N = NULL;
    int* BC_DOF = NULL;
    double** Normal = NULL;
    bool DuplicatedNode=false;
    long GlobalIndex=-1;
    bool CornerNode=false;
    bool EdgeNode=false;
    bool FaceNode=false;
    bool MultipleBCRows=false;
    
    //Exact field value at this node
    double* ExactU = NULL;
    double* ExactS = NULL;
    bool HasExactU=false;
    bool HasExactS=false;
    
    //Associated volume and charateristic length
    double Vol=1;
    double* SupNodeVol = NULL;;
    double* SupNodeW = NULL;
    double TotalVol=0;
    double CharactLength=0;
    double ProdN=0;
    bool MarkedForRefinement=false;
    
    //Singular Node properties
    bool IsSingular=false;
    bool HasSingularSupNodes=false;
    bool HasDiffractedSupNodes=false;
    double* DiffractedDist=NULL;
    double DistToSing=0;
    bool EnrichedNode=false;
    double* U1=NULL;
    
    //Collocation point offset
    bool CollocationPointOffset=false;
    bool CollocDOF=true;
    double* CollocX = NULL;
    
    //Attached elements
    vector<long> AttachedElements;
    
    void SetNode(long, int, double, double, double);
    void SetBoundNode(bool,int,int,int,string,double, double, double, double,string,string);
    long NodeNumber();
    void SetSupRadius(double);
    void FreeCoeff();
    
    //Voronoi neighbors of the node (before refinement)
    void FindVoroCN();
    vector<vector<double>> VoroCN;
    double* VoroCN_DistToRef=NULL;
    vector<long> VoroCN_Index;
    bool InDomain=true;
    double DistClosestNode=-1;
    
    //Function for extended supports
    vector<long> SupNodesEx;
    vector<long> SupNodesExRev;
    vector<long> ParentNodes;
    vector<double> StiffCoeff;
    vector<double> ARow0;
    vector<double> ARow1;
    
private:
    bool BC_D_Init=false;
    bool BC_N_Init=false;
};

#endif /* NODE_H */
