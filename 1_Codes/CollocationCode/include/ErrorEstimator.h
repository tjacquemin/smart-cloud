#ifndef ERRORESTIMATOR_H
#define ERRORESTIMATOR_H

#include "Model.h"
#include "StencilSelect.h"
#include "GFD_Problem.h"
#include "GFD_Intrinsic.h"
#include "LinearProblem.h"
#include "Node.h"

class ErrorEstimator {
public:
    ErrorEstimator();
    ErrorEstimator(const ErrorEstimator& orig);
    virtual ~ErrorEstimator();
    
    void ComputeErrorEstimator(Model*,StencilSelect*,LinearProblem*);
    MatrixXd ErrorEstimU;
    VectorXd Estim;
    VectorXd EstimVMS;
    VectorXd ErrorEstim;
    
    Node* VoronoiNodes=NULL;
    VectorXd EstimVoro;
    VectorXd ErrorEstimVoro;
    
    //Variables for residual error estimator
    vector<vector<long>> AttachedVoroNodes;
    long VoroNodeCount=0;
    
private:
    void ComputeErrorEstimator_TAY(Model*,LinearProblem*);
    void ComputeErrorEstimator_RES(Model*,StencilSelect*,LinearProblem*);
    void ComputeErrorEstimator_BEN(Model*,int,int,LinearProblem*);
    void ComputeErrorEstimator_BEN_RES(Model*,int,int,LinearProblem*);
    void ComputeErrorEstimator_GR(Model*,int,int,LinearProblem*);
    
    //Functions for residual error estimator
    void GetAllVoronoiNodes(Model*,StencilSelect*);
    void GetVoronoiResiduals(Model*,GFD_Problem*,LinearProblem*,long,int,int);
    void GetVoronoiErrors(Model*);
        
    double Dimension=2;
    MatrixXd U_;
    MatrixXd U_xxx;
    
    void FillFirstSecondDerivatives(Model*,LinearProblem*);
    
    double w(double,int);
    double ws(Model*,double,double,double,double,int,int);
    double w_MLS(double,double,double,double,int,int);
};

#endif /* ERRORESTIMATOR_H */