#ifndef OUTPUTFILE_H
#define OUTPUTFILE_H

#include "Model.h"
#include "StencilSelect.h"
#include "LinearProblem.h"
#include "ErrorEstimator.h"

class OutputFile {
public:
    OutputFile();
    OutputFile(const OutputFile& orig);
    virtual ~OutputFile();
    void PrintOutputFile(Model*,StencilSelect*,LinearProblem*,ErrorEstimator*,string);
    
private:
    void PrintOutputFileHeader(Model*,StencilSelect*,LinearProblem*,std::ofstream*);
    void PrintErrorNorms(Model*,LinearProblem*,ErrorEstimator*,std::ofstream*);
    void PrintApproxSolution(Model*,LinearProblem*,std::ofstream*);
    void PrintEstimExactSolution(Model*,LinearProblem*,ErrorEstimator*,std::ofstream*);
    
    void CalculateErrorNorms(Model*,LinearProblem*,std::ofstream*);
    void CalculatePrintExactErrorS(Model*,LinearProblem*,std::ofstream*);
    void CalculatePrintExactErrorU(Model*,LinearProblem*,std::ofstream*);
        
    void CalculateEstimatorErrorNorms(Model*,LinearProblem*,ErrorEstimator*,std::ofstream*);
    double GetAverageBCNodeSupport(Model*);
    double GetAverageInnerNodeSupport(Model*);
    VectorXd L1_S, L1_U, L2_S, L2_SR, L2_U, LInf_S, LInf_U;
};

#endif /* OUTPUTFILE_H */