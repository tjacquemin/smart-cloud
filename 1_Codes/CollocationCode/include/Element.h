#ifndef ELEMENT_H
#define ELEMENT_H

#include "Main.h"
#include "Node.h"

class Element {
public:
    Element();
    Element(const Element& orig);
    virtual ~Element();
    
    long ElNum=0;
    int NumbNodes=0;
    int Dim=2;
    string ElementType="";
    double* Normal=NULL;
    Node** AttachedNodes=NULL;
    Node CenterNode;
    
    //Boundary conditions associated to an element
    bool* LocalBoundaryCondition = NULL;
    bool* BC_D_Bool = NULL;
    bool* BC_N_Bool = NULL;
    int* BC_DOF = NULL;
    double* BC_D = NULL;
    double* BC_N = NULL;
    
    //Functions
    void NewElement(long,string,double*);
    void SetBC_2D(string,string,double,string,string,double);
    void SetBC_3D(string,string,double,string,string,double,string,string,double);
    void LoadConnectionsToNodes();
    void LoadCenterNode();
    double MinElementNodesDistance();
    
    //Reference to CAD geometry
    int RefSurf=-1;
    int RefSurfOfEl=-1;
    int RefEdge=-1;
    
    bool NewElementsCreated=false;
    bool NewNodesCreated=false;
    vector<long> NewNodes;

private:

};

#endif /* ELEMENT_H */

