#ifndef GFD_PROBLEM_H
#define GFD_PROBLEM_H

#include "Model.h"

class GFD_Problem {
public:
    GFD_Problem();
    GFD_Problem(const GFD_Problem& orig);
    virtual ~GFD_Problem();
    void InitializeProblem(Model*,long*);
    void LoadDerivativeCoefficients(Model*,Node*,long,int,int);
    void LoadVectors(Model*,double*,T*,T*,vector<vector<Trow>>&,vector<vector<Trow>>&,vector<vector<Trow>>&,int,int);
    
    //Public variables
    double d1=0, d2=0, d3=0;
    
    //Functions for enrichment
    void LoadDistToSingular(Model*);
    void LoadU1Values(Model*,int,int);
    void RefineEnrichmentZone(Model*);
    
    //Conditioning
    double MinCond=0;
    double MaxCond=0;
    bool MaxCondFound=false;
    
private:
    MatrixXd C_Matrix(Model*,Node*);
    void LoadRows2D_Stress(VectorXd,VectorXd,long,long,int,int,T*,long&);
    void LoadRows2D_Strain(VectorXd,VectorXd,long,long,int,int,T*,long&);
    void LoadRows3D_Stress(VectorXd,long,long,int,int,T*,long&);
    void LoadRows2D_Stiff(Model*,VectorXd,VectorXd,long,long,int,int,double*,vector<vector<Trow>>&,vector<vector<Trow>>&,vector<vector<Trow>>&,int,long,int,long);
    void LoadRows3D_Stiff(Model*,VectorXd,long,long,int,int,double*,vector<vector<Trow>>&,vector<vector<Trow>>&,vector<vector<Trow>>&,int,long,int,long);
    void NormalizeTriplets(Model*,double*,vector<vector<Trow>>&,int,int);
    
    int Dimension=2;
    double w(Model*,double,double*,double*,double);
    double factorial(int);
    int lTayl=0;
    long *TripStressTreadStartInc;
    
    //Functions for enrichment
    double Angle(Model*,long,int,long);
    double* MinU1=NULL;
    double* MaxU1=NULL;
    void GetModifiedMatricesC(Model*,long,MatrixXd&,MatrixXd&,MatrixXd&);
    void GetU1_LShape(Model*,long,MatrixXd&,MatrixXd&,MatrixXd&);
    void GetU1_CrackedPlate(Model*,long,MatrixXd&,MatrixXd&,MatrixXd&);
    void Get3DLocalNormals(double*,int,double*);
    void Get3DRotatedTerms(double*,int,double&,double,double,double,double,double,double,int);
};

#endif /* GFD_PROBLEM_H */

