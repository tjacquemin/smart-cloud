#ifndef CAD_MODEL_H
#define CAD_MODEL_H

#include "Main.h"
#include "OCCTLib.h"

class CAD_Model {
public:
    CAD_Model();
    CAD_Model(const CAD_Model& orig);
    virtual ~CAD_Model();
    
    //Geometry variables
    TopoDS_Shape TopoShape;
    TopoDS_Face* FaceLoc;
    int SurfaceCvNumber=0;
    int Dimension=0;
    int NumFaces=0;
    
    BRepClass3d_SolidClassifier* ShapeCAD;
        
    //Functions
    void LoadCAD_Model(string,int);
    bool NodeInCAD(double*);
    Vector3d PointProjection(Vector3d, Vector3d &Normal,Vector3d);
    Vector3d PointProjection3D(Vector3d, Vector3d &Normal,Vector3d);
    Vector3d PointProjectionEdge(Vector3d,int,int);
    Vector3d GetNormal(Vector3d, int);
private:

};

#endif /* CAD_MODEL_H */

