#ifndef VERTEX_H
#define VERTEX_H

#include "Main.h"

class Vertex {
public:
    Vertex();
    Vertex(const Vertex& orig);
    virtual ~Vertex();
    
    double* X = NULL;
    double* Normal = NULL;
private:

};

#endif /* VERTEX_H */

