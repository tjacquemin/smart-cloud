#ifndef DOXYGENDOC_H
#define DOXYGENDOC_H

/** 
 * \mainpage General
 * \tableofcontents
 * 
 * \section intro_sec Introduction
 * This code has been developped by Thibault Jacquemin, as part of his PhD with the University of Luxembourg, under the supervision of Pr Stéphane P.A. Bordas.
 * 
 * The purpose of this code is to study the collocation methods based on Taylor's series expansion for 2D and 3D problems, in the domain of linear elasticity.
 * 
 * \section overview_sec Code Overview
 * The code is split into the following main classes:
 *      - Model
 *      - LinearProblem which includes the method classes (DCPSE_Problem, GFD_Problem, IMLS_Problem, RBF_FD_Problem)
 *      - Solver: (Seq_Solver or MPI_Solver)
 *      - ErrorEstimator
 *      - Refinement
 * 
 * The geometry is discretized with nodes and the domain is limited by elements. The following classes are considered for this:
 *      - Node
 *      - Element
 *
 * \section input_sec Input File Structure
 * 
 * The input files (.inp) are split into different sections:
 *      - Header infomation
 *      - %Node
 *      - %Surface
 *      - Boundary 
 *      - Exact solution 
 * 
 * A # symbol is placed before each keyword. Note that there should not be any blank line between the keywords and sections of the input file.
 * 
 * \subsection keyword_sec Header Basic Information
 * 
 * In the header section, all the parameters of the model and of the desired solution of the problem are listed. The keyword shall be placed in the correct order.
 * The header below represent the minnimum requirement for the code to run.
 *      - \b #ANALYSIS_TYPE=XXX \n
 *          "2D_PLANE_STRESS" for 2D plane stress problems\n
 *          "2D_PLANE_STRAIN" for 2D plane strain problems\n
 *          "3D_ELASTICITY" for 3D problems
 *      - \b #PARAMETERS \n
 *          *E=XXX*       (Young's Modulus) \n
 *          *nu=XXX*      (Poisson Ratio) \n
 *      - \b #METHOD=XXX \n
 *          "GFD" for a solution of the problem based on the Generalized Finite Difference \n
 *          "DCPSE0", "DCPSE1" or "DCPSE2" for a solution of the problem based on the Discretization Corrected Particle Strength Exchange \n
 *          "MLS1" or "IMLS1" respectively for the Moving Least Square of for the Interpolating Moving Least Square methods \n
 *          "RBF-FD" Radial Basis Finite Difference Method \n
 *      - \b #WINDOW_TYPE=Type, \b Scaling\n
 *          - Type: \n
 *            "SPLINE_3" or "SPLINE_4" for the GFD Method \n
 *            "EXPX.X", "SPLINE_3" or "SPLINE_4" for the DC PSE Method \n
 *          - Scaling: X.XX \n
 *          SPLINE_4,0.75   (typical for GFD) \n
 *          EXP1,0.30       (typical for DC PSE) \n
 *      - \b #SUP_SIZE=Inner, \b Bound \n
 *          - Inner: Support size for the inner nodes\n
 *          - Bound: Support size for the boundary nodes\n
 *      - \b #THREAD=XXX \n
 *          Number of threads to be used for the preprocessing part of the code (neighbour search, derivative approximation, assembly, ...) \n
 *      - \b #SOLVER=XXX \n
 *          "DIRECT_MUMPS" for a direct solver based on the solver MUMPS \n
 *          "KSP" for an iterative solver based on Krylov subspace iteration \n
 * 
 * \subsection other_sec Domain Definition
 * 
 * The interior of the domain is discretized with %nodes. The coordinates of the nodes are listed in a section #NODE_DATA" as follows:
 *      - \b #NODE_DATA \n
 *          *NodeNumber, X, Y, Z* \n
 *          For 2D problems, the "Z" coordinate can be ommited. \n
 *      - \b #SURFACE_NUMBER=X \n
 *          "X" is the number of surfaces in the model \n
 *      - \b #SURFACE_TYPE=%Surface **Number,%Surface Type** \n
 *          The surface type is "INNER" or "OUTER" \n
 *      - \b #SURFACE_EL=%Surface **Number, Number of Elements, Elements Type** \n
 *          *ElementNumber, N1, N2, Nx, Ny, Cx, Cy* \n
 *          *ElementNumber, N1, N2, N3, Nx, Ny, Nz, Cx, Cy, Cz* \n
 *          "N1", "N2" and "N3" correspond to the element corner nodes \n
 *          "Nx", "Ny" and "Nz" correspond to the element normal \n
 *          "Cx", "Cy" and "Cz" correspond to the center node coordinate for the quadratic elements \n
 *          - For 2D problems, the element types are "2D_LINEAR" and "2D_QUAD" \n
 *          - For 3D problems, the element types are "3D_LINEAR" and "3D_QUAD" \n
 * 
 * \subsection BC_sec Boundary Conditions
 * 
 * The domain boundary conditions are set in a single keyword section.
 *      - \b #BOUNDARY \n
 *          *NodeNumber, *, Nx, Ny, A_x, B_x, Val_x, A_y, B_y, Val_y* \n
 *          *NodeNumber, *, Nx, Ny, Nz, A_x, B_x, Val_x, A_y, B_y, Val_y, A_z, B_z, Val_z* \n
 *          "*" if all the DOFs are set based on the same node normal \n
 *          "Nx", "Ny" and "Nz" correspond to the node normal \n
 *          "A_x", "A_y" and "A_z" define the type of boundary condition set to each degree of freedom: \n
 *          - "D" for Dirichlet boundary conditions; \n
 *          - "N" for Neumann boundary conditions. \n
 *          "B_x", "B_y" and "B_z" define the axis system in which the boundary condition is set:
 *          - "G" for the Global axis system; \n
 *          - "L" for the Local axis system defined by the node normal. \n
 * 
 * \subsection exact_sec Exact Solution
 * 
 * If an exact solution is available, if can be provided to the code in order to calculate error norms. \n
 *      - \b #EXACT_SOLUTION \n
 *          *NodeNumber, Ux_e, Uy_e, S11_e, S12_e, S22_e* \n
 *          *NodeNumber, Ux_e, Uy_e, Uz_e, S11_e, S12_e, S13_e, S22_e, S23_e, S33_e* \n
 *          If one of the solution value is not available (displacement field or stress field), the value shall be replaced by "~". \n
 *          For singular nodes with infinite stress solutions, the exact value shall be replaced by "Inf". This value will not be included in the error norm calculation. \n
 * 
*/

/** \page Solvers Solvers
 *   \tableofcontents
 *   This page describes the solvers used in the code and lists the various keyword used to set the solver parameters.
 *   \section SolverSec1 Direct Solver
 *   The direct solver is based on MUMPS which is a library capable of working accross distributed memories system.
 *   Direct solvers are suitable for relatively small problems (mostly 2D) as the triangulation of the linear problem matrix uses a lot of memory.
 *   
 *   \section SolverSec2 Iterative Solver
 *   Iterative solvers use Krylov subspace to solve the linear problem. For an efficient solution of the system, the system matrix shell be well preconditionned.
 *   
 *   \subsection subsec1 Solver Type
 *   More text.
*/

/** \page ErrorEstim Error Estimation
 *   \tableofcontents
 *   This pages gives information on the various error estimators which can be used.
 *   
 *   \section EstimSec1 Gradient Recovery Estimator
 *   The error estimator is based on a moving least square approximation of the solution field.
 *   
 *   \section EstimSec2 Residual Based Estimator
 *   The error estimator is based on the ability of the approximated field to reproduce a Taylor's series expansion.
 *   
*/

#endif /* DOXYGENDOC_H */

