#ifndef NEWNODE_H
#define NEWNODE_H

#include "Main.h"
#include "Model.h"

class NewNode {
public:
    NewNode();
    NewNode(const NewNode& orig);
    virtual ~NewNode();
    
    //The node dimension is always 3. For 2D problem, the 3rd coordinate is zero.
    int Dimension=3;
    vector<double> X;
    vector<double> Normal;
    long ParentNode=0;
    long ParentElement=0;
    long GlobalIndex=-1;
    bool NewElementEdgeNode=false;
    bool NewModelEdgeNode=false;
    double DistClosestNode=-1;
    
    double ParentNodeRadius=0;
    vector<long> ParentNodes;
    bool BoundaryNode=false;
    bool DuplicatedNode=false;
    bool InDomain=true;
    void SetNewNode(vector<double>,long,long,double,bool);
    void SetBC(int);
    void CopyFromNewNode(Model*,NewNode*);

    //Vectors for boundary conditions
    bool* LocalBoundaryCondition = NULL;
    bool* BC_D_Bool = NULL;
    bool* BC_N_Bool = NULL;
    int* BC_DOF = NULL;
    double* BC_D = NULL;
    double* BC_N = NULL;
private:

};

#endif /* NEWNODE_H */

