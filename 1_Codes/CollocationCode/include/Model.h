#ifndef MODEL_H
#define MODEL_H

#include "Main.h"
#include "CgalLib.h"
#include "Node.h"
#include "Element.h"
#include "Surface.h"
#include "CAD_Model.h"

class Model {
public:
    Model();
    Model(const Model& orig);
    virtual ~Model();
    
    //----Public Variables------------------------------------------------------
    
    //Model Input/Output Information
    string* AnalysisFile=NULL;
    string InputFilePath="";
    bool ExactSolutionAvailable=false;
    
    //Processing Data
    int ThreadNumber=0;
    int ProcessNumber=0;
    
    //Model type
    string Method="";
    string StencilSol="MLS";
    int ApproxOrder=2;
    string ModelType="";
    string BCTreatType="";
    bool LagrangeSym=false;
    bool WeakBC=false;
    bool InterpBC=false;
    bool Stabilization=false;    
    bool PartialStab=false;
    string WindowFunctionType="";
    bool Voronoi=false;
    
    //Constant
    const double pi=3.14159265358979323846;
    
    //Mechanical Properties
    double E=0;
    double nu=0;
    
    //Model Geometry and general variables
    int Dimension=0;
    double Volume=0;
    Node* Nodes=NULL;
    long NodeNumber=0;
    MatrixXd Exponents;
    int lTayl=6;
    Tree tree;
    
    //Solver and PC Information
    string SolverType="";
    string SolverPackage="PETSC";
    string SolverPC="";
    string SolverKSP="";
    double  SolverTol=0;
    long  SolverMaxIt=0;
    double AMG_Threshold=0.02;
    int AMG_Levels=0;
    int AMG_SmoothN=0;
    int AMG_CycleN=0;
    string AMG_Operator="WITHBC";
    int Mat_Partitioning=0;
    int Mat_Ordering=1;
    int Mat_NullSpace=0;
    double AdjThreshold=1;
    int ILU_Levels=3;
    double ILU_DiagFill=0.02;
    MatOrderingType ILU_Ordering=MATORDERINGRCM;
    
    //Hypre parameters
    int H_CoarsenType=6; //BoomerAMG default = 6
    double H_StrongThreshold=0.9; //BoomerAMG default = 0.25
    int H_AggNumLevel=2; //BoomerAMG default = 0
    int H_MaxCoarseSize=100; //BoomerAMG default = 9
    int H_MaxLevels=25; //BoomerAMG default = 25
    int H_MaxAMGIter=50; //BoomerAMG default = 20
    int H_RelaxType=6; //BoomerAMG default = 6
    int H_InterpType=4; //BoomerAMG default = 0
    int H_SmoothType=6; //BoomerAMG default = 6
    int H_NumSweeps=1;
    double H_AMGTol=0; //0 when BoomerAMG is used as PC
    double H_NonGalerkTol=-1;
    int H_CycleType=1; //1 = "V", 2 = "W"
    
    //Singular Nodes
    long SingularNodeNumber=0;
    long SingularNode=0;
    bool IncludeSingularNodes=false;
    string VisibilityType="";
    double VisibilityThresholdAngle=1;
    string SingularEnrichment="";
    bool FullEnrichment=true;
    double DistThreshold=-1;
    double ScaleFactor=100;
    int NumberEnrichmentCoeff=1;
    int FNum=1;
    double WeightScaleFactor=2;
    bool Inter=true;
    double GetEnrichmentFunctionVal(long,int,int,int);
    bool SingProd=true;
    bool SingProdSplit=true;
    Node* CNodes=NULL;
    long CNodesNum=0;
    
    //Error Estimator
    bool ErrEstimator=false;
    string EstimatorType="";
    bool ZZ_DirectVMS=false;
    double StencilScaling=1;
    double SupScaling=1;
    string EstimVar="";
    string EstimWeight="";
    int EstimSupSize=0;
    int MaxRowWidth=60;
    int BenEstOrder=3;
    string FunctionBasis="";
    bool ResVoroCorner=false;
    
    //Print options
    bool PrintStrain=false;
    int PrintMat=0;
    int PrintConditionNumber=0;
    double* RunTime=NULL;
    bool TimeSplit=false;
    bool PrintVTK=false;
    bool PrintLocResidual=false;
    
    //Node Support Information
    double SupRatio=0;
    double SupExp=0;
    double MinWeight=0;
    int SupSizeIntM=0;
    
    //----Public Function-------------------------------------------------------
    
    //Main Public Functions
    string ToString(double,int,int,bool,int);
    string trim(string);
    string split(string, char, int);
    double DistanceNN(double*,double*);
    double w(double);
    double factorial(int);
    
    void ModelInitialization();
    void LoadTree();
    
    vector<long>***  SupportGrid;
    void ProcessMemUsage(double&, double&);
    void Print(string,bool);
    void PrintLoading(long,long,string,bool);
    void GetStartEndNode(int,int,long&,long&,long);
    
    //Surface information
    Surface* Surfaces=NULL;
    int SurfaceNumber=0;
    bool SurfaceDefined=false;
    void LoadSurface2D();
    Element* SurfElement=NULL;
    long ElementNumber=0;
    
    //CAD Model
    bool CAD_Model_Available=false;
    string PathToCAD="";
    CAD_Model* ModelCAD=NULL;
    
    //Adaptivity information
    bool Adaptivity=false;
    double AdaptThreshold=0;
    double AdaptThresholdLog=0;
    double AdaptThresholdFrac=0;
    double AdaptFraction=0;
    int NumLevel=1;
    string RefType="GLOBAL";
    string RefMethod="VORO_CN"; //VORO_NN (natural neighbor) or VORO_CN (corners or the voronoi cell)
    bool DirectCollocRefinement=false;
    float DeletionFactor_Bound=4;
    float DeletionFactor_Inner=4;
    bool RefineEvenBOnly=false;
    bool DistToBoundCriterion=false;
    
    //Collocation Node Offset
    bool HasCollocationOffset=false;
    
    //Stabilization
    void ComputeCharactLength();
    
    //Error norm calculation
    double ErrorNorm(string,string,VectorXd,VectorXd,int,int,int);
    VectorXd ExactS;
    VectorXd ExactU;
    
private:
    //Support Loading variables
    Point* BoundaryPoints=NULL;
    long SegmentsNumber=0;
    
};

#endif /* MODEL_H */
