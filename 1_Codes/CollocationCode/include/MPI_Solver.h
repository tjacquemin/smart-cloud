#ifndef MPI_SOLVER_H
#define MPI_SOLVER_H

#include "Model.h"
#include "LinearProblem.h"
#include "Triangulation.h"

//Hypre includes
#include "HYPRE.h"
#include "HYPRE_parcsr_ls.h"
#define my_min(a,b)  (((a)<(b)) ? (a) : (b))

class MPI_Solver {
public:
    MPI_Solver();
    MPI_Solver(const MPI_Solver& orig);
    virtual ~MPI_Solver();
    
    void Solve(Model*,LinearProblem*,int,int);
    void LoadProcessData(Model*,LinearProblem*);
    void InitializeMatrices();
    void GetMatrixRanges();
    void FillMatrices();
    void SolveGMRES(Model*,LinearProblem*);
    void SolveMUMPS();
    void ReturnResult(LinearProblem*);
        
    //Main model variables
    int Dimension=0;
    long NodeNumber=0,NumLocMatRow=0;
    string FilePath;
    int PrintMat=0;
    int PrintConditionNumber=0;
    string SolverPackage="";
    string SolverType="";
    string SolverPC="";
    string SolverKSP="";
    double  SolverTol=0;
    long  SolverMaxIt=0;
    PetscReal AMG_Threshold=0.02;
    int AMG_Levels=0;
    int AMG_SmoothN=0;
    int AMG_CycleN=0;
    int Mat_Partitioning=0;
    int Mat_Ordering=0;
    int Mat_NullSpace=0;

    int rank=0,size=0;
    double* F_VecAll;
    long TripStiffLength;
    T* TripStiff;
            
    Mat StiffMat,F;
    Vec b,U;
    VectorXd U_Out;
    MPI_Comm comm;
    
    long Istart=0,Iend=0;
    long LocTripletLength=0;
    long LocTripletNullSpaceLength=0;
    
    long* StartRangeArray=NULL;
    long* EndRangeArray=NULL;
    long* StartTripRangeArray=NULL;
    long* EndTripRangeArray=NULL;
    PetscLogStage  stage1,stage2,stage3;
    
    //Function to split tripliets
    void SplitTriplet(Model*,LinearProblem*,long*);
    
    //Load the NullMatrix
    double* CoordAll=NULL;
    Vec Coord;
    
private:
    void CompressRowVector(PetscInt*,PetscInt*,long,long);
    void ShareNewOrdering(LinearProblem*);
    void SplitForceVector(LinearProblem*);
    void SplitCoordVector(LinearProblem*);
    void FillVector_b();
    void SetPreconditioner(PC*);
    void ComputeAdjacencyTriplets(Model*);
    void LoadAdjacencyMatrix();
        
    long* nindices_Ordering=NULL;
    long* nindices_OrdGlob=NULL;
    long* nindices_OrdOpp=NULL;
    
    long* nindices_Partitioning=NULL;
    long* nindices_PartGlob=NULL;
    long* nindices_PartOpp=NULL;
    
    //Adjacency matrix triplets array for computation of the adacency matrix
    Triplets* TripletVectAdj=NULL;
    long TripLengthAdj=0;
    
    //Salve the solution
    double* UVectLoc=NULL;
    void FillUVectLoc();
    
    Mat AdjMatrix;
    long IstartAdj=0,IendAdj=0;
    long* StartRangeArrayAdj=NULL;
    long* EndRangeArrayAdj=NULL;
    long* StartTripRangeArrayAdj=NULL;
    long* EndTripRangeArrayAdj=NULL;
    double AdjThreshold=1;
    
    //Hypre objects
    HYPRE_IJMatrix ij_matrix;
    HYPRE_ParCSRMatrix parcsr_matrix;
    HYPRE_ParCSRMatrix parcsr_ij;
    HYPRE_IJVector b_H;
    HYPRE_ParVector par_b;
    HYPRE_IJVector x_H;
    HYPRE_ParVector par_x;
    int nrows;
    int ilower, iupper;
    int    SolveHypreNumIterations;
    double SolveHypreFinalResNorm;
    //Parameters
    int H_CoarsenType=6;
    double H_StrongThreshold=0.9;
    int H_AggNumLevel=2;
    int H_MaxCoarseSize=100;
    int H_MaxLevels=25;
    int H_MaxAMGIter=50;
    int H_RelaxType=6;
    int H_InterpType=4;
    int H_SmoothType=6;
    int H_NumSweeps=1;
    double H_AMGTol=1e-7;
    double H_NonGalerkTol=0;
    int H_CycleType=1;
    //Functions
    void LoadHypreParameters(Model*);
    void CreateHypreMatrix();
    void LoadHypreMatrix();
    void CreateLoadHypreForce();
    void SolveHypre(Model*);
    void FillUVectLocHypre();
    void ReturnSolverInfoHypre(LinearProblem*);
    
    /*Hypre default parameters (amg_hybrid.c)
    (AMGhybrid_data -> setup_type)       = 1;
    (AMGhybrid_data -> strong_threshold)  = 0.25;
    (AMGhybrid_data -> max_row_sum)  = 0.9;
    (AMGhybrid_data -> trunc_factor)  = 0.0;
    (AMGhybrid_data -> pmax)  = 4;
    (AMGhybrid_data -> max_levels)  = 25;
    (AMGhybrid_data -> measure_type)  = 0;
    (AMGhybrid_data -> coarsen_type)  = 10;
    (AMGhybrid_data -> interp_type)  = 6;
    (AMGhybrid_data -> cycle_type)  = 1;
    (AMGhybrid_data -> relax_order)  = 0;
    (AMGhybrid_data -> keepT)  = 0;
    (AMGhybrid_data -> max_coarse_size)  = 9;
    (AMGhybrid_data -> min_coarse_size)  = 1;
    (AMGhybrid_data -> seq_threshold)  = 0;
    (AMGhybrid_data -> num_grid_sweeps)  = NULL;
    (AMGhybrid_data -> grid_relax_type)  = NULL;
    (AMGhybrid_data -> grid_relax_points)  = NULL;
    (AMGhybrid_data -> relax_weight)  = NULL;
    (AMGhybrid_data -> omega)  = NULL;
    (AMGhybrid_data -> agg_num_levels)  = 0;
    (AMGhybrid_data -> num_paths)  = 1;
    (AMGhybrid_data -> num_functions)  = 1;
    (AMGhybrid_data -> nodal)  = 0;
    (AMGhybrid_data -> dof_func)  = NULL;
    (AMGhybrid_data -> nongalerk_num_tol)  = 0;
    (AMGhybrid_data -> nongalerkin_tol)  = NULL;*/
};

#endif /* MPI_SOLVER_H */

