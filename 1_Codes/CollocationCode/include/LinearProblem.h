#ifndef LINEARPROBLEM_H
#define LINEARPROBLEM_H

#include "Model.h"
#include "GFD_Problem.h"
#include "GFD_Intrinsic.h"
#include "GFD_Extrinsic.h"
#include "Triangulation.h"

class LinearProblem {
public:
    LinearProblem();
    LinearProblem(const LinearProblem& orig);
    virtual ~LinearProblem();
    
    void Build(Model*);
    void LoadMatrices(Model*);
    void FillStressMat_Eigen(Model*);
    void FillStrainMat_Eigen(Model*,long,long);
    void FillLocMat_Eigen(Model*);
    
    void ComputeSolution(Model*);
    void ReorderS(Model*);
    void SetConvergenceReason(KSPConvergedReason);
    
    double d1=0, d2=0, d3=0;
    
    double* F_Vec=NULL;
    vector<T> TripStiffVect;
    vector<vector<Trow>> StiffRows;
    vector<vector<Trow>> OpRows;
    vector<vector<Trow>> StiffRowsLagrange;
    
    long TripStressLength=0;
    T* TripStress=NULL;
    T* TripStrain=NULL;
    double* Coord_Vec=NULL;
    
    long TripLocLength=0;
    T* TripLoc=NULL;
    
    long AdditionalDOFs=0; 
    long AdditionalForceVectDOFs=0;
    
    SpMat StressMat_Eigen;
    SpMat StrainMat_Eigen;
    SpMat LocMat_Eigen;
    VectorXd S, U, Epsilon; 
    VectorXd U_DOF;
    VectorXd S_Ord;
    VectorXd LocResidual;
            
    string SolverConvergenceStatus="";
    PetscInt SolverIterationNum=0;
    string IterationStopReason_Str="";
    PetscReal ConditionNumber=0;
    double MinCond=0;
    double MaxCond=0;
    double AvgCond=0;
    
    //Node Triangulation
    Triangulation* Tri=NULL;
    bool HasTriangulation=false;
   
    long LagrangeBCNumber=0;
private:
    
    VectorXd ReducedIndex(int);
    VectorXd ExpandedIndex(int);
    VectorXd ForceVector(int);
    
    void BuildInitialization(Model*);
    void GetTripTreadStartIndices(Model*,long*,long*);
    void GetWeakFormSup(Model*);
    void TestTriplets(Model*);
};

#endif /* LINEARPROBLEM_H */
